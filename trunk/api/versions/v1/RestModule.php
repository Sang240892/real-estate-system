<?php

namespace api\versions\v1;
use Yii;
class RestModule extends \yii\base\Module
{
    public $controllerNamespace = 'api\versions\v1\controllers';

    public function init()
    {
        parent::init();
        Yii::$app->language = 'vn_VN';
//        \Yii::$app->setComponents([
//            'response' => [
//                'format' => 'json',
//                'class' => 'yii\web\Response',
//                'charset' => 'UTF-8',
//                'on beforeSend' => function ($event) {
//                    $response = $event->sender;
//                    $data =[
//                        'method'          => strtolower(\Yii::$app->request->getUserAgent()),
//                        'request_url'     => strtolower(\Yii::$app->request->getUrl()),
//                        'request_string'  => '',
//                        'reponse_string'  => \json_encode($response->data),
//                        'user_id'         => '',
//                        'duration'        => '',
//                        'request_ip'      => \Yii::$app->request->getUserIP(),
//                        'device_type'     => null,
//                        'device_version'  => \Yii::$app->request->userAgent
//
//                    ];
//                    $logAPI = new \api\common\models\entities\LogApi();
//                    $logAPI->writeLog($data);
//                    if ($response->data !== null) {
//                        $response->data = [
//                            'success' => $response->isSuccessful,
//                            'data' => [
//                                'code'=>$response->statusCode,
//                                'msgError'=>$response->data['previous']['message'],
//                                'msgClient'=>$response->data['message']
//                            ],
//                        ];
//                    }
//                },
//            ]
//        ]);
        // custom initialization code goes here
    }
}
