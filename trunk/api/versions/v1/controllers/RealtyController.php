<?php

namespace api\versions\v1\controllers;
use common\models\entities\RealEstateBookmark;
use common\models\entities\RealEstateGallery;
use Yii;
use api\commons\forms\CreateRealtyForm;
use api\components\RestController;
use common\models\entities\RealEstateItem;
use api\commons\repositories\RealtyRepository;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\filters\VerbFilter;

/**
 * Class RealtyController
 *
 * @package \api\versions\v1\controllers
 */
class RealtyController extends RestController
{

    public function actionGetSubjectList(){
        $dataChild = [];
        $dataParent = [];
        $data = [];
        $parents = \common\models\entities\RealEstateCategory::findAll(['parent'=>0]);
        if(!empty($parents)){
            foreach($parents as $parent){
                $dataParent = [];
                $dataChild = [];
                $childs = \common\models\entities\RealEstateCategory::findAll(['parent'=>$parent->id,'is_display'=>1]);
                if(!empty($childs)){
                    foreach($childs as $child){
                        $items =[
                            'id'=>$child->id,
                            'name'=>$child->name
                        ];
                        $dataChild['items'][] = $items;
                        $dataParent[$parent->name] = $dataChild;
                    }
                }
                $data['subjects'][] = $dataParent;
            }
        }
        $this->sendResponse($data);
    }
    public function actionGetRealtyLocal(){
        $this->requireAuthToken();
        $realty = new RealEstateItem();
        $params=[];
        $response = [];
        $data = [];

        $params['is_sold'] = (boolean) (isset($this->request->isSold)
            && !empty($this->request->isSold))?
            $this->request->isSold:null;

        $params['searching'] = (string) (isset($this->request->searching)
            && !empty($this->request->searching))?
            $this->request->searching:null;

        $params['address'] = (string) (isset($this->request->address)
            && !empty($this->request->address))?
            $this->request->address:null;

        $params['district'] = (string) (isset($this->request->district)
            && !empty($this->request->district))?
            $this->request->district:null;

        $params['city'] = (string) (isset($this->request->city)
            && !empty($this->request->city))?
            $this->request->city:null;

        $params['is_self'] =  (isset($this->request->isSelf)
            && !empty($this->request->isSelf))?(boolean)
        $this->request->isSelf:null;

        $params['recordPerpage']      = (int) (isset($this->request->pagination->recordPerpage)
            && $this->request->pagination->recordPerpage !=0)?
            $this->request->pagination->recordPerpage:20;

        $params['page']     = (int) (isset($this->request->pagination->currentPage)
            && $this->request->pagination->currentPage !=0)?
            $this->request->pagination->currentPage - 1 :0;

        $params['price_min']     = (int) (isset($this->request->priceMin)
            && !empty($this->request->priceMin))?
            $this->request->priceMin :0;

        $params['price_max']     = (int) (isset($this->request->priceMax)
            && !empty($this->request->priceMax))?
            $this->request->priceMax :0;

        $params['area_min']     = (int) (isset($this->request->areaMin)
            && !empty($this->request->areaMin))?
            $this->request->areaMin :0;

        $params['area_max']     = (int) (isset($this->request->areaMax)
            && !empty($this->request->areaMax))?
            $this->request->areaMax :0;

        $params['sortKey']     = (int) (isset($this->request->sortKey->key)
            && !empty($this->request->sortKey->key))?
            $this->request->sortKey->key :null;

        $params['sortValue']     = (int) (isset($this->request->sortKey->value)
            && !empty($this->request->sortKey->value))?
            $this->request->sortKey->value :1;

        $params['type'] = (string) (isset($this->request->type)
            && !empty($this->request->type))?
            $this->request->type:null;

        $params['subject_id'] = (int) (isset($this->request->subjectId)
            && $this->request->subjectId != 0)?
            $this->request->subjectId:null;

        $params['is_new'] = (int) (isset($this->request->isNew)
            && $this->request->isNew !=0 )?
            $this->request->isNew:null;

        $params['local'] = true;
        $params['user_id'] = $this->user->id;
        $provider = $realty->register(RealtyRepository::class)->getList($params);

        $results = $provider->getModels();

        if($results && !empty($results)){
            foreach($results as $rs){
                $data[] = $rs->register(RealtyRepository::class)->getDataSelf();
            }
        }
        $response['pagination'] = $this->outputObjectPaging($provider);
        $response['items'] = $data;
        $this->sendResponse($response);

    }
    public function actionDelete(){
        $this->requireAuthToken();

        $realtyId = (int) (isset($this->request->id)
            && !empty($this->request->id)  && $this->request->id != 0)?
            $this->request->id:null;

        $realty = RealEstateItem::findOne(['id'=>$realtyId,'user_id'=>$this->user->id]);

        if(!$realty){
            $this->sendResponse($this->builtErrorCode(400,\Yii::t('api/error','Realty not found!')));
        }

        $images = RealEstateGallery::findAll(['item_id'=>$realty->id]);

        if(!empty($images)){
            foreach($images as $image){
                $url = $image->file_name;
                try{
                    @unlink(Yii::getAlias('@publicProductImagePatch').'/'.$url);
                    @unlink(Yii::getAlias('@publicProductImagePatch').'/'.Yii::$app->params['thumbName'].$url);
                    @unlink(Yii::getAlias('@publicProductImagePatch').'/'.Yii::$app->params['normalName'].$url);
                }catch (Exception $e){
                    Yii::error($e .'unlink food image');
                }
                $image->delete();
            }
        }
        try{
            @unlink(Yii::getAlias('@publicProductImagePatch').'/'.$realty->avatar);
            @unlink(Yii::getAlias('@publicProductImagePatch').'/'.Yii::$app->params['thumbName'].$realty->avatar);
            @unlink(Yii::getAlias('@publicProductImagePatch').'/'.Yii::$app->params['normalName'].$realty->avatar);
        }catch (Exception $e){
            Yii::error($e .'unlink realty image');
        }

        $realty->delete();

        $this->sendResponse();

    }

    public function actionCreate(){
        $this->requireAuthToken();

        $realtyId = (int) (isset($this->request->id)
            && !empty($this->request->id)  && $this->request->id != 0)?
            $this->request->id:null;

        if($realtyId === null){
            $form = new CreateRealtyForm();
        }else{
            $form = new CreateRealtyForm(['scenario'=>'update']);

        }
        $form->realtyId = $realtyId;
        $form->owner_id = $this->user->id;

        $form->title = (string) (isset($this->request->title)
            && !empty($this->request->title))?
            $this->request->title:null;
        $form->description = (string) (isset($this->request->description)
            && !empty($this->request->description))?
            $this->request->description:null;
        $form->price = (string) (isset($this->request->price)
            && !empty($this->request->price))?
            $this->request->price:null;
        $form->area = (string) (isset($this->request->area)
            && !empty($this->request->area))?
            $this->request->area:null;

        $form->address = (string) (isset($this->request->address)
            && !empty($this->request->address))?
            $this->request->address:null;
        $form->phone = (string) (isset($this->request->phone)
            && !empty($this->request->phone))?
            $this->request->phone:null;
        $form->contactName = (string) (isset($this->request->contactName)
            && !empty($this->request->contactName))?
            $this->request->contactName:null;
        $form->sold = (string) (isset($this->request->isSold)
            && !empty($this->request->isSold))?
            $this->request->isSold:false;
        $form->type = (int) (isset($this->request->type)
            && !empty($this->request->type))?
            $this->request->type:0;
        $form->floor = (string) (isset($this->request->floor)
            && !empty($this->request->floor))?
            $this->request->floor:null;
        $form->front = (string) (isset($this->request->front)
            && !empty($this->request->front))?
            $this->request->front:null;
        $form->bedroom = (string) (isset($this->request->bedroom)
            && !empty($this->request->bedroom))?
            $this->request->bedroom:null;

        $form->tip = (string) (isset($this->request->tip)
            && !empty($this->request->tip))?
            $this->request->tip:null;

        $deleteImageIds = (isset($this->request->deleteImageIds)
            && !empty($this->request->deleteImageIds))?
            $this->request->deleteImageIds:[];

        if(!empty($deleteImageIds)){
            foreach($deleteImageIds as $idImg){
                $image = RealEstateGallery::findOne(['id'=>$idImg]);
                if($image){
                    $url = $image->file_name;
                    try{
                        @unlink(Yii::getAlias('@publicProductImagePatch').'/'.$url);
                        @unlink(Yii::getAlias('@publicProductImagePatch').'/'.Yii::$app->params['thumbName'].$url);
                        @unlink(Yii::getAlias('@publicProductImagePatch').'/'.Yii::$app->params['normalName'].$url);
                    }catch (Exception $e){
                        Yii::error($e .'unlink food image');
                    }
                    $image->delete();
                }
            }
        }

        if(!$form->save()){
            $this->getFormError($form);
        }else{
            $uploadFiles = $this->uploadAttachmentFile( Yii::getAlias('@publicProductImagePatch'));
            if($uploadFiles && !empty($uploadFiles->output)){
                if(empty($form->realty->avatar)){
                    $form->realty->avatar = $uploadFiles->output[0];
                    $form->realty->save();
                }
                foreach($uploadFiles->output as $image){
                    $galleryData = [
                        'file_name'=>$image,
                        'title'=>$image,
                        'item_id'=>$form->realty->id
                    ];
                    $gallery = new RealEstateGallery();
                    $gallery->attributes = $galleryData;
                    try{
                        $gallery->save();
                    }catch (ErrorException $e){

                    }
                }
            }
            $data = $form->realty->register(RealtyRepository::class)->getDataSelf();
            $this->sendResponse(['realty'=>$data]);
        }
    }

    public function actionGetList(){
        $this->requireAuthToken();
        $realty = new RealEstateItem();
        $params=[];
        $response = [];
        $data = [];

        $params['is_saved'] = (boolean) (isset($this->request->isSave)
            && !empty($this->request->isSave))?
            $this->request->isSave:false;

        $params['searching'] = (string) (isset($this->request->searching)
            && !empty($this->request->searching))?
            $this->request->searching:null;

        $params['address'] = (string) (isset($this->request->address)
            && !empty($this->request->address))?
            $this->request->address:null;

        $params['district'] = (string) (isset($this->request->district)
            && !empty($this->request->district))?
            $this->request->district:null;

        $params['city'] = (string) (isset($this->request->city)
            && !empty($this->request->city))?
            $this->request->city:null;

        $params['is_self'] =  (isset($this->request->isSelf)
            && !empty($this->request->isSelf))?(boolean)
            $this->request->isSelf:null;

        $params['recordPerpage']      = (int) (isset($this->request->pagination->recordPerpage)
            && $this->request->pagination->recordPerpage !=0)?
            $this->request->pagination->recordPerpage:20;

        $params['page']     = (int) (isset($this->request->pagination->currentPage)
            && $this->request->pagination->currentPage !=0)?
            $this->request->pagination->currentPage - 1 :0;

        $params['price_min']     = (int) (isset($this->request->priceMin)
            && !empty($this->request->priceMin))?
            $this->request->priceMin :0;

        $params['price_max']     = (int) (isset($this->request->priceMax)
            && !empty($this->request->priceMax))?
            $this->request->priceMax :0;

        $params['area_min']     = (int) (isset($this->request->areaMin)
            &&  !empty($this->request->areaMin))?
            $this->request->areaMin :0;

        $params['area_max']     = (int) (isset($this->request->areaMax)
            && !empty($this->request->areaMax))?
            $this->request->areaMax :0;

        $params['sortKey']     = (int) (isset($this->request->sortKey->key)
            && !empty($this->request->sortKey->key))?
            $this->request->sortKey->key :null;

        $params['sortValue']     = (int) (isset($this->request->sortKey->value)
            && !empty($this->request->sortKey->value))?
            $this->request->sortKey->value :1;

        $params['type'] = (string) (isset($this->request->type)
            && !empty($this->request->type))?
            $this->request->type:null;

        $params['subject_id'] = (int) (isset($this->request->subjectId)
            && $this->request->subjectId != 0)?
            $this->request->subjectId:null;

        $params['is_new'] = (int) (isset($this->request->is_new)
            && $this->request->is_new !=0 )?
            $this->request->is_new:null;

        $params['search_address'] = (int) (isset($this->request->searchAddress)
            && !empty($this->request->searchAddress) )?
            $this->request->searchAddress:null;

        $params['local'] = false;

        $provider = $realty->register(RealtyRepository::class)->getList($params);

        $results = $provider->getModels();

        if($results && !empty($results)){
            foreach($results as $rs){
                $data[] = $rs->register(RealtyRepository::class)->getDataSelf();
            }
        }
        $response['pagination'] = $this->outputObjectPaging($provider);
        $response['items'] = $data;
        $this->sendResponse($response);
    }
    public function actionRemoveBookmark(){
        $this->requireAuthToken();
        $itemId =  (isset($this->request->id)
            && !empty($this->request->id))?
            $this->request->id:null;
        $bookmark = RealEstateBookmark::findOne(['item_id'=>$itemId,'user_id'=>$this->user->id]);

        if($bookmark){
            $bookmark->delete();
        }
        $this->sendResponse();
    }
    public function actionAddBookmark(){
        $this->requireAuthToken();
        $itemId =  (isset($this->request->id)
            && !empty($this->request->id))?
        $this->request->id:null;

        $realty = RealEstateItem::findOne(['id'=>$itemId]);
        if(!$realty){
            $this->sendResponse($this->builtErrorCode(400,\Yii::t('api/error','Realty not found!')));
        }

        $bookmark = new RealEstateBookmark();
        $bookmark->item_id = $realty->id;
        $bookmark->user_id = $this->user->id;

        if($bookmark->save()){
            $this->sendResponse();
        }else{
            $this->getFormError($bookmark,400);
        }
    }
    public function actionNotifyList(){
        $this->requireAuthToken();

        $realty = new RealEstateItem();
        $params=[];
        $response = [];
        $data = [];

        $params['is_saved'] = (boolean) (isset($this->request->isSave)
            && !empty($this->request->isSave))?
            $this->request->isSave:false;

        $params['searching'] = (string) (isset($this->request->searching)
            && !empty($this->request->searching))?
            $this->request->searching:null;

        $params['address'] = (string) (isset($this->request->address)
            && !empty($this->request->address))?
            $this->request->address:null;

        $params['district'] = (string) (isset($this->request->district)
            && !empty($this->request->district))?
            $this->request->district:null;

        $params['city'] = (string) (isset($this->request->city)
            && !empty($this->request->city))?
            $this->request->city:null;

        $params['is_self'] =  (isset($this->request->isSelf)
            && !empty($this->request->isSelf))?(boolean)
        $this->request->isSelf:null;

        $params['recordPerpage']      = (int) (isset($this->request->pagination->recordPerpage)
            && $this->request->pagination->recordPerpage !=0)?
            $this->request->pagination->recordPerpage:20;

        $params['page']     = (int) (isset($this->request->pagination->currentPage)
            && $this->request->pagination->currentPage !=0)?
            $this->request->pagination->currentPage - 1 :0;

        $params['price_min']     = (int) (isset($this->request->priceMin)
            && !empty($this->request->priceMin))?
            $this->request->priceMin :0;

        $params['price_max']     = (int) (isset($this->request->priceMax)
            && !empty($this->request->priceMax))?
            $this->request->priceMax :0;

        $params['area_min']     = (int) (isset($this->request->areaMin)
            &&  !empty($this->request->areaMin))?
            $this->request->areaMin :0;

        $params['area_max']     = (int) (isset($this->request->areaMax)
            && !empty($this->request->areaMax))?
            $this->request->areaMax :0;

        $params['sortKey']     = (int) (isset($this->request->sortKey->key)
            && !empty($this->request->sortKey->key))?
            $this->request->sortKey->key :null;

        $params['sortValue']     = (int) (isset($this->request->sortKey->value)
            && !empty($this->request->sortKey->value))?
            $this->request->sortKey->value :1;

        $params['type'] = (string) (isset($this->request->type)
            && !empty($this->request->type))?
            $this->request->type:null;

        $params['subject_id'] = (int) (isset($this->request->subjectId)
            && $this->request->subjectId != 0)?
            $this->request->subjectId:null;

        $params['is_new'] = (int) (isset($this->request->is_new)
            && $this->request->is_new !=0 )?
            $this->request->is_new:null;

        $params['local'] = false;

        $provider = $realty->register(RealtyRepository::class)->getList($params,true);

        $results = $provider->getModels();

        if($results && !empty($results)){
            foreach($results as $rs){
                $data[] = $rs->register(RealtyRepository::class)->getDataSelf();
            }
        }
        $response['pagination'] = $this->outputObjectPaging($provider);
        $response['items'] = $data;

        $time = Yii::$app->db->createCommand('update notification_history set is_read = 1 where user_id = :user_id',[
            ':user_id'=>$this->user->id
        ])->query();

        $this->sendResponse($response);
    }
}
