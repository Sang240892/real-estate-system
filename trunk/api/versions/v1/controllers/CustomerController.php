<?php

namespace api\versions\v1\controllers;
use api\commons\forms\FormCreateCustomer;
use api\commons\forms\FormUpdateCustomer;
use api\components\RestController;
use backend\commons\helpers\UtilHelper;
use common\models\entities\RealEstateCustomer;
use yii\filters\VerbFilter;
use api\commons\repositories\CustomerRepository;
use Yii;
/**
 * Class CustomerController
 *
 * @package \api\versions\v1\controllers
 */
class CustomerController extends RestController
{

    public function actionList(){
        $this->requireAuthToken();
        $out = [];
        $params = [];
        $customer = new RealEstateCustomer();
        $data = [];
        $params['recordPerpage']      = (int) (isset($this->request->pagination->recordPerpage)
            && $this->request->pagination->recordPerpage !=0)?
            $this->request->pagination->recordPerpage:20;

        $params['page']     = (int) (isset($this->request->pagination->currentPage)
            && $this->request->pagination->currentPage !=0)?
            $this->request->pagination->currentPage - 1 :0;

        $params['user_id'] = $this->user->id;
        $provider = $customer->register(CustomerRepository::class)->getList($params);

        $results = $provider->getModels();

        if($results && !empty($results)){
            foreach($results as $rs){
                $data[] = $rs->register(CustomerRepository::class)->getDataSelf();
            }
        }
        $response['pagination'] = $this->outputObjectPaging($provider);
        $response['items'] = $data;
        $this->sendResponse($response);
    }
    public function actionDelete(){
        $this->requireAuthToken();
        $id =  (isset($this->request->id)
            && !empty($this->request->id))?
            $this->request->id:0;
        $customer =  RealEstateCustomer::findOne(['id'=>$id]);
        if(!$customer){
            $this->sendResponse($this->builtErrorCode(400,\Yii::t('api/error','Không tìm thấy khách hàng')));
        }

        $customer->delete();

        $this->sendResponse();
    }
    public function actionCreate(){
        $this->requireAuthToken();
        $form = new FormCreateCustomer();

        $form->customers =  (isset($this->request->customers)
            && !empty($this->request->customers))?
            $this->request->customers:[];
        $form->user_id =  ($this->user->id)?$this->user->id:0;

        $customers= $form->save();
        $this->sendResponse([
            'customers'=>$customers
        ]);
    }
    public function actionUpdate(){
        $this->requireAuthToken();
        $customer = null;
        $form = new FormUpdateCustomer();
        $form->user_id = $this->user->id;
        $form->id =  (isset($this->request->id)
            && !empty($this->request->id))?
            $this->request->id:0;
        $form->firstName =  (isset($this->request->firstName)
            && !empty($this->request->firstName))?
            $this->request->firstName:null;
        $form->lastName =  (isset($this->request->lastName)
            && !empty($this->request->lastName))?
            $this->request->lastName:null;
        $form->phone =  (isset($this->request->phone)
            && !empty($this->request->phone))?
            $this->request->phone:null;
        $form->address =  (isset($this->request->address)
            && !empty($this->request->address))?
            $this->request->address:null;
        $form->note =  (isset($this->request->note)
            && !empty($this->request->note))?
            $this->request->note:null;
        $form->area =  (isset($this->request->area)
            && !empty($this->request->area))?
            $this->request->area:null;
        $form->price =  (isset($this->request->price)
            && !empty($this->request->price))?
            $this->request->price:null;
        $form->type =  (isset($this->request->type)
            && !empty($this->request->type))?
            $this->request->type:null;
        $form->email =  (isset($this->request->email)
            && !empty($this->request->email))?
            $this->request->email:null;

        if($form->save()){
            $customer = $form->customer;
        }else{
            $object = $form;
            if(empty($form->getFirstErrors())){
                $object = $form->customer;
            }
            $this->getFormError($object,400);
           }

        $this->sendResponse([
            'customers'=>$customer
        ]);
    }
}
