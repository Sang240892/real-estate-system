<?php
/**
 * User: sangnguyen
 * Date: 9/29/15
 * Time: 14:24
 * File name: UserController.php
 * Project name: Fit Road
 */
namespace api\versions\v1\controllers;

use api\commons\forms\SentTokenRestPasswordForm;
use api\commons\forms\SignUpForm;
use api\commons\helpers\ApiHelpers;

use api\components\RestController;
use backend\commons\helpers\UtilHelper;
use common\models\entities\NotificationCategoryRule;
use common\models\entities\RealEstateCategory;
use common\models\entities\RealEstateUser;
use common\models\UserIdentity;
use Yii;


Class AuthController extends RestController{

    /**
     * @var: User Login
     */
    public function actionVerify(){
       // $this->requireDeviceToken();
        $form = new \api\commons\forms\SignInForm();

        $form->email = isset ($this->request->email) ? $this->request->email : null;
        $form->password = isset ($this->request->password) ? $this->request->password : null;

        $deviceToken = isset($this->request->authentication->instanceIDToken)?(string)$this->request->authentication->instanceIDToken:null;
        $deviceType = isset($this->request->authentication->deviceType)?(string)$this->request->authentication->deviceType:null;
        $osType = isset($this->request->authentication->osType)?(string)$this->request->authentication->osType:null;

        if($form->validate()){
            $form->login();
            $userIndentity = Yii::$app->user->getIdentity();

            $this->user = $userIndentity;
            $tokenParams = [
                'user_id'      =>  $userIndentity->id,
                'auth_token'    =>  Yii::$app->security->generateRandomString(),
                'device_token'  =>  $deviceToken,
                'device_type'   =>  $deviceType,
                'os_type'       =>  $osType,
                'user_host'     => Yii::$app->request->userHost,
                'user_ip'       =>Yii::$app->request->userIP,
                'last_accessed' => UtilHelper::getUnixUTCMinuteAfter(UserIdentity::EXPIRED_TIME) //expired time
            ];

            $userSelf  = RealEstateUser::findOne(['id'=>$userIndentity->id]);

            $token = $userSelf->register($this->userRepository)->verifyAuthKey($tokenParams);

            if(!$token){
                $this->sendResponse(false,$this->builtErrorCode(100));
            }

            $this->outPutUser($userSelf,$token);

        }else{
            $this->getFormError($form);
        }
    }

    public function actionSignUp(){
        $businessMsg = null;
        $isUpdate  = FALSE;
        $token = null;

        if (isset($this->request->authentication->authToken) && !empty($this->request->authentication->authToken)) {
            $this->requireAuthToken();
            $form = new SignUpForm(['scenario'=>'update']);
            $form->user = $this->user;
            $isUpdate = TRUE;
        }else{
            $form = new SignUpForm(['scenario'=>'register']);
        }
//        echo '<pre>';
//        print_r($form->getScenario());
//        echo '</pre>';
//        die();

        $form->email = isset ($this->request->email) ? $this->request->email : null;
        $form->password = isset ($this->request->password) ? (string)$this->request->password : null;
        $form->firstName = isset ($this->request->firstName) ? (string)$this->request->firstName : null;
        $form->lastName = isset ($this->request->lastName) ? (string)$this->request->lastName : null;
        $form->phone = isset ($this->request->phone) ? (string)$this->request->phone : null;

        $form->isNotify = isset ($this->request->isNotify) ? (int)$this->request->isNotify : 0;
        $form->notificationCityRule = isset ($this->request->notifyCityRule) ? (string)$this->request->notifyCityRule : null;
        $form->notificationMinPriceRule = isset ($this->request->notifyMinPriceRule) ? (int)$this->request->notifyMinPriceRule : null;
        $form->notificationMaxPriceRule = isset ($this->request->notifyMaxPriceRule) ? (int)$this->request->notifyMaxPriceRule : null;
        $notificationCategories = isset ($this->request->notifyCategories) ? $this->request->notifyCategories : null;

        $deviceToken = isset($this->request->authentication->instanceIDToken)?(string)$this->request->authentication->instanceIDToken:null;
        $deviceType = isset($this->request->authentication->deviceType)?(string)$this->request->authentication->deviceType:null;
        $osType = isset($this->request->authentication->osType)?(string)$this->request->authentication->osType:null;

        if(empty($notificationCategories)){
            $this->sendResponse(['msgClient'=>Yii::t('app','Bạn phải chọn ít nhất một chuyện mục để nhận thông báo.')]);
        }
        if(empty($form->notificationCityRule)){
            $this->sendResponse(['msgClient'=>Yii::t('app','Bạn phải chọn khu vực tin bất động sản để nhận thông báo.')]);
        }

        if($form->validate() && $form->verify()){

            if($isUpdate === FALSE){
                $this->user = $form->user;

                $tokenParams = [
                    'user_id'      =>  $this->user->id,
                    'auth_token'    =>  Yii::$app->security->generateRandomString(),
                    'device_token'  =>  $deviceToken,
                    'device_type'   =>  $deviceType,
                    'os_type'       =>  $osType,
                    'user_host'     => Yii::$app->request->userHost,
                    'user_ip'       =>Yii::$app->request->userIP,
                    'last_accessed' => UtilHelper::getUnixUTCMinuteAfter(UserIdentity::EXPIRED_TIME) //expired time
                ];

                $token = $this->user->register($this->userRepository)->verifyAuthKey($tokenParams);

            }

            if(!$token && $isUpdate === FALSE){
                $this->sendResponse(false,$this->builtErrorCode(100));
            }
            $this->user->notify_categories = json_encode($notificationCategories);
            $this->user->save();
            // add category notification rules
            if(!empty($notificationCategories)){

                $rules = $this->user->getNotificationCategoryRules()->all();
                   if(!empty($rules)){
                       foreach($rules as $r){
                           $r->delete();
                       }
                   }

                foreach($notificationCategories as $cate){
                        $category = RealEstateCategory::findOne(['id'=>$cate]);
                        if($category){
                            $finders =  $category->getRealEstateCategoryRelations()->all();
                            if(!empty($finders)){
                                foreach($finders as $finder){
                                    try{
                                        $notification = new NotificationCategoryRule();
                                        $notification->user_id  =   $this->user->id;
                                        $notification->category_id  =   $finder->category_children_id;
                                        $notification->save();
                                    }catch (\yii\db\Exception $e){
                                        Yii::error('Error \'s name: '.$e->getName(), 'Users');
                                        Yii::error('Error \'s message: '.$e->getMessage(), 'Users');
                                        Yii::error('Error get data self contact', 'Users');
                                    }
                                }

                            }
                        }
                }
            }
            $this->outPutUser($this->user,$token);

        }else{
            $this->getFormError($form);
        }
    }

    public function actionSignOut(){
        $authToken = $this->requireAuthToken();
        $authToken->user->is_online = 0;
        if( $authToken->user->save(false)){
            Yii::$app->user->logout();
            try{
                $authToken->delete();
                $this->sendResponse();
            }
            catch (\yii\db\Exception $e){
                Yii::error('Error \'s name: '.$e->getName(), 'Users');
                Yii::error('Error \'s message: '.$e->getMessage(), 'Users');
                Yii::error('Error Delete authetication token of user '.Yii::$app->user->id, 'Users');
            }
        }
        $this->sendResponse(false,$this->builtErrorCode(400));

    }
    /**
     * @var : Password reset request
     */
    public function actionPasswordResetRequest(){

        $form = new SentTokenRestPasswordForm();
        $form->isBackend = FALSE;
        $form->email = isset ($this->request->email) ? $this->request->email : null;
        if($form->validate()){
            $send = $form->sendEmail();
            if($send == true)
                $this->sendResponse(['msgClient'=>Yii::t('app','Please check mail.')]);
            else
                $this->sendResponse(false,$this->builtErrorCode(94));
        }else{
            $this->getFormError($form);
        }
    }
    public function actionInstanceidtoken(){
        $authToken = $this->requireAuthToken();
        $deviceToken = isset($this->request->authentication->instanceIDToken)?(string)$this->request->authentication->instanceIDToken:null;
        $osType = isset($this->request->authentication->osType)?(int)$this->request->authentication->osType:null;
        $authToken->device_token = $deviceToken;
        $authToken->os_type =$osType;
        $authToken->save(false);
        $this->sendResponse();
    }


}
