<?php 
return [
    'Realty Loan'=>'Bất đông sản cho thuê',
    'Realty Self'=>'Bất đông sản bán',
    'Realty Sold'=>'Bất đông sản đã bán',
        'check email'=>'Let check your email!',
        'Send successful' => 'Send successful',
        'without a hosital' => 'Not a member of a hospital.',
        'from hospital' => 'From Hospital',
        'all list'      => 'All contacts',
        'sent request list'      => 'Sent requests',
        'contact list'      => 'Contact list',
        'recevied request list'      => 'Recevied requests',
        'rejected request list'      => 'Rejected requests',
        "meet {customerName}"    => "Meet {customerName}",
        "{username} has just leave out of group \"{groupName}\"" => "{username} has just leave out of group \"{groupName}\"",
        "You have just been invited join into group \"{groupName}\"" => "You have just been invited join into group \"{groupName}\"",
        "You have just been removed out of group \"{groupName}\"" => "You have just been removed out of group \"{groupName}\""
];
