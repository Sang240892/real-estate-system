<?php
/**
 * User: sangnguyen
 * Date: 9/30/15
 * Time: 08:43
 * File name: SingInForm.php
 * Project name: cycmover
 */

namespace api\commons\forms;

use backend\commons\helpers\UtilHelper;
use common\components\repositories\EntityFactory;
use Yii;
use yii\base\Model;
use common\models\UserIdentity;

/**
 * @property string $email
 * @property string $password

 * Class SingInForm
 * @package api\common\forms
 */
class SignInForm extends Model
{
    public $email;
    public $password;

    private $_user = false;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],

            ['password','string'],
            // rememberMe must be a boolean value
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }
    public function validateUserName($attribute, $params){

        if(!filter_var($this->email, FILTER_VALIDATE_EMAIL) && !preg_match("/^[0-9]{9,12}$/", $this->email) ) {
            $this->addError('email',Yii::t('api/error','Tên đăng nhập phải là email hoặc số điện thoại'));
            return false;
        }
    }
    /**
     * @param $attribute
     * @param $params
     * @return bool
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if(empty($this->password)){
                $this->addError($attribute, Yii::t('api/error', 'Tài khoản hoặc mật khẩu không đúng!'));
            }
            if (!$user || !$user->validatePassword((string)$this->password)) {
                $this->addError($attribute, Yii::t('api/error', 'Tài khoản hoặc mật khẩu không đúng!'));
            }
            elseif((int)$user->is_online == UserIdentity::IS_ONLINE && $user->checkExpiredAuthenticationToken()){
                $this->addError($attribute, Yii::t('api/error','Tài khoản này đang online!'));
                return false;
            }
            elseif($user->status !== UserIdentity::STATUS_ACTIVE){
                $this->addError($attribute, Yii::t('api/error','Tài khoản đã bị khóa!'));
                return false;
            }
        }
    }
    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser());
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return UserIdentity|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = UserIdentity::findByEmail($this->email)
                ->andOnCondition(['in','role',[
                    EntityFactory::USER_ADMIN,EntityFactory::USER_ROOT,
                    EntityFactory::USER_STAFF,EntityFactory::USER_MANAGER,EntityFactory::USER_CUSTOMER
                ]])->one();
        }
        return $this->_user;
    }
}
