<?php
/**
 * User: sangnguyen on  11/04/15 at 22:18
 * File name: SignUpForm.php
 * Project name: Fit Road
 * Copyright (c) 2015 by COMPANY
 * All rights reserved
 */

namespace api\commons\forms;


use api\commons\helpers\ApiHelpers;
use common\models\entities\RealEstateUser;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;


class SignUpForm extends Model{
    CONST SCENARIO_UPDATE       =   'update';
    CONST SCENARIO_REGISTER     =   'register';

    public $user;
    public $password;

    public $firstName;
    public $lastName;
    public $role = -1;
    public $email;
    public $phone;
    public $isNotify = 0;
    public $notificationCityRule = null;
    public $notificationMinPriceRule = null;
    public $notificationMaxPriceRule = null;
    public $notificationCategory = null;
    /**
     * @inheritdoc
     */

    public function rules()
    {
        $parentRules = parent::rules();
        ($this->user)?$user_id=$this->user->id:$user_id=null;

        $currentRules =  [
            ['role', 'integer'],
            ['role', 'in','range' => ['0011','4','0010','-1'],'message' => Yii::t('backend/error', 'role not existed')],

//            ['gender','filter','filter' => 'trim'],
//            ['gender', 'in','range' => ['0','1'],'message' => Yii::t('backend/error', 'gender not existed')],
//
//            ['phone_number', 'integer'],
//            ['phone_number', 'match', 'pattern'=>'/^[1-9]{9,12}$/','message' => Yii::t('backend/error',
//                'phone number validate')],

            ['lastName','filter','filter' => 'trim'],
            ['lastName', 'required', 'on'    =>  self::SCENARIO_REGISTER],

            ['firstName','filter','filter' => 'trim'],
            ['firstName', 'required', 'on'    =>  self::SCENARIO_REGISTER],

           // ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'on'    =>  self::SCENARIO_REGISTER],
            ['email', 'validateUserName','on'=>self::SCENARIO_REGISTER],

            [
                'email', 'unique', 'targetClass' => '\common\models\entities\RealEstateUser',
                'filter'=> ['not in','id',[$user_id]],
                'on'    =>  self::SCENARIO_UPDATE,
                'message' => Yii::t('backend/error', 'Email đã được sử dụng!'),
            ],

            ['phone','required','message'=>'Số điện thoại không được phép rỗng.'],

            [
                'phone', 'unique', 'targetClass' => '\common\models\entities\RealEstateUser',
                'on'    =>  self::SCENARIO_REGISTER,
                'message' => Yii::t('backend/error', 'Số điện thoại đã được sử dụng!'),
            ],

            [
                'phone', 'unique', 'targetClass' => '\common\models\entities\RealEstateUser',
                'filter'=> ['not in','id',[$user_id]],
                'on'    =>  self::SCENARIO_UPDATE,
                'message' => Yii::t('backend/error', 'Số điện thoại đã được sử dụng!'),
            ],

//            ['password', 'required','on'=>self::SCENARIO_REGISTER],
//            ['password', 'required','on'=>self::SCENARIO_REGISTER],

            [['password'], 'required','on'=>self::SCENARIO_REGISTER],
            //['password','filter','filter' => 'trim'],


        ];
        return ArrayHelper::merge($parentRules,$currentRules);
    }
/*    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_UPDATE] = ['username', 'password'];
        $scenarios[self::SCENARIO_REGISTER] = ['username', 'email', 'password'];
        return $scenarios;
    }*/
    public function validateUserName($attribute, $params){

        if(filter_var($this->email, FILTER_VALIDATE_EMAIL) ) {
            if(self::getScenario() === self::SCENARIO_REGISTER){
                $user = RealEstateUser::findOne(['email'=>$this->email]);
                if($user != null){
                    $this->addError('email',Yii::t('api/error','Tên đăng nhập đã có người sử dụng'));
                    return false;
                }else{
                    return true;
                }
            }
//
        }

        if(preg_match("/^[0-9]{9,12}$/", $this->email)){
            if(self::getScenario() === self::SCENARIO_REGISTER){
                $user = RealEstateUser::findOne(['phone'=>$this->email]);

                if($user != null){
                    $this->addError('email',Yii::t('api/error','Tên đăng nhập đã có người sử dụng'));
                    return false;
                }else{
                    $this->phone = $this->email;
                    $this->email = null;
                    return true;
                }
            }
//
        }
        $this->addError('email',Yii::t('api/error','Tên đăng nhập phải là email hoặc số điện thoại'));
        return false;
    }
    public function verify(){
        $model = new RealEstateUser();

        if($this->user){
            $model = $this->user;
        }else{
            $model->role        =   '-1';
        }

        $oldPw = $model->password;

//        $this->phone_number     =   $this->phone_number;
        $model->status      =   1;
        $model->last_name   =   $this->lastName;
        $model->first_name  =   $this->firstName;

        if(!empty($this->password)){
            $model->password = \Yii::$app->security->generatePasswordHash(trim($this->password));
        }else{
            $model->password  =  $oldPw;
        }
        $model->phone       =  !empty($this->phone)?$this->phone:$model->phone;
        $model->email       =   !empty($this->email)?$this->email:$model->email;

        $model->is_notify =    !empty( $this->isNotify)? $this->isNotify:1;
        $model->notification_city_rule = $this->notificationCityRule;
        $model->notification_price_min_rule = $this->notificationMinPriceRule;
        $model->notification_price_max_rule = $this->notificationMaxPriceRule;

//        $this->role             =   $this->role;
        if(!$model->save()){
            $this->addError('user',ApiHelpers::getFormMessageError($model));
            return FALSE;
        }
        $model->refresh();
        return $this->user =$model;
    }
}
