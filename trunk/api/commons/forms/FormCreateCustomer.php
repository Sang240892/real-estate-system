<?php

namespace api\commons\forms;
use common\models\entities\RealEstateCustomer;
use yii\base\Model;
use Yii;
use api\commons\repositories\CustomerRepository;
use yii\helpers\HtmlPurifier;

/**
 * Class FormCreateCustomer
 *
 * @package \api\commons\forms
 */
class FormCreateCustomer extends Model
{
    public $customers;
    public $user_id;
    public function rules(){
        return [
            ['customers','validateCustomer'],
        ];
    }

    public function validateCustomer($attribute){
        if(is_array($this->customers) && !empty($this->customers)){
            $i= 1;
            foreach($this->customers as $customer){
                if(!isset($customer->firstName) || empty($customer->firstName)){
                    $this->addError($attribute,Yii::t('api/error','Họ của khách hàng thứ {i} rỗng',[
                        'i'=>$i
                    ]));
                    return FALSE;
                }
                if(!isset($customer->lastName) || empty($customer->lastName)){
                    $this->addError($attribute,Yii::t('api/error','Tên của khách hàng thứ {i} rỗng',[
                        'i'=>$i
                    ]));
                    return FALSE;
                }
                $i++;
            }
        }else{
            $this->addError($attribute,Yii::t('api/error','Dữ liệu rỗng'));
            return FALSE;
        }
    }

    public function save(){
        $response = [];
        if($this->validate()){
            foreach($this->customers as $customer){

                $model = new RealEstateCustomer();

                if(!empty($customer->phone)){

                    $validate = RealEstateCustomer::findOne([
                        'phone'=>$customer->phone
                    ]);

                    if($validate === null){
                        $model->first_name = isset($customer->firstName)?HtmlPurifier::process($customer->firstName):null;
                        $model->last_name = isset($customer->lastName)?HtmlPurifier::process($customer->lastName):null;
                        $model->address = isset($customer->address)?HtmlPurifier::process($customer->address):null;
                        $model->area = isset($customer->area)?$customer->area:null;
                        $model->type = isset($customer->type)?HtmlPurifier::process($customer->type):null;
                        $model->note = isset($customer->note)?HtmlPurifier::process($customer->note):null;
                        $model->price_string = isset($customer->price)?HtmlPurifier::process($customer->price):null;
                        $model->phone = isset($customer->phone)?trim($customer->phone):null;
                        $model->email = isset($customer->email)?HtmlPurifier::process($customer->email):null;
                        $model->user_id = $this->user_id;
                        $model->status = 1;

                        if($model->save(false)){
                            $model->refresh();
                            $response[] = $model->register(CustomerRepository::class)->getDataSelf();
                        }else{
                            $this->addError('customers',$model->getFirstErrors());
                            return FALSE;
                        }

                    }
                }

            }
        }else{
            $this->addError('customers',$this->getFirstErrors());
            return FALSE;
        }
        return $response;
    }

}
