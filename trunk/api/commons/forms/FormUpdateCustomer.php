<?php

namespace api\commons\forms;
use common\models\entities\RealEstateCustomer;
use yii\base\Model;
use api\commons\repositories\CustomerRepository;
use Yii;
/**
 * Class FormUpdateCustomer
 *
 * @package \api\commons\forms
 */
class FormUpdateCustomer extends Model
{
    public $id;
    public $user_id;
    public $firstName;
    public $lastName;
    public $phone;
    public $area;
    public $price;
    public $address;
    public $email;
    public $note;
    public $type;
    public $customer;
    public function rules(){
        return [
            [['lastName', 'firstName'], 'required', 'message'   => Yii::t('api/error', 'Tên khách hàng không được rỗng!'),],
            ['id', 'exist',
                'targetAttribute'=>'id','targetClass' => '\common\models\entities\RealEstateCustomer',
                'message'   => Yii::t('api/error', 'Khách hàng không tồn tại'),
            ],
            [
                'phone', 'unique', 'targetClass' => '\common\models\entities\RealEstateCustomer',
                'filter'=> ['id'=>$this->user_id],
                'message' => Yii::t('backend/error', 'Số điện thoại đã được sử dụng!'),
            ],
        ];
    }
    public function save(){
        $customer =  RealEstateCustomer::findOne(['id'=>$this->id]);
        $validate = RealEstateCustomer::find()
            ->andOnCondition(['phone'=>$this->phone,'user_id'=>$this->user_id])
        ->andOnCondition(['not in','id',[$this->id]])->one();

        if($validate !== null){
            $this->addError('phone','Số điện thoại khách hàng đã tồn tại');
            return false;
        }

        if($this->validate()){
            $customer->first_name = $this->firstName;
            $customer->last_name = $this->lastName;
            $customer->phone = $this->phone;
            $customer->area = $this->area;
            $customer->address = $this->address;
            $customer->note = $this->note;
            $customer->type = $this->type;
            //$customer->price = $this->price;
            $customer->price_string = $this->price;
            $customer->user_id = $this->user_id;
            $customer->email = $this->email;

            if(!$customer->save(false)){
                $this->customer = $customer;
                return FALSE;
            }
        }else{
            return FALSE;
        }

        $customer->refresh();

        return $this->customer = $customer->register(CustomerRepository::class)->getDataSelf();
    }

}
