<?php

namespace api\commons\forms;
use api\commons\helpers\ApiHelpers;
use backend\commons\helpers\UtilHelper;
use common\models\entities\RealEstateItem;
use yii\base\Model;
use Yii;
use yii\helpers\HtmlPurifier;

/**
 * Class CreateRealtyForm
 *
 * @package \api\commons\forms
 */
class CreateRealtyForm extends Model
{
    CONST SCENARIO_UPDATE       =   'update';
    CONST SCENARIO_CREATE   =   'create';

    public $owner_id;
    public $realtyId;
    public $title;
    public $description;
    public $price;
    public $address;
    public $area;
    public $phone;
    public $images;
    public $contactName;
    public $sold;
    public $realty;
    public $type;
    public $front;
    public $floor;
    public $bedroom;
    public $tip;


    public function rules()
    {
        return [
            [['title','price','address','area'], 'required'],

            ['phone', 'integer'],
            ['phone', 'match', 'pattern'=>'/^[0-9]{9,12}$/','message' => Yii::t('backend/error',
                'This phone number is not validate,Etc: 09....')],

            ['realtyId','required',  'on'    =>  self::SCENARIO_UPDATE],
            ['realtyId','integer',  'on'=>self::SCENARIO_UPDATE],
            ['realtyId', 'exist',
             'targetAttribute'=>'id','targetClass' => '\common\models\entities\RealEstateItem',
                'filter'    => ['user_id'=>$this->owner_id],
                'message'   => Yii::t('api/error', 'Realty not found'),
                'on'    =>  self::SCENARIO_UPDATE,
            ],
            ['type', 'integer'],
            ['type', 'in',
                'range' => [RealEstateItem::REALTY_LOAN,RealEstateItem::REALTY_SALE,RealEstateItem::REALTY_SOLD]
                ,'message' => Yii::t('backend/error', 'Realty type not support!')
            ],
        ];
    }

    /**
     * @return bool|RealEstateItem
     */
    public function save(){
        if($this->validate()){
            $realty = null;
            $realty = new RealEstateItem();

            if($this->realtyId !== null){
                $realty = RealEstateItem::find()->where(['id'=>$this->realtyId])->one();
            }

            if($realty === null){
                $realty = new RealEstateItem();
            }

            $realty->title = HtmlPurifier::process($this->title);
            $realty->description = HtmlPurifier::process($this->description);
            $realty->price = UtilHelper::convertPriceStringToNumber($this->price);
            $realty->price_string = $this->price;
            $realty->address = HtmlPurifier::process($this->address);
            $realty->area = $this->area;
            $realty->phone_contact = HtmlPurifier::process($this->phone);
            $realty->is_sold = ($this->sold === false)?0:1;
            $realty->local = 1;
            $realty->realty_type = HtmlPurifier::process($this->type);
            $realty->floor  =   HtmlPurifier::process($this->floor);
            $realty->front = HtmlPurifier::process($this->front);
            $realty->bedroom = HtmlPurifier::process($this->bedroom);
            $realty->user_id = $this->owner_id;
            $realty->contact_name = HtmlPurifier::process($this->contactName);
            $realty->category_id = null;
            $realty->tip = HtmlPurifier::process($this->tip);

            if($realty->save()){
                $realty->refresh();
                $this->realty = $realty;
                return $this->realty;
            }else{
                $this->addError('title',ApiHelpers::getFormMessageError($realty));
                return FALSE;
            }
        }else{
            //$this->addError('title',ApiHelpers::getFormMessageError($realty));
            return FALSE;
        }

    }
}
