<?php
namespace api\commons\repositories;
use common\models\entities\AuthenticationToken;
use common\models\entities\NotificationCategoryRule;
use common\models\entities\NotificationHistory;
use common\models\entities\RealEstateCategory;
use common\models\entities\RealEstateCategoryRelation;
use common\models\entities\RealEstateItem;
use common\models\entities\RealEstateUser;
use common\models\UserIdentity;
use Yii;
use yii\helpers\HtmlPurifier;

/**
 * Class UserRepository
 *
 * @package \\${NAMESPACE}
 */
class UserRepository
{
   protected $repository;
    public function __construct(RealEstateUser $repository){
        $this->repository = $repository;
    }

    /**
     * @param array $params
     * @return bool|AuthenticationToken|null|static
     */
    public function verifyAuthKey($params = []){
        $token  = AuthenticationToken ::findOne([
            'user_id' => $params['user_id'],
        ]);
        if($token){
            $token->attributes = $params;
            $token->save(false);
        }else{
            $token = new AuthenticationToken();
            $token->attributes = $params;
            $token->save(false);
        }
        if($token){
            $token->user->is_online = UserIdentity::IS_ONLINE;
            $token->user->save(false);
            return $token;
        }

        return false;
    }
    /**
     * @return array
     */
    public function getDataSelfUser(){
        $conditionCategory = [];
        $subjectId = [];

//        $categoryRules = $this->repository->getNotificationCategoryRules()->all();
//
//        if(!empty($categoryRules)){
//            foreach($categoryRules as $cate){
//                $conditionCategory[] = $cate->category_id;
//            }
//        }
//        $subjects = RealEstateCategoryRelation::find()
//            ->andOnCondition(['in','category_children_id',$conditionCategory])
//            ->select('category_parent_id')->distinct()->all();

//        if(!empty($subjects)){
//            foreach($subjects as $s){
//                $subjectId[] = $s->category_parent_id;
//            }
//        }
        $notifyCategories = json_decode($this->repository->notify_categories);
        if(!empty($notifyCategories)){
            foreach($notifyCategories as $k=>$v){
                $subjectId[] = $v;
            }
        }
        try{
            return $data = [
                "id"    =>      (string)$this->repository->id,
                "createdAt" =>  (int)$this->repository->created_at,
                "email"     =>  (string)$this->repository->email,
                "phone"     =>  (string)$this->repository->phone,
                "lastName" =>  (string)$this->repository->last_name,
                "firstName" =>  (string)$this->repository->first_name,
                "notifyCityRule"=>$this->repository->notification_city_rule,
                "notifyCategories"=>$subjectId,
                "notifyPriceMin"=>$this->repository->notification_price_min_rule,
                'notifyPriceMax'=>$this->repository->notification_price_max_rule,
            ];
        }catch (\yii\db\Exception $e){
            Yii::error('Error \'s name: '.$e->getName(), 'Users');
            Yii::error('Error \'s message: '.$e->getMessage(), 'Users');
            Yii::error('Error get data self contact', 'Users');
        }
    }

    public function getBadgeRealtyNotification(){
        $number = $this->repository
            ->getNotificationHistories()->andOnCondition([
                'is_read'=>0,
                'is_push'=>0
            ])->count();
        return $number;
    }
    public function getRealtyNotification($params=[]){
        $realties = $this->repository
            ->getNotificationHistories()->andOnCondition([
                'is_read'=>0
            ])->all();

    }
}
