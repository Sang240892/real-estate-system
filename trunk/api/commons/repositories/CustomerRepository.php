<?php

namespace api\commons\repositories;
use common\models\entities\RealEstateCustomer;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\HtmlPurifier;

/**
 * Class CustomerRepository
 *
 * @package \api\commons\repositories
 */
class CustomerRepository
{
    protected $repository;
    public function __construct(RealEstateCustomer $repository){
        $this->repository = $repository;
    }

    public function getList($params=[]){
        $query = $this->repository->find();

        $query->andOnCondition(['user_id'=>$params['user_id']]);

        if(isset($params['searching']) && $params['searching']){
            $query->andFilterWhere([
                'or',
                ['like', 'real_estate_customer.first_name',HtmlPurifier::process( $params['searching'])],
                ['like', 'real_estate_customer.last_name', HtmlPurifier::process( $params['searching'])],
            ]);
        }

        $modelProvider = new ArrayDataProvider([
            'allModels' => $query->all(),
            'sort' => [
                'defaultOrder'=>['created_at' => SORT_DESC],
                'attributes' => ['name','price','area','created_at']
            ],
            'pagination' => [
                'pageSize'  =>  ($params['recordPerpage'] > 2000)?2000:$params['recordPerpage'],
                'page'      =>  $params['page']
            ]
        ]);
        return $modelProvider;
    }

    public function getDataSelf(){
        try{
            return $data = [
                "id"    =>      (string)$this->repository->id,
                "createdAt" =>  (int)$this->repository->created_at,
                "email"     =>  (string)$this->repository->email,
                "lastName" =>  (string)$this->repository->last_name,
                "firstName" =>  (string)$this->repository->first_name,
                "phone" =>  (string)trim($this->repository->phone),
                "address" =>  (string)$this->repository->address,
                "area" =>  (int)$this->repository->area,
                "price" =>  (string)$this->repository->price_string,
                "note" =>  (string)$this->repository->note,
                "type" =>  (int)$this->repository->type,
            ];
        }catch (\yii\db\Exception $e){
            Yii::error('Error \'s name: '.$e->getName(), 'Users');
            Yii::error('Error \'s message: '.$e->getMessage(), 'Users');
            Yii::error('Error get data self contact', 'Users');
        }
    }
}
