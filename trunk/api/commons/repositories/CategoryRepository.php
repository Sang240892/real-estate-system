<?php

namespace api\commons\repositories;
use common\models\entities\RealEstateCategory;

/**
 * Class CategoryRepository
 *
 * @package \api\commons\repositories
 */
class CategoryRepository
{
    protected $category;

    public function __construct(RealEstateCategory $category){
        $this->category = $category;
    }

    public function getDataSelf(){
        return[
            'id'=>(string)$this->category->id,
            'name'=>$this->category->name,
        ];
    }
}
