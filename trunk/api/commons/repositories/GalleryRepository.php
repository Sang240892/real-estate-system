<?php

namespace api\commons\repositories;
use api\commons\helpers\ApiHelpers;
use common\models\entities\RealEstateGallery;
use Yii;
/**
 * Class GalleryRepository
 *
 * @package \api\commons\repositories
 */
class GalleryRepository
{
    protected $image;
    public function __construct(RealEstateGallery $image){
        $this->image = $image;
    }

    public function getDataSelf(){
        return  ApiHelpers::getAllSizeImages(Yii::$app->params['productFinder'],$this->image);
    }
}
