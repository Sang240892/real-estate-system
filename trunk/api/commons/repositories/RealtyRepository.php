<?php

namespace api\commons\repositories;
use api\commons\helpers\ApiHelpers;
use backend\commons\helpers\UtilHelper;
use common\models\entities\NotificationHistory;
use common\models\entities\RealEstateBookmark;
use common\models\entities\RealEstateCategory;
use common\models\entities\RealEstateCategoryRelation;
use common\models\entities\RealEstateItem;
use common\models\entities\RealEstateItemPostDate;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use api\commons\repositories\CategoryRepository;
use api\commons\repositories\GalleryRepository;
use Yii;
use yii\helpers\HtmlPurifier;
use api\commons\repositories\UserRepository;

/**
 * Class RealtyRepository
 *
 * @package \api\commons\repositories
 */
class RealtyRepository
{
    protected $realty;
    protected $data;
    public function __construct(RealEstateItem $realty){
        $this->realty  = $realty;
    }

    public function getList($params=[],$isNotify=false){
        $sortKey = 'real_estate_date';
        $sortValue = 0;
        $query = $this->realty->find()->distinct();

        if((!isset($params['local'] ) || $params['local'] == 0)){
            $query->andOnCondition(['is_self'=>1,'is_not_sure_self'=>0]);
        }else{
            $query->andOnCondition(['is_not_sure_self'=>0]);
        }

        $query->leftJoin(RealEstateItemPostDate::tableName(),
            RealEstateItemPostDate::tableName().'.item_id = '.RealEstateItem::tableName().'.id');
        $query->with('realEstateItemPostDates');
        //$query->with('category');
        $cat = [];

        if(isset($params['is_saved']) && $params['is_saved'] !== false){
            $query->rightJoin(RealEstateBookmark::tableName(),
                RealEstateBookmark::tableName().'.item_id = '.RealEstateItem::tableName().'.id'
            )->andOnCondition([RealEstateBookmark::tableName().'.user_id'=>Yii::$app->user->id]);
        }

        if(isset($params['subject_id'] ) && $params['subject_id'] !== null ){
            $categories = RealEstateCategoryRelation::findAll(['category_parent_id'=>$params['subject_id']]);
            if(!empty($categories)){
                foreach($categories as $re){
                    $cat[] = $re->category_children_id;
                }
            }else{
                $cat[] = 0;
            }
            $query->andFilterWhere(['in','category_id',$cat]);
        }

        if($isNotify === true){
            $query->rightJoin(NotificationHistory::tableName(),
                NotificationHistory::tableName().'.item_id = '.RealEstateItem::tableName().'.id')
                ->andOnCondition([NotificationHistory::tableName().'.user_id'=>Yii::$app->user->id])
                ->andOnCondition([NotificationHistory::tableName().'.is_push'=>1]);
        }

//        if(isset($params['is_self'] ) && $params['is_self'] !== null ){
//            $query->andFilterWhere(['real_estate_item.is_self'=>($params['is_self'] === true)?1:0]);
//        }
        if(isset($params['user_id'] ) && $params['user_id'] ){
            $query->andFilterWhere(['real_estate_item.user_id'=>(int)$params['user_id']]);
        }
        if(isset($params['local'] ) && $params['local']  !== null ){
            $query->andFilterWhere(['real_estate_item.local'=>($params['local'] === true)?1:0]);
        }
        if(isset($params['type'] ) && $params['type']  !== null ){
            $query->andFilterWhere(['real_estate_item.realty_type'=>$params['type']]);
        }
        if(isset($params['is_sold'] ) && $params['is_sold'] !== null ){
            $query->andFilterWhere(['real_estate_item.is_sold'=>($params['is_sold'] === true)?1:0]);
        }
        if(isset($params['is_new'] ) && $params['is_new'] !== null ){
            $query->andFilterWhere(['real_estate_item.is_new'=>($params['is_new'] == 1)?1:0]);
        }
        if(isset($params['price_min'] ) && isset($params['price_max'] ) && $params['price_max'] !== 0 ){
            if($params['price_max'] == '-1'){
                $query->andFilterWhere([
                    'or',
                    ['like', 'real_estate_item.price_string','thỏa thuận'],
                    ['=','price_string',0],
                ]);
            }else{
                $query->andFilterWhere( ['between', 'real_estate_item.price',(int) $params['price_min'],(int) $params['price_max']]);
            }
        }
        if(isset($params['area_min'] ) && isset($params['area_max'] ) && $params['area_max'] !== 0 ){
            $query->andFilterWhere( ['between', 'real_estate_item.area_number',(int) $params['area_min'],(int) $params['area_max']]);
        }
        if(isset($params['searching']) && $params['searching']){
            $query->andFilterWhere([
                'or',
                ['like', 'real_estate_item.title',HtmlPurifier::process( $params['searching'])],
                ['like', 'real_estate_item.summary', HtmlPurifier::process( $params['searching'])],
                ['like', 'real_estate_item.description',HtmlPurifier::process( $params['searching'])],
            ]);
        }

        if(isset($params['search_address']) && !empty($params['search_address'])){
            $address = $params['search_address'];
            $city  = (isset($address[0]) && !empty($address[0]))?HtmlPurifier::process($address[0]):null;
            $province = (isset($address[1]) && !empty($address[1]))?HtmlPurifier::process($address[1]):null;

            if(!empty($province)){
                $query->andWhere("MATCH (`address`) AGAINST ('\"$province\"' IN BOOLEAN MODE) or `real_estate_item`.`description` like "."'%".$province."%'"."");
            }

            if(!empty($city)){
                $query->andWhere("MATCH (`address`) AGAINST ('\"$city\"' IN BOOLEAN MODE) or `real_estate_item`.`description` like "."'%".$city."%'"."");
            }


        }elseif(isset($params['address']) && $params['address']){
           $query = UtilHelper::parseAddressWhenSearchingOnAPIs($query,$params['address']);
        }

        if(isset($params['sortKey']) && in_array($params['sortKey'],['time','area','price','title'])){
            if($params['sortKey'] == 'title'){
                $sortKey = 'title';
            }
            if($params['sortKey'] == 'time'){
                $sortKey = 'real_estate_date';
            }
            if($params['sortKey'] == 'area'){
                $sortKey = 'area_number';
            }
            if($params['sortKey'] == 'price'){
                $sortKey = 'price';
            }
        }

        if(isset($params['sortKey']) && in_array($params['sortValue'],[1,-1])){
            if($params['sortValue'] == 1){
                $sortValue = SORT_ASC;
            }
            if($params['sortKey'] == -1){
                $sortValue = SORT_DESC;
            }
        }

        //$query->orderBy([RealEstateItem::tableName().'.'.$sortKey=>$sortValue]);

        $modelProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                 'defaultOrder'=>[$sortKey => $sortValue],
                 'attributes' => [
                     'title',
                     'price' => [
                        'asc' => [RealEstateItem::tableName().'.price' => SORT_ASC],
                        'desc' => [RealEstateItem::tableName().'.price' => SORT_DESC],
                        'label'=>Yii::t('backend','Re Post Date'),
                        //'default' => SORT_ASC
                     ],
                     'area_number',
                     'real_estate_date' => [
                         'asc' => [RealEstateItemPostDate::tableName().'.post_date' => SORT_ASC],
                         'desc' => [RealEstateItemPostDate::tableName().'.post_date' => SORT_DESC],
                         'label'=>Yii::t('backend','Re Post Date'),
                         //'default' => SORT_ASC
                     ],
                 ]
            ],
            'pagination' => [
                'pageSize'  =>  ($params['recordPerpage'] > 2000)?2000:$params['recordPerpage'],
                'page'      =>  $params['page']
            ]
        ]);

        return $modelProvider;
    }
    public function detectSubjectId(){
        $relation = RealEstateCategoryRelation::findOne(['category_children_id'=>$this->realty->category_id]);
        if($relation){
            return $relation->category_parent_id;
        }
        return 0;
    }
    public function getDataSelf(){
        $this->realty->refresh();
        $contact = "";
        $owner = $this->realty->getUser()->one();
        $images =[];
        $isSaved = false;
        if(!empty($this->realty->realEstateGalleries)){
            foreach($this->realty->realEstateGalleries as $img){
                $images[] = $img->register(GalleryRepository::class)->getDataSelf();
            }
        }
        if($owner && !empty($owner)){
            $contact = $owner->register(UserRepository::class)->getDataSelfUser();
        }
        if(empty($images)){
            $images = (string)($this->realty->avatar && $this->realty->local == 0)?ApiHelpers::getAllSizeImages(Yii::$app->params['productFinder'],
                $this->realty->avatar):"";
        }
        $avatar = $this->realty->avatar;
        if(!$avatar){
            if(isset($this->realty->realEstateGalleries) && !empty($this->realty->realEstateGalleries)){
                $avatar = $this->realty->realEstateGalleries[0]->file_name;
            }
        }

        $bookmark = $this->realty->getRealEstateBookmarks()->andOnCondition(['user_id'=>Yii::$app->user->id])->one();

        if($bookmark){
            $isSaved = true;
        }

        return [
            'id'=>(string)$this->realty->id,
            'title'=>(string)$this->realty->title,
            'description'=>(string)$this->realty->description,
            'summary'=>(string)$this->realty->summary,
            'source'=>(string)$this->realty->source,
            'address'=>(string)$this->realty->address,
            'isSelf'=>(boolean)$this->realty->is_self,
            'street' => (string) $this->realty->street,
            'district' => (string)$this->realty->district,
            'ward' => (string)$this->realty->ward,
            'city' =>(string)$this->realty->city,
            'country' =>(string) $this->realty->country,
            'detailLink'=> ($this->realty->realEstateUrlCrawlingItem)?$this->realty->realEstateUrlCrawlingItem->url:"",
            'phoneContact' =>(string) $this->realty->phone_contact,
            'phoneBackup' =>(string) $this->realty->phone_backup,

            'type' =>(int)$this->realty->realty_type,
            'price' =>(string)$this->realty->price,
            'priceString' =>(string)UtilHelper::convertFromNumberToStringOfPrice($this->realty->price_string),

            'area' =>(string)$this->realty->area,
            'categoryInSystem' =>($this->realty->category)?
                $this->realty->category->register(CategoryRepository::class)->getDataSelf():"",

            'createdAt' =>(int) $this->realty->created_at,
            'bedroom'=>(string)$this->realty->bedroom,
            'floor'=>(string)$this->realty->floor,
            'front'=>(string)$this->realty->front,
            'images'=>$images,
            'isLocal'=>boolval($this->realty->local),
            'isSold'=>boolval($this->realty->is_sold),
            'categoryName' => $this->realty->category_name,
            'latitude' =>(string) $this->realty->latitude,
            'longitude' => (string)$this->realty->longitude,
            'isSelf' => (boolean)$this->realty->is_self,
            'realtyDate' =>(int)(isset($this->realty->realEstateItemPostDates[0]))?$this->realty->realEstateItemPostDates[0]->post_date:'',
            'avatar' => (string)(!empty($avatar))?ApiHelpers::getAllSizeImages(Yii::$app->params['productFinder'],
                $avatar):"",
            "owner"=>$contact,
            "contact"=>($this->realty->contact_name)?$this->realty->contact_name:"",
            'isNew'=>(int)$this->realty->is_new,
            'subjectId'=>$this->detectSubjectId(),
            'isSave'=>$isSaved,
            'tip'=>(string)$this->realty->tip
            //'category_id'=>$this->realty->category_id
        ];
    }

}
