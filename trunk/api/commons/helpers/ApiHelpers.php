<?php
namespace api\commons\helpers;


use api\components\SmartImage;

use backend\commons\helpers\UtilHelper;
use common\models\entities\RealEstateItem;
use Yii;

class ApiHelpers {

    /**
     *
     * @param string $path
     * @param string $file_name
     * @param string $new_file_name
     * @param string $width
     * @param string $height
     */
    public static function resizeImg($path, $file_name, $new_file_name, $width = false, $height = false) {

        $width = $width && is_numeric ( $width ) ? $width : Yii::$app->params ['mainImg'];

        $height = $height && is_numeric ( $height ) ? $height : Yii::$app->params ['mainImg'];

        $image = new SmartImage( $path . $file_name );

        $image->resize ( $width, $height);

        $image->saveImage ( $path . $new_file_name );
    }

    /**
     * @author sangnguyen
     * @param integer $length
     * @param boolean $character
     */
    public static function randomGen($length,$character = true) {
        $random= "";
        srand((double)microtime()*1000000);

        $char_list  = "1234567890";

        if($character){
            $char_list .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $char_list .= "abcdefghijklmnopqrstuvwxyz";
        }

        for($i = 0; $i < $length; $i++)
        {
            $random .= substr($char_list,(rand()%(strlen($char_list))), 1);
        }
        return $random;
    }

    public static function calculatingAmount($unit,$value,$amount){
        $amountFinal = 0;

        if($unit === '%'){
            $amountFinal = (float) ($amount*$value)/100;
        }elseif($unit == '$'){
            $amountFinal = (int) $value;
        }

        return $amountFinal;
    }

    public static function catchLogDb($e,$msg=null){
        Yii::error('Error \'s name: '.$e->getName(), 'Users');
        Yii::error('Error \'s message: '.$e->getMessage(), 'Users');
        Yii::error('Error '.$msg, 'Users');
    }

    /**
     * @param $formModel
     */
    public static function getFormMessageError($formModel){
        if(\is_array($formModel->attributes)){
            foreach ($formModel->attributes as $key => $value){
                if($formModel->hasErrors($key)){
                    return $formModel->getFirstError($key);
                }
            }
        }
    }

    protected static function checkAssignments($assignmentName) {
        if(isset(Yii::$app->user)) {

            $assignments = Yii::$app->getAuthManager()->getAssignments(Yii::$app->user->id);

            foreach($assignments as $key => $assignment) {
                if(is_array($assignmentName)) {
                    foreach($assignmentName as $name) {
                        if($key == $name) {
                            return true;
                        }
                    }
                } else {
                    if($key == $assignmentName) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    public static function base64_to_jpeg($images=[], $path) {
        $path.='/';
        $outPut = [];
        if(is_array($images) && !empty($images)){
            foreach($images as $base64String){
                $img = str_replace('data:image/png;base64,', '', $base64String);
                $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);
                $fileName =  'attachment_files-'. uniqid() . '.png';
                $success = file_put_contents($path.$fileName, $data);

                self::resizeImg($path, $fileName,
                    Yii::$app->params['normalName']. $fileName, Yii::$app->params['sizeNormalImg'],
                    Yii::$app->params['sizeNormalImg']);

                self::resizeImg($path, $fileName,
                    Yii::$app->params['thumbName']. $fileName,
                    Yii::$app->params['sizeThumbImg'], Yii::$app->params['sizeThumbImg']);

                $outPut[] = $fileName;
            }
        }
        return $outPut;
    }

    public static function getRealtyType($type){
        switch($type){
            case RealEstateItem::REALTY_LOAN:
                return Yii::t('api/app','Bất động sản cho thuê');
            case RealEstateItem::REALTY_SALE:
                return Yii::t('api/app','Bất động sản bán');
            case RealEstateItem::REALTY_SOLD:
                return Yii::t('api/app','Bất động sản đã bán');
        }
    }
    public static function getAllSizeImages($finder,$image){
        if(is_object($image) && !is_string($image)){
            $fileName = $image->file_name;
        }else{
            $fileName = $image;
        }
        return [
            'id'=>(string)(isset($image->id))?$image->id:0,
            'original'=>UtilHelper::builtUrlImages($finder,$fileName),
            'thumb'=>UtilHelper::builtUrlImages($finder,$fileName),
            'normal'=>""
        ];
    }

}
