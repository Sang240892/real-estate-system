<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    // public $basePath = '@webroot';
    // public $siteUrl = '@web';
    public $sourcePath = '@bower/backend/';

    public $css = [
        // <!-- BEGIN GLOBAL MANDATORY STYLES -->
        'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all',
        'theme/assets/global/plugins/font-awesome/css/font-awesome.min.css',
        'theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css',
        'theme/assets/global/plugins/bootstrap/css/bootstrap.min.css',
        'theme/assets/global/plugins/uniform/css/uniform.default.css',
        'theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
        //'theme/assets/global/plugins/select2/css/select2.css',

        // <!-- BEGIN THEME STYLES -->
        'theme/assets/layouts/layout/css/layout.min.css',
        'theme/assets/global/css/components-md.min.css',
        'theme/assets/global/css/plugins-md.min.css',
        'theme/assets/layouts/layout/css/themes/darkblue.min.css',
        'theme/assets/layouts/layout/css/custom.min.css',
        'theme/assets/global/plugins/bootstrap-toastr/toastr.min.css',
        //'theme/assets/global/plugins/jquery-multi-select/css/multi-select.css',

    ];

    public $js = [
        'theme/assets/global/plugins/respond.min.js',
        //'theme/assets/global/plugins/excanvas.min.js',
        'theme/assets/global/plugins/jquery.min.js',
        'theme/assets/global/plugins/jquery-migrate.min.js',
        // IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip
        'theme/assets/global/plugins/jquery-ui/jquery-ui.min.js',
        'theme/assets/global/plugins/bootstrap/js/bootstrap.min.js',
        //'theme/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
        'theme/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        'theme/assets/global/plugins/jquery.blockui.min.js',
        //'theme/assets/global/plugins/jquery.cokie.min.js',
        'theme/assets/global/plugins/uniform/jquery.uniform.min.js',
        //'theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        //'theme/assets/global/plugins/select2/select2.min.js',

        /*'theme/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js',
        'theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js',*/
        //'theme/assets/global/plugins/jquery.pulsate.min.js',
        'theme/assets/global/scripts/app.min.js',
        'theme/assets/layouts/layout/scripts/layout.min.js',
        'theme/assets/layouts/layout/scripts/demo.min.js',
        'theme/assets/layouts/global/scripts/quick-sidebar.min.js',
        'theme/assets/global/plugins/bootstrap-toastr/toastr.min.js',

        //'theme/assets/global/plugins/bootbox/bootbox.min.js',
        //'theme/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js',


        /*'theme/assets/global/plugins/flot/jquery.flot.min.js',
        'theme/assets/global/plugins/flot/jquery.flot.resize.min.js',
        'theme/assets/global/plugins/flot/jquery.flot.pie.min.js',
        'theme/assets/global/plugins/flot/jquery.flot.stack.min.js',
        'theme/assets/global/plugins/flot/jquery.flot.crosshair.min.js',
        'theme/assets/global/plugins/flot/jquery.flot.categories.min.js',*/

        /*'theme/assets/global/plugins/amcharts/amcharts/amcharts.js',
        'theme/assets/global/plugins/amcharts/amcharts/serial.js',
        'theme/assets/global/plugins/amcharts/amcharts/pie.js',
        'theme/assets/global/plugins/amcharts/amcharts/radar.js',
        'theme/assets/global/plugins/amcharts/amcharts/themes/light.js',
        'theme/assets/global/plugins/amcharts/amcharts/themes/patterns.js',
        'theme/assets/global/plugins/amcharts/amcharts/themes/chalk.js',
        'theme/assets/global/plugins/amcharts/ammap/ammap.js',
        'theme/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js',
        'theme/assets/global/plugins/amcharts/amstockcharts/amstock.js',*/

    ];

    public $depends = [
        'yii\web\YiiAsset',
        //         'yii\bootstrap\BootstrapAsset'
    ];
}
