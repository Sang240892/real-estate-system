<?php
return [
    'adminEmail' => 'admin@example.com',

    'bat-dong-san'=>[
        'categoriesQuery' =>[
            'bán-căn-hộ-chung-cư'=>'ban-can-ho-chung-cu',
            'bán-nhà-đất'=>'ban-nha-dat',
            'bán-đất-nền'=>'ban-dat-dat-nen',
            'bán-trang-trại-khu-nghỉ-dưỡng'=>'ban-trang-trai-khu-nghi-duong',
            'bán-kho-nhà-xưởng'=>'ban-kho-nha-xuong',
            'bán-loại-bất-động-sản-khác'=>'ban-loai-bat-dong-san-khac',
            'nhà-riêng'=>'ban-nha-rieng'
        ]
    ],
    'mua-ban'=>[
        'categoriesQuery' =>[
            'bán-nhà-căn-hộ'=>'ban-nha-can-ho-toan-quoc-l0-c32',
            'bán-đất' => 'ban-dat-toan-quoc-l0-c31',
            'sang-nhượng-cửa-hàng'=> 'sang-nhuong-cua-hang-toan-quoc-l0-c33',
            'cho-thuê-nhà-đất'=>'cho-thue-nha-dat-toan-quoc-l0-c34'
        ],
        'detectCate' =>[
            'ban-nha-can-ho-toan-quoc-l0-c32'=>'bán-nhà-căn-hộ',
            'ban-dat-toan-quoc-l0-c31' =>'bán-đất' ,
            'sang-nhuong-cua-hang-toan-quoc-l0-c33'=>'sang-nhượng-cửa-hàng',
            'cho-thue-nha-dat-toan-quoc-l0-c34'=>'cho-thuê-nhà-đất'
        ]
    ]
];
