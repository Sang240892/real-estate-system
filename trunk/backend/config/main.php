<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
use \yii\web\Request;

$baseUrl = str_replace('/backend/web', '', (new Request)->getBaseUrl());

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],

    'modules' => [
        'authorization' => [
            'class' => 'backend\modules\authorization\Module',
        ],
        'auth' => [
            'class' => 'backend\modules\auth\Module',
        ],
        'product' => [
            'class' => 'backend\modules\product\Module',
        ],
        'crawling' => [
            'class' => 'backend\modules\crawling\Module',
        ],
        'user' => [
            'class' => 'backend\modules\user\Module',
        ],
        'category' => [
            'class' => 'backend\modules\category\Module',
        ],
        'payment' => [
            'class' => 'backend\modules\payment\Module',
        ],

//        'debug' => [
//            'class' => 'yii\debug\Module',
//            'allowedIPs' => ['42.118.145.212', '127.0.0.1', '::1']
//        ],
    ],
   // 'sourceLanguage'=>'vn_VN',

    'components' => [
        //'language'=>'vn_VN',
        'request' => [
            'baseUrl' => $baseUrl,
        ],
        'user' => [
            'identityClass' => 'common\models\UserIdentity',
            'enableAutoLogin' => true,
            'loginUrl' => ['auth/authenticate/verify'],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],

                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info','error', 'warning'],
                    'categories' => ['Crawling'],
                    'logFile' => '@api/runtime/logs/Crawling/requests'.date("Ymd").'.log',
                    'maxFileSize' => 1024 * 2,
                    'maxLogFiles' => 20,
                ],

            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/'=>'user/default/index',
//                'debug/<controller>/<action>' => 'debug/<controller>/<action>',
                '<module:[\w\-]+>/<controller:[\w\-]+>/<action:[\w\-]+>' => '<module>/<controller>/<action>',
                'product/crawl/index/<site:[\w\-]+>' => 'product/crawl/index',
                'product/default/clear-realty' => 'product/default/clear-realty',
                'product/default/clear-realty/<date:[\w\-]+>' => 'product/default/clear-realty',

                'crawling/cronjob/clear-realty' => 'crawling/cronjob/clear-realty',
                'crawling/cronjob/clear-realty/<date:[\w\-]+>' => 'crawling/cronjob/clear-realty',

                'crawling/cronjob/filtering' => 'crawling/cronjob/filtering',
                'crawling/cronjob/filtering/<date:[\w\-]+>' => 'crawling/cronjob/filtering',
                'crawling/cronjob/mua-ban-nha-ngo-hem-ha-noi' => 'crawling/cronjob/mua-ban-nha-ngo-hem-ha-noi',
                'crawling/cronjob/mua-ban-nha-ngo-hem-ha-noi/<from:\d+>' => 'crawling/cronjob/mua-ban-nha-ngo-hem-ha-noi',
                'crawling/cronjob/crawling-bds/<from:\d+>' => 'crawling/cronjob/crawling-bds',
                'crawling/cronjob/cho-tot/<from:\d+>' => 'crawling/cronjob/cho-tot',
                'crawling/cronjob/cho-tot-hcm/<from:\d+>' => 'crawling/cronjob/cho-tot-hcm',
                'crawling/cronjob/update-notification/<minute:\d+>' => 'crawling/cronjob/update-notification',
                'user/default/payment-history/<user_id:\d+>'=>'user/default/payment-history',
                 'user/default/create-history/<user_id:\d+>'=>'user/default/create-history',
                'user/default/create-history/<user_id:\d+>/<h_id:\d+>'=>'user/default/create-history',
                //'/auth/reset-password/<token:[\w\-]+>'                    => 'auth/authenticate/reset-password',
                //                'permission/<action:[\w\-]+>/<id:\d+>'           => 'permission/permission/<action>',
            ],
        ],
        'view' => [
            'theme'=>[
                'pathMap' => ['@app/views' => '@webroot/themes/real_estate/views'],
            ],
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'layout'=>'@webroot/themes/real_estate/views/layouts/main.php',
    'params' => $params,
    'aliases' => [
        // Set the editor language dir
        '@uploadPathName' => '/public/upload',
        '@rootPath' => realpath(dirname(__FILE__).'/../../'),
    ],
];
