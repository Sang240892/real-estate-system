<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile(Yii::getAlias('@assetsScript').'/page/login/loginForm.js',[ 'depends' => 'yii\web\YiiAsset']);
$this->registerJsFile(Yii::getAlias('@assetsScript').'/page/global/app.js',[ 'depends' => 'yii\web\YiiAsset']);


$this->registerCss("
.user-login-5 .form-group.has-error{
     border: none  !important;
     border-bottom: none !important;
}
.user-login-5 .form-group.has-error .form-control{
    border-bottom: 2px solid #ed6b75!important;
}
.user-login-5 .form-md-line-input .help-block {
    position: absolute;
    margin:-32px 0 0 14px;
   	color:#ed6b75 !important;
    font-size: 13px;
}
");


$asset		= backend\assets\LoginAsset::register($this);

$asset->css[] = 'theme/assets/pages/css/login-5.min.css';


$script = <<< JS
 Login.init();
 App.init();
JS;
$this->registerJs($script, \yii\web\View::POS_READY, $key = null);

?>
<!-- BEGIN LOGO -->
<!-- BEGIN : LOGIN PAGE 5-1 -->
<div class="user-login-5">
	<div class="row bs-reset">
		<div class="col-md-6 bs-reset">
			<div class="login-bg" style="background-image:url(<?=\Yii::getAlias('@assetsUrlImageLoginPage')?>/bg1.jpg)">
				</div>
		</div>
		<div class="col-md-6 login-container bs-reset">
			<div class="login-content">
				<h1>Admin Login</h1>
				<p>Lorem ipsum dolor sit amet, coectetuer adipiscing elit sed diam nonummy et nibh euismod aliquam erat volutpat. Lorem ipsum dolor sit amet, coectetuer adipiscing. </p>

					<?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['class' => 'login-form']]); ?>
					<div class="alert alert-danger display-hide">
						<button class="close" data-close="alert"></button>
						<span>Enter any email and password. </span>
					</div>
					<div class="row">
						<div class='col-xs-6'>
						<?= $form->field($model, 'email',[
							'inputOptions' => [
								'placeholder' => $model->getAttributeLabel('email'),
								'class'=>'form-control',
								'required'=>''
							],
							'template' => "<div class='form-md-line-input'>{input}\n{hint}\n{error}</div>",
						])->label(FALSE)?>
						</div>
						<div class='col-xs-6'>
						<?= $form->field($model, 'password',[
							'inputOptions' => [
								'placeholder' => $model->getAttributeLabel('password'),
								'class'=>'form-control form-group',
								'required'=>''
							],
							'template' => "<div class='form-md-line-input '>{input}\n{hint}\n{error}</div>",
						])->passwordInput()->label(FALSE)?>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
							<div class="rem-password">
								<?= $form->field($model, 'rememberMe')->checkbox() ?>
							</div>
						</div>
						<div class="col-sm-8 text-right">
							<div class="forgot-password">
								<a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
							</div>
							<?= Html::submitButton('Login', ['class' => 'btn blue', 'name' => 'login-button']) ?>
						</div>
					</div>
					<?php ActiveForm::end(); ?>
				<!-- BEGIN FORGOT PASSWORD FORM -->
					<?php $form = ActiveForm::begin(['action'=>['/auth/authenticate/password-reset-request'],'id' => 'forget-form', 'options' => ['class' => 'forget-form']]); ?>

					<h3 class="font-green">Forgot Password ?</h3>
					<p> Enter your e-mail address below to reset your password. </p>
					<div class="form-group">
						<input class="form-control placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
					<div class="form-actions">
						<button type="button" id="back-btn" class="btn grey btn-default">Back</button>
						<button type="submit" class="btn blue btn-success uppercase pull-right">Submit</button>
					</div>
					<?php ActiveForm::end(); ?>
				<!-- END FORGOT PASSWORD FORM -->
			</div>
			<div class="login-footer">
				<div class="row bs-reset">
					<div class="col-xs-5 bs-reset">
						<ul class="login-social">
							<li>
								<a href="javascript:;">
									<i class="icon-social-facebook"></i>
								</a>
							</li>
							<li>
								<a href="javascript:;">
									<i class="icon-social-twitter"></i>
								</a>
							</li>
							<li>
								<a href="javascript:;">
									<i class="icon-social-dribbble"></i>
								</a>
							</li>
						</ul>
					</div>
					<div class="col-xs-7 bs-reset">
						<div class="login-copyright text-right">
							<p>Copyright <?php echo date("Y") ?> &copy; COMPANY</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

