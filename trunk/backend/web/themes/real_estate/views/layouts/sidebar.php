<?php
// use kartik\sidenav\SideNav;
use yii\widgets\Menu;
use yii\helpers\Url;
?>
<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		 <?php
         $menuItems = [
    [
        'options' => [
            'class' => 'sidebar-toggler-wrapper'
        ],
        'template' => '<!-- BEGIN SIDEBAR TOGGLER BUTTON --><div class="sidebar-toggler"></div><!-- END SIDEBAR TOGGLER BUTTON -->'
    ],
    [
        'options' => [
            'class' => 'sidebar-search-wrapper'
        ],
        'template' => '
            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
            <p></p>
          <!--  <form class="sidebar-search " action="extra_search.html" method="POST">
                <a href="javascript:;" class="remove">
                    <i class="icon-close"></i>
                </a>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
                    </span>
                </div>
            </form>-->
            <!-- END RESPONSIVE QUICK SEARCH FORM -->'
    ],
//    [
//        'label' => 'Dashboard',
//        'url' => 'javascript:;',
//        'template' => '<a href="{url}"><i class="icon-home"></i><span class="selected"></span><span class="title">{label}</span><span class="arrow"></span></a>',
//        'items' => [
//            [
//                'label' => 'Dashboard1',
//                'class'=>'active',
//                'url' => [
//                   '/'
//                ],
//                'template' => '<a href="{url}"><i class="icon-home"></i><span class="title">{label}</span></a>'
//            ],
//            [
//                'label' => 'Dashboard2',
//                'url' => [
//                    '/'
//                ],
//                'template' => '<a href="{url}"><i class="icon-home"></i><span class="title">{label}</span></a>'
//            ],
//            [
//                'label' => 'Manager Dashboard',
//                'url' => [
//                    '/'
//                ],
//                'template' => '<a href="{url}"><i class="icon-home"></i><span class="title">{label}</span></a>'
//            ]
//        ]
//    ],
             Yii::$app->user->can('user')?[
                 'label' => \Yii::t('backend','Users'),
                 'active'=>Yii::$app->controller->module->id == 'user',
                 'url'	=>[Url::to('/user/default/index')],
                // 'template'=> '<a href="{url}"><i class="icon-user"></i><span class="selected"></span><span class="title">{label}</span><span class="arrow"></span></a>',
                 'template' => '<a href="{url}"><i class="icon-user"></i><span class="title">{label}</span></a>',
                 /*'items' => [
                     [
                         'label' =>'<i class="icon-user"></i> '. \Yii::t('backend','Members'),
                         'url' => [Url::to('/user/manage/index')],
                         'template' => '<a href="{url}"><span class="title">{label}</span></a>'
                     ],
                     (Yii::$app->user->can('user-manage-staff') || Yii::$app->user->can('user'))?[
                         'label' =>'<i class="icon-user"></i> '. \Yii::t('backend','Employees'),
                         'url' => [Url::to('/user/manage/staff')],
                         'template' => '<a href="{url}"><span class="title">{label}</span></a>'
                     ]:FALSE
                 ]*/
             ]:false,
             Yii::$app->user->can('product')?[
                 'label' => \Yii::t('backend','Product List'),
                 'active'=>(Yii::$app->controller->id == 'default' && Yii::$app->controller->module->id == 'product' && ( Yii::$app->controller->action->id == 'create' ||  Yii::$app->controller->action->id == 'update' || Yii::$app->controller->action->id == 'view' || Yii::$app->controller->action->id == 'index')),
                 'url'	=> [Url::to('/product/default/index')],
                 'template'=> '<a href="{url}" class="nav-link nav-toggle"><i class="fa fa-table"></i><span class="title">{label}</span></a>',
                 //'template' => '<a href="{url}"><i class="fa fa-list"></i><span class="title">{label}</span></a>'
             ]:false,
             Yii::$app->user->can('category')?[
                 'label' => \Yii::t('backend','Category'),
                 // 'active'=>Yii::$app->controller->module->id == 'product',
                 'url'	=>'javascript:;',
                 'template'=> '<a href="{url}" class="nav-link nav-toggle"><i class="fa fa-folder-open"></i><span class="selected"></span><span class="title">{label}</span><span class="arrow"></span></a>',
                 //'template' => '<a href="{url}"><i class="fa fa-list"></i><span class="title">{label}</span></a>'
                 'items' => [
                     [
                         'label' =>'<i class="fa fa-folder"></i> '. \Yii::t('backend','Doanh mục'),
                         'url' => [Url::to('/category/default/index')],
                         'active'=>(Yii::$app->controller->id == 'default' && Yii::$app->controller->module->id == 'category' && ( Yii::$app->controller->action->id == 'create' ||  Yii::$app->controller->action->id == 'update' || Yii::$app->controller->action->id == 'view' || Yii::$app->controller->action->id == 'index')),
                         'template' => '<a href="{url}"><span class="title">{label}</span></a>'
                     ],
                     [
                         'label' =>'<i class="fa fa-compress"></i> '. \Yii::t('backend','Merge Category'),
                         'url' => [Url::to('/category/default/merge-category')],
                         'active'=>(Yii::$app->controller->id == 'default' && Yii::$app->controller->module->id == 'category' && Yii::$app->controller->action->id == 'merge-category'),
                         'template' => '<a href="{url}"><span class="title">{label}</span></a>'
                     ],
                 ]
             ]:false,
             Yii::$app->user->can('crawling')?[
                 'label' => \Yii::t('backend','Crawling'),
                  'active'=>Yii::$app->controller->module->id == 'crawling',
                 'url'	=>['/crawling/default/index'],
                 //'template'=> '<a href="{url}" class="nav-link nav-toggle"><i class="fa fa-list"></i><span class="selected"></span><span class="title">{label}</span></a>',
                 'template' => '<a href="{url}"><i class="fa fa-cloud-download"></i><span class="title">{label}</span></a>'

             ]:false,
             Yii::$app->user->can('authorization')?[
                 'label' => 'Quản lý phân quyền',
                 'url'	=> 'javascript:;',
                // 'active'=> Yii::$app->controller->module->id == 'authorization',
                 'template'=> '<a href="{url}" class="nav-link nav-toggle"><i class="fa fa-unlock-alt"></i><span class="selected"></span><span class="title">{label}</span><span class="arrow"></span></a>',
                 'items' => [
                     [
                         'label' =>'<i class="fa fa-unlock-alt"></i>'. \Yii::t('backend','Danh sách quyền'),
                         'url' => [Url::to('/authorization/permission/index')],
                         'active'=>(Yii::$app->controller->id == 'permission' && Yii::$app->controller->module->id == 'authorization'),
                     ]
                 ]
             ]:false
//             [
//                 'label' => \Yii::t('backend','Crawl'),
//                 //'active'=> Yii::$app->controller->action->id == 'crawl',
//                 'url'	=>'javascript:;',
//                 'template'=> '<a href="{url}" class="nav-link nav-toggle"><i class="fa fa-list"></i><span class="selected"></span><span class="title">{label}</span><span class="arrow"></span></a>',
//                 //'template' => '<a href="{url}"><i class="fa fa-list"></i><span class="title">{label}</span></a>'
//                 'items' => [
//                     [
//                         'label' =>'<i class="fa fa-table"></i> '. \Yii::t('backend','Batdongsan.com.vn'),
//                         'url' => ['/product/crawl/index/','site'=>'bat-dong-san'],
//                         'active'=> (isset(Yii::$app->controller->actionParams['site']) && Yii::$app->controller->actionParams['site']  == 'bat-dong-san'),
//                         'template' => '<a href="{url}"><span class="title">{label}</span></a>'
//                     ],
//                     [
//                         'label' =>'<i class="fa fa-table"></i> '. \Yii::t('backend','muaban.net'),
//                         'url' => ['/product/crawl/index/','site'=>'mua-ban'],
//                         'active'=> (isset(Yii::$app->controller->actionParams['site']) && Yii::$app->controller->actionParams['site']  == 'mua-ban'),
//                         'template' => '<a href="{url}"><span class="title">{label}</span></a>'
//                     ],
//                 ]
//             ],

];
echo Menu::widget([
    'options' => [
        'class' => 'page-sidebar-menu page-header-fixed page-sidebar-menu-closed',
        'data-keep-expanded' => FALSE,
        'data-auto-scroll' => TRUE,
        'data-slide-speed' => '200',
        'style'=>'padding-top: 20px'
    ],
    'items' => $menuItems,
    'submenuTemplate' => "\n<ul class='sub-menu'>\n{items}\n</ul>\n",
    'encodeLabels' => FALSE, // allows you to use html in labels
    'activateParents' => TRUE,
    'activeCssClass' => 'active',
    'itemOptions'=>[
        'class'=>'nav-item'
    ]
]);
?>
    
	</div>
</div>
