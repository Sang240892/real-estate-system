<?php
use yii\helpers\Html;
?>
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="#">
				
				<!--<img src="<?php /*echo Yii::$app->params['urlAssetsImagesUploaded'] . "icon_cms.png"; */?>" style="width:110px;padding-top:10px"alt="logo" class="logo-default">-->
			</a>
			<div class="menu-toggler sidebar-toggler hide">
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<li class="dropdown dropdown-dark dropdown-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<img alt="" class="img-circle" src="<?=Yii::getAlias('@assetsUrl')?>/images/default-user-avatar.png">
						<span class="username username-hide-on-mobile"><?php echo (yii::$app->user->identity)?yii::$app->user->identity->email:'anonymous'?> </span>
						<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default">
						<li>
							<?php echo Html::a('<i class="icon-user"></i> My Profile', ['/user/manage/profile','id'=>Yii::$app->user->id], ['data-method'=>'post']) ?>
						</li>
						<li>
							<?php echo Html::a('<i class="icon-logout"></i> Logout', ['/auth/authenticate/logout'], ['data-method'=>'post']) ?>
						</li>
					</ul>
				</li>
				<!-- END USER LOGIN DROPDOWN -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>
