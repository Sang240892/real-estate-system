<?php
/**
 * User: sangnguyen on  10/20/15 at 00:34
 * File name: extra_profile.php
 * Project name: ysd-tee-shirt
 * Copyright (c) 2015 by AppsCyclone
 * All rights reserved
 */

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use \yii\web\Request;
use yii\helpers\Url;
// use yii\bootstrap\Nav;
// use yii\bootstrap\NavBar;
// use yii\widgets\Breadcrumbs;
// use common\widgets\Alert;

// AppAsset::register($this);
$asset		= backend\assets\AppAsset::register($this);
$baseUrl 	= $asset->baseUrl;
$dashboard  = (new Request)->getBaseUrl();
$jsScripts = <<<JS
    Metronic.init(); // init metronic core componets
    Layout.init(); // init layout
    QuickSidebar.init(); // init quick sidebar
JS;
$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);
$userId = Yii::$app->request->get('id');
$userRequest = null;
if(isset($userId)){
    $userRequest = \backend\commons\models\entities\FitRoadUser::findOne($userId);
    if(!$userRequest || is_null($userRequest))
        throw new \yii\web\NotFoundHttpException(\Yii::t('backend/error','The requested page does not exist'));
}else{
    throw new \yii\web\NotFoundHttpException(\Yii::t('backend/error','The requested page does not exist'));
}
$avatar = $userRequest->getFitRoadGalleryImages()->where([
    'status'=>1,
    'is_current'=>1
])->one();
$this->registerCssFile(Yii::$app->homeUrl.'css/site.css',[ 'depends' => 'yii\web\YiiAsset']);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script>
        var BASE_URL = '<?php echo Yii::$app->getUrlManager()->getHostInfo();?>';
    </script>
</head>
<body class="page-header-fixed page-quick-sidebar-over-content">
<?php $this->beginBody() ?>
<!-- BEGIN HEADER -->
<?= $this->render('header.php',['baseUrl'=>$baseUrl, 'dashboard' => $dashboard]);?>
<!-- END HEADER -->
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?= $this->render('sidebar.php',['baseUrl'=>$baseUrl]);?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <?= $this->render('extra_profile.php',['avatar'=>$avatar,
        'userRequest'=>$userRequest,'baseUrl'=>$baseUrl, 'dashboard' => $dashboard,'content'=>$content, 'asset' => $asset]);?>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?= $this->render('footer.php',['baseUrl'=>$baseUrl]);?>
<!-- END FOOTER -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
