<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use \yii\web\Request;
use yii\helpers\Url;
// use yii\bootstrap\Nav;
// use yii\bootstrap\NavBar;
// use yii\widgets\Breadcrumbs;
// use common\widgets\Alert;

// AppAsset::register($this);
$asset		= backend\assets\AppAsset::register($this);
$baseUrl 	= $asset->baseUrl;
$dashboard  = (new Request)->getBaseUrl();
$jsScripts = <<<JS
    //Metronic.init(); // init metronic core componets
    //Layout.init(); // init layout
    //QuickSidebar.init(); // init quick sidebar
JS;

$this->registerCssFile(Yii::$app->homeUrl.'css/site.css',[ 'depends' => 'yii\web\YiiAsset']);
$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);
?>
<?php $this->beginPage();?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script>
		var BASE_URL = '<?php echo Yii::$app->getUrlManager()->getHostInfo();?>';
    </script>
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md page-sidebar-closed">
<?php $this->beginBody() ?>
<!-- BEGIN HEADER -->
<?= $this->render('header.php',['baseUrl'=>$baseUrl, 'dashboard' => $dashboard]);?>
<!-- END HEADER -->
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
	<?= $this->render('sidebar.php',['baseUrl'=>$baseUrl]);?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<?= $this->render('content.php',['baseUrl'=>$baseUrl, 'dashboard' => $dashboard,'content'=>$content, 'asset' => $asset]);?>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?= $this->render('footer.php',['baseUrl'=>$baseUrl]);?>
<!-- END FOOTER -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
