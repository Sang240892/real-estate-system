/************************************************
 * User: sangnguyen on  6/29/16 at 21:35         *
 * File name:                       *
 * Project name:            *
 * Copyright (c) 2016 by                  *
 * All rights reserved                          *
 ************************************************
 */

var Crawl = function(domain){
    //this.site = site;
    //this.category = category
    this.domain = domain;
    this.tableData = false;

};

Crawl.prototype = {
    constructor : Crawl,

    handleSubmitFormData: function (){
        jQuery('#detail_item_model').on('submit','#form_data_crawling',function(e){
            e.preventDefault();
            var form = this;
            Helper.blockUI({
                target:'#item_detail',
                box:true
            });

            jQuery.ajax({
                url: jQuery(form).attr('action'), // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: new FormData(form), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(response)   // A function to be called if request succeeds
                {
                    Helper.unblockUI('#item_detail');
                    alert('Luư thông tin bất động sản thành công');
                },
                error: function(response){

                }
            });
        })
    },
    handleCrawlData : function (site){
        var self = this;
        $('#btn-crawling').on('click',function(){

            var pageReq = jQuery('#crawling_paging').val();
            var paging = [];
            var pagingQueryString;
            var pageNumber = 1;

            if( pageReq == null ){
                 if(site == 'mua-ban'){
                    pagingQueryString = '?cp=';
                    paging.push(pagingQueryString+pageNumber);
                }else{
                     pagingQueryString = '/p';
                     paging.push(pagingQueryString+pageNumber);
                 }
            }else{
                paging = pageReq;
            }

            Helper.blockUI({
                target: '#crawling_data',
                box:true
            });
            jQuery.ajax({
                url: self.domain+'/product/crawl/crawling',
                type: 'post',
                dataType : 'json',
                accepts: "application/json",
                data: {baseUrl:site,category:$('#urlCrawl').val(),_csrf:yii.getCsrfToken(),page:paging},
                success: function (response) {
                    if(response.status){

                        Helper.unblockUI('#crawling_data');

                        //return false;
                        self.handleDataTable(response.data,response.category);
                        self.handleBuildCrawlPaging(response.totalPage,pagingQueryString,response.selectedPage);
                    }else{
                        alert('Quá trình cào dữ liệu bị lỗi xin vui lòng thử lại!');
                        Helper.unblockUI('#crawling_data');
                    }

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if(textStatus==="timeout") {
                        alert('Quá trình cào dữ liệu quá lâu nên đã tạm dừng!');
                        Helper.unblockUI('#crawling_data');
                    }
                },
                timeout: 120000
            });
        });
    },
    handleDataTable: function(dataSet,category){
        if(this.tableData){
            //this.tableData.clear();
            this.tableData.clear().draw();
            this.tableData.destroy();

        }
        this.tableData = $('#data_crawled').DataTable( {
            data: dataSet,
            fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            },
            columns: [
                {
                    title:"Hình ảnh",
                    data:function(data){
                    return "<img src='"+data[0]+"' style='width: 100px'>";
                }},
                { title: "Tiêu đề" },
                { title: "Mô tả" },
                { title: "Giá" },
                { title: "Diện tích" },
                { title: "Nguồn" },
                { title: "Địa chỉ" },
                { title: "Ngày đăng" },
                { title: "Chi tiết" ,class:"hidden"},
                { title: "Loại" },
                { title: "Action" },
            ],
            columnDefs: [
                {
                "targets": -2,
                "data": null,
                "defaultContent": category
                },
                {
                    "targets": -1,
                    "data": function (data) {

                        if(data[5] == 'https://muaban.net'){
                            var url = data[8];
                        }else{
                            var url = data[5]+'/'+data[8];
                        }
                        if(data[9]){
                            return "<span class='btn_action' data-category='"+category+"'  data-source='"+data[5]+"' data-price='"+data[3]+"' data-area='"+data[4]+"'  data-url='"+url+"' data-created='"+data[7]+"' data-img='"+data[0]+"' data-summary='"+data[2]+"' data-address='"+data[6]+"'>" +
                                "</a>&nbsp;<a class='btn_get_detail_crawling_item' title='chi tiết' class='' data-toggle='modal' data-target='#detail_item_model'>" +
                                "<i class='fa fa-download' aria-hidden='true'></i></a>" +
                                "</span>";
                        }else{
                            return '';
                        }

                    }
                }
            ]

        } );
    },
    handleBuildCrawlPaging : function(totalPage,pagingLink,selectedPage){
        var pages = [];
        if(totalPage > 2000){
            totalPage= 2000;
        }
        for(var i= 1; i <= totalPage; i++){
            var page={};

            page.id = pagingLink + i ;
            page.text = 'Trang ' + i;

            pages.push(page);
        }
        this.handleInitDataCrawlPaging(pages,selectedPage);

    },

    handleInitDataCrawlPaging : function(pages,selectedPage){
        jQuery('#box_crawling_paging').attr('style','display:block');
        $("#crawling_paging").select2({
            maximumSelectionLength: 10,
            placeholder: "Chọn những trang muốn lấy dữ liệu",
            allowClear: true,
            initSelection: function (element, callback) {
                callback(selectedPage);
            },
            data: pages
        });
    },
     handleEditor : function () {
        $('#summernote_1').summernote({height: 300});
         if (!jQuery().wysihtml5) {
             return;
         }

         if ($('.wysihtml5').size() > 0) {
             $('.wysihtml5').wysihtml5({
                 "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
             });
         }
    },
    handleGetDetailItem : function (site){
        var self = this;
        jQuery('#data_crawled').on('click','.btn_get_detail_crawling_item',function(){
            jQuery('#item_detail').html('');
            var parent,
                detail_url,
                _img_instead,
                _summary,
                _created,
                _area,
                _price,
                _source,
                _category,
                _address;
            parent = jQuery(this).closest('.btn_action');
            parent = jQuery(parent);

            detail_url = parent.data('url');
            _img_instead = parent.data('img');
            _summary = parent.data('summary');
            _created = parent.data('created');
            _address = parent.data('address');
            _area = parent.data('area');
            _price =  parent.data('price');
            _source = parent.data('source');
            _category = parent.data('category');
            Helper.blockUI({
                target: '#item_detail',
                box:true
            });
            jQuery.ajax({
                url: self.domain+'/product/crawl/get-detail-item',
                type: 'post',
                dataType : 'json',
                accepts: "application/json",
                data: {baseUrl:site,url:detail_url,_csrf:yii.getCsrfToken()},
                success: function (response) {
                    if(response.status){
                        Helper.unblockUI('#item_detail');
                        response.data.img = _img_instead;
                        response.data.created = _created;
                        response.data.summary = _summary;
                        response.data.address = _address;
                        response.data.area   = _area;
                        response.data.price = _price;
                        response.data.source = _source;
                        response.data.category = _category;
                        response.data.detail_url = detail_url;

                        jQuery('#data_crawling').val(JSON.stringify(response.data));
                        var examplesHTML = Mustache.to_html($('#crawling_item_detail').html(), response.data);
                        jQuery('#item_detail').html(examplesHTML);
                    }else{
                        alert('Quá trình cào dữ liệu bị lỗi xin vui lòng thử lại!');
                        Helper.unblockUI('#item_detail');
                    }

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if(textStatus==="timeout") {
                        alert('Quá trình cào dữ liệu quá lâu nên đã tạm dừng!');
                        Helper.unblockUI('#crawling_data');
                    }
                },
                timeout: 120000
            });
        });
    }

}
