/**
 * Created by sangnguyen on 10/13/15.
 */
var Authorization = {
    //ajaxAssignMentPermission : BASE_URL + '/fitroad-cms/trunk/authorization/permission/ajax-assign-permission',
    ajaxAssignMentPermission : BASE_URL + '/authorization/permission/ajax-assign-permission',

    handelAssignmentPermission : function(){
        var self = this;
        jQuery('#w1 input[type="checkbox"]').on('click',function(el) {
            var current = el.toElement,
                currentId = $(current).attr('id'),
                dataKey = $(current).data('key');

            if(typeof dataKey == 'undefined'){
                alert('Module is not exist,can you check out agian!pls');
                $(current).closest('span').removeClass('checked');
                $(current).prop("checked", false);
                return;
            }
                var key = dataKey.split("-"),
                    permissionsChecked = [],
                    permissionUnChecked = [];
            /**
             * Checking, checkbox is clicking is a module and checked = true?
             */
            if (key[0] == 'm' && $(current).prop('checked') == true) {
                permissionsChecked.push($(current));
                jQuery('#w1 input[type="checkbox"]').each(function (i, el) {
                    var child = el;
                        if ($(current).attr('name') == $(child).attr('name')) {
                            if ($(child).data('key') == 'c-' + key[1] && $(child).prop('checked') == false) {
                                if (!$(child).closest('span').hasClass('checked')) {
                                    $(child).closest('span').addClass('checked');
                                    $(child).prop("checked", true);
                                    permissionsChecked.push($(child));
                                }
                            }
                        }
                })
            }else if(key[0] == 'm' && $(current).prop('checked') == false){
                permissionUnChecked.push($(current));
            }else if(key[0] == 'c'){
                if($(current).prop('checked') == true){
                    permissionsChecked.push($(current));
                }else if($(current).prop('checked') == false){
                    permissionUnChecked.push($(current));
                }
                if ($('#'+currentId+key[1]).closest('span').hasClass('checked')) {
                    $('#'+currentId+key[1]).closest('span').removeClass('checked');
                    $('#'+currentId+key[1]).prop("checked", false);
                    permissionUnChecked.push($('#'+currentId+key[1]));
                }
            }
            //console.log(permissionsChecked);
            //console.log(permissionUnChecked);
            self.handelAjaxAssignPermission(permissionsChecked,permissionUnChecked);
        });

    },
    handelAjaxAssignPermission:function(checks,unChecks){
        var dataUnCheck = [],
            dataCheck = [];
        for(var i=0;i<checks.length;i++){
            var item = {'id':$(checks[i]).attr('name').replace('[]',''),'value':$(checks[i]).closest('tr').data('key')};
            dataCheck.push(item);
        }
        for(var i=0;i<unChecks.length;i++){
            var itemUn = {'id':$(unChecks[i]).attr('name').replace('[]',''),'value':$(unChecks[i]).closest('tr').data('key')};
            dataUnCheck.push(itemUn);
        }
        console.log(JSON.stringify(dataUnCheck));
        console.log(JSON.stringify(dataCheck));

        jQuery.ajax({
            url: Authorization.ajaxAssignMentPermission,
            type: 'post',
            dataType : 'json',
            accepts: "application/json",
            data: {uc:JSON.stringify(dataUnCheck),c:JSON.stringify(dataCheck),_csrf:yii.getCsrfToken()},
            success: function (response) {

            }
        });
    }


};