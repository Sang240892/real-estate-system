/************************************************
 * User: sangnguyen on  7/14/16 at 09:12         *
 * File name:                       *
 * Project name:                *
 * Copyright (c) 2016 by                  *
 * All rights reserved                          *
 ************************************************
 */
RealItem = function (){

};
RealItem.prototype = {
  Constructor : RealItem,
    handleExportExcel :function (){
        var self = this;
        // $('#btn_export_excel').on('click',function(e){
        //     var $this = this;

        //     // var date = $('#defaultrange input').val();
        //     // if(date == '' || date.length == 0){
        //     //      alert("Xin hãy chọn ngày trước.");
        //     //     return false;
        //     // }
        //      // jQuery('#rangeDate').val(date);

        //     Helper.blockUI({
        //         target: '#girdView',
        //         box:true
        //     });

        //     window.location.reload();

            
        //    // jQuery('#export-form').submit();

        //    // jQuery.ajax({
        //    //      url: BASE_URL + $($this).data('url'),
        //    //      type: 'post',
        //    //      dataType : 'json',
        //    //      accepts: "application/json",
        //    //      data: {_csrf:yii.getCsrfToken()},
        //    //      success: function (response) {
                   
        //    //          Helper.unblockUI('#girdView');

        //    //      },
        //    //      error: function(jqXHR, textStatus, errorThrown) {
                    
        //    //          window.open(BASE_URL + $($this).data('url'),'_blank' );
                  
        //    //      },
        //    //      timeout: 120000
        //    //  });


           
            
        // })
    },
    handleFilterRangDate: function(){
		 $('#filterRangeDate').daterangepicker({
                "locale": {
                    "format": "MM/DD/YYYY",
                    "separator": " - ",
                    "applyLabel": "Chọn",
                    "cancelLabel": "Hủy",
                    "fromLabel": "Từ",
                    "toLabel": "Đến",
                    "customRangeLabel": "Tùy chỉnh",
                    "weekLabel": "W",
                    "daysOfWeek": [
                        "Su",
                        "Mo",
                        "Tu",
                        "We",
                        "Th",
                        "Fr",
                        "Sa"
                    ],
                    "monthNames": [
                        "January",
                        "February",
                        "March",
                        "April",
                        "May",
                        "June",
                        "July",
                        "August",
                        "September",
                        "October",
                        "November",
                        "December"
                    ],
                    "firstDay": 1
                },
              
                opens: (App.isRTL() ? 'left' : 'right'),
                format: 'YYYY-MM-DD',
                separator: ' to ',
                startDate: moment().subtract('days', 1),
                endDate: moment(),
                ranges: {
                    'Hôm nay': [moment(), moment()],
                    'Hôm qua': [moment().subtract('days', 1), moment().subtract('days', 1)],
                    '7 ngày trước': [moment().subtract('days', 6), moment()],
                    '30 ngày trước': [moment().subtract('days', 29), moment()],
                    'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                    'Tháng trước': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                },
                minDate: '01/01/2012',
                maxDate: '12/31/2018'
            },
            function (start, end) {
                $('#filterRangeDateInput').val(start.format('YYYY-MM-DD') + ',' + end.format('YYYY-MM-DD'));
            }
        );
		 $('#filterRangeDate').on('apply.daterangepicker', function(ev, picker) {
			
		 	$('#filteringForm').submit();

		});

    },
    handleDateRange: function (){
        //$.fn.datepicker.defaults.language = 'vi';
        $('#defaultrange').daterangepicker({
                "locale": {
                    "format": "MM/DD/YYYY",
                    "separator": " - ",
                    "applyLabel": "Chọn",
                    "cancelLabel": "Hủy",
                    "fromLabel": "Từ",
                    "toLabel": "Đến",
                    "customRangeLabel": "Tùy chỉnh",
                    "weekLabel": "W",
                    "daysOfWeek": [
                        "Su",
                        "Mo",
                        "Tu",
                        "We",
                        "Th",
                        "Fr",
                        "Sa"
                    ],
                    "monthNames": [
                        "January",
                        "February",
                        "March",
                        "April",
                        "May",
                        "June",
                        "July",
                        "August",
                        "September",
                        "October",
                        "November",
                        "December"
                    ],
                    "firstDay": 1
                },
                opens: (App.isRTL() ? 'left' : 'right'),
                format: 'YYYY-MM-DD',
                separator: ' to ',
                startDate: moment().subtract('days', 1),
                endDate: moment(),
                ranges: {
                    'Hôm nay': [moment(), moment()],
                    'Hôm qua': [moment().subtract('days', 1), moment().subtract('days', 1)],
                    '7 ngày trước': [moment().subtract('days', 6), moment()],
                    '30 ngày trước': [moment().subtract('days', 29), moment()],
                    'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                    'Tháng trước': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                },
                minDate: '01/01/2012',
                maxDate: '12/31/2018',
            },
            function (start, end) {
                $('#defaultrange input').val(start.format('YYYY-MM-DD') + ',' + end.format('YYYY-MM-DD'));
            }
        );
    },
    handleBootstrapSwitch: function (){
        $('.real_estate_item_switch').on('switchChange.bootstrapSwitch', function(event, state) {
            //console.log(this); // DOM element
            //console.log(event); // jQuery event
            Helper.blockUI({
                target: '#girdView',
                box:true
            });

            jQuery.ajax({
                url: BASE_URL+'/product/default/switch-self',
                type: 'post',
                dataType : 'json',
                accepts: "application/json",
                data: {id:jQuery(this).attr('id'),self:state,_csrf:yii.getCsrfToken()},
                success: function (response) {
                    if(response.status){
                        Helper.unblockUI('#girdView');
                    }else{
                        Helper.unblockUI('#girdView');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    Helper.unblockUI('#girdView');
                },
                timeout: 120000
            });
        });
    },
    handleBootstrapSwitchCheckOut: function (){
    	$('.real_estate_item_switch_check_out').on('switchChange.bootstrapSwitch', function(event, state) {
            //console.log(this); // DOM element
            //console.log(event); // jQuery event
            var tr  = $(this).closest('tr');
            var number  = $('#numberOfNotSureItem').text();
            Helper.blockUI({
                target: '#girdView',
                box:true
            });

            jQuery.ajax({
                url: BASE_URL+'/product/default/switch-check-out',
                type: 'post',
                dataType : 'json',
                accepts: "application/json",
                data: {id:jQuery(this).data('id'),self:state,_csrf:yii.getCsrfToken()},
                success: function (response) {
                    if(response.status){
                        Helper.unblockUI('#girdView');
                        $(tr).remove();

                       	var newNumber = parseInt(number) - 1;

                        $('#numberOfNotSureItem').text(newNumber);
                        
                    }else{
                        Helper.unblockUI('#girdView');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    Helper.unblockUI('#girdView');
                },
                timeout: 120000
            });
        });	
    },
    handleShowCityList: function(cityAddress,address){
    	var self = this;
    		
    	var list = Config.provinces_list;
    	
		var html = '';

			html += '<select id="filter_city" class="bs-select form-control">';

    	for(var i in list){

    		if(cityAddress == list[i].name){

    			html += '<option selected value="'+i+'">'+list[i].name+'</option>';

    			var ctiySelected = i;

    		}else{

				html += '<option value="'+i+'">'+list[i].name+'</option>';

    		}
    		
    	}

    	html += '</select>';

    	if(cityAddress != ''){
    		console.log('city '+cityAddress);

    		self.handleBuildDropListProvince(ctiySelected,address)
    		
    	}

    	$('#dropCityList').html(html);

  
    },
    handleShowProvincesList:function(){
    	var self = this;

    	jQuery('#product_filter').on('change','#filter_city',function(){
    		
    		var city = $(this).val();

    		self.handleBuildDropListProvince(city,'');

    		this.form.submit();

    	});

    },
     handleShowCategory2ToMerge:function(list){
    	var self = this;

    	var list = JSON.parse(list);

    	jQuery('#category1').on('change',function(){
    		var cate = $(this).val();
    	
    		var html = '';

			html += '<select name="category2" id="category2" class="form-control">';

	    	for(var i in list){

	    		if(cate != list[i].id){

	    			html += '<option value="'+list[i].id+'">'+list[i].name+'</option>';

	    		}

    		}

    		html += '</select>';
    		
    	
    		$('#dropDownCategory2List').html(html);

    	});

    	

    },
    handleBuildDropListProvince:function(city,address){
    		console.log(address);
			var list = Config.provinces_list[city].districts;

	    	var html = '';

				html += '<select name="SearchRealEstateItem[address]" onchange="this.form.submit()" id="filter_provinces" class="bs-select form-control">';

	    	for(var i in list){
	    		var value = Config.provinces_list[city].name+', '+list[i];

	    		if(address == value){

    					html += '<option selected value="'+value+'">'+list[i]+'</option>';

		    		}else{

						html += '<option value="'+value+'">'+list[i]+'</option>';

		    		}		
	    		
	    	}

	    	html += '</select>';

	    	$('#dropProvincesList').html(html);


    }

};






























