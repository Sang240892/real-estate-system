<?php
/**
 * User: sangnguyen
 * Date: 10/11/15
 * Time: 14:46
 * File name: UtilHelper.php
 * Project name: ysd-tee-shirt
 */

namespace backend\commons\helpers;


use common\components\helpers\SmartImage;
use common\models\entities\RealEstateCategoryRelation;
use common\models\entities\RealEstateItem;
use common\models\entities\RealEstateItemPostDate;
use common\models\entities\SiteCrawlingHref;
use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\log\Logger;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use common\forms\UploadAttachmentFileForm;

class UtilHelper{

    public static function street_lib_ha_noi(){
    return $streets = [
"An Xá","Bà Huyện Thanh Quan","Bắc Sơn","Bưởi","Cao Bá Quát","Cầu Giấy","Châu Long","Chu Văn An","Chùa Một Cột","Cửa Bắc","Đặng Dung","Đặng Tất","Đào Tấn","Điện Biên Phủ","Độc Lập","Đốc Ngữ","Đội Cấn","Đội Nhân","Giang Văn Minh","Giảng Võ","Hàng Bún","Hàng Đậu","Hàng Than","Hoàng Diệu","Hoàng Hoa Thám","Hoàng Văn Thụ","Hòe Nhai","Hồng Hà","Hồng Phúc","Hùng Vương","Huỳnh Thúc Kháng","Khúc Hạo","Kim Mã","Kim Mã Thượng","La Thành","Lạc Chính","Láng Hạ","Lê Duẩn","Lê Hồng Phong","Lê Trực","Liễu Giai","Linh Lang","Lý Nam Đế","Mạc Đĩnh Chi","Mai Anh Tuấn","Nam Cao","Nam Tràng","Nghĩa Dũng","Ngọc Hà","Ngọc Khánh","Ngũ Xã","Nguyễn Biểu","Nguyễn Cảnh Chân","Nguyễn Chí Thanh","Nguyễn Công Hoan","Nguyên Hồng","Nguyễn Khắc Hiếu","Nguyễn Khắc Nhu","Nguyễn Phạm Tuân","Nguyễn Thái Học","Nguyễn Thiếp","Nguyễn Tri Phương","Nguyễn Trung Trực","Nguyễn Trường Tộ","Nguyễn Văn Ngọc","Núi Trúc","Ông Ích Khiêm","Phạm Hồng Thái","Phạm Huy Thông","Phan Đình Phùng","Phan Huy Ích","Phan Kế Bính","Phó Đức Chính","Phúc Xá","Quần Ngựa","Quán Thánh","Sơn Tây","Tân Ấp","Thanh Bảo","Thành Công","Thanh Niên","Tôn Thất Đàm","Tôn Thất Thiệp","Trần Huy Liệu","Trần Phú","Trần Tế Xương","Trấn Vũ","Trúc Bạch","Vạn Bảo","Văn Cao","Vạn Phúc","Vĩnh Phúc","Yên Ninh","Yên Phụ","Yên Thế","Nàng Ngòi","An Dương","An Dương Vương","Âu Cơ","Bưởi","Đặng Thai Mai","Hoàng Hoa Thám","Hồng Hà","Hùng Vương","Lạc Long Quân","Mai Xuân Thưởng","Nghi Tàm","Nguyễn Đình Thi","Nguyễn Hoàng Tôn","Nhật Chiêu","Phan Đình Phùng","Phú Gia","Phú Thượng","Phú Xá","Phúc Hoa","Quảng An","Quảng Bá","Quảng Khánh","Tam Đa","Tây Hồ","Thanh Niên","Thượng Thụy","Thụy Khuê","Tô Ngọc Vân","Trích Sài","Trịnh Công Sơn","Từ Hoa","Vệ Hồ","Võ Chí Công","Võng Thị","Xuân Diệu","Xuân La","Yên Hoa","Yên Phụ","Yên Phụ ","Ấu Triệu","Bà Triệu","Bạch Đằng","Báo Khánh","Bảo Linh","Bát Đàn","Bát Sứ","Cao Thắng","Cầu Đất","Cầu Đông","Cầu Gỗ","Chả Cá","Chân Cầm","Chợ Gạo","Chương Dương Độ","Cổ Tân","Cổng Đục","Cửa Đông","Cửa Nam","Dã Tượng","Đặng Thái Thân","Đào Duy Từ","Điện Biên Phủ","Đinh Công Tráng","Đinh Lễ","Đinh Liệt","Đình Ngang","Đinh Tiên Hoàng","Đoàn Nhữ Hài","Đông Thái","Đồng Xuân","Đường Thành","Gầm Cầu","Gia Ngư","Hà Trung","Hai Bà Trưng","Hàm Long","Hàm Tử Quan","Hàn Thuyên","Hàng Bạc","Hàng Bài","Hàng Bè","Hàng Bồ","Hàng Bông","Hàng Buồm","Hàng Bút","Hàng Cá","Hàng Cân","Hàng Chai","Hàng Chiếu","Hàng Chĩnh","Hàng Cót","Hàng Da","Hàng Đào","Hàng Đậu","Hàng Điếu","Hàng Đồng","Hàng Đường","Hàng Gà","Hàng Gai","Hàng Giầy","Hàng Giấy","Hàng Hòm","Hàng Khay","Hàng Khoai","Hàng Lược","Hàng Mã","Hàng Mắm","Hàng Mành","Hàng Muối","Hàng Ngang","Hàng Nón","Hàng Quạt","Hàng Rươi","Hàng Thiếc","Hàng Thùng","Hàng Tre","Hàng Trống","Hàng Vải","Hàng Vôi","Hồ Hoàn Kiếm","Hỏa Lò","Hồng Hà","Huế","Lãn Ông","Lê Duẩn","Lê Lai","Lê Phụng Hiểu","Lê Thạch","Lê Thái Tổ","Lê Thánh Tông","Lê Văn Hưu","Lê Văn Linh","Liên Trì","Lò Rèn","Lò Sũ[5]","Lương Ngọc Quyến","Lương Văn Can","Lý Đạo Thành","Lý Nam Đế","Lý Quốc Sư","Lý Thái Tổ","Lý Thường Kiệt","Mã Mây","Nam Ngư","Ngõ Gạch","Ngô Quyền","Ngô Thì Nhậm","Ngõ Trạm","Ngô Văn Sở","Nguyễn Chế Nghĩa","Nguyễn Gia Thiều","Nguyễn Hữu Huân","Nguyễn Khắc Cần","Nguyên Khiết","Nguyễn Quang Bích","Nguyễn Siêu","Nguyễn Thái Học","Nguyễn Thiện Thuật","Nguyễn Thiếp","Nguyễn Tư Giản","Nguyễn Văn Tố","Nguyễn Xí","Nhà Chung","Nhà Hỏa","Nhà thờ","Ô Quan Chưởng","Phạm Ngũ Lão","Phạm Sư Mạnh","Phan Bội Châu","Phan Chu Trinh","Phan Đình Phùng","Phan Huy Chú","Phủ Doãn","Phúc Tân","Phùng Hưng","Quán Sứ","Quang Trung","Tạ Hiện","Thanh Hà","Thanh Yên","Thợ Nhuộm","Thuốc Bắc","Tô Tịch","Tôn Thất Thiệp","Tông Đản","Tống Duy Tân","Trần Bình Trọng","Trần Hưng Đạo","Trần Khánh Dư","Trần Nguyên Hãn","Trần Nhật Duật","Trần Phú","Trần Quang Khải","Trần Quốc Toản","Tràng Thi","Tràng Tiền","Triệu Quốc Đạt","Trương Hán Siêu","Vạn Kiếp","Vọng Đức","Vọng Hà","Yết Kiêu","Xóm Hạ Hồi","Bưởi","Cầu Giấy","Chùa Hà","Đặng Thùy Trâm","Dịch Vọng","Dịch Vọng Hậu","Đỗ Quang","Doãn Kế Thiện","Dương Đình Nghệ","Dương Khuê","Dương Quảng Hàm","Duy Tân","Hồ Tùng Mậu","Hoa Bằng","Hoàng Đạo Thúy","Hoàng Minh Giám","Hoàng Ngân","Hoàng Quốc Việt","Hoàng Sâm","Khuất Duy Tiến","Lạc Long Quân","Lê Đức Thọ","Lê Văn Lương","Mạc Thái Tổ","Mạc Thái Tông","Mai Dịch","Nghĩa Tân","Nguyễn Chánh","Nguyễn Đình Hoàn","Nguyễn Khả Trạc","Nguyễn Khang","Nguyễn Khánh Toàn","Nguyễn Ngọc Vũ","Nguyễn Phong Sắc","Nguyễn Thị Định","Nguyễn Thị Thập","Nguyễn Văn Huyên","Phạm Hùng","Phạm Thận Duật","Phạm Tuấn Tài","Phạm Văn Bạch","Phạm Văn Đồng","Phan Văn Trường","Phùng Chí Kiên","Quan Hoa","Quan Nhân","Thành Thái","Thọ Tháp","Tô Hiệu","Tôn Thất Thuyết","Trần Bình","Trần Cung","Trần Đăng Ninh","Trần Duy Hưng","Trần Kim Xuyến","Trần Quốc Hoàn","Trần Quốc Vượng","Trần Quý Kiên","Trần Thái Tông","Trần Tử Bình","Trần Vỹ","Trung Hòa","Trung Kính","Trương Công Khải","Võ Chí Công","Vũ Phạm Hàm","Xuân Thủy","Yên Hòa","Bùi Xuân Phái","Cao Xuân Huy","Cầu Cốc","Cầu Diễn","Châu Văn Liêm","Cương Kiên","Đại Linh","Đại Mỗ","Đình Thôn","Đỗ Đình Thiện","Đỗ Đức Dục","Do Nha","Đỗ Xuân Hợp","Đồng Me","Dương Đình Nghệ","Dương Khuê","Duy Tân","Hàm Nghi","Hồ Tùng Mậu","Hoài Thanh","Hòe Thị","Hữu Hưng","Lê Đức Thọ","Lê Quang Đạo","Lương Thế Vinh","Lưu Hữu Phước","Mạc Thái Tổ","Mạc Thái Tông","Mễ Trì","Mễ Trì Hạ","Mễ Trì Thượng","Miếu Đầm","Miêu Nha","Mỹ Đình","Ngọc Trục","Nguyễn Cơ Thạch","Nguyễn Đổng Chi","Nguyễn Hoàng","Nguyễn Trãi","Nguyễn Xuân Nguyên","Nhổn","Phạm Hùng","Phú Đô","Phúc Diễn","Phùng Khoang","Phương Canh","Sa Đôi","Tân Mỹ","Tây Mỗ","Đại lộ Thăng Long","Thanh Bình","Thị Cấm","Thiên Hiền","Tố Hữu","Tôn Thất Thuyết","Trần Bình","Trần Hữu Dực","Trần Văn Cẩn","Trần Văn Lai","Trung Văn","Tu Hoàng","Vũ Hữu","Vũ Quỳnh","Xuân Phương","An Dương Vương","Cầu Diễn","Châu Đài","Cổ Nhuế","Đăm","Đặng Thùy Trâm","Đỗ Nhuận","Đông Ngạc","Đức Diễn","Đức Thắng","Hồ Tùng Mậu","Hoàng Công Chất","Hoàng Liên","Hoàng Quốc Việt","Hoàng Tăng Bí","Kẻ Vẽ","Kiều Mai","Kỳ Vũ","Lê Văn Hiến","Liên Mạc","Lộc","Mạc Xá","Ngọa Long","Nguyễn Hoàng Tôn","Nhật Tảo","Nhổn","Phạm Tuấn Tài","Phạm Văn Đồng","Phan Bá Vành","Phú Diễn","Phú Kiều","Phú Minh","Phúc Minh","Sùng Khang","Tân Nhuệ","Tân Phong","Tân Xuân","Tây Đăm","Tây Tựu","Thanh Lâm","Thượng Cát","Thụy Phương","Trần Cung","Trung Tựu","Văn Tiến Dũng","Văn Trì","Viên","Võ Quý Huân","Xuân Đỉnh","Xuân Phương","Yên Nội","Bằng Liệt","Bùi Huy Bích","Bùi Xương Trạch","Đại Từ","Đặng Xuân Bảng","Định Công","Định Công Hạ","Định Công Thượng","Đông Thiên","Giải Phóng","Giáp Bát","Giáp Nhị","Hoàng Liệt","Hoàng Mai","Hồng Quang","Hưng Phúc","Khuyến Lương","Kim Đồng","Kim Giang","Linh Đàm","Linh Đường","Lĩnh Nam","Lương Khánh Thiện","Mai Động","Nam Dư","Nghiêm Xuân Yêm","Ngọc Hồi","Ngũ Nhạc","Nguyễn An Ninh","Nguyễn Cảnh Dị","Nguyễn Chính","Nguyễn Công Thái","Nguyễn Đức Cảnh","Nguyễn Duy Trinh","Nguyễn Hữu Thọ","Nguyễn Khoái","Nguyễn Xiển","Sở Thượng","Tam Trinh","Tân Mai","Tây Trà","Thanh Đàm","Thanh Lân","Thịnh Liệt","Thúy Lĩnh","Trần Điền","Trần Hòa","Trần Nguyên Đán","Trần Thủ Độ","Trương Định","Tương Mai","Vĩnh Hưng","Vũ Tông Phan","Yên Duyên","Yên Sở","Bùi Xương Trạch","Chính Kinh","Cù Chính Lan","Cự Lộc","Định Công","Giải Phóng","Giáp Nhất","Hạ Đình","Hoàng Đạo Thành","Hoàng Đạo Thúy","Hoàng Minh Giám","Hoàng Ngân","Hoàng Văn Thái","Khuất Duy Tiến","Khương Đình","Khương Hạ","Khương Trung","Kim Giang","Lê Trọng Tấn","Lê Văn Lương","Lê Văn Thiêm","Lương Thế Vinh","Ngụy Như Kon Tum","Nguyễn Huy Tưởng","Nguyễn Lân","Nguyễn Ngọc Nại","Nguyễn Quý Đức","Nguyễn Thị Định","Nguyễn Thị Thập","Nguyễn Trãi","Nguyễn Tuân","Nguyễn Văn Trỗi","Nguyễn Viết Xuân","Nguyễn Xiển","Nhân Hòa","Phan Đình Giót","Phương Liệt","Quan Nhân","Thượng Đình","Tố Hữu","Tô Vĩnh Diện","Trần Điền","Triều Khúc","Trường Chinh","Vọng","Vũ Hữu","Vũ Tông Phan","Vũ Trọng Phụng","Vương Thừa Vũ","Đường 19-5","Phố An Hòa","Phố Ao Sen","Phố Ba La","Phố Bà Triệu","Phố Bạch Thái Bưởi","Phố Bế Văn Đàn","Đường Biên Giang","Phố Bùi Bằng Đoàn","Phố Cao Thắng","Phố Cầu Am","Phố Cầu Tuân","Đường Chiến Thắng","Phố Chu Văn An","Đường Đa Sĩ","Phố Đại An","Đường Đại Mỗ","Phố Đinh Tiên Hoàng","Phố Do Lộ","Phố Động Lãm","Phố Hà Trì","Phố Hoàng Diệu","Phố Hoàng Hoa Thám","Phố Hoàng Văn Thụ","Phố Huỳnh Thúc Kháng","Phố Lê Anh Xuân","Phố Lê Hồng Phong","Phố Lê Lai","Phố Lê Lợi","Phố Lê Quý Đôn","Đường Lê Trọng Tấn","Phố Lụa","Phố Lương Ngọc Quyến","Phố Lương Văn Can","Phố Lý Thường Kiệt","Phố Lý Tự Trọng","Phố Mậu Lương","Phố Minh Khai","Phố Mộ Lao","Phố Ngô Gia Khảm","Phố Ngô Gia Tự","Phố Ngô Quyền","Phố Ngô Thì Nhậm","Phố Ngô Thì Sĩ","Phố Nguyễn Công Trứ","Đường Nguyễn Khuyến","Phố Nguyễn Thái Học","Phố Nguyễn Thượng Hiền","Phố Nguyễn Trãi","Đường Nguyễn Trực","Phố Nguyễn Văn Lộc","Phố Nguyễn Văn Trỗi","Phố Nguyễn Viết Xuân","Phố Nhuệ Giang","Phố Phan Bội Châu","Phố Phan Chu Trinh","Phố Phan Đình Giót","Phố Phan Đình Phùng","Phố Phan Huy Chú","Phố Phú Lãm","Phố Phú Lương","Đường Phùng Hưng","Đường Quang Trung","Phố Tản Đà","Phố Tây Sơn","Phố Thanh Bình","Phố Thành Công","Phố Thanh Lãm","Phố Thượng Mạo","Phố Tiểu Công nghệ","Phố Tô Hiến Thành","Phố Tô Hiệu","Phố Tố Hữu","Phố Trần Đăng Ninh","Phố Trần Hưng Đạo","Đường Trần Phú","Phố Trần Văn Chuông","Phố Trinh Lương","Phố Trưng Nhị","Phố Trưng Trắc","Phố Trương Công Định","Phố Văn La","Phố Văn Phú","Đường Vạn Phúc","Phố Văn Quán","Phố Văn Yên","Phố Võ Thị Sáu","Phố Xa La","Phố Xốm","Phố Yên Bình","Phố Yên Lộ","Phố Yên Phúc","Phố Yết Kiêu","8-3.","Bà Triệu","Bạch Đằng","Bạch Mai","Bùi Ngọc Dương","Bùi Thị Xuân","Cảm Hội","Cao Đạt","Chùa Vua","Đại Cồ Việt","Đại La","Đỗ Hành","Đỗ Ngọc Du","Đoàn Trần Nghiệp","Đội Cung","Đống Mác","Đồng Nhân","Giải Phóng","Hàn Thuyên","Hàng Chuối","Hồ Xuân Hương","Hoa Lư","Hòa Mã","Hoàng Mai","Hồng Mai","Huế","Hương Viên","Kim Ngưu","Lạc Nghiệp","Lạc Trung","Lãng Yên","Lê Đại Hành","Lê Duẩn","Lê Gia Đỉnh","Lê Ngọc Hân","Lê Quý Đôn","Lê Thanh Nghị","Lê Văn Hưu","Liên Trì","Lò Đúc","Lương Yên","Mạc Thị Bưởi","Mai Hắc Đế","Minh Khai","Ngô Thì Nhậm","Nguyễn An Ninh","Nguyễn Bỉnh Khiêm","Nguyễn Cao","Nguyễn Công Trứ","Nguyễn Đình Chiểu","Nguyễn Du","Nguyễn Hiền","Nguyễn Huy Tự","Nguyễn Khoái","Nguyễn Quyền","Nguyễn Thượng Hiền","Nguyễn Trung Ngạn","Phạm Đình Hổ","Phù Đổng Thiên Vương","Phùng Khắc Khoan","Quang Trung","Quỳnh Lôi","Quỳnh Mai","Tạ Quang Bửu","Tam Trinh","Tăng Bạt Hổ","Tây Kết","Thái Phiên","Thanh Nhàn","Thể Giao","Thi Sách","Thiền Quang","Thịnh Yên","Thọ Lão","Tô Hiến Thành","Trần Bình Trọng","Trần Cao Vân","Trần Đại Nghĩa","Trần Hưng Đạo","Trần Khánh Dư","Trần Khát Chân","Trần Nhân Tông","Trần Thánh Tông","Trần Xuân Soạn","Triệu Việt Vương","Trương Định","Trương Hán Siêu","Tuệ Tĩnh","Tương Mai","Vân Đồn","Vân Hồ","Vạn Kiếp","Vĩnh Tuy","Võ Thị Sáu","Vọng","Vũ Hữu Lợi","Yên Bái","Yên Lạc","Yersin","Yết Kiêu","An Trạch","Bích Câu","Cát Linh","Cầu Giấy","Cầu Mới","Chợ Khâm Thiên","Chùa Bộc","Chùa Láng","Đặng Tiến Đông","Đặng Trần Côn","Đặng Văn Ngữ","Đào Duy Anh","Đoàn Thị Điểm","Đông Các","Đông Tác","Giải Phóng","Giảng Võ","Hàng Cháo","Hào Nam","Hồ Đắc Di","Hồ Giám","Hoàng Cầu","Hoàng Ngọc Phách","Hoàng Tích Trí","Huỳnh Thúc Kháng","Khâm Thiên","Khương Thượng","Kim Hoa","La Thành","Láng","Láng Hạ","Lê Duẩn","Lương Định Của","Lý Văn Phức","Mai Anh Tuấn","Nam Đồng","Ngô Sĩ Liên","Ngô Tất Tố","Nguyễn Chí Thanh","Nguyên Hồng","Nguyễn Khuyến","Nguyễn Lương Bằng","Nguyễn Như Đổ","Nguyễn Phúc Lai","Nguyễn Thái Học","Nguyễn Trãi","Ô Chợ Dừa","Phạm Ngọc Thạch","Phan Phù Tiên","Phan Văn Trị","Pháo Đài Láng","Phương Mai","Quốc Tử Giám","Tây Sơn","Thái Hà","Thái Thịnh","Tôn Đức Thắng","Tôn Thất Tùng","Trần Hữu Tước","Trần Quang Diệu","Trần Quý Cáp","Trịnh Hoài Đức","Trúc Khê","Trung Liệt","Trường Chinh","Văn Miếu","Vĩnh Hồ","Võ Văn Dũng","Vọng","Vũ Ngọc Phan","Vũ Thạnh","Xã Đàn","Y Miếu","Yên Lãng","đường 2 Hàng Cau","phố Ngọc Hà","Đặng Xuân Bảng","Đốc Ngữ", "Đốc Ngữ - Phường Cống Vị"
          ];
    }
    public static function street_lib_ho_chi_minh(){
        return [
            "Trần Đình Xu",  "Đề Thám"," NGUYỄN ĐÌNH CHIỂU"," NGUYỄN BỈNH KHIÊM"," NGÔ VĂN NĂM"," NGÔ ĐỨC KẾ"," PASTEUR"," PHAN BỘI CHÂU"," PHAN CHÂU TRINH"," PHAN KẾ BÍNH"," PHAN LIÊM"," PHAN NGỮ"," PHAN TÔN"," PHAN VĂN TRƯỜNG"," PHAN VĂN ĐẠT"," PHẠM HỒNG THÁI"," PHẠM NGỌC THẠCH"," PHẠM NGŨ LÃO"," PHẠM VIẾT CHÁNH"," PHÓ ĐỨC CHÍNH"," PHÙNG KHẮC KHOAN"," SƯƠNG NGUYỆT ÁNH"," THI SÁCH"," THÁI VĂN LUNG"," THẠCH THỊ THANH"," THỦ KHOA HUÂN"," TRẦN CAO VÂN"," TRẦN DOÃN KHANH"," TRẦN HƯNG ĐẠO"," TRẦN KHÁNH DƯ"," TRẦN KHẮC CHÂN"," TRẦN NHẬT DUẬT"," TRẦN QUANG KHẢI"," TRẦN QUÝ KHOÁCH"," TRẦN ĐÌNH XU"," TRỊNH VĂN CẤN"," TRƯƠNG HÁN SIÊU"," TRƯƠNG ĐỊNH"," TÔN THẤT THIỆP"," TÔN THẤT TÙNG"," TÔN THẤT ĐẠM"," TÔN ĐỨC THẮNG"," VÕ VĂN KIỆT"," VÕ THỊ SÁU"," YERSIN"," NGUYỄN HỮU CẢNH"," BÀ HUYỆN THANH QUAN"," BÀN CỜ"," CÁCH MẠNG THÁNG 8"," CAO THẮNG"," CÔNG TRƯỜNG QUỐC TẾ"," ĐIỆN BIÊN PHỦ"," ĐOÀN CÔNG BỬU"," HAI BÀ TRƯNG"," HOÀNG SA"," HỒ XUÂN HƯƠNG"," HUỲNH TỊNH CỦA"," KỲ ĐỒNG"," LÊ NGÔ CÁT"," LÊ QUÝ ĐÔN"," LÊ VĂN SỸ"," LÝ CHÍNH THẮNG"," LÝ THÁI TỔ"," NAM KỲ KHỞI NGHĨA"," NGUYỄN GIA THIỀU"," NGUYỄN HIỀN"," NGUYỄN PHÚC NGUYÊN"," NGUYỄN SƠN HÀ"," NGUYỄN THỊ DIỆU"," NGUYỄN THỊ MINH KHAI"," NGUYỄN THIỆN THUẬT"," NGUYỄN THÔNG"," NGUYỄN THƯỢNG HIỀN"," NGUYỄN VĂN MAI",
            " PASTEUR"," PHẠM ĐÌNH TOÁI"," PHẠM NGỌC THẠCH"," RẠCH BÙNG BINH"," SƯ THIỆN CHIẾU"," TRẦN CAO VÂN"," TRẦN QUANG DIỆU"," TRẦN QUỐC THẢO"," TRẦN QUỐC TOẢN"," TRẦN VĂN ĐANG"," TRƯƠNG ĐỊNH"," TRƯƠNG QUYỀN"," TRƯỜNG SA"," TÚ XƯƠNG"," VÕ THỊ SÁU"," VÕ VĂN TẦN"," VƯỜN CHUỐI"," BẾN VÂN ĐỒN"," HOÀNG DIỆU "," KHÁNH HỘI"," LÊ QUỐC HƯNG"," LÊ THẠCH"," LÊ VĂN LINH","LÊ VĂN LINH NỐI DÀI"," NGÔ VĂN SỞ"," NGUYỄN HỮU HÀO"," NGUYỄN KHOÁI"," NGUYỄN TẤT THÀNH"," NGUYỄN THẦN HIẾN"," NGUYỄN TRƯỜNG TỘ"," TÂN VĨNH"," TÔN ĐẢN"," TÔN THẤT THUYẾT"," TRƯƠNG ĐÌNH HỢI"," VĨNH HỘI"," VĨNH KHÁNH"," XÓM CHIẾU"," AN BÌNH"," AN DƯƠNG VƯƠNG"," AN ĐIỀM"," BẠCH VÂN"," BÀ TRIỆU"," BÃI SẬY"," BÙI HỮU NGHĨA"," CAO ĐẠT"," CHÂU VĂN LIÊM"," CHIÊU ANH CÁC"," CÔNG TRƯỜNG AN ĐÔNG"," DƯƠNG TỬ GIANG"," ĐẶNG THÁI THÂN"," ĐÀO TẤN"," ĐỖ NGỌC THẠNH"," ĐỖ VĂN SỬU"," GIA PHÚ"," GÒ CÔNG"," HÀ TÔN QUYỀN"," HẢI THƯỢNG LÃN ÔNG"," HỒNG BÀNG"," HỌC LẠC"," HÙNG VƯƠNG"," HUỲNH MẪN ĐẠT"," KIM BIÊN"," KÝ HOÀ"," LÃO TỬ"," LÊ HỒNG PHONG"," LÊ QUANG ĐỊNH"," LƯƠNG NHỮ HỌC"," LƯU XUÂN TÍN"," LÝ THƯỜNG KIỆT"," MẠC CỬU"," MẠC THIÊN TÍCH"," NGHĨA THỤC"," NGÔ GIA TỰ"," NGÔ NHÂN TỊNH"," NGÔ QUYỀN"," NGUYỄN AN KHƯƠNG"," NGUYỄN ÁN"," NGUYỄN BIỂU"," NGUYỄN CHÍ THANH"," NGUYỄN DUY DƯƠNG"," NGUYỄN KIM"," NGUYỄN THI"," NGUYỄN THỊ NHỎ"," NGUYỄN THỜI TRUNG"," NGUYỄN TRÃI"," NGUYỄN TRI PHƯƠNG"," NGUYỄN VĂN CỪ"," NGUYỄN VĂN ĐỪNG"," NHIÊU TÂM"," PHẠM BÂN"," PHẠM HỮU CHÍ"," PHẠM ĐÔN"," PHAN HUY CHÚ"," PHAN PHÚ TIÊN"," PHAN VĂN KHOẺ"," PHAN VĂN TRỊ"," PHƯỚC HƯNG"," PHÓ CƠ ĐIỀU"," PHÙ ĐỔNG THIÊN VƯƠNG"," PHÙNG HƯNG"," PHÚ GIÁO"," PHÚ HỮU"," PHÚ ĐINH"," SƯ VẠN HẠNH"," TÂN HÀNG"," TÂN HƯNG"," TÂN THÀNH"," TĂNG BẠT HỔ"," TẠ UYÊN"," TẢN ĐÀ"," THUẬN KIỀU"," TỐNG DUY TÂN"," TRẦN BÌNH TRỌNG"," TRẦN CHÁNH CHIẾU"," TRẦN HOÀ"," TRẦN HƯNG ĐẠO"," TRẦN NHÂN TÔN"," TRẦN ĐIỆN"," TRẦN PHÚ"," TRẦN TƯỚNG CÔNG"," TRẦN TUẤN KHẢI"," VÕ VĂN KIỆT "," TRẦN XUÂN HOÀ"," TRANG TỬ"," TRIỆU QUANG PHỤC"," TRỊNH HOÀI  ĐỨC"," VẠN KIẾP"," VẠN TƯỢNG"," VÕ TRƯỜNG TOẢN"," VŨ CHÍ HIẾU"," XÓM CHỈ"," XÓM VÔI"," YẾT KIÊU"," AN DƯƠNG VƯƠNG"," BÀ HOM"," BÀ KÝ"," BÀ LÀI"," BÃI SẬY"," BẾN LÒ GỐM"," BẾN PHÚ LÂM"," BÌNH PHÚ"," BÌNH TÂY"," BÌNH TIÊN"," BỬU ĐÌNH"," CAO VĂN LẦU"," CHỢ LỚN"," CHU VĂN AN"," ĐẶNG NGUYÊN CẨN"," ĐƯỜNG NỘI BỘ CƯ XÁ PHÚ LÂM B"," GIA PHÚ"," HẬU GIANG"," HỒNG BÀNG"," HOÀNG LÊ KHA"," KINH DƯƠNG VƯƠNG"," LÊ QUANG SUNG"," LÊ TẤN KẾ"," LÊ TRỰC"," LÊ TUẤN MẬU"," LÝ CHIÊU HOÀNG"," MAI XUÂN THƯỞNG"," MINH PHỤNG"," NGÔ NHÂN TỊNH"," NGUYỄN HỮU THẬN"," NGUYỄN ĐÌNH CHI"," NGUYỄN PHẠM TUÂN"," NGUYỄN THỊ NHỎ"," NGUYỄN VĂN LUÔNG"  ," NGUYỄN XUÂN PHỤNG"," PHẠM ĐÌNH HỔ"," PHẠM PHÚ THỨ"," PHẠM VĂN CHÍ"," PHAN ANH"," PHAN VĂN KHOẺ"," THÁP MƯỜI"," TRẦN BÌNH"," TRẦN TRUNG LẬP"," VÕ VĂN KIỆT"," TRANG TỬ"," VĂN THÂN"," TRẦN VĂN KIỂU "," BÙI VĂN BA"," ĐÀO TRÍ"," GÒ Ô MÔI"," HOÀNG QUỐC VIỆT"," HUỲNH TẤN PHÁT"," LÂM VĂN BỀN"," LÊ VĂN LƯƠNG"," LƯU TRỌNG LƯ"," LÝ PHỤC MAN"," MAI VĂN VĨNH"," NGUYỄN HỮU THỌ"," NGUYỄN THỊ THẬP"," NGUYỄN VĂN LINH"," NGUYỄN VĂN QUỲ"," PHẠM HỮU LẦU"," PHAN HUY THỰC"," TÂN MỸ"," TÂN THUẬN TÂY"," TRẦN TRỌNG CUNG"," TRẦN VĂN KHÁNH"," TRẦN XUÂN SOẠN"," CAO TRIỀU PHÁT "," ĐẶNG ĐẠI ĐỘ"," ĐẶNG ĐỨC THUẬT"," ĐÔ ĐỐC TUYẾT "," NGUYỄN KHẮC VIỆN "," NGUYỄN LƯƠNG BẰNG "," NGUYỄN PHAN CHÁNH "," PHẠM THÁI BƯỜNG "," PHẠM THIỀU "," PHẠM VĂN NGHỊ"," PHAN KHIÊM ÍCH"," PHAN VĂN CHƯƠNG "," PHỐ TIỂU BẮC"," PHỐ TIỂU ĐÔNG"," PHỐ TIỂU NAM"," RAYMONDIENNE "," TÂN PHÚ "," TÂN TRÀO "," TÔN DẬT TIÊN "," TRẦN VĂN TRÀ "," DẠ NAM"," DÃ TƯỢNG"," DƯƠNG BÁ TRẠC"," ĐẶNG CHẤT"," ĐÀO CAM MỘC"," ĐẠI LỘ VÕ VĂN KIỆT"," ĐÌNH AN TÀI"," ĐINH HOÀ"," ĐÔNG HỒ"," PHẠM THẾ HIỂN ","PHÚ ĐỊNH"," ĐƯỜNG 44 - TRƯƠNG ĐÌNH HỘI"," 111 "," ĐƯỜNG VEN SÔNG XÁNG"," HỒ HỌC LÃM"," HOÀNG MINH ĐẠO"," HƯNG PHÚ"," HOÀI THANH"," HOÀNG ĐẠO THUÝ"," HOÀNG SĨ KHẢI"," HUỲNH THỊ PHỤNG"," LÊ QUANG KIM"," LÊ THÀNH PHƯƠNG"," LƯƠNG NGỌC QUYẾN"," LƯƠNG VĂN CAN"," LƯU HỮU PHƯỚC"," LƯU QUÝ KỲ"," LÝ ĐẠO THÀNH"," MẶC VÂN"," MAI HẮC ĐẾ"," MAI AM"," MỄ CỐC"," NGÔ SĨ LIÊN"," NGUYỄN CHẾ NGHĨA"," NGUYỄN DUY"," NGUYỄN NGỌC CUNG"," NGUYỄN NHƯỢC THỊ"," NGUYỄN QUYỀN"," NGUYỄN SĨ CỐ"," NGUYỄN THỊ THẬP"," NGUYỄN THỊ TẦN"," NGUYỄN VĂN CỦA"," NGUYỄN VĂN LINH"," PHẠM HÙNG (P4, P5)"," PHẠM HÙNG (P9, P10)"," PHẠM THẾ HIỂN"," PHONG PHÚ"," QUỐC LỘ 50"," RẠCH CÁT"," RẠCH CÙNG"," RẠCH LỒNG ĐÈN"," TẠ QUANG BỬU"," TRẦN NGUYÊN HÃN"," TRẦN VĂN THÀNH"," TRỊNH QUANG NGHỊ"," TRƯƠNG ĐÌNH HỘI"," TÙNG THIỆN VƯƠNG"," TUY LÝ VƯƠNG"," ƯU LONG"," VẠN KIẾP"," VĨNH NAM"," VÕ TRỨ"," AN DƯƠNG VƯƠNG"," ÂU DƯƠNG LÂN"," BA ĐÌNH"," BẾN CẦN GIUỘC"," BẾN PHÚ ĐỊNH"," BẾN XÓM CỦI"," BẾN Ụ CÂY"," BÌNH ĐÔNG"," BÌNH ĐỨC"," BÔNG SAO"," BÙI HUY BÍCH"," BÙI MINH TRỰC"," CÂY SUNG"," CAO LỖ"," CAO XUÂN DỤC"," GÒ CÁT"," GÒ NỔI "," HAI BÀ TRƯNG"," HÀNG TRE"," HỒ BÁ PHẤN "," HOÀNG HỮU NAM"," HỒNG SẾN"," HUỲNH THÚC KHÁNG"," ÍCH THẠNH "," KHỔNG TỬ"," LÃ XUÂN OAI"," LÊ LỢI"," LÊ VĂN VIỆT"," LÒ LU "," LONG PHƯỚC"," LONG SƠN "," LONG THUẬN "," MẠC HIỂN TÍCH "," MAN THIỆN"," NAM CAO"," NAM HOÀ "," NGÔ QUYỀN "," NGUYỄN CÔNG TRỨ"," NGUYỄN DUY TRINH "," NGUYỄN THÁI HỌC"," NGUYỄN VĂN TĂNG "," NGUYỄN VĂN THẠNH"," NGUYỄN XIỂN "," PHAN CHU TRINH"," PHAN ĐẠT ĐỨC"," PHAN ĐÌNH PHÙNG"," PHƯỚC THIỆN "," QUANG TRUNG"," QUANG TRUNG (NỐI DÀI)"," TAM ĐA "," TÂN HOÀ II"," TÂN LẬP I"," TÂN LẬP II"," TĂNG NHƠN PHÚ "," TÂY HOÀ "," TRẦN HƯNG ĐẠO"," TRẦN QUỐC TOẢN"," TRẦN TRỌNG KHIÊM "," TRỊNH HOÀI ĐỨC"," TRƯƠNG HANH "," TRƯỜNG LƯU "," TRƯƠNG VĂN HẢI"," TRƯƠNG VĂN THÀNH"," TÚ XƯƠNG"," VÕ VĂN HÁT "," XA LỘ HÀ NỘI "," BÙI QUỐC KHÁI"," BƯNG ÔNG THOÀN"," CẦU ĐÌNH"," CẦU XÂY 1"," CẦU XÂY 2"," DÂN CHỦ"," DƯƠNG ĐÌNH HỘI"," ĐẠI LỘ 2"," ĐẠI LỘ 3"," ĐÌNH PHONG PHÚ"," ĐỖ XUÂN HỢP"," BÀ HẠT"," BA VÌ"," BẠCH MÃ"," BỬU LONG"," BẮC HẢI"," BẮC HẢI (NỐI DÀI)"," CAO THẮNG"," NGUYỄN GIÃN THANH"," CÁCH MẠNG THÁNG 8"," CHÂU THỚI"," CỬU LONG"," ĐIỆN BIÊN PHỦ"," ĐỒNG NAI"," ĐÀO DUY TỪ"," 3 THÁNG 2"," HỒ BÁ KIỆN"," HỒ THỊ KỶ"," HỒNG LĨNH"," HƯNG LONG "," HÒA HẢO"," HÒA HƯNG"," HOÀNG DƯ KHƯƠNG"," HƯƠNG GIANG"," HÙNG VƯƠNG"," LÊ HỒNG PHONG"," LÝ THÁI TỔ"," LÝ THƯỜNG KIỆT"," NGÔ GIA TỰ"," NGÔ QUYỀN"," NGUYỄN NGỌC LỘC"," NGUYỄN CHÍ THANH"," NGUYỄN DUY DƯƠNG"," NGUYỄN KIM"," NGUYỄN LÂM"," NGUYỄN THƯỢNG HIỀN"," NGUYỄN TIỂU LA"," NGUYỄN TRI PHƯƠNG"," NHẬT TẢO"," SƯ VẠN HẠNH"," TAM ĐẢO"," TÂN PHƯỚC"," THẤT SƠN"," THÀNH THÁI "," THÀNH THÁI (NỐI DÀI)"," TÔ HIẾN THÀNH"," TRẦN BÌNH TRỌNG"," TRẦN MINH QUYỀN"," TRẦN NHÂN TÔN"," TRẦN THIỆN CHÁNH"," TRƯỜNG SƠN"," VĨNH VIỄN"," ÂU CƠ"," BÌNH DƯƠNG THI XÃ"," BÌNH THỚI"," CÔNG CHÚA NGỌC HÂN"," ĐẶNG MINH KHIÊM"," ĐÀO NGUYÊN PHỔ"," ĐỖ NGỌC THẠNH"," ĐỘI CUNG (QUÂN SỰ CŨ)","  LÝ THƯỜNG KIỆT"," ĐƯỜNG 3/2","HỒNG BÀNG","HỒNG BÀNG"," 2 CƯ XÁ LỮ GIA"," CƯ XÁ LỮ GIA"," DƯƠNG ĐÌNH NGHỆ"," DƯƠNG TỬ GIANG"," HÀ TÔN QUYỀN"," HÀN HẢI NGUYÊN"," HÀN HẢI NGUYÊN (NỐI DÀI)"," HỒNG BÀNG"," HOÀNG ĐỨC TƯƠNG"," HÒA BÌNH"," HÒA HẢO"," HUYỆN TOẠI"," KHUÔNG VIỆT"," LẠC LONG QUÂN"," LẠC LONG QUÂN "," LÃNH BINH THĂNG"," LÊ ĐẠI HÀNH"," LÊ THỊ BẠCH CÁT"," LÊ TUNG"," LỮ GIA"," LÒ SIÊU"," LÝ NAM ĐẾ"," LÝ THƯỜNG KIỆT"," MINH PHỤNG"," NGUYỄN BÁ HỌC"," NGUYỄN CHÍ THANH"," NGUYỄN THỊ NHỎ"," NGUYỄN THỊ NHỎ (NỐI DÀI)"," NGUYỄN VĂN PHÚ"," NHẬT TẢO"," ÔNG ÍCH KHIÊM"," PHAN XÍCH LONG"," PHÓ CƠ ĐIỀU"," PHÚ THỌ"," QUÂN SỰ"," TÂN HÓA"," TÂN KHAI"," TÂN PHƯỚC"," TÂN THÀNH"," TẠ UYÊN"," THÁI PHIÊN"," THIÊN PHƯỚC"," THUẬN KIỀU"," TÔN THẤT HIỆP"," TỔNG LUNG"," TỐNG VĂN TRÂN"," TRẦN QUÝ"," TRỊNH ĐÌNH TRỌNG"," TUỆ TĨNH"," VĨNH VIỄN"," XÓM ĐẤT"," BÙI CÔNG TRỪNG"," BÙI VĂN NGỮ"," ĐÌNH GIAO KHẨU"," ĐÔNG HƯNG THUẬN 02"," HÀ HUY GIÁP"," HIỆP THÀNH 13"," HIỆP THÀNH 17"," HIỆP THÀNH 27"," HƯƠNG LỘ 80B"," LÊ ĐỨC THỌ"," LÊ THỊ RIÊNG"," LÊ VĂN KHƯƠNG"," NGUYỄN ẢNH THỦ"," NGUYỄN THÀNH VĨNH "," NGUYỄN VĂN QUÁ"," PHAN VĂN HỚN"," QUỐC LỘ 1A "," QUỐC LỘ 22"," TÂN CHÁNH HIỆP 10"," TÂN CHÁNH HIỆP 13"," TÂN CHÁNH HIỆP 24"," TÂN CHÁNH HIỆP 33 "," TÂN THỚI HIỆP 10"," TÂN THỚI NHẤT 1"," TÂN THỚI NHẤT 2"," TÂN THỚI NHẤT 06"," TÂN THỚI NHẤT 08"," THẠNH LỘC 30"," THẠNH XUÂN 13"," THỚI AN 19"," TÔ KÝ (TỈNH LỘ 15 CŨ)"," TÔ KÝ "," TÔ NGỌC VÂN"," TRUNG MỸ TÂY 13"," TRUNG MỸ TÂY 2A"," TRUNG MỸ TÂY 9A"," TRUNG MỸ TÂY 08"," TRƯỜNG CHINH"," VƯỜN LÀI"," THẠNH LỘC 15"," TÂN THỚI HIÊP 07"," TRUNG MỸ TÂY 18A"," HIỆP THÀNH 44"," HIỆP THÀNH 42"," HÀ ĐẶC"," TÂN CHÁNH HIỆP 25"," TÂN CHÁNH HIỆP 07"," TÂN CHÁNH HIỆP 03"," TÂN CHÁNH HIỆP 18"," TÂN CHÁNH HIỆP 35"," TÂN CHÁNH HIỆP 34"," TÂN CHÁNH HIỆP 36"," THỚI AN 32"," THỚI AN 16"," THỚI AN 13"," TUYẾN SONG HÀNH"," THẠNH LỘC 16"," TÂN THỚI NHẤT 17"," CẦM BÁ THƯỚC"," CAO THẮNG"," CHIẾN THẮNG"," CÔ BẮC"," CÔ GIANG"," CÙ LAO"," DUY TÂN"," ĐẶNG THAI MAI"," ĐẶNG VĂN NGỮ"," ĐÀO DUY ANH"," ĐÀO DUY TỪ "," ĐỖ TẤN PHONG"," ĐOÀN THỊ ĐIỂM"," HỒ BIỂU CHÁNH"," HOÀNG HOA THÁM"," HỒ VĂN HUÊ"," HOÀNG DIỆU"," HOÀNG MINH GIÁM"," HOÀNG VĂN THỤ"," HUỲNH VĂN BÁNH"," KÝ CON"," LAM SƠN"," LÊ QUÝ ĐÔN"," LÊ TỰ TÀI"," LÊ VĂN SỸ"," MAI VĂN NGỌC"," NGÔ THỜI NHIỆM"," NGUYỄN CÔNG HOAN"," NGUYỄN KIỆM"," NGUYỄN ĐÌNH CHIỂU"," NGUYỄN ĐÌNH CHÍNH"," NGUYỄN LÂM"," NGUYỄN THỊ HUỲNH"," NGUYỄN THƯỢNG HIỀN"," NGUYỄN TRỌNG TUYỂN"," NGUYỄN TRƯỜNG TỘ"," NGUYỄN VĂN ĐẬU"," NGUYỄN VĂN TRỖI"," NHIÊU TỨ"," PHAN ĐĂNG LƯU"," PHAN ĐÌNH PHÙNG"," PHAN TÂY HỒ","  PHAN XÍCH LONG"," PHÙNG VĂN CUNG"," THÍCH QUẢNG ĐỨC"," TRẦN CAO VÂN"," TRẦN HỮU TRANG"," TRẦN HUY LIỆU"," TRẦN KẾ XƯƠNG"," TRẦN KHẮC CHÂN"," TRƯƠNG QUỐC DUNG"  ,  "TRƯỜNG SA    ",  " HỒNG HÀ"," PHỔ QUANG"," BẠCH ĐẰNG"," BÌNH LỢI"," BÌNH QUỚI"," BÙI HỮU NGHĨA"," BÙI ĐÌNH TUÝ"," CHU VĂN AN"," CÔNG TRƯỜNG HOÀ BÌNH"," CÔNG TRƯỜNG TỰ DO"," DIÊN HỒNG"," ĐIỆN BIÊN PHỦ"," ĐINH BỘ LĨNH"," ĐINH TIÊN HOÀNG"," ĐỐNG ĐA"," ĐƯỜNG 12AB KHU MIẾU NỔI"," ĐƯỜNG 16 KHU MIẾU NỔI"," ĐƯỜNG D1"," ĐƯỜNG D2"," ĐƯỜNG D3"," ĐƯỜNG D5"," ĐƯỜNG TRỤC 30M"," HỒ XUÂN HƯƠNG"," HỒNG BÀNG"," HOÀNG HOA THÁM"," HUỲNH MẪN ĐẠT"," HUỲNH ĐÌNH HAI"," HUỲNH TÁ BANG"," HUỲNH TỊNH CỦA"," LAM SƠN"," LÊ QUANG ĐỊNH"," LÊ TRỰC"," LƯƠNG NGỌC QUYẾN"," MAI XUÂN THƯỞNG"," MÊ LINH"," NGUYỄN NGỌC PHƯƠNG"," NGUYỄN THƯỢNG HIỀN"," NGÔ NHÂN TỊNH"," NGÔ ĐỨC KẾ"," NGÔ TẤT TỐ"," ĐƯỜNG PHÚ MỸ"," NGUYỄN TRUNG TRỰC"," NGUYỄN  XÍ"," NGUYỄN AN NINH"," NGUYỄN BỈNH KHIÊM"," NGUYỄN CÔNG HOAN"," NGUYỄN CÔNG TRỨ"," NGUYỄN CỬU VÂN"," NGUYỄN DUY"," NGUYÊN HỒNG"," NGUYỄN HỮU CẢNH"," NGUYỄN HUY LƯỢNG"," NGUYỄN HUY TƯỞNG"," NGUYỄN KHUYẾN"," NGUYỄN LÂM"," NGUYỄN THÁI HỌC"," NGUYỄN THIỆN THUẬT"," NGUYỄN VĂN LẠC"," NGUYỄN VĂN ĐẬU"," NGUYỄN XUÂN ÔN"," NƠ TRANG LONG"," PHẠM VIẾT CHÁNH"," PHAN HUY ÔN"," PHAN BỘI CHÂU"," PHAN CHU TRINH"," PHAN ĐĂNG LƯU"," PHAN XÍCH LONG"," PHAN VĂN HÂN"," PHAN VĂN TRỊ"," PHÓ ĐỨC CHÍNH"," QUỐC LỘ 13"," TĂNG BẠT HỔ"," THANH ĐA"," THIÊN HỘ DƯƠNG"," TRẦN BÌNH TRỌNG"," TRẦN KẾ XƯƠNG"," TRẦN QUÝ CÁP"," TRẦN VĂN KỶ"," TRỊNH HOÀI ĐỨC"," TRƯỜNG SA"," UNG VĂN KHIÊM"," VẠN KIẾP"," VÕ DUY NINH"," VÕ TRƯỜNG TOẢN"," VŨ HUY TẤN"," VŨ NGỌC PHAN"," VŨ TÙNG"," XÔ VIẾT NGHỆ TĨNH"," YÊN ĐỖ"," PHẠM VĂN ĐỒNG"," AN DƯƠNG VƯƠNG"," AO ĐÔI"," ẤP CHIẾN LƯỢC"," BẾN LỘI"," BÌNH LONG"," BÌNH THÀNH"," BÙI DƯƠNG LỊCH"," BÙI HỮU DIÊN"," BÙI HỮU DIỆN"," BÙI TỰ TOÀN"," CẦU KINH"," CÂY CÁM"," CHIẾN LƯỢC"," DƯƠNG BÁ CUNG"," DƯƠNG TỰ QUÁN"," KHIẾU NĂNG TỈNH"," KINH DƯƠNG VƯƠNG"," LÂM HOÀNH"," LÊ CƠ"," LÊ CÔNG PHÉP"," LÊ ĐÌNH CẨN"," LÊ ĐÌNH DƯƠNG"," LÊ NGUNG"," LÊ TẤN BÊ"," LÊ TRỌNG TẤN"," LÊ VĂN QUỚI"," LỘ TẺ"," LÔ TƯ"," MÃ LÒ"," NGÔ Y LINH"," NGUYỄN CỬU PHÚ"," NGUYỄN HỚI"," NGUYỄN QUÝ YÊM"," NGUYỄN THỊ TÚ"," NGUYỄN THỨC ĐƯỜNG"," NGUYỄN THỨC TỰ"," NGUYỄN TRIỆU LUẬT"," NGUYỄN TRỌNG TRÍ"," NGUYỄN VĂN CỰ"," PHẠM BÀNH"," PHẠM ĐĂNG GIẢNG"," PHAN CÁT TỰU"," PHAN ANH"," PHAN ĐÌNH THÔNG"," PHÙNG TÁ CHU"," QUỐC LỘ 1A"," SINCO"," SÔNG SUỐI"," TẠ MỸ DUẬT"," TÂN HOÀ ĐÔNG"," TÂN KỲ TÂN QUÝ"," TẬP ĐOÀN 6B"," TÂY LÂN"," TÊN LỬA"," TỈNH LỘ 10"," TRẦN ĐẠI NGHĨA"," TRẦN THANH MẠI"," TRẦN VĂN GIÀU"," TRƯƠNG PHƯỚC PHAN"," VÀNH ĐAI TRONG"," VĨNH LỘC (HƯƠNG LỘ 80)"," VÕ VĂN VÂN "," VŨ HỮU"," VƯƠNG VĂN HUỐNG"," VÕ VĂN KIỆT"," HOÀNG HƯNG"," HẠNH THÔNG"," HOÀNG HOA THÁM"," HOÀNG MINH GIÁM"," HUỲNH KHƯƠNG AN"," HUỲNH VĂN NGHỆ"," LÊ ĐỨC THỌ"," LÊ HOÀNG PHÁI"," LÊ LAI"," LÊ LỢI"," LÊ QUANG ĐỊNH"," LÊ THỊ HỒNG"," LÊ VĂN THỌ"," LƯƠNG NGỌC QUYẾN"," LÝ THƯỜNG KIỆT"," NGUYỄN BỈNH KHIÊM"," NGUYỄN DU"," NGUYÊN HỒNG"," NGUYỄN KIỆM"," NGUYỄN OANH"," NGUYỄN THÁI SƠN"," NGUYỄN THƯỢNG HIỀN"," NGUYỄN TUÂN"," NGUYỄN VĂN BẢO"," NGUYỄN VĂN CÔNG"," NGUYỄN VĂN DUNG"," NGUYỄN VĂN LƯỢNG"," NGUYỄN VĂN NGHI"," PHẠM HUY THÔNG"," PHẠM NGŨ LÃO"," PHẠM VĂN BẠCH"," PHẠM VĂN CHIÊU"," PHAN HUY ÍCH"," PHAN VĂN TRỊ"," QUANG TRUNG"," TÂN SƠN"," THÍCH BỬU ĐĂNG"," THIÊN HỘ DƯƠNG"," THỐNG NHẤT"," THÔNG TÂY HỘI"," TRẦN BÁ GIAO"," TRẦN BÌNH TRỌNG"," TRẦN PHÚ CƯỜNG"," TRẦN QUỐC TUẤN"," TRẦN THỊ NGHĨ"," TRƯNG NỮ VƯƠNG"," TRƯƠNG ĐĂNG QUẾ"," TRƯƠNG MINH KÝ"," TÚ MỠ"," BÙI QUANG LÀ"," ĐỖ THÚC TỊNH"," NGUYỄN DUY CUNG"," NGUYỄN TƯ GIẢN"," PHẠM VĂN ĐỒNG"," AN HỘI"," AN NHƠN"," CÂY TRÂM"," DƯƠNG QUẢNG HÀM"," ẤP BẮC"," ÂU CƠ"," BA GIA"," BA VÂN"," BA VÌ"," BẮC HẢI"," BẠCH ĐẰNG 1"," BẠCH ĐẰNG 2"," BÀNH VĂN TRÂN"," BÀU BÀNG"," BÀU CÁT"," BÀU CÁT 1"," BÀU CÁT 2"," BÀU CÁT 3"," BÀU CÁT 4"," BÀU CÁT 5"," BÀU CÁT 6"," BÀU CÁT 7"," BÀU CÁT 8"," BẢY HIỀN"," BẾ VĂN ĐÀN"," BẾN CÁT"," BÌNH GIÃ"," BÙI THỊ XUÂN"," CA VĂN THỈNH"," CỐNG LỞ"," CÁCH MẠNG THÁNG 8"," CHẤN HƯNG"," CHÂU VĨNH TẾ"," CHÍ CÔNG"," CHÍ LINH"," CHỮ ĐỒNG TỬ"," CỘNG HOÀ"," CỬU LONG"," CÙ CHÍNH LAN"," DÂN TRÍ"," DƯƠNG VÂN NGA"," DUY TÂN"," ĐẠI NGHĨA"," ĐẶNG LỘ "," ĐẤT THÁNH"," ĐỒ SƠN"," ĐÔNG HỒ"," ĐỒNG NAI"," ĐỒNG ĐEN"," ĐÔNG SƠN"," ĐỒNG XOÀI"," ĐINH ĐIỀN "," ĐƯỜNG A4 "," ĐƯỜNG B6"," THÁI THỊ NHẠN"," NGÔ THỊ THU MINH"," NGUYỄN ĐỨC THUẬN"," ĐƯỜNG C1"," ĐƯỜNG C12"," ĐƯỜNG C18"," TRẦN VĂN DANH"," ĐƯỜNG C22"," ĐƯỜNG C27"," ĐƯỜNG C3"," ĐƯỜNG D52"," ĐẶNG MINH TRỨ"," BÙI THẾ MỸ"," ĐỐNG ĐA"," GIẢI PHÓNG"," GÒ CẨM ĐỆM"," HẬU GIANG"," HÀ BÁ TƯỜNG"," HÁT GIANG"," HIỆP NHẤT"," HỒNG HÀ"," HỒNG LẠC"," HOÀNG BẬT ĐẠT"," HOÀNG HOA THÁM"," HOÀNG KẾ VIÊM (C21)"," HOÀNG SA"," HOÀNG VĂN THỤ"," HOÀNG VIỆT"," HƯNG HOÁ"," HOÀ HIỆP"," HUỲNH LAN KHANH"," HUỲNH TỊNH CỦA"," HUỲNH VĂN NGHỆ"," KHAI TRÍ"," LẠC LONG QUÂN"," LAM SƠN"," LÊ BÌNH"," LÊ DUY NHUẬN (C28)"," LÊ LAI"," LÊ LỢI"," LÊ MINH XUÂN"," LÊ  NGÂN"," LÊ TẤN QUỐC"," LÊ TRUNG NGHĨA (C26)"," LÊ VĂN HUÂN"," LÊ VĂN SỸ"," LỘC HƯNG"," LỘC VINH"," LONG HƯNG"," LƯU NHÂN CHÚ"," LÝ THƯỜNG KIỆT"," MAI LÃO BẠNG"," NĂM CHÂU"," NGHĨA HƯNG"," NGHĨA HOÀ"," NGHĨA PHÁT"," NGÔ BỆ"," NGỰ BÌNH"," NGUYỄN BẶC"," NGUYỄN BÁ TÒNG"," NGUYỄN BÁ TUYỂN "," NGUYỄN CẢNH DỊ"," NGUYỄN CHÁNH SẮT"," NGUYỄN HIẾN LÊ "," NGUYỄN HỒNG ĐÀO"," NGUYỄN MINH HOÀNG"," NGUYỄN ĐÌNH KHƠI"," NGUYỄN PHÚC CHU"," NGUYỄN QUANG BÍCH"," NGUYỄN SỸ SÁCH"," NGUYỄN THANH TUYỀN"," NGUYỄN THÁI BÌNH"," NGUYỄN THẾ LỘC"," NGUYỄN THỊ NHỎ"," NGUYỄN TỬ NHA"," NGUYỄN TRỌNG LỘI"," NGUYỄN TRỌNG TUYỂN"," NGUYỄN VĂN MẠI"," NGUYỄN VĂN TRỖI"," NGUYỄN VĂN VĨ"," NGUYỄN VĂN VĨNH"," NHẤT CHI MAI"," NI SƯ HUỲNH LIÊN"," NÚI THÀNH"," PHẠM CỰ LƯỢNG"," PHẠM PHÚ THỨ"," PHẠM VĂN BẠCH"," PHẠM VĂN HAI"," PHAN BÁ PHIẾN"," PHAN HUY ÍCH"," PHAN ĐÌNH GIÓT"," PHAN SÀO NAM"," PHAN THÚC DUYỆN"," PHAN VĂN LÂU"," PHAN VĂN SỬU"," PHỔ QUANG"," PHÚ HOÀ"," PHÚ LỘC"," QUÁCH VĂN TUẤN"," QUẢNG HIỀN"," SẦM SƠN"," SAO MAI"," SÔNG ĐÀ"," SÔNG ĐÁY"," SÔNG NHUỆ"," SÔNG THAO"," SÔNG THƯƠNG"," SƠN CANG"," SƠN HƯNG"," TÂN CANH"," TÂN CHÂU"," TÂN KHAI"," TÂN KỲ TÂN QUÍ"," TÂN HẢI"," TÂN LẬP"," TÂN PHƯỚC"," TÂN SƠN HOÀ"," TÂN TẠO"," TÂN THỌ"," TÂN TIẾN"," TÂN TRANG"," TÂN TRỤ"," TÂN XUÂN"," TÁI THIẾT"," TẢN VIÊN"," THÂN NHÂN TRUNG"," THĂNG LONG"," THÀNH MỸ"," THÉP MỚI"," THÍCH MINH NGUYỆT"," THIÊN PHƯỚC"," THỦ KHOA HUÂN"," TIỀN GIANG"," TỐNG VĂN HÊN"," TỰ CƯỜNG"," TỰ LẬP"," TỨ HẢI"," TRẦN MAI NINH"," TRẦN QUỐC HOÀN"," TRẦN THÁI TÔNG"," TRẦN THÁNH TÔNG"," TRẦN TRIỆU LUẬT"," TRẦN VĂN DƯ"," TRẦN VĂN HOÀNG"," TRẦN VĂN QUANG"," TRÀ KHÚC"," TRƯƠNG CÔNG ĐỊNH"," TRƯƠNG HOÀNG THANH"," TRƯỜNG CHINH"," TRƯỜNG SA"," TRƯỜNG SƠN"," TRUNG LANG"," ÚT TỊCH"," VÂN CÔI"," VĂN CHUNG"," TÂN SƠN"," VÕ THÀNH TRANG"," XUÂN DIỆU"," XUÂN HỒNG"," YÊN THẾ"," ÂU CƠ"," BÁC ÁI"," BÌNH LONG"," BÙI CẨM HỔ"," BÙI XUÂN PHÁI"," CẦU XÉO"," CÁCH MẠNG"," CAO VĂN NGỌC"," CHÂN LÝ"," CHẾ LAN VIÊN"," CHU THIÊN"," CHU VĂN AN"," CỘNG HOÀ 3"," DÂN CHỦ"," DÂN TỘC"," DƯƠNG KHUÊ"," DƯƠNG ĐỨC HIỀN"," DƯƠNG THIỆU TƯỚC"," DƯƠNG VĂN DƯƠNG"," DIỆP MINH CHÂU"," ĐÀM THẬN HUY"," ĐẶNG THẾ PHONG"," ĐINH LIỆT"," ĐOÀN HỒNG PHƯỚC"," ĐOÀN GIỎI"," ĐOÀN KẾT"," ĐỖ BÍ"," ĐỖ CÔNG TƯỜNG"," ĐÔ ĐỐC CHẤN"," ĐÔ ĐỐC LỘC"," ĐÔ ĐỐC LONG"," ĐÔ ĐỐC THỦ"," ĐỖ ĐỨC DỤC "," ĐỖ THỊ TÂM"," ĐỖ THỪA LUÔNG"," ĐỖ THỪA TỰ"," ĐỖ NHUẬN"," ĐỘC LẬP"," ĐƯỜNG C1"," ĐƯỜNG C4"," ĐƯỜNG C4A"," ĐƯỜNG C5"," ĐƯỜNG C6"," ĐƯỜNG C6A"," ĐƯỜNG C8"," ĐƯỜNG CC1"," ĐƯỜNG CC2"," ĐƯỜNG CC3"," ĐƯỜNG CC4"," ĐƯỜNG CC5"," ĐƯỜNG CN1"," ĐƯỜNG CN6"," ĐƯỜNG CN11"," ĐƯỜNG D9"," ĐƯỜNG D14A"," ĐƯỜNG DC1"," ĐƯỜNG DC11"," ĐƯỜNG D10"," ĐƯỜNG D11"," ĐƯỜNG D12"," ĐƯỜNG D13"," ĐƯỜNG D14B"," ĐƯỜNG D15"," ĐƯỜNG D16"," ĐƯỜNG DC3"," ĐƯỜNG DC4"," ĐƯỜNG DC5"," ĐƯỜNG DC7"," ĐƯỜNG DC9"," ĐƯỜNG KÊNH 19/5"," ĐƯỜNG  30/4"," ĐƯỜNG BỜ BAO TÂN THẮNG"," ĐƯỜNG CÂY KEO"," ĐƯỜNG S5"," ĐƯỜNG S1"," ĐƯỜNG S11"," ĐƯỜNG S3"," ĐƯỜNG S7"," ĐƯỜNG S9"," ĐƯỜNG S2 (P. TÂY THẠNH)"," ĐƯỜNG C2 (P. TÂY THẠNH)"," ĐƯỜNG S4 (P. TÂY THẠNH)"," ĐƯỜNG T3 (P. TÂY THẠNH)"," ĐƯỜNG T5 (P. TÂY THẠNH)"," ĐƯỜNG KÊNH NƯỚC ĐEN"," ĐƯỜNG KÊNH TÂN HOÁ"," ĐƯỜNG T1"," ĐƯỜNG T4A"," ĐƯỜNG T4B"," ĐƯỜNG T6"," GÒ DẦU"," HÀN MẶC TỬ"," HIỀN VƯƠNG"," HỒ ĐẮC DI"," HỒ NGỌC CẨN"," HOA BẰNG"," HÒA BÌNH "," HOÀNG NGỌC PHÁCH"," HOÀNG THIỀU HOA"," HOÀNG VĂN HOÈ"," HOÀNG XUÂN HOÀNH"," HOÀNG XUÂN NHỊ"," HUỲNH VĂN CHÍNH"," HUỲNH VĂN MỘT"," HUỲNH VĂN GẤM"," HUỲNH THIỆN LỘC"," ÍCH THIỆN"," KHUÔNG VIỆT"," LÊ CAO LÃNG"," LÊ CẢNH TUÂN"," LÊ KHÔI"," LÊ LÂM"," LÊ LĂNG"," LÊ LIỄU"," LÊ LƯ"," LÊ LỘ"," LÊ ĐẠI"," LÊ NGÃ"," LÊ NIỆM"," LÊ ĐÌNH THÁM"," LÊ ĐÌNH THỤ"," LÊ QUANG CHIỂU"," LÊ QUỐC TRINH"," LÊ SAO"," LÊ SÁT"," LÊ QUÁT"," LÊ THẬN"," LÊ THIỆT"," LÊ THÚC HOẠCH"," LÊ TRỌNG TẤN"," LÊ VĂN PHAN"," LÊ VĨNH HOÀ"," LƯƠNG MINH NGUYỆT"," LƯƠNG ĐẮC BẰNG"," LƯƠNG THẾ VINH"," LƯƠNG TRÚC ĐÀM"," LƯU CHÍ HIẾU"," LŨY BÁN BÍCH"," LÝ THÁI TÔNG"," LÝ THÁNH TÔNG"," LÝ TUỆ"," NGÔ QUYỀN"," NGHIÊM TOẢN"," NGỤY NHƯ KONTUM"," NGUYỄN BÁ TÒNG"," NGUYỄN CHÍCH"," NGUYỄN CỬU ĐÀM"," NGUYỄN  DỮ"," NGUYỄN ĐỖ CUNG"," NGUYỄN HÁO VĨNH"," NGUYỄN HẬU"," NGUYỄN HỮU DẬT"," NGUYỄN HỮU TIẾN"," NGUYỄN LỘ TRẠCH"," NGUYỄN LÝ"," NGUYỄN MINH CHÂU"," NGUYỄN MỸ CA"," NGUYỄN NGHIÊM"," NGUYỄN NGỌC NHỰT"," NGUYỄN NHỮ LÃM"," NGUYỄN SÁNG"," NGUYỄN SƠN"," NGUYỄN SUÝ"," NGUYỄN QUANG DIÊU"," NGUYỄN QUÝ ANH"," NGUYỄN THÁI HỌC"," NGUYỄN THẾ TRUYỆN"," NGUYỄN THIỆU LÂU"," NGUYỄN TRƯỜNG TỘ"," NGUYỄN TRỌNG QUYỀN"," NGUYỄN VĂN DƯỠNG"," NGUYỄN VĂN HUYÊN"," NGUYỄN VĂN NGỌC"," NGUYỄN VĂN SĂNG"," NGUYỄN VĂN TỐ"," NGUYỄN VĂN VỊNH"," NGUYỄN VĂN YẾN"," NGUYỄN XUÂN KHOÁT"," PHẠM NGỌC"," PHẠM NGỌC THẢO"," PHẠM VẤN"," PHẠM VĂN XẢO"," PHẠM QUÝ THÍCH"," PHAN ANH"," PHAN CHU TRINH"," PHAN ĐÌNH PHÙNG"," PHAN VĂN NĂM"," PHỐ CHỢ"," PHÙNG CHÍ KIÊN"," PHÚ THỌ HOÀ"," QUÁCH ĐÌNH BẢO"," QUÁCH VŨ"," QUÁCH HỮU NGHIÊM"," SƠN KỲ"," TÂN HƯƠNG"," TÂN KỲ TÂN QUÝ"," TÂN QUÝ"," TÂN SƠN NHÌ"," TÂN THÀNH"," TÂY SƠN"," TÂY THẠNH"," THẨM MỸ"," THẠCH LAM"," THÀNH CÔNG"," THOẠI NGỌC HẦU"," THỐNG NHẤT"," TÔ HIỆU"," TỰ DO 1"," TỰ QUYẾT"," TRẦN HƯNG ĐẠO"," TRẦN QUANG CƠ"," TRẦN QUANG QUÁ"," TRẦN TẤN"," TRẦN THỦ ĐỘ"," TRẦN VĂN CẨN"," TRẦN VĂN GIÁP"," TRẦN VĂN ƠN"," TRỊNH LỖI"," TRỊNH ĐÌNH THẢO"," TRỊNH ĐÌNH TRỌNG"," TRƯƠNG VĨNH KÝ"," TRƯƠNG VÂN LĨNH"," TRƯỜNG CHINH"," VĂN CAO"," VẠN HẠNH"," VÕ CÔNG TỒN"," VÕ HOÀNH"," VÕ VĂN DŨNG"," VƯỜN LÀI"," VŨ TRỌNG PHỤNG"," YÊN ĐỖ"," Ỷ LAN"," ĐƯỜNG A KHU ADC"," ĐƯỜNG B KHU ADC"," ĐƯỜNG B1"," ĐƯỜNG B2"," ĐƯỜNG B3"," ĐƯỜNG B4"," ĐƯỜNG T8"
        ];
    }
    public static function buildUrlImageFromSiteOriginal($url,$size = 'thumb'){
        if(self::checkImageUrl($url) == 1){
            if(strpos(mb_strtolower($url, 'UTF-8'), 'muaban') !== FALSE){
                //site muaban
                if($size == 'thumb'){
                    return str_replace('thumb-detail','thumb-list',$url);
                }else{
                    return $url;
                }
            }
            if(strpos(mb_strtolower($url, 'UTF-8'), 'chotot') !== FALSE){
                //site choto
                if($size == 'thumb'){
                   return str_replace('small_thumbs','wm_images',$url);
                }else{
                    return $url;
                }
            }
        }
        return null;
    }
    public static function checkImageUrl($url){
        //$url = 'https://cdn.muaban.net/cdn/images/thumb-detail/201607/13/029/4b847ff1eda24726b56e502162868a8f.jpg';
       return $r = preg_match_all('@https?://[^\s]*(\.gif|\.jpg|\.png|\.GIF|\.JPG|\.PNG)[^\s]*@', $url);

    }
    public static function  detectAddress($address){
        $API_KEY = 'AIzaSyCB6pfVpg9zYLejBFwdMJUQ_YrCP3Yu-N4';
        if(empty($address)){
            return null;
        }

        try{
            $coordinates = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?key='.$API_KEY.'&address=' . urlencode($address) . '&sensor=true');
            $coordinates = json_decode($coordinates);

            if(isset($coordinates->results[0]->geometry->location)){
                return [
                    'lat'=>isset($coordinates->results[0]->geometry->location->lat)?$coordinates->results[0]->geometry->location->lat:null,
                    'lng'=>isset($coordinates->results[0]->geometry->location->lng)?$coordinates->results[0]->geometry->location->lng:null
                ];
            }
        }catch (Exception $e){
            Yii::error('detect location: '.$e->getMessage(),'Crawling');
        }

        return null;
    }
    public static function getNowUnixUTC(){
        $time = Yii::$app->db->createCommand('select UNIX_TIMESTAMP() as tsm')->queryOne();
        return (int) $time['tsm'];
    }
    public static function getUnixUTCMinuteAfter($minute=3){
        $time = Yii::$app->db->createCommand('select UNIX_TIMESTAMP(NOW() + INTERVAL :minute MINUTE) as tsm',[
            ':minute'=>$minute
        ])->queryOne();
        return (int) $time['tsm'];
    }
    public static function checkAddress($addressStreet,$position=0){

        $address = explode(' ',$addressStreet);

        if(!empty($address) && isset($address[$position])){
            if($position == 0){
                $condition1 = preg_match('/^[0-9]+$/', $address[$position]); //only number
                $condition2 = preg_match('/[0-9]+[a-zA-Z]/',$address[$position]); // number and character
                $condition3 = explode('/',$address[$position]);
                $condition4 = explode('\\',$address[$position]);
                $condition5 = explode('-',$address[$position]);

                if($condition1 == 0 && $condition2 == 0){
                    if(count($condition3) > 1  || count($condition4 || count($condition5) > 1) > 1){
                        return true;
                    }
                }else{
                    if(strpos(mb_strtolower($addressStreet, 'UTF-8'), 'hẻm') !== FALSE){
                        return false;
                    }
                    if(strpos(mb_strtolower($addressStreet, 'UTF-8'), 'hẽm') !== FALSE){
                        return false;
                    }
                    if(strpos(mb_strtolower($addressStreet, 'UTF-8'), 'thông') !== FALSE){
                        return false;
                    }
                    return true;
                }
                if(isset($address[1])){
                    $condition1 = preg_match('/^[0-9]+$/', $address[1]); //only number
                    $condition2 = preg_match('/[0-9]+[a-zA-Z]/',$address[1]); // number and character
                    $condition3 = explode('/',$address[1]);
                    $condition4 = explode('\\',$address[1]);
                    $condition5 = explode('-',$address[$position]);

                    if($condition1 == 0 && $condition2 == 0){
                        if(count($condition3) > 1  || count($condition4) > 1 || count($condition5) > 1){
                            return true;
                        }
                    }else{

                        if(strpos(mb_strtolower($addressStreet, 'UTF-8'), 'hẻm') !== FALSE){
                            return false;
                        }
                        if(strpos(mb_strtolower($addressStreet, 'UTF-8'), 'hẽm') !== FALSE){
                            return false;
                        }

                        return true;
                    }
                }

            }else{
                $condition1 = preg_match('/^[0-9]+$/', $address[$position]); //only number
                $condition2 = preg_match('/[0-9]+[a-zA-Z]/',$address[$position]); // number and character
                $condition3 = explode('/',$address[$position]);
                $condition4 = explode('\\',$address[$position]);
                $condition5 = explode('-',$address[$position]);

                if($condition1 == 0 && $condition2 == 0){
                    if(count($condition3) > 1  || count($condition4) > 1 || count($condition5) > 1){
                        return true;
                    }
                }else{
                    return true;
                }
            }

        }

        return false;
    }
        public static function checkAddressInString($string){

            $lib = array_merge(self::street_lib_ha_noi(), self::street_lib_ho_chi_minh());

            $address= self::findBeforeWithKeyword($string,$lib);

            if(!empty($address) && count($address) < 5){
                foreach($address as $a){
                    if(self::checkAddress(trim($a)) === true){
                        return true;
                    }
                }
            }

            return false;
        }
    public static function character_lib(){
        return $characters = [
            'chính chủ cần bán','chính chủ bán','liên hệ chủ nhà','liên hệ chính chủ','LHCN',
            'LHCC','chính chủ cho thuê','chính chủ cần cho thuê','số nhà','LH:chính chủ','LH: chính chủ',
            'LH chính chủ','liên hệ: chính chủ','cô','chú','bác'
        ];
    }
    public static function checkCharacter($title,$description){
            $characters = self::character_lib();
            foreach($characters as $c){
                if(strpos(mb_strtolower($title, 'UTF-8'), $c) !== FALSE
                    && strpos(mb_strtolower($title, 'UTF-8'),"chút" ) === FALSE
                    && strpos(mb_strtolower($title, 'UTF-8'),"công" ) === FALSE
                    && strpos(mb_strtolower($title, 'UTF-8'),"bách" ) === FALSE
                    && strpos(mb_strtolower($title, 'UTF-8'),"chúng" ) === FALSE){
                    return true;
                }
                if(strpos(mb_strtolower($description, 'UTF-8'), $c) !== FALSE
                    && strpos(mb_strtolower($description, 'UTF-8'),"chút" ) === FALSE
                    && strpos(mb_strtolower($description, 'UTF-8'),"công" ) === FALSE
                    && strpos(mb_strtolower($description, 'UTF-8'),"bách" ) === FALSE
                    && strpos(mb_strtolower($title, 'UTF-8'),"chúng" ) === FALSE){
                    return true;
                }
            }

//        if(
//            strpos(mb_strtolower($title, 'UTF-8'), 'chính chủ cần bán') !== FALSE
//            || strpos(mb_strtolower($description, 'UTF-8'), 'chính chủ cần bán') !== FALSE
//            || strpos(mb_strtolower($title, 'UTF-8'), 'chính chủ bán') !== FALSE
//
//            || strpos(mb_strtolower($description, 'UTF-8'), 'chính chủ bán') !== FALSE
//            || strpos(mb_strtolower($title, 'UTF-8'), 'liên hệ chủ nhà') !== FALSE
//            || strpos(mb_strtolower($description, 'UTF-8'), 'liên hệ chủ nhà') !== FALSE
//            || strpos(mb_strtolower($title, 'UTF-8'), 'LHCC') !== FALSE
//            || strpos(mb_strtolower($description, 'UTF-8'), 'LHCC') !== FALSE
//            || strpos(mb_strtolower($title, 'UTF-8'), 'liên hệ chủ nhà') !== FALSE
//            || strpos(mb_strtolower($description, 'UTF-8'), 'liên hệ chủ nhà') !== FALSE
//            || strpos(mb_strtolower($title, 'UTF-8'), 'liên hệ chính chủ') !== FALSE
//            || strpos(mb_strtolower($description, 'UTF-8'), 'liên hệ chính chủ') !== FALSE
//            || strpos(mb_strtolower($title, 'UTF-8'), 'LHCN') !== FALSE
//            || strpos(mb_strtolower($description, 'UTF-8'), 'LHCN') !== FALSE
//
//
//            || strpos(mb_strtolower($title, 'UTF-8'), 'chính chủ cho thuê') !== FALSE
//            || strpos(mb_strtolower($description, 'UTF-8'), 'chính chủ cho thuê') !== FALSE
//            || strpos(mb_strtolower($title, 'UTF-8'), 'chính chủ cần cho thuê') !== FALSE
//            || strpos(mb_strtolower($description, 'UTF-8'), 'chính chủ cần cho thuê') !== FALSE
//
//            || strpos(mb_strtolower($description, 'UTF-8'), 'số nhà') !== FALSE
//
//            || strpos(mb_strtolower($description, 'UTF-8'), 'LH:chính chủ') !== FALSE
//            || strpos(mb_strtolower($description, 'UTF-8'), 'LH: chính chủ') !== FALSE
//            || strpos(mb_strtolower($description, 'UTF-8'), 'LH chính chủ') !== FALSE
//            || strpos(mb_strtolower($description, 'UTF-8'), 'liên hệ: chính chủ') !== FALSE
//
//        )
//        {
//            return true;
//        }
//        if( strpos(mb_strtolower($description, 'UTF-8'), 'chính chủ')  !== FALSE
//            && ( strpos(mb_strtolower($description, 'UTF-8'), 'sổ hồng chính chủ') === FALSE ||
//                strpos  (mb_strtolower($description, 'UTF-8'), 'chính chủ cần tiền') === FALSE  ) ){
//            return true;
//        }
        return false;
    }
     public static function validateIsSelfRealItem($title,$description,$address){

        if(self::checkAddress($address)){
            return true;
        }
         if(self::checkAddressInString($title)){
             return true;
         }

        if(self::checkAddressInString($description)){
            return true;
        }
         foreach(UtilHelper::targetAddressReal() as $character=>$position){
             if(self::checkNumberWithCharacter($description,[$character],$position)){
                 return true;
             }
         }
       return self::checkCharacter($title,$description);
   }
    public static function targetAddressReal(){
        return [
            "số"=>3,
            "ngõ"=>3,
            "ngỏ"=>3,
            "mặt phố"=>4
        ];
    }
    public static function showAddressReal($description){
        foreach(UtilHelper::targetAddressReal() as $character=>$position){

            $number = \backend\commons\helpers\UtilHelper::findBeforeWithKeyword($description,[$character],true) ;

            if(!empty($number)){
                foreach($number as $n){
                    $n = trim($n,'-');
                    $n = trim($n,',');

                    if(\backend\commons\helpers\UtilHelper::checkAddress(trim($n),$position)){
                        $description = str_replace(trim($n),"<strong>".trim($n)."</strong>",$description);
                        return $description;
                    }
                }
            }
        }
        return $description;
    }

    public static function checkNumberWithCharacter($description,$array_character,$position=3){
        $number = UtilHelper::findBeforeWithKeyword($description,$array_character,true) ;

        if(!empty($number)){
            foreach($number as $n){
                $n = trim($n,'-');
                $n = trim($n,',');

                if(self::checkAddress(trim($n),$position)){
                    return true;
                }
            }
        }
        return false;
    }

public  static  function _utf8_decode($string)
    {
        $tmp = $string;
        $count = 0;
        while (mb_detect_encoding($tmp)=="UTF-8")
        {
            $tmp = utf8_decode($tmp);
            $count++;
        }

        for ($i = 0; $i < $count-1 ; $i++)
        {
            $string = utf8_decode($string);

        }
        return $string;

    }

    public static  function decode($utf8){
        $iso88591_1 = utf8_decode($utf8);
        $iso88591_2 = iconv('UTF-8', 'ISO-8859-1', $utf8);
        return $iso88591_2 = mb_convert_encoding($utf8, 'ISO-8859-1', 'UTF-8');
    }
    public static function create_slug($str,$options = array()) {
        // Make sure string is in UTF-8 and strip invalid UTF-8 characters
        $str = mb_convert_encoding((string)$str, 'UTF-8', mb_list_encodings());

        $defaults = array(
            'delimiter' => '-',
            'limit' => null,
            'lowercase' => true,
            'replacements' => array(),
            'transliterate' => false,
        );

        // Merge options
        $options = array_merge($defaults, $options);

        $char_map = array(
            // Latin
            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
            'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
            'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
            'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
            'ß' => 'ss',
            'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
            'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
            'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
            'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
            'ÿ' => 'y',
            // Latin symbols
            '©' => '(c)',
            // Greek
            'Α' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'H', 'Θ' => '8',
            'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M', 'Ν' => 'N', 'Ξ' => '3', 'Ο' => 'O', 'Π' => 'P',
            'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'W',
            'Ά' => 'A', 'Έ' => 'E', 'Ί' => 'I', 'Ό' => 'O', 'Ύ' => 'Y', 'Ή' => 'H', 'Ώ' => 'W', 'Ϊ' => 'I',
            'Ϋ' => 'Y',
            'α' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'h', 'θ' => '8',
            'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => '3', 'ο' => 'o', 'π' => 'p',
            'ρ' => 'r', 'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'w',
            'ά' => 'a', 'έ' => 'e', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ή' => 'h', 'ώ' => 'w', 'ς' => 's',
            'ϊ' => 'i', 'ΰ' => 'y', 'ϋ' => 'y', 'ΐ' => 'i',
            // Turkish
            'Ş' => 'S', 'İ' => 'I', 'Ç' => 'C', 'Ü' => 'U', 'Ö' => 'O', 'Ğ' => 'G',
            'ş' => 's', 'ı' => 'i', 'ç' => 'c', 'ü' => 'u', 'ö' => 'o', 'ğ' => 'g',
            // Russian
            'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'Yo', 'Ж' => 'Zh',
            'З' => 'Z', 'И' => 'I', 'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
            'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sh', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'Yu',
            'Я' => 'Ya',
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh',
            'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
            'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu',
            'я' => 'ya',
            // Ukrainian
            'Є' => 'Ye', 'І' => 'I', 'Ї' => 'Yi', 'Ґ' => 'G',
            'є' => 'ye', 'і' => 'i', 'ї' => 'yi', 'ґ' => 'g',
            // Czech
            'Č' => 'C', 'Ď' => 'D', 'Ě' => 'E', 'Ň' => 'N', 'Ř' => 'R', 'Š' => 'S', 'Ť' => 'T', 'Ů' => 'U',
            'Ž' => 'Z',
            'č' => 'c', 'ď' => 'd', 'ě' => 'e', 'ň' => 'n', 'ř' => 'r', 'š' => 's', 'ť' => 't', 'ů' => 'u',
            'ž' => 'z',
            // Polish
            'Ą' => 'A', 'Ć' => 'C', 'Ę' => 'e', 'Ł' => 'L', 'Ń' => 'N', 'Ó' => 'o', 'Ś' => 'S', 'Ź' => 'Z',
            'Ż' => 'Z',
            'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z',
            'ż' => 'z',
            // Latvian
            'Ā' => 'A', 'Č' => 'C', 'Ē' => 'E', 'Ģ' => 'G', 'Ī' => 'i', 'Ķ' => 'k', 'Ļ' => 'L', 'Ņ' => 'N',
            'Š' => 'S', 'Ū' => 'u', 'Ž' => 'Z',
            'ā' => 'a', 'č' => 'c', 'ē' => 'e', 'ģ' => 'g', 'ī' => 'i', 'ķ' => 'k', 'ļ' => 'l', 'ņ' => 'n',
            'š' => 's', 'ū' => 'u', 'ž' => 'z'
        );

        // Make custom replacements
        $str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

        // Transliterate characters to ASCII
        if ($options['transliterate']) {
            $str = str_replace(array_keys($char_map), $char_map, $str);
        }

        // Replace non-alphanumeric characters with our delimiter
        $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);

        // Remove duplicate delimiters
        $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);

        // Truncate slug to max. characters
        $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

        // Remove delimiter from ends
        $str = trim($str, $options['delimiter']);

        return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
    }
    public static function commonIsImageUrl($content)
    {
        $ext=substr($content,strlen($content)-4,4);
        if($ext=='.gif' || $ext=='.jpg'|| $ext=='.png'){
            return true;
        }
        return false;
    }

    public static function downloadImageFromLink($path,$url_image,$resize=true){
        $path = $path.'/';
        if(!self::commonIsImageUrl($url_image)){
            return null;
        }
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        try{
            $img_file = file_get_contents ( $url_image , false, stream_context_create($arrContextOptions));

            $filename =Yii::$app->security->generateRandomString() . time() . '.jpg';

            file_put_contents ($path.$filename, $img_file );

            if($resize){
                self :: resizeImg($path, $filename, Yii::$app->params['thumbName'].$filename,
                    Yii::$app->params['sizeThumbImg'],  Yii::$app->params['sizeThumbImg']);
            }

            return  $filename;
        }catch (\yii\base\ErrorException $e){
            return null;
        }

    }

    /**
     * @param $path
     * @param $fileName
     * @param string $file
     * @param bool|TRUE $resize
     * @return UploadAttachmentFileForm|bool
     * @throws \yii\base\Exception
     */
    public static function uploadAttachmentFile($path,$fileName=null,$file='files',$resize = true,$imageRemove=[]){
        $path .='/';

        $dirImage =  is_dir($path);

        if(!$dirImage ){
            $dirImage = BaseFileHelper::createDirectory($path,0777);
        }

        if(!$dirImage){
           // $this->sendResponse(200,false,$this->builtErrorCode(96));
        }

        $form = new UploadAttachmentFileForm();

        $files   =  UploadedFile::getInstancesByName($file);

        if(!empty($imageRemove)){
            foreach($imageRemove as $item){
                unset($files[$item]);
            }
        }

        if(is_array($files) && !empty($files)) {

            $form->attachmentFiles = $files;
            $form->path = $path;
            $form->resize = $resize;
            $form->fileName = $fileName;


            if ($form->save()) {
                return $form;
            } else {
                //$this->getFormError($form);
            }
        }else{
            return null;
        }
    }


    /**
     * @param $token
     * @param array $notification
     * @return mixed
     */
    public static function sendNotification($token,$notification=[]) {

        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';


        $fields = [
            'to'=>$token,
            'notification'=>$notification,
            'time_to_live' => 108,
            'priority'=>'high'
        ];


        $headers = array(
            'Authorization: key='.Yii::$app->params['FIRE_BASE_SERVER_KEY'],
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);
        return $result;
    }


    public static function getDataUrlByGetMethod($url){

        $curl = curl_init();

        $options = array(
            CURLOPT_URL            => $url,
            CURLOPT_HEADER         => false,
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_RETURNTRANSFER => true,
        );

        curl_setopt_array($curl, $options);

        $response = curl_exec($curl);

        if ($error = curl_error($curl))
        {
            throw new \Exception('CURL Error: ' . $error);
        }

        curl_close($curl);

        return $response;

    }

    /**
     *
     * @param string $path
     * @param string $file_name
     * @param string $new_file_name
     * @param string $width
     * @param string $height
     */
    public static function resizeImg($path, $file_name, $new_file_name, $width = false, $height = false) {

        $width = $width && is_numeric ( $width ) ? $width : Yii::$app->params ['mainImg'];

        $height = $height && is_numeric ( $height ) ? $height : Yii::$app->params ['mainImg'];

        $image = new SmartImage( $path . $file_name );

        $image->resize ( $width, $height);

        $image->saveImage ( $path . $new_file_name );
    }

    public static function builtBreadcrumb($level,$self,$option=[]){

        if($level === 1) {
            $self->params['breadcrumbs'][] = ['label' =>$self->title, 'template' => "<li><b>{link}</b></li>\n"];
        }elseif($level === 2){
            $self->params['breadcrumbs'][] = ['label' =>(isset($option['titleLevel1']))?$option['titleLevel1']:null, 'url' =>(isset($option['urlLevel1']))?$option['urlLevel1']:null];
            $self->params['breadcrumbs'][] = ['label' =>(isset($option['objectName']) && !is_null($option['objectName']))?$option['objectName']:$self->title,'template' => "<li><i class='fa fa-angle-right'></i><b>{link}</b></li>\n"];
        }elseif($level ===3 ){
            $self->params['breadcrumbs'][] = ['label' =>(isset($option['titleLevel1']))?$option['titleLevel1']:null, 'url' => (isset($option['urlLevel1']))?$option['urlLevel1']:null];
            $self->params['breadcrumbs'][] = ['label' =>(isset($option['objectName']) && !is_null($option['objectName']))?$option['objectName']:null, 'url' =>(isset($option['urlLevel2']))?$option['urlLevel2']:null,'template' => "<li><i class='fa fa-angle-right'></i>{link}</li>\n"];
            $self->params['breadcrumbs'][] = ['label' =>(isset($option['titleLevel2']))?$option['titleLevel2']:null,'template' => "<li><i class='fa fa-angle-right'></i><b>{link}</b></li>\n"];
        }

    }

    /**
     * @param null string $role : Role name to check
     * @param null AuthItem Entity Model Object $model : Permission model to check with role
     * @return bool
     */
    public static function checkMatrixPermission($role=null,$model=null){
        $access = FALSE;
        if(is_null($role))
            return $access;
        if(is_null($model))
            return $access;

        $permissions = Yii::$app->authManager->getPermissionsByRole($role);
        if(is_array($permissions) && !empty($permissions)){
            foreach($permissions as $_permission){
                $permissionName = strtolower($_permission->name);
                $modelName = strtolower($model->name);
                if($_permission->name === $model->name) {
                    $access = TRUE;
                }
                else{
                    if(\preg_match("/^.$permissionName.[a-zA-Z.]*$/",$modelName) !== 0){
                        $access = TRUE;
                    }
                }
            }
        }
        return $access;
    }

    /**
     * @param $model
     * @return array|null
     */
    public static function generateDataPermissionBelongMoudle($model){
        $option = null;
        $permissionName = strtolower($model->name);
        $modules = \Yii::$app->modules;
        if(is_array($modules) && !empty($modules)){
            foreach($modules as $key=>$_module){
                $moduleName = strtolower($key);
                if($moduleName === $permissionName){
                    return ['data-key'=>'m-'.$moduleName,'id'=>$moduleName];
                }elseif(\preg_match("/^".$moduleName."[-a-zA-Z.]*$/",$permissionName) !== 0){
                    return ['data-key'=>'c-'.$moduleName,'id'=>null];
                }
            }
        }
        $explodePermission = explode('-',$permissionName);

        if($explodePermission[0] === 'v1'){
            return ['data-key'=>'c-'.$explodePermission[0],'id'=>null];
        }

        return null;
    }
    /**
     * @param null $phone: Phone number is revert
     * @param null $regionCode: Region code of phone number
     * @return mixed|null
     */
    public static function revertPhoneNumber($phone=null,$regionCode=null){
        if(is_null($phone))
            return null;
        if(is_null($regionCode))
            return null;

        return str_replace($regionCode,'',$phone);
    }
    /**
     * @param $roleId
     * @return string
     */
    public static function getRoleName($roleId){
        $role = \backend\modules\authorization\models\entity\AuthItem::findOne(['role_id'=>$roleId]);
        return ($role)?$role->name:"(None)";
    }
    /**
     * @param $modelClass is Entity model
     * @param array $options
     * @return null|string
     */
    public static function summaryChangedAttributes($modelClass,$options=[],$outSideAttribute=[]){
        if(!$modelClass)
            return null;

        $attributes = $modelClass->getChangedAttributes();

        if(empty($attributes))
            return null;

        $header = isset($options['header']) ? $options['header'] : '<p>' . Yii::t('yii', 'Successful updated attributes:') . '</p>';
        $footer = isset($options['footer']) ? $options['footer'] : '';
        $encode = !isset($options['encode']) || $options['encode'] !== false;
        unset($options['header'], $options['footer'], $options['encode']);

        $lines = [];
        if (!is_array($attributes)) {
            $attributes = [$attributes];
        }
       // $attributes = ['avatar'];

        foreach ($attributes as $attribute=>$value) {
            $lines[] = $encode ? Html::encode($modelClass->getAttributeLabel($attribute)) : $modelClass->getAttributeLabel($attribute);
        }

        if(is_array($outSideAttribute) && !empty($outSideAttribute)){
            foreach($outSideAttribute as $attribute=>$value){
                $lines[] = $encode ? Html::encode($attribute) :$attribute;
            }
        }
        if (empty($lines)) {
            $content = "<ul></ul>";
            $options['style'] = isset($options['style']) ? rtrim($options['style'], ';') . '; display:none' : 'display:none';
        } else {
            $content = "<ul><li>" . implode("</li>\n<li>", $lines) . "</li></ul>";
        }
        return Html::tag('div', $header . $content . $footer, $options);
    }

    /**
     * @return array
     */
    public static function getConditionDropListRole(){
        $notInRole = $notInRole=[FitRoadUser::FIX_ROAD_USER_ROOT,FitRoadUser::FIX_ROAD_ADMIN];
        if(!\Yii::$app->user){
            return $notInRole;
        }
        (\Yii::$app->user->can('authorization'))?$notInRole=[FitRoadUser::FIX_ROAD_USER_ROOT]:null;

        return $notInRole;
    }

    /**
     * @param $finder
     * @return string
     */
    public static function builtPathImages($finder){
        return Yii::$app->basePath.Yii::$app->params['pathImages'.$finder];
    }
	
	static function resMulti($name='restaurant_id', $values=array()){		
		$model = UtilHelper::listRestaurants();
		$output = '<select multiple="multiple" class="multi-select" id="restaurant_id" name="restaurant[]" >';
		if(!empty($default)) {
			$output .= '<option value=""></option>';
		}
		if($model){			
			foreach($model AS $item){				
				$selected = "";
				foreach($values as $value){
					if($item['id'] == $value){
						$selected = " selected ";
						break;
					}
				}
				$output .= '<option class="popovers" data-original-title="'.$item['name'].'" data-container="body" data-placement="top" data-trigger="hover" data-content="'.$item['address'].'" value="'.$item['id'].'" '.$selected.'>'.$item['name'].'</option>';
			}
		}						                 					                 
	    $output .= '</select>';	
	    return $output;
	}

    public static function buildClassForCalories($calories){
            switch($calories){
                case $calories > 100:
                    return 'badge-success';
                break;
                case $calories > 300:
                    return 'badge-warning';
                break;
                case $calories > 500:
                    return 'bade-danger';
                break;
                default:  return 'bade-info';

            }
    }

    /**
     * @param $finder
     * @param $fileName
     * @return string
     */
    public  static function builtUrlImages($finder,$fileName,$sizeName = null){

        if(!$fileName){
            return null;
        }

        if($sizeName === null){

            $baseUrl = Yii::getAlias('@assetsImagesUploadUrl').'/'.$finder;

        }
        elseif($sizeName == Yii::$app->params['thumbName']) {

            $baseUrl = Yii::getAlias('@assetsImagesUploadUrl').'/'. $finder . Yii::$app->params['thumbName'];
        }
        elseif($sizeName == Yii::$app->params['normalName']){

            $baseUrl = Yii::getAlias('@assetsImagesUploadUrl').'/'
                .$finder.Yii::$app->params['normalName'];

        }

        return (string) $baseUrl.$fileName;
    }
    /**
     * trim up to 30 characters
     * @param string $str the string to shorten
     * @param int $length (optional) the max string length to return
     * @return string the shortened string
     */

    public static function shorten($text, $maxchars = 0, $def = null) {

        $newtext = self::safeStrip($text);
        $strlen = strlen($newtext);

        if ($maxchars > 0 && $maxchars < $strlen) {
            $text = substr($newtext, 0, $maxchars);

            $text1 = substr($text, 0, strripos($text, ' ', 0));
            if (strlen($text1) == 0) {
                $text1 = $text;
            }
            if ($def == 'none_ext') {
                $text = $text1;
            } else {
                $text = $text1 . "...";
            }

        } else {
            $text = self::safeStrip($text);
        }

        return $text;
    }

    /**
     * Strips tags without removing possible white space between words.
     *
     * @param string String to strip tags from.
     */
   public static function  safeStrip($text) {

        $text = preg_replace('/</', ' <', $text);
        $text = preg_replace('/>/', '> ', $text);
        $desc = strip_tags($text);
        $desc = preg_replace('/  /', ' ', $desc);

        return $desc;
    }

    public static function getCategoriesQueryToCrawl($siteName){
        switch($siteName){
            case 'bat-dong-san':
                $cate = SiteCrawlingHref::findAll(['site_crawling_id'=>1]);
                return ArrayHelper::map($cate,'name','href');
                //return Yii::$app->params['bat-dong-san']['categoriesQuery'];
            break;
            case 'mua-ban':
                $cate = SiteCrawlingHref::findAll(['site_crawling_id'=>2]);
                return ArrayHelper::map($cate,'name','href');
                //return Yii::$app->params['mua-ban']['categoriesQuery'];
            break;
        }
    }

     public static function buildParamsCrawlData($site){
        $params = [];
        switch($site){
            case 'bat-dong-san':
                $params['baseUrl'] = 'http://batdongsan.com.vn/';
                $params['queryStringPaging'] = '/p';
                break;
            case 'mua-ban':
                $params['baseUrl'] = 'https://muaban.net/';
                $params['queryStringPaging'] = '?cp=';
                break;
        }
        return $params;
    }

    public static function BuildSelectedPagingSelect2Options($pageSelected=[],$queryStingPaging=null){
        $options = [];

        foreach($pageSelected as $page){
            $options[] = [
                'id'=>$page,
                'text'=> Yii::t('backend','page').' '.str_replace($queryStingPaging,'',$page)
            ];
        }
        return $options;
    }
    public static function convertFromNumberToStringOfPrice($price){
        if(strpos($price, 'triệu') || strpos($price, 'tỷ')) {
            return $price;
        }

        $price = str_replace('đ','',$price);
        $billion = null;
        $unit = null;
        $million = null;
        try{
            $priceArray = explode('.',$price);

        }catch (\ErrorException $e){
            $priceArray = [];
        }
        if(is_array($priceArray) && !empty($priceArray)){
            if(count($priceArray) == 5){
                $unitBillion = 'tỷ';
                $billion = (int) $priceArray[0];
                $million = (int) $priceArray[1];
                $unitMillion = 'triệu';

                return $billion.' '.$unitBillion.' '.$million.' '.$unitMillion;
            }
            if(count($priceArray) == 4){
                $unit = 'triệu';
                $million = (int) $priceArray[0];
                return $million.' '.$unit;
            }
        }


    }
    public static function convertPriceStringToNumber($price){
//        $price = '11.5 tỷ';
        $oneBillionNumber = 1000000000;
        $oneMillionNumber = 1000000;
     //   $price = ltrim('.',$price);
        $millionString = null;
        $billionString = null;
        $posMillion = strpos($price, 'trăm triệu');
        $posBillion = strpos($price, 'tỷ');
        $formatBM = explode(' ',$price);
        $formatNormal =  explode('.',$price);

        if(count($formatNormal) > 1 ){
            $priceNumber = '';

            $billionString = strpos($price, 'tỷ');

            if($millionString === false){
                $millionString = strpos($price, 'triệu/m2');
            }

            if($millionString === false){
                $millionString = strpos(mb_strtolower($price, 'UTF-8'),"triệu/m²" );
            }

            $price = str_replace('đ','',$price);
            $formatNormal =  explode('.',$price);
            foreach($formatNormal as $number){
                $priceNumber .= $number;
            }

            $priceNumber = (int)trim($priceNumber);

            if($millionString !== false){
                $priceNumber = 0;
                $priceNumber =  $formatNormal[0] * $oneMillionNumber;
                $priceNumber += $formatNormal[1] *  100000;
            }
            if($billionString !== false){
                $priceNumber = 0;
                $priceNumber =  $formatNormal[0] * $oneBillionNumber;
                $priceNumber += $formatNormal[1] *  $oneMillionNumber * 100;
            }

        }else{
            if($posBillion !== false){
                $bilion = 0;
                $million = 0;
                $posDot = strpos($price, '.');
                if($posDot){
                    $formatDot = explode('.',$price);
                    if(count($formatDot) > 1){
                        $bilion =  $formatNormal[0] * $oneBillionNumber;

                        $bilion += $formatNormal[1] *  100000000;
                    }

                }else{
                    $bilion = (int)$formatBM[0];
                    $million = (isset($formatBM[2]))?(int)$formatBM[2]:0;
                }

                if($posMillion !== false){
                    $million =  $million * 100;
                }

                $bilion = $oneBillionNumber * $bilion;
                $million = $oneMillionNumber * $million;

                $priceNumber = $bilion +$million;

            }else if($posMillion !== false){
                $million = (int)$formatBM[0];
                $million =  $million * 100;
                $priceNumber = $oneMillionNumber * $million ;
            }else{
                $million = (int)$formatBM[0];
                $priceNumber = $oneMillionNumber * $million;
            }
        }
    return $priceNumber;
    }
        public static function  parseAddressWhenSearchingOnAPIs($query,$string){
            $strings = [];
            $conditions = [];
            $string = HtmlPurifier::process(trim($string));
            $string = mb_strtolower($string, 'UTF-8');
            $detectString = explode(',',$string);

            if(!empty($detectString)){
                foreach($detectString as $add){

                }
            }

            if( strpos(mb_strtolower($string, 'UTF-8'), 'bất kỳ') !== false){
                $string = str_replace('bất kỳ','',$string);

            }else {
                $searchingString = $string;
                $string = str_replace('quận', '', $string);
                $string = str_replace('huyện', '', $string);
            }
            $searchingAddress = explode(',',$string);

            if(!empty($searchingAddress)){

                $city =   (isset($searchingAddress[1]) && !empty($searchingAddress[1]))?$searchingAddress[1]:null;

                $province = (isset($searchingAddress[0]) && !empty($searchingAddress[0]))?$searchingAddress[0]:null;
                $province = trim($province);
//                if($city == 'hồ chí minh' || $city == 'hcm' || $city == 'ho chi minh'|| $city == 'tp.ho chi minh' || $city == 'tp ho chi minh'||
//                    $city == 'tp.hồ chí minh' ||  $city == 'tp hồ chí minh'  ){
//
//                   $query->andWhere("MATCH (`address`) AGAINST ('\"hồ chí minh\"' IN BOOLEAN MODE) or `real_estate_item`.`description` like '%hồ chí minh%'");
//
//                }else if($city == 'hà nội' || $city == 'hn' || $city == 'ha noi'|| $city == 'tp.ha noi' || $city == 'tp ha noi'||
//                    $city == 'tp.hà nội' ||  $city == 'tp hà nội'  ){
//                    $query->andWhere("MATCH (`address`) AGAINST ('\"hà nội\"' IN BOOLEAN MODE) ");
//                }elseif($city == 'an giang'){
//                    $query->andWhere("MATCH (`address`) AGAINST ('\"AG\"' IN BOOLEAN MODE)");
//                }
//                else

                if(!empty($city) && empty($province)){
                    $query->andWhere("MATCH (`address`) AGAINST ('\"$city\"' IN BOOLEAN MODE)");
                }
                if(!empty($province)){
                    if($province == 'quận 1' || $province == 'quận nhất' || $province == 'quận một'){

                        $query->andWhere("MATCH (`real_estate_item`.`address`) AGAINST ('\"quận nhất\"' IN BOOLEAN MODE) or MATCH (`real_estate_item`.`address`) AGAINST ('\"quận 1\"' IN BOOLEAN MODE) or MATCH (`real_estate_item`.`address`) AGAINST ('\"quận một\"'IN BOOLEAN MODE)");

                    }else{
                        $query->andWhere("MATCH (`address`) AGAINST ('\" $province $city\"' IN BOOLEAN MODE) ");
//                        $query->andFilterWhere([
//                            'or',
//                            ['like','real_estate_item.address',$province],
//                            //['like','real_estate_item.description',$province],
//                        ]);
//                        $query->andWhere('MATCH (`real_estate_item`.`address`) AGAINST ("'.$province.'" IN BOOLEAN MODE) or `real_estate_item`.`description` like "%'.$province.'%"');

                    }
                }
            }
            return $query;
        }
        public static function parseAddressWhenSearching($query,$string){
            $string = str_replace('+',' ',$string);
            $strings = [];
            $conditions = [];
            $string = HtmlPurifier::process(trim($string));
            $string = mb_strtolower($string, 'UTF-8');
            $detectString = explode(',',$string);

            if(!empty($detectString)){
                foreach($detectString as $add){

                }
            }


            if( strpos(mb_strtolower($string, 'UTF-8'), 'bất kỳ') !== false){
                $string = str_replace('bất kỳ','',$string);

            }else {
                $searchingString = $string;
                $string = str_replace('quận', '', $string);
                $string = str_replace('huyện', '', $string);
            }
            $searchingAddress = explode(',',$string);

            if(!empty($searchingAddress)){

                $city =  (isset($searchingAddress[0]) && !empty($searchingAddress[0]))?$searchingAddress[0]:null;

                $province = (isset($searchingAddress[1]) && !empty($searchingAddress[1]))?$searchingAddress[1]:null;
                $province = trim($province);

//                if($city == 'hồ chí minh' || $city == 'hcm' || $city == 'ho chi minh'|| $city == 'tp.ho chi minh' || $city == 'tp ho chi minh'||
//                    $city == 'tp.hồ chí minh' ||  $city == 'tp hồ chí minh'  ){
//
//                   $query->andWhere("MATCH (`address`) AGAINST ('\"hồ chí minh\"' IN BOOLEAN MODE) or `real_estate_item`.`description` like '%hồ chí minh%'");
//
//                }else if($city == 'hà nội' || $city == 'hn' || $city == 'ha noi'|| $city == 'tp.ha noi' || $city == 'tp ha noi'||
//                    $city == 'tp.hà nội' ||  $city == 'tp hà nội'  ){
//                    $query->andWhere("MATCH (`address`) AGAINST ('\"hà nội\"' IN BOOLEAN MODE) ");
//                }elseif($city == 'an giang'){
//                    $query->andWhere("MATCH (`address`) AGAINST ('\"AG\"' IN BOOLEAN MODE)");
//                }
//                else

                if(!empty($city) && empty($province)){
                    if( strpos(mb_strtolower($string, 'UTF-8'), 'hồ chí minh') !== false){
                        $query->andWhere("MATCH (`address`) AGAINST ('\"hồ chí minh\"' IN BOOLEAN MODE) or
                        MATCH (`address`) AGAINST ('\"TP.HCM\"' IN BOOLEAN MODE)");
                    }else{
                        $query->andWhere("MATCH (`address`) AGAINST ('\"$city\"' IN BOOLEAN MODE)");
                    }

                }
                if(!empty($province)){
                    if($province == 'quận 1' || $province == 'quận nhất' || $province == 'quận một'){

                        $query->andWhere("MATCH (`real_estate_item`.`address`) AGAINST ('\"quận nhất\"' IN BOOLEAN MODE) or MATCH (`real_estate_item`.`address`) AGAINST ('\"quận 1\"' IN BOOLEAN MODE) or MATCH (`real_estate_item`.`address`) AGAINST ('\"quận một\"'IN BOOLEAN MODE)");

                    }else{

                        if( strpos(mb_strtolower($string, 'UTF-8'), 'hồ chí minh') !== false){
                            $query->andWhere("MATCH (`address`) AGAINST ('\"$province hồ chí minh\"' IN BOOLEAN MODE) or
                        MATCH (`address`) AGAINST ('\"$province TP.HCM\"' IN BOOLEAN MODE)");
                        }else{
                            $query->andWhere("MATCH (`address`) AGAINST ('\" $province $city\"' IN BOOLEAN MODE) ");
                        }


                    }
                }
            }
        return $query;
    }

    public static function getCategoryName($category_id){
        $relation =RealEstateCategoryRelation::findOne(['category_children_id'=>$category_id]);
        if($relation){
            return $relation->categoryParent->name;
        }
        return null;

    }

    public static function verifyRePostRealty($validate){
        $lat = null;
        $lng = null;

        $model =  RealEstateItem::findOne(['id'=>isset($validate->real_estate_item_id)?$validate->real_estate_item_id:0]);

        if($model !== null){
            $repost_date_timestamp = UtilHelper::getNowUnixUTC();

            $itemDate = \Yii::$app->formatter->asDate($model->created_at,'Y-M-d');
            $now = \Yii::$app->formatter->asDate($repost_date_timestamp,'Y-M-d');
            $repostDate = \Yii::$app->formatter->asDate($model->repost_date,'Y-M-d');

            if($itemDate !== $now && $now !== $repostDate ){
                $model->repost_date  = $repost_date_timestamp;
                $model->is_new  = 0;
                $model->save(false);

                UtilHelper::trackingRePostDate($model,$repost_date_timestamp,false);

                return $model;
            }
            //get location if null
//           if(empty($model->latitude) || empty($model->longitude)){
//               $location = UtilHelper::detectAddress($model->address);
//
//               if(!empty($location)){
//                   $lat = isset($location['lat'])?$location['lat']:null;
//                   $lng = isset($location['lng'])?$location['lng']:null;
//
//                   $model->latitude = $lat;
//                   $model->longitude = $lng;
//                   $model->save(false);
//               }
//           }

        }
    }
    
    public static function clearSpecialCharacter($description =null){
        $description = str_replace('🏢','',$description);
        $description = str_replace('🏪','',$description);

        $description = str_replace('🔧','',$description);

        $description = str_replace('🏩','',$description);

        $description  = str_replace('🏦','',$description);

        $description = str_replace('Ⓜ','',$description);

        $description = str_replace('🏡','',$description);


        $description = str_replace('🏡','',$description);
        $description = str_replace('❤️','',$description);
        $description = str_replace('🌳','',$description);
        $description = str_replace('🌲','',$description);
        $description = str_replace('🌴','',$description);
        $description = str_replace('🏃','',$description);

        $description = str_replace('🏠','',$description);
        $description = str_replace('🏡','',$description);
        $description = str_replace('🚗','',$description);
        $description = str_replace('🚲','',$description);
        $description = str_replace('👨','',$description);
        $description = str_replace('🌞','',$description);

        $description = str_replace('🌸','',$description);
        $description = str_replace('🌹','',$description);
        $description = str_replace('🌳','',$description);
        $description = str_replace('💰','',$description);
        $description = str_replace('🏥','',$description);


        $description = str_replace('🎯','',$description);
        $description = str_replace('💒','',$description);
        $description = str_replace('📞','',$description);
        $description = str_replace('💲','',$description);
        $description = str_replace('📞','',$description);

        $description = str_replace('🎉','',$description);
        $description = str_replace('🎉','',$description);
        $description = str_replace(' 🎉🎉','',$description);
        $description = str_replace(' 🎉','',$description);
        $description = str_replace('💎 ','',$description);
        $description = str_replace('🔻','',$description);

        $description = str_replace('🔻','',$description);
        $description = str_replace('😀','',$description);
        $description = str_replace('😀 ','',$description);
        $description = str_replace(' 😀','',$description);

        $description = str_replace('🎁','',$description);
        $description = str_replace(' 🎁','',$description);
        $description = str_replace('🎁 ','',$description);
        $description = str_replace('🍀','',$description);
        $description = str_replace('☀','',$description);
        $description = str_replace('⚠','',$description);
        $description = str_replace('🎼','',$description);
        $description = str_replace('👉👉','',$description);
        $description = str_replace('.👉','',$description);
        $description = str_replace(':👉','',$description);
        $description = str_replace('👉','',$description);
        $description = str_replace('.🔶','',$description);
        $description = str_replace('📢 ','',$description);
        $description = str_replace('🔶','',$description);
        $description = str_replace(' 🔶 ','',$description);
        $description = str_replace('💥💥','',$description);
        $description = str_replace('🚩🚩','',$description);
        $description = str_replace('🔔🔔','',$description);
        $description = str_replace('💸💸','',$description);
        $description = str_replace('☎','',$description);
        $description = str_replace('😭','',$description);
        $description = str_replace('👍🏻','',$description);
        $description = str_replace('😄😄','',$description);
        $description=  mb_convert_encoding($description, 'UTF-8', 'UTF-8');
        return $description;
    }
    public static function countRealtyOfPhoneNumber($phone=0){
        return  count(RealEstateItem::find()->select('id')->andOnCondition(['phone_contact'=>$phone])->all());
    }
    public static function trackingRePostDate($model,$unixTime,$isNew = true){
        $postDate = new RealEstateItemPostDate();
        $postDate->item_id = $model->id;
        $postDate->post_date = $unixTime;
        $postDate->price = $model->price;
        $postDate->new_post = ($isNew === true)?1:0;

        $postDate->save(false);
    }

    public static function findBeforeWithKeyword($str,$arr_keywords,$after=false){
        if(empty($str)){
            return null;
        }
        if(empty($arr_keywords)){
            return null;
        }
        $str = str_replace(',','',$str);
        $currpos = 0;
        $newstr = '';
        $kwds_plus_surround = array();
        //$str = mb_strtolower($str, 'UTF-8');
        $len = strlen($str);
        while ($currpos < $len) {
            // Search for the earliest match of any of the keywords from our current position.
            list($newpos, $kw_index) = self::strpos_arr($str, $arr_keywords, $currpos);

            if ($newpos == -1) {
                // We're beyond the last keyword - do replacement to the end and
                // add to the output.
                $newstr .= self::do_replace(@substr($str, $currpos));
                $currpos = $len + 1;
            } else {
                // Found a keyword.
                // Now look two words back (separating words on single spaces).
                $secondspc_back = $newpos - 1;
                for ($i = 2; $i > 0; $i--) {
                    $secondspc_back = @strrpos($str, ' ', $secondspc_back - $len - 1);
                    if ($secondspc_back === false) break;
                }
                if ($secondspc_back === false || $secondspc_back < $currpos) {
                    $secondspc_back = $currpos;
                } else  $secondspc_back++;

                // Do replacement on the stuff between the previous keyword
                // (plus 2 words after) and this one (minus two words before),
                // and add to the output.
                $in_between = @substr($str, $currpos, $secondspc_back - $currpos);
                $newstr .= self::do_replace($in_between);

                // Now look two words forward (separating words on single spaces).
                $secondspc_fwd = $newpos + strlen($arr_keywords[$kw_index]);

                   if($after === true){
                       for ($i = 1 ; $i > 0; $i--) {
                           $secondspc_fwd = @strpos($str, ' ', $secondspc_fwd + 1);
                           if ($secondspc_fwd === false) break;
                       }
                       if ($secondspc_fwd === false) $secondspc_fwd = $len + 1;
                   }


                // Add the keyword plus two words before and after to both the array
                // and the output.
                $kw_plus = @substr($str, $secondspc_back, $secondspc_fwd - $secondspc_back);
                $kwds_plus_surround[] = $kw_plus;
                $newstr .= $kw_plus;

                // Update our current position in the string.
                $currpos = $secondspc_fwd;

            }

        }

        return $kwds_plus_surround;

    }
    // Replaces in $str all occurrences of 'e' with 'U'.
    public static function do_replace($str) {
        //return str_replace('e', 'U', $str);
    }
// Finds the earliest match, if any, of any of the $needles (an array)
// in $str (a string) starting from $currpos (an integer).
// Returns an array whose first member is the index of the earliest match,
// or -1 if no match was found, and whose second member is the index into
// $needles of the entry that matched in the $str.
    public static function strpos_arr($str, $needles, $currpos) {
        $ret = array(-1, -1);
        foreach ($needles as $idx => $needle) {
            if(!empty($needle)){
                $needle = trim($needle);
                $offset = stripos($str, mb_strtolower($needle, 'UTF-8'),$currpos);
                if ($offset !== false &&
                    ($offset < $ret[0] || $ret[0] == -1)) {
                    $ret = array($offset, $idx);
                }
            }
        }
        return $ret;
    }

    public static function detectPhoneNumberFromImage($link_image){
        if(empty($link_image)){
            return null;
        }
        if (preg_match('/^[0-9]+$/', $link_image)) {
            return $link_image;
        }

        $api_key = 'AIzaSyCvzYvdJWFHWqYS-DiJWCNy7Nqlor5Dehs';
        $cvurl = "https://vision.googleapis.com/v1/images:annotate?key=" . $api_key;
        $type = "TEXT_DETECTION";

        $data = file_get_contents($link_image);
        $base64 = base64_encode($data);

        $r_json ='{
                "requests": [
                    {
                      "image": {
                        "content":"' . $base64. '"
                      },
                      "features": [
                          {
                            "type": "' .$type. '",
                            "maxResults": 200
                          }
                      ]
                    }
                ]
            }';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $cvurl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $r_json);
        $json_response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if ( $status != 200 ) {
            die("Error: $cvurl failed status $status" );
        }

        if($json_response && !empty($json_response)){
            $data = json_decode($json_response);
            $re = (isset($data->responses))?$data->responses:null;
            $textAnnotations = (isset($re[0]) && !empty($re[0]))?$re[0]->textAnnotations:null;
            $phone_number =  (isset($textAnnotations[0]) &&  !empty($textAnnotations[0]))
                ?$textAnnotations[0]->description:null;

            $phone_number = str_replace('\n','',$phone_number);
            return $phone_number = preg_replace('/\s+/', '', $phone_number);
        }

    }
}
