<?php
/**
 * User: sangnguyen on  11/04/15 at 22:18
 * File name: SignUpForm.php
 * Project name: Fit Road
 * Copyright (c) 2015 by COMPANY
 * All rights reserved
 */

namespace backend\commons\forms;


use common\models\entities\RealEstateUser;
use Yii;
use yii\helpers\ArrayHelper;


class SignUpForm extends RealEstateUser{
    CONST SCENARIO_UPDATE       =   'update';
    CONST SCENARIO_REGISTER     =   'register';

    public $user;

    public $password_repeat;
    /**
     * @inheritdoc
     */

    public function rules()
    {
        $parentRules = parent::rules();
        $currentRules =  [
           // ['role', 'integer'],
            //['role', 'in','range' => ['0011','4','0010','-1'],'message' => Yii::t('backend/error', 'role not existed')],

            ['phone', 'integer'],
            ['phone', 'match', 'pattern'=>'/^[0-9]{9,12}$/','message' => Yii::t('backend/error', 'Không đúng định dạng số điện thoại!')],

            [
                'phone', 'unique', 'targetClass' => '\common\models\entities\RealEstateUser',
                'filter'=> ['not in','id',[$this->id]],
                //'on'    =>  self::SCENARIO_UPDATE,
                'message' => Yii::t('backend/error', 'Số điện thoại đã được sử dụng!'),
            ],
            [
                'phone', 'unique', 'targetClass' => '\common\models\entities\RealEstateUser',
                'on'    =>  self::SCENARIO_REGISTER,
                'message' => Yii::t('backend/error', 'Số điện thoại đã được sử dụng!'),
            ],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email','message'=>'Không đúng định dạng email.'],

            [
                'email', 'unique', 'targetClass' => '\common\models\entities\RealEstateUser',
                'filter'=> ['not in','id',[$this->id]],
                'message' => Yii::t('backend/error', 'email existed'),
            ],

//            ['password', 'required','on'=>self::SCENARIO_REGISTER],
//            ['password', 'required','on'=>self::SCENARIO_REGISTER],

            [['password','password_repeat'], 'required','on'=>self::SCENARIO_REGISTER],

            [['password_repeat'], 'compare', 'compareAttribute' => 'password'],
        ];
        return ArrayHelper::merge($parentRules,$currentRules);
    }
/*    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_UPDATE] = ['username', 'password'];
        $scenarios[self::SCENARIO_REGISTER] = ['username', 'email', 'password'];
        return $scenarios;
    }*/
    public function attributeLabels(){
        $attr =  parent::attributeLabels();
        $attrNew = [
            'password_repeat' => Yii::t('backend', 'Password Repeat'),
        ];
        return array_merge($attrNew,$attr);
    }
    public function signup(){
        $this->status           =   1;
        $this->role             =   -1;
        $this->save(FALSE);
    }
}
