<?php
namespace backend\commons\repositories;
use common\models\entities\RealEstateGallery;
use common\models\entities\RealEstateItem;
use Yii;
use yii\base\Exception;

/**
 * Class RealtyRepository
 *
 * @package \\${NAMESPACE}
 */
class RealtyRepository
{
    protected $realty;
    protected $data;
    public function __construct(RealEstateItem $realty){
        $this->realty  = $realty;
    }

    public function deleteGallery(){
        $images = RealEstateGallery::findAll(['item_id'=>$this->realty->id]);

        if(!empty($images)){
            foreach($images as $image){
                $url = $image->file_name;
                try{
                    @unlink(Yii::getAlias('@publicProductImagePatch').'/'.$url);
                    @unlink(Yii::getAlias('@publicProductImagePatch').'/'.Yii::$app->params['thumbName'].$url);
                   // @unlink(Yii::getAlias('@publicProductImagePatch').'/'.Yii::$app->params['normalName'].$url);
                }catch (Exception $e){
                    Yii::error($e .'unlink food image');
                }
                if($image->delete()){
                    Yii::info('remove gallery image file_name= '.$image->file_name.' of item id= '.$this->realty->id);
                }
            }
        }
       return true;
    }
}
