<?php

namespace backend\modules\authorization\controllers;

use backend\modules\auth\controllers\AuthenticateController;
use backend\modules\authorization\models\entity\AuthRule;
use Yii;
use backend\modules\authorization\models\entity\AuthItem;
use backend\modules\authorization\models\search\AuthItemSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RoleController implements the CRUD actions for AuthItem model.
 */
class RoleController extends AuthenticateController
{
    /**
     * Lists all AuthItem models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new AuthItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AuthItem model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $auth = Yii::$app->authManager;
        $rule = new \backend\modules\authorization\rbac\UserGroupRule();
        $model = new AuthItem();
        $userGroupModel = new AuthRule();

        if ($model->load(Yii::$app->request->post())) {

            if(!$userGroupModel->findOne(['name'=>'userGroup'])){

                $auth->add($rule);

                $role = $auth->createRole($model->name);
                $role->ruleName = $rule->name;
                $auth->add($role);

            }else{

                $role = $auth->createRole($model->name);
                $role->description =$model->description;
                $role->ruleName = $rule->name;
                $auth->add($role);

                $admin = $auth->getRole('admin');
                $sysadmin = $auth->getRole('sysadmin');

                if($admin){
                    $auth->addChild($admin, $role);
                }

                if($sysadmin){
                    $auth->addChild($sysadmin, $admin);
                }
            }

            return $this->redirect(['view', 'id' => $model->name]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->name]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AuthItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
