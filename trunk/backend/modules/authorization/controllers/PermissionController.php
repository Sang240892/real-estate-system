<?php

namespace backend\modules\authorization\controllers;

use backend\modules\auth\controllers\AuthenticateController;
use Yii;
use backend\modules\authorization\models\entity\AuthItem;
use backend\modules\authorization\models\entity\AuthItemChild;
use backend\modules\authorization\models\search\AuthItemChildSearch;
use backend\modules\authorization\models\search\PermissionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PermissionController implements the CRUD actions for AuthItemChild model.
 */
class PermissionController extends AuthenticateController
{
//    public function behaviors()
//    {
//        return [
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['post'],
//                ],
//            ],
//        ];
//    }
    public function actionAjaxAssignPermission(){
        $auth = Yii::$app->authManager;
        if (\Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $permissionUnChecked = \Yii::$app->request->post('uc');
            $permissionChecked = \Yii::$app->request->post('c');

            $unCheck = json_decode($permissionUnChecked);
            $check = json_decode($permissionChecked);

            if(is_array($unCheck) && !empty($unCheck)){
               foreach($unCheck as $_u){
                   $parentU = $auth->getRole($_u->id);
                   $permissionU = $auth->getPermission($_u->value);
                   if(is_null($parentU) || !$parentU || is_null($permissionU) || !$permissionU){
                       Yii::error('Module: '.\Yii::$app->controller->module->id.'\n Controller: '.\Yii::$app->controller->id.
                       'Action: '.\Yii::$app->controller->action->id,'User');
                       Yii::error('Get data role '.$_u->id.' to unchecked permission '.$_u->value,'User');
                       return FALSE;
                   }
                   $auth->removeChild($parentU,$permissionU);
               }
            }
            if(is_array($check) && !empty($check)){
                $sysadmin   = $auth->getRole('sysadmin');
                $admin      = $auth->getRole('admin');
                foreach($check as $_c){
                    $parentC = $auth->getRole($_c->id);
                    $permissionC = $auth->getPermission($_c->value);
                    if(is_null($parentC) || !$parentC || is_null($permissionC) || !$permissionC){
                        Yii::error('Module: '.\Yii::$app->controller->module->id. '\n Controller: '.\Yii::$app->controller->id.
                            'Action: '.\Yii::$app->controller->action->id,'User');
                        Yii::error('Get data role '.$_c->id.' to checked permission '.$_c->value,'User');
                        return FALSE;
                    }
                    $this->addPermissionWithRole($parentC,$permissionC);
                    $this->addPermissionWithRole($admin,$permissionC);
                    $this->addPermissionWithRole($sysadmin,$permissionC);
                }
            }
            return [
                'status'=>true
            ];
        }
    }

    private function addPermissionWithRole($role,$permission){
        $auth = Yii::$app->authManager;
        try{
            $auth->addChild($role,$permission);
        }catch(\yii\db\Exception $e){
            \Yii::error('Error \'s name: '.$e->getName(), 'Users');
            \Yii::error('Error \'s message: '.$e->getMessage(), 'Users');
            \Yii::error('Add permission with role : ' . $role->name . 'and permission name :' . $permission->name, 'Users');
        }
    }

    public function actionMatrix(){
        $searchModel = new AuthItemChildSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('matrix', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all AuthItemChild models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PermissionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Displays a single AuthItem model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AuthItemChild model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AuthItem();
        $auth = Yii::$app->authManager;

        if ($model->load(Yii::$app->request->post())) {
            $user = $auth->createPermission($model->name);
            $user->description = $model->description;
            $auth->add($user);

            $permissionC = $auth->getPermission($model->name);
            $sysadmin   = $auth->getRole('sysadmin');
            $this->addPermissionWithRole($sysadmin,$permissionC);

            return $this->redirect(['view', 'id' => $model->name]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Updates an existing AuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->name]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AuthItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
