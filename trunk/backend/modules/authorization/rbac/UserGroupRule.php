<?php
namespace backend\modules\authorization\rbac;

use common\models\entities\AuthenticationToken;
use common\models\UserIdentity;
use Yii;
use yii\rbac\Rule;

/**
 * Checks if user group matches
 */
class UserGroupRule extends Rule
{
    public $name = 'userGroup';

    protected function getContents() {
        return file_get_contents('php://input');
    }

    public function execute($user, $item, $params)
    {
        if (Yii::$app->user->isGuest){
            return Yii::$app->getResponse()->redirect('/auth/authenticate/verify');
        }
        $user = null;
        $role = '-1';
        $content = $this->getContents();
        $request = null;
        if (!empty ($content)){
            $request = json_decode($content);
        }
        $module = Yii::$app->controller->module->id;

        $authentication = isset($request->authentication->authToken)?$request->authentication->authToken:null;

        if($module === 'v1' && $authentication){

            $authToken  = AuthenticationToken ::findOne([
                'auth_token' => $authentication,
            ]);

            if (!$authToken && \is_null($authToken)) {
                Yii::error('Authentication failed', 'Users');
                return false;
            } else {
                $user = $authToken->getUser()->one();
                $role = $user->role;
                $identity = UserIdentity::findIdentity($user->id);
                Yii::$app->user->setIdentity($identity);
            }
            //set identity

        }else{
            $role =Yii::$app->user->identity->role;
        }

        if (!Yii::$app->user->isGuest) {
            if ($item->name === 'sysadmin') {
               return  $role === '0001';
            } elseif ($item->name === 'admin') {
                return  $role === '0001' || $role === '0010';
            }elseif ($item->name === 'manager') {
                return  $role === '0001' || $role === '0010' ||  $role === '0011';
            }elseif ($item->name === 'staff') {
                return  $role === '0001' || $role === '0010' ||  $role === '0011' ||  $role === '4';
            } elseif ($item->name === 'member') {
                return  $role === '0001' || $role === '0010' ||  $role === '0011' ||  $role === '4'||$role === '-1';
            }
        }
        return false;
    }
//     public function execute($user, $item, $params)
//     {
//         if (!Yii::$app->user->isGuest) {
//             $role = Yii::$app->user->identity->role;
//             if ($item->name === 'sysadmin') {
//                 return $role === $item->name;
//             } elseif ($item->name === 'admin') {
//                 return $role === $item->name || $role === 'sysadmin';
//             } 
//             elseif ($item->name === 'user') {
//                 return $role === $item->name || $role === 'superadmin' || $role === 'admin';
//             }
//         }
//         return false;
//     }
}
