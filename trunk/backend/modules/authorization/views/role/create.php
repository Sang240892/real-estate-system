<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\authorization\models\entity\AuthItem */
$asset		= backend\assets\AppAsset::register($this);
$this->title = Yii::t('backend', 'Create Role');
\backend\commons\helpers\UtilHelper::builtBreadcrumb(2,$this,[
    'urlLevel1'=>['index'],
    'titleLevel1'=>Yii::t('backend', 'Role Management')
]);

?>
<div class="auth-item-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
