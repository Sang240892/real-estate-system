<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\authorization\models\entity\AuthItem */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Auth Item',
]) . ' ' . $model->name;

\backend\commons\helpers\UtilHelper::builtBreadcrumb(3,$this,[
    'urlLevel1'=>['index'],
    'titleLevel1'=>Yii::t('backend', 'Role Management'),
    'urlLevel2'=>['view', 'id' => $model->name],
    'titleLevel2'=>Yii::t('backend','Update'),
    'model'=>$model
]);
?>
<div class="auth-item-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
