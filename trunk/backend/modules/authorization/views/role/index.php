<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\authorization\models\search\AuthItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$asset		= backend\assets\AppAsset::register($this);
$this->title = Yii::t('backend', 'Role Management');
\backend\commons\helpers\UtilHelper::builtBreadcrumb(1,$this);
/* @var $this yii\web\View */
?>
<!-- BEGIN INCLUSION TABLE PORTLET-->
<div class="portlet gren">
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <?php
                echo \nterms\pagesize\PageSize::widget([
                    'label' => false,
                    'template'=>'Show {list} records',
                    'options'=> ['class'=>'form-control input-xsmall input-inline']
                ]);

                ?>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="dataTables_length pull-right">
                    <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('backend', 'Create Role'), ['create'], ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
        <hr/>
        <div class="col-md-12 col-sm-12">
            <div class="row row-vgrid-20">
                <?= GridView::widget([
                    'layout'=>"<div class='pull-left'>{summary}</div>\n{items}\n{pager}\n",
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'filterSelector' => 'select[name="per-page"]',
                    'columns' => [
                        // ['class' => 'yii\grid\DataColumn'],
                        'name',
                        //'type',
                        'description:ntext',
                        //'rule_name',
                        //'data:ntext',
                        // 'created_at',
                        // 'updated_at',

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'header'=> Yii::t('backend','btn_group_action')
                        ],
                    ],
                    'tableOptions' => [
                        'id'=> 'girdViewRole',
                        'class' => 'table table-striped table-bordered table-hover'
                    ]
                ]); ?>
            </div>
        </div>
    </div>
</div>
<!-- END INCLUSION TABLE PORTLET-->

