<?php
/**
 * User: sangnguyen
 * Date: 10/11/15
 * Time: 17:17
 * File name: matrix.php
 * Project name: ysd-tee-shirt
 */
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\authorization\models\search\AuthItemChildSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$asset		= backend\assets\AppAsset::register($this);
$this->title = Yii::t('backend', 'Auth Item Children');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-child-index">
    <p>
        <?= Html::a(Yii::t('backend', 'Create Auth Item Child'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'parent',
            'child',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>