<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\authorization\models\entity\AuthItem */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Permission',
]) . ' ' . $model->name;

\backend\commons\helpers\UtilHelper::builtBreadcrumb(3,$this,[
    'titleLevel1'=>Yii::t('backend', 'Permission Management'),
    'titleLevel2'=>Yii::t('backend','Update'),
    'urlLevel1'=>['index'],
    'urlLevel2'=>['view', 'id' => $model->name],
    'objectName'=>$model->name
]);

?>
<div class="auth-item-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
