<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\authorization\models\entity\AuthItem */

$this->title = Yii::t('backend', 'Create Permission');

\backend\commons\helpers\UtilHelper::builtBreadcrumb(2,$this,[
    'urlLevel1'=>['index'],
    'titleLevel1'=>Yii::t('backend', 'Permission Management')
    ]);
$asset		= backend\assets\AppAsset::register($this);
/* @var $this yii\web\View */
?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>