<?php

namespace backend\modules\crawling;

use common\models\entities\RealEstateCategory;
use common\models\entities\SiteCrawling;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\entities\SiteCrawlingHref;

/**
 * models represents the model behind the search form about `common\models\entities\SiteCrawlingHref`.
 */
class models extends SiteCrawlingHref
{
    public $site_name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'site_crawling_id', 'created_at'], 'integer'],
            [['name', 'href','site_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RealEstateCategory::find();
        $query->joinWith(['siteCrawling']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $query->andOnCondition(['<>','parent',0])->andOnCondition(['is_display'=>0,'status'=>1]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $dataProvider->setSort([
            'defaultOrder' => [
                'created_at' => SORT_DESC
            ],
            'attributes' => [
                'id',
                'site_name' => [
                    'asc' => [SiteCrawling::tableName().'.name' => SORT_ASC],
                    'desc' => [SiteCrawling::tableName().'.name' => SORT_DESC],
                    'label'=>Yii::t('backend','Site Name'),
                    //'default' => SORT_ASC
                ],
                'name',
                'href',
                'created_at'
            ]
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'site_crawling_id' => $this->site_crawling_id,
            SiteCrawling::tableName().'.name'=>$this->site_name,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like',RealEstateCategory::tableName() .'.name', $this->name])
            ->orFilterWhere(['like', SiteCrawling::tableName().'.name', $this->site_name])
            ->andFilterWhere(['like', 'href', $this->href]);

        return $dataProvider;
    }
}
