<?php

namespace backend\modules\crawling;

/**
 * crawling module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\crawling\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        \Yii::$app->language = 'vn_VN';
        // custom initialization code goes here
    }
}
