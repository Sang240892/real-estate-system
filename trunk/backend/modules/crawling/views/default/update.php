<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\entities\SiteCrawlingHref */

//$this->title = Yii::t('backend', 'Update {modelClass}: ', [
//    'modelClass' => 'Site Crawling Href',
//]) . $model->name;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Site Crawling Hrefs'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
//$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');

?>
<div class="site-crawling-href-update">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'relationId'=>$relationId,
        'model' => $model,
    ]) ?>

</div>
