<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\crawling\models */
/* @var $dataProvider yii\data\ActiveDataProvider */
$asset		= backend\assets\AppAsset::register($this);

$this->title = Yii::t('backend', 'Site Crawling Hrefs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-crawling-href-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Create Site Crawling Href'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
           // 'id',
            'name',
            'href',

            [
                'attribute'=>'site_name',
                'header'=>Yii::t('backend','Site Name'),
                // 'headerOptions'=>['class'=>$classSortEvent],
//                'filterInputOptions' => [
//                    'class'       => 'form-control',
//                    'placeholder' => 'Search name or address'
//                ],
                'content'=>function ($model, $key, $index, $column){
                    if($model->siteCrawling){
                        return $model->siteCrawling->name;
                    }
                }
            ],
            //'created_at',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {update} '],
        ],
    ]); ?>
</div>
