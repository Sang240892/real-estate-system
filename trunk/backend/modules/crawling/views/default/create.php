<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\entities\SiteCrawlingHref */

$this->title = Yii::t('backend', 'Create Site Crawling Href');

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-crawling-href-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
