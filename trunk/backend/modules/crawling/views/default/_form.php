<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\entities\SiteCrawlingHref */
/* @var $form yii\widgets\ActiveForm */
$asset		= backend\assets\AppAsset::register($this);

$arraySite = \common\models\entities\SiteCrawling::find()->all();
$arrayParent = \common\models\entities\RealEstateCategory::findAll(['is_display'=>1]);
if(!$arraySite){
    $arraySite = [];
}
if(!$arrayParent){
    $arrayParent = [];
}
$parents = \common\models\entities\RealEstateCategory::findAll(['parent'=>0]);

?>

<div class="site-crawling-href-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'site_crawling_id')->dropDownList(ArrayHelper::map($arraySite, 'id', 'name'),['style'=>'width:20%'])->label(Yii::t('backend','Trang web')) ?>
    <div class="form-group field-realestatecategory-site_crawling_id">
        <label class="control-label" for="realestatecategory-site_crawling_id"><?=Yii::t('backend','Doanh mục')?></label>
        <select name="category_relation" class="bs-select form-control">
            <?php
            $item='';
            if(!empty($parents)){
                foreach($parents as $parent){
                    $childs = \common\models\entities\RealEstateCategory::findAll(['parent'=>$parent->id,'is_display'=>1]);
                    $item.='<optgroup label="'.$parent->name.'">';

                    if(!empty($childs)){
                        foreach($childs as $child){
                            $selected = (isset($relationId) && $relationId == $child->id)?'selected':'';
                            $item.= "<option ".$selected." value='".$child->id."'>".$child->name."</option>";
                        }
                    }
                    $item.='</optgroup>';
                }
            }
            echo $item;
            ?>
        </select>

        <div class="help-block"></div>
    </div>


    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'href')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
