<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\entities\SiteCrawlingHref */

//$this->title = $model->name;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Site Crawling Hrefs'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-crawling-href-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<!--        --><?//= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
//            'class' => 'btn btn-danger',
//            'data' => [
//                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
//                'method' => 'post',
//            ],
//        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'href',
            'site_crawling_id',
            'created_at',
        ],
    ]) ?>

</div>
