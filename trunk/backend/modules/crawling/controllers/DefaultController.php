<?php

namespace backend\modules\crawling\controllers;

use backend\modules\auth\controllers\AuthenticateController;
use common\models\entities\RealEstateCategory;
use common\models\entities\RealEstateCategoryRelation;
use Yii;
use common\models\entities\SiteCrawlingHref;
use backend\modules\crawling\models;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for SiteCrawlingHref model.
 */
class DefaultController extends AuthenticateController
{

    /**
     * Lists all SiteCrawlingHref models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new models();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SiteCrawlingHref model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SiteCrawlingHref model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RealEstateCategory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $relation = new RealEstateCategoryRelation();
            $relation->category_parent_id = Yii::$app->request->post()['category_relation'];
            $relation->category_children_id  = $model->id;
            $relation->save();

            $model->parent = $relation->category_parent_id;
            $model->save(false);

            $relation =RealEstateCategoryRelation::findOne(['category_children_id'=>$model->id]);
            $model->finder_name =$relation->categoryParent->name;
            $model->save(false);

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SiteCrawlingHref model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $relation = RealEstateCategoryRelation::findOne(['category_children_id'=>$model->id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if($relation){
                $relation->category_parent_id  = Yii::$app->request->post()['category_relation'];
                $relation->save();

            }else{
                $relation = new RealEstateCategoryRelation();
                $relation->category_parent_id = Yii::$app->request->post()['category_relation'];
                $relation->category_children_id  = $model->id;
                $relation->save();
            }

            $relation =RealEstateCategoryRelation::findOne(['category_children_id'=>$model->id]);
            $model->finder_name =$relation->categoryParent->name;
            $model->save(false);

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'relationId'=>($relation)?$relation->category_parent_id:0
            ]);
        }
    }

    /**
     * Deletes an existing SiteCrawlingHref model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SiteCrawlingHref model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SiteCrawlingHref the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RealEstateCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
