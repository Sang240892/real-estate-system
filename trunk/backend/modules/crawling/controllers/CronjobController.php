<?php

namespace backend\modules\crawling\controllers;
use backend\commons\components\CrawlerClient;
use backend\commons\helpers\Curl;
use backend\commons\helpers\UtilHelper;
use backend\modules\auth\controllers\AuthenticateController;
use Carbon\Carbon;
use common\components\repositories\EntityFactory;
use common\models\entities\AuthenticationToken;
use common\models\entities\NotificationCategoryRule;
use common\models\entities\NotificationHistory;
use common\models\entities\NotificationLog;
use common\models\entities\RealEstateCategory;
use common\models\entities\RealEstateGallery;
use common\models\entities\RealEstateItem;
use common\models\entities\RealEstateItemHasPhoneContact;
use common\models\entities\RealEstateItemPostDate;
use common\models\entities\RealEstatePhoneContact;
use common\models\entities\RealEstateUrlCrawlingItem;
use common\models\entities\RealEstateUser;
use common\models\entities\SiteCrawlingHref;
use backend\commons\repositories\RealtyRepository;
use Goutte\Client;
use Jaybizzle\CrawlerDetect\CrawlerDetect;
use Sunra\PhpSimple\HtmlDomParser;
use Symfony\Component\DomCrawler\Crawler;
use Yii;
use yii\db\Exception;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\web\Controller;
use api\commons\repositories\UserRepository;
/**
 * Class CronjobController
 *
 * @package \backend\modules\crawling\controllers
 */
class CronjobController extends Controller
{
    public function actionClearRealtyChoTot(){

        if(empty($date)){
//            $repost_date_timestamp = UtilHelper::getNowUnixUTC();
//            $date = \Yii::$app->formatter->asDate($repost_date_timestamp,'Y-M-d');
        }

        $date = Carbon::now()->format('Y-m-d');

        $realty =  RealEstateItem::find()
            ->andOnCondition("date(CONVERT_TZ(FROM_UNIXTIME(real_estate_item.repost_date), @@session.time_zone, '+07:00')) = :date ",[
                ':date'=>$date
            ])
            ->andOnCondition(['is_not_sure_self'=>1])->andFilterWhere(['like','source','https://www.chotot.com/'])
            ->orderBy('id')
            ->all();
        if($realty){
            foreach($realty as $item){
                $portfolio = $item->getRealEstateGalleries()->all();
                if($portfolio && !empty($portfolio)) {
                    foreach ($portfolio as $img) {
                        $url = $img->file_name;
                        try{
                            @unlink(Yii::getAlias('@publicProductImagePatch').'/'.$url);
                            @unlink(Yii::getAlias('@publicProductImagePatch').'/'.Yii::$app->params['thumbName'].$url);
                            @unlink(Yii::getAlias('@publicProductImagePatch').'/'.Yii::$app->params['normalName'].$url);
                        }catch (Exception $e){
                            Yii::error($e .'unlink realty images');
                        }
                    }
                }

                $avatar = $item->avatar;
                try{
                    @unlink(Yii::getAlias('@publicProductImagePatch').'/'.$avatar);
                    @unlink(Yii::getAlias('@publicProductImagePatch').'/'.Yii::$app->params['thumbName'].$avatar);
                    @unlink(Yii::getAlias('@publicProductImagePatch').'/'.Yii::$app->params['normalName'].$avatar);
                }catch (Exception $e){
                    Yii::error($e .'unlink realty images avatar');
                }

                try{
                    $image_phone = explode('/',$item->phone_contact);
                    $index = count($image_phone) - 1;
                    @unlink(Yii::getAlias('@assetsUploadPhoneImagePatch').'/'.$image_phone[$index]);

                }catch (Exception $e){
                    Yii::error($e .'unlink realty images phone');
                }
                echo $item->title;
                echo '<br/>';
                echo $item->id;
                echo '<br/>';
                $item->delete();

            }
        }
    }
    public function actionClearRealty($date=null){

        if(empty($date)){
//            $repost_date_timestamp = UtilHelper::getNowUnixUTC();
//            $date = \Yii::$app->formatter->asDate($repost_date_timestamp,'Y-M-d');
        }

        $threeDaysAgo = Carbon::now()->subDays(3)->format('Y-m-d');
        $realty =  RealEstateItem::find()
            ->andOnCondition("date(CONVERT_TZ(FROM_UNIXTIME(real_estate_item.repost_date), @@session.time_zone, '+07:00')) = :date ",[
                ':date'=>$threeDaysAgo
            ])
            ->andOnCondition(['is_self'=>0])
            ->orderBy('id')
            ->all();
        if($realty){
            foreach($realty as $item){
                $portfolio = $item->getRealEstateGalleries()->all();
                if($portfolio && !empty($portfolio)) {
                    foreach ($portfolio as $img) {
                        $url = $img->file_name;
                        try{
                            @unlink(Yii::getAlias('@publicProductImagePatch').'/'.$url);
                            @unlink(Yii::getAlias('@publicProductImagePatch').'/'.Yii::$app->params['thumbName'].$url);
                            @unlink(Yii::getAlias('@publicProductImagePatch').'/'.Yii::$app->params['normalName'].$url);
                        }catch (Exception $e){
                            Yii::error($e .'unlink realty images');
                        }
                    }
                }

                $avatar = $item->avatar;
                try{
                    @unlink(Yii::getAlias('@publicProductImagePatch').'/'.$avatar);
                    @unlink(Yii::getAlias('@publicProductImagePatch').'/'.Yii::$app->params['thumbName'].$avatar);
                    @unlink(Yii::getAlias('@publicProductImagePatch').'/'.Yii::$app->params['normalName'].$avatar);
                }catch (Exception $e){
                    Yii::error($e .'unlink realty images avatar');
                }

                try{
                    $image_phone = explode('/',$item->phone_contact);
                    $index = count($image_phone) - 1;
                    @unlink(Yii::getAlias('@assetsUploadPhoneImagePatch').'/'.$image_phone[$index]);

                }catch (Exception $e){
                    Yii::error($e .'unlink realty images phone');
                }
                echo $item->title;
                echo '<br/>';
                echo $item->id;
                echo '<br/>';
                $item->delete();

            }
        }
    }
    public function actionPush(){
        $authen = AuthenticationToken::find()->andOnCondition('device_token <> :device_token',[
            ':device_token'=>''
        ])->all();

        if(!empty($authen)){
            foreach($authen as $au){
                $token = $au->device_token;
                $badge = $au->user->register(UserRepository::class)->getBadgeRealtyNotification();
                if($badge > 0 && !empty($token)){
                    $notifications = [
                        'title'=>'BDS-Smart',
                        'body'=>'Bạn có '.$badge.' tin bất động sản mới',
                        'badge'=>$badge,
                        "sound" => "default"
                    ];
                    $response = UtilHelper::sendNotification($token,$notifications);
                    $result = json_decode($response);
                    if($result !== null && isset($result->success) && $result->success == 1){
                        $time = Yii::$app->db->createCommand('update notification_history set is_push = 1 where user_id = :user_id',[
                            ':user_id'=>$au->user->id
                        ])->query();

                    }
                    // write notification log
                    $notificationLog = [
                        'user_id'=>$au->user->id,
                        'fcm_response'=>$response,
                        'params_request'=>json_encode($notifications)
                    ];

                    $log = new NotificationLog();
                    $log->attributes = $notificationLog;
                    $log->save(false);

                }
            }
        }

    }
    public function actionUpdateNotification($minute){
        $users = RealEstateUser::find()->andOnCondition([
            'status'=>1,
            'is_notify'=>1
        ])->all();

        if(!empty($users)){
            foreach($users as $user){
                $categoryRules = NotificationCategoryRule::findAll(['user_id'=>$user->id]);
                if(!empty($categoryRules)){
                    $conditionCategory = [];
                    if(!empty($categoryRules)){
                        foreach($categoryRules as $cate){
                            $conditionCategory[] = $cate->category_id;
                        }
                    }

                    $cityRule = $user->notification_city_rule;
                    $minPriceRule = $user->notification_price_min_rule;
                    $maxPriceRule = $user->notification_price_max_rule;

                    $queryProperties = RealEstateItem::find()
                        ->limit(1000)
                        ->andOnCondition("FROM_UNIXTIME(real_estate_item.created_at) >= NOW() - INTERVAL :min MINUTE ",[
                            ":min"=>$minute
                            ])->andOnCondition(['is_self'=>1,'is_not_sure_self'=>0,'is_new'=>1]);

                    if(!empty($conditionCategory)){
                        $queryProperties->andOnCondition(['in','category_id',$conditionCategory]);
                    }


                    if(!empty($cityRule)){
                       if(strpos(mb_strtolower($cityRule, 'UTF-8'), "hà nội") !== FALSE){
                            $queryProperties->andWhere("MATCH (`address`) AGAINST ('\" hà nội \"' IN BOOLEAN MODE) ");
                        }else{
                            $queryProperties->andWhere("MATCH (`address`) AGAINST ('\" $cityRule \"' IN BOOLEAN MODE) ");
                        }

                    }
                    if(!empty($maxPriceRule) && (int) $maxPriceRule != 0){
                        $queryProperties->andFilterWhere( ['between', 'real_estate_item.price',(int) $minPriceRule,(int) $maxPriceRule]);
                    }

                    $properties =  $queryProperties->all();

                    if($properties){
                        foreach($properties as $pro){
                            $history = NotificationHistory::findOne(['item_id'=>$pro->id,'user_id'=>$user->id]);
                            if($history == null){
                                $notificationHistory = new NotificationHistory();
                                $notificationHistory->user_id  = $user->id;
                                $notificationHistory->item_id = $pro->id;
                                try{
                                    $notificationHistory->save(false);
                                }catch (\yii\db\ErrorException $e){
                                    Yii::error('Error \'s name: '.$e->getName(), 'Users');
                                    Yii::error('Error \'s message: '.$e->getMessage(), 'Users');
                                    Yii::error('Error get data self contact', 'Users');
                                }
                            }
                        }
                    }
                }

            }
        }
        return true;
    }
    public function actionFiltering($date){
        if(empty($date)){
            $repost_date_timestamp = UtilHelper::getNowUnixUTC();
            $date = \Yii::$app->formatter->asDate($repost_date_timestamp,'Y-M-d');
        }

        $realty =  RealEstateItem::find()
            ->limit(4000)
            ->andOnCondition("date(CONVERT_TZ(FROM_UNIXTIME(real_estate_item.created_at), @@session.time_zone, '+07:00')) = :date ",[
                ':date'=>$date
            ])
            //->andFilterWhere(['like', 'source', 'muaban.net'])
            ->orderBy('id')
            ->all();
        if($realty){
            foreach($realty as $item){
                if (UtilHelper::validateIsSelfRealItem($item->title, $item->description,$item->address)) {
                    $phone = $item->phone_contact;
                    if(empty($phone)){
                        $phone = $item->phone_backup;
                    }

                    $checkNumber = RealEstateItem::find()
                        ->andOnCondition(['phone_contact'=>$phone])->all();

                    $item->is_self = 1;

                    if(count($checkNumber) > 3){
                        $item->is_self = 0;
                        foreach($checkNumber as $itemBefore){
                            $itemBefore->is_self=0;
                            $itemBefore->save(false);
                        }
                    }

                    if(count($checkNumber) > 1 && count($checkNumber) <= 3){
                        $item->is_self = 0;
                        $item->is_not_sure_self = 1;

                        foreach($checkNumber as $itemBefore){
                            $itemBefore->is_self=0;
                            $item->is_not_sure_self = 1;
                            $itemBefore->save(false);
                        }
                        $lat = null;
                        $lng = null;

                        //get location
                        $location = UtilHelper::detectAddress($item->address);

                        if(!empty($location)){
                            $lat = isset($location['lat'])?$location['lat']:null;
                            $lng = isset($location['lng'])?$location['lng']:null;
                        }

                        $item->latitude = $lat;
                        $item->longitude = $lng;

                    }

                    $item->save(false);

                }else{
                    $item->is_self = 0;
                }

                if($item->is_self == 0){
                    if ($item->phone_contact && !empty($item->phone_contact)) {
                        $phoneContactModel = new RealEstatePhoneContact();
                        $phoneContactModel->phone = $item->phone_contact;
                        try {
                            $phoneContactModel->save(false);
                            $itemHasPhone = new RealEstateItemHasPhoneContact();
                            $itemHasPhone->item_id = $item->id;
                            $itemHasPhone->phone_contact_id = $phoneContactModel->id;
                            $itemHasPhone->phone = $item->phone_contact;
                            $itemHasPhone->save(false);
                        } catch (\yii\db\IntegrityException $e) {

                        }
                    }
                    if ($item->phone_backup && !empty($item->phone_backup)) {
                        $phoneContactModelBackup = new RealEstatePhoneContact();
                        $phoneContactModelBackup->phone = $item->phone_backup;
                        try {
                            $phoneContactModelBackup->save(false);
                            $itemHasPhone = new RealEstateItemHasPhoneContact();
                            $itemHasPhone->item_id = $item->id;
                            $itemHasPhone->phone_contact_id = $phoneContactModelBackup->id;
                            $itemHasPhone->phone = $item->phone_backup;
                            $itemHasPhone->save(false);
                        } catch (\yii\db\IntegrityException $e) {

                        }
                    }
                }

                $item->save(false);
            }
        }
    }
    public function actionUpdatePrice(){

        $realty =  RealEstateItem::find()
           // ->andOnCondition(['is_self'=>1])
           ->offset(65000)
            ->limit(40000)
            ->all();

        if($realty){

            foreach($realty as $item){
                $price_number = UtilHelper::convertPriceStringToNumber($item->price_string);
                $item->price = $price_number;
//                echo '<br/>';
//                echo $item->id;
//                echo '<br/>';
//                echo $price_number .' => '. $item->price_string;

                if($item->save(false)){
                    echo '<br/>';
                    echo $item->id;
                    echo '<br/>';
                    echo $price_number .' => '. $item->price_string;
                }
            }
            die();
        }
    }
    public function actionDetectAddress(){
        $repost_date_timestamp = UtilHelper::getNowUnixUTC();
        $now = \Yii::$app->formatter->asDate($repost_date_timestamp,'Y-M-d');
        $realty =  RealEstateItem::find()
            ->leftJoin(RealEstateItemPostDate::tableName(),
                RealEstateItemPostDate::tableName().'.item_id = '.RealEstateItem::tableName().'.id')
            ->andOnCondition("date(CONVERT_TZ(FROM_UNIXTIME(real_estate_item_post_date.post_date), @@session.time_zone, '+07:00')) = :date ",[
                ':date'=>$now
            ])
            ->limit(2200)
            ->all();
        if($realty){

            foreach($realty as $item){
                if(empty($item->latitude) || empty($item->longitude)){
                    $location = UtilHelper::detectAddress($item->address);
                    if(!empty($location)){
                        $item->latitude = $location['lat'];
                        $item->longitude = $location['lng'];
                        $item->save(false);
                        $item->refresh();
                        echo 'lat:'.$item->latitude;
                        echo '<br/>';
                        echo 'lat:'.$item->longitude;
                        echo '<br/>';
                        echo 'id:'.$item->id;
                        echo '<br/>';
                    }
                }
            }
        }
    }
    public function actionCrawlerBds(){
        $CrawlerDetect = new CrawlerDetect();

// Check the user agent of the current 'visitor'
        if($CrawlerDetect->isCrawler()) {
            // true if crawler user agent detected
        }

// Pass a user agent as a string
        if($CrawlerDetect->isCrawler('Mozilla/5.0 (compatible; Sosospider/2.0; +http://batdongsan.com.vn)')) {
            // true if crawler user agent detected
        }

// Output the name of the bot that matched (if any)
        echo $CrawlerDetect->getMatches();
    }

    public function actionCrawlingBds($from){

        $cc = new Curl();
        $proxies = [
            "43.229.84.159:80"
        ];

        $useragent = [
            'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.14 (KHTML, like Gecko) Chrome/9.0.601.0 Safari/534.14',
            'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.5 (KHTML, like Gecko) Chrome/4.0.249.0 Safari/532.5',
            'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/527 (KHTML, like Gecko, Safari/419.3) Arora/0.6 ',
            'Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.9 (KHTML, like Gecko) Chrome/5.0.310.0 Safari/532.9',
            'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.7 (KHTML, like Gecko) Chrome/7.0.514.0 Safari/534.7',
            'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.14 (KHTML, like Gecko) Chrome/9.0.601.0 Safari/534.14',
            'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3',
            'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6',
            'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1090.0 Safari/536.6',
            'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1'
        ];

        $kRandomAgent = array_rand($useragent);
        $valueAgent = $useragent[$kRandomAgent];

        $kRandomProxy = array_rand($proxies);
        $valueProxy = $proxies[$kRandomProxy];

        $cc->proxy = $valueProxy;
        $cc->user_agent = $valueAgent;

        $site = 'http://batdongsan.com.vn/';
        $page = '/p';
        $validate = null;
        $categorySystem = null;
        $parentCategoryId = 1;
        $categories = RealEstateCategory::find()->where(['site_crawling_id'=>2,'status'=>1])->all();

        $to = $from+2;

        if($categories) {
            foreach ($categories as $cate) {
                $data = [];
                $page = $page . $from;

                if ($cate) {
                    $ret = $cc->get($site . trim($cate->href) . $page);
                    $categorySystem = $cate->id;

                    if(!empty($ret)){

                        $rs = HtmlDomParser::str_get_html($ret);
                        try{
                            $p1 = $rs->find('div.search-productItem');
                        }catch (\yii\base\ErrorException $e){
                            $p1 = [];
                        }

                        if(is_array($p1) && !empty($p1)) {
                            foreach ($p1 as $element) {
                                $firstDiv =$element->find('div.p-title a',0);
                                $floatleft = $element->find('div.p-main div.p-bottom-crop div.floatleft',0);
                                $price = $floatleft->find('span.product-price',0);
                                $area = $floatleft->find('span.product-area',0);
                                $date = $element->find('div.p-main div.p-bottom-crop div.mar-right-10',0);

                                try{
                                    $title = $firstDiv->title;
                                }catch (\yii\base\ErrorException $e){
                                    $title = null;
                                }
                                try{
                                    $itemUrl = $firstDiv->href;
                                }catch (\yii\base\ErrorException $e){
                                    $itemUrl = null;
                                }
                                try{
                                    $price =$price->plaintext;
                                }catch (\yii\base\ErrorException $e){
                                    $price = null;
                                }
                                try{
                                    $area =$area->plaintext;
                                }catch (\yii\base\ErrorException $e){
                                    $area = null;
                                }
                                try{
                                    $date =$date->plaintext;
                                }catch (\yii\base\ErrorException $e){
                                    $date = null;
                                }

                                if($itemUrl){

                                    $itemUrl = 'http://batdongsan.com.vn'.$itemUrl;

                                    // $validate = RealEstateUrlCrawlingItem::findOne(['url'=>$itemUrl]);
                                    $validate = null;
                                    if($validate !== null){
                                        UtilHelper::verifyRePostRealty($validate);
                                    }else{
                                        $detail = $cc->get($itemUrl);
                                        $rsDetail = HtmlDomParser::str_get_html($detail);

                                        if(!empty($rsDetail)){
                                            try{
                                                $dataHtml = $rsDetail->find('div#product-detail',0);
                                                $description = $dataHtml->find('div.pm-content',0);
                                                $removeString = $description->find('div#LeftMainContent__productDetail_panelTag',0);
                                                $contactName = $dataHtml->find('div#LeftMainContent__productDetail_contactName div.right',0);
                                                $phone = $dataHtml->find('div#LeftMainContent__productDetail_contactPhone div.right',0);
                                                $phoneBackup = $dataHtml->find('div#LeftMainContent__productDetail_contactMobile div.right',0);

                                                $addressDom = $dataHtml->find('div div.right',0);
                                            }catch (\yii\base\ErrorException $e){
                                                $dataHtml = null;
                                                $description = null;
                                                $addressDom = null;
                                            }

                                            try{
                                                $removeString =$removeString->plaintext;
                                                $description = $description->plaintext;
                                                $description = str_replace($removeString,'',$description);

                                            }catch (\yii\base\ErrorException $e){
                                                $description = null;
                                            }

                                            try{
                                                $address  = $addressDom->plaintext;
                                            }catch (\yii\base\ErrorException $e){
                                                $address = null;
                                            }
                                            try{
                                                $phone  = $phone->plaintext;
                                            }catch (\yii\base\ErrorException $e){
                                                $phone = null;
                                            }
                                            try{
                                                $phoneBackup  = $phoneBackup->plaintext;
                                            }catch (\yii\base\ErrorException $e){
                                                $phoneBackup = null;
                                            }
                                            try{
                                                $contactName  = $contactName->plaintext;
                                            }catch (\yii\base\ErrorException $e){
                                                $contactName = null;
                                            }
                                            $data[] = [
                                                'avatarUrl'=> '',
                                                'region_sale'=>'',
                                                'contact_phone'=>trim($phone),
                                                'contact_phone_backup'=>trim($phoneBackup),
                                                'contact_name'=>trim($contactName),
                                                'title'=>trim($title),
                                                'summary'=>trim($description),
                                                //trim($category),
                                                'address'=>trim($address),
                                                'description'=>trim($description),
                                                'price'=>trim($price),
                                                'area'=>trim($area),
                                                'source'=>'http://batdongsan.com.vn',
                                                // 'address'=>trim($address),
                                                'created'=>trim($date),
                                                'detailUrl'=>trim($itemUrl),
                                                'validate'=>($validate != null)?0:1,
                                                'systemRealtyId'=>0,
                                                'categoryId' =>$categorySystem
                                            ];

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if(sleep(1)!=0)
                {
                    echo "sleep failed script terminating";
                }
                if(!empty($data)){
                    foreach($data as $item) {
                        if (!empty($item) && isset($item['detailUrl']) && !empty($item['detailUrl'])) {
                            if($item['validate'] != 0) {
                                $rs = $this->_saveItemCrawling(json_encode($item));
                                if ($rs) {
                                    echo '</br> ****';
                                    echo '</br> Successfull save item';
                                    echo '</br> ' . $rs->title;
                                    echo '</br> ****';
                                }

                            }
                        }
                    }
                }
            }

        }

    }
    public function actionBatDongSan($from=0){
        $client = new Client();

//        $proxies= [
//            '43.229.84.159:80',
//        ];



        $client= $client->setClient(new \GuzzleHttp\Client([
            'proxy' => [
            'http' => '43.229.84.159:80'
            ],
            'headers'=>[
                'User-Agent'=>'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'
            ]
        ]));

        $site = 'http://batdongsan.com.vn/';
        $page = '/p';
        $validate = null;
        $categorySystem = null;
        $parentCategoryId = 1;
        $categories = RealEstateCategory::find()->where(['site_crawling_id'=>2,'status'=>1])->all();

        $to = $from+5;

        if($categories){
            for($from;$from<$to;$from++){
                $from++;
                foreach($categories as $cate){

                   $page = $page.$from;

                   if($cate){
                       $cateName= null;
                       $validate = null;
                       $categorySystem = null;

                       $crawler = $client->request('GET', $site . trim($cate->href) . $page);
                       echo '<pre>';
                       print_r($crawler);
                       echo '</pre>';
                       die();
                   }

                   $categorySystem = $cate->id;
                   $items = $crawler->filter('.search-productItem')->each(function (Crawler $node, $i) use ($categorySystem){

                       try{
                           $title = $node->filter('.p-title')->text();
                       }catch (\InvalidArgumentException $e){
                           $title = null;
                       }
                       try{
                           $itemUrl = $node->filter('.p-title a')->attr('href');
                       }catch (\InvalidArgumentException $e){
                           $itemUrl = null;
                       }
                       try{
                           $imgUrl = $node->filter('.product-avatar img')->attr('src');
                       }catch (\InvalidArgumentException $e){
                           $imgUrl = null;
                       }
                       try{
                           $description = $node->filter('.p-main-text')->text();
                       }catch (\InvalidArgumentException $e){
                           $description = null;
                       }
                       try{
                           $price = $node->filter('.product-price')->text();
                       }catch (\InvalidArgumentException $e){
                           $price = null;
                       }
                       try{
                           $area = $node->filter('.product-area')->text();
                       }catch (\InvalidArgumentException $e){
                           $area = null;
                       }
                       try{
                           $address = $node->filter('.product-city-dist')->text();
                       }catch (\InvalidArgumentException $e){
                           $address = null;
                       }
                       try{
                           $createAt = $node->filter('.floatright')->text();
                       }catch (\InvalidArgumentException $e){
                           $createAt = null;
                       }

                       if($imgUrl == null || empty($imgUrl)){
                           $imgUrl = null;
                       }

                       $data = [];

                       if($itemUrl){

                           $itemUrl = 'http://batdongsan.com.vn'.$itemUrl;

                           $validate = RealEstateUrlCrawlingItem::findOne(['url'=>$itemUrl]);

                           if($validate !== null){
                               UtilHelper::verifyRePostRealty($validate);
                           }else{
                               $data = [
                                   'avatarUrl'=> trim(str_replace('120x90','745x510',$imgUrl)),
                                   'title'=>trim($title),
                                   'summary'=>trim($description),
                                   //trim($category),
                                   'price'=>trim($price),
                                   'area'=>trim($area),
                                   'source'=>'http://batdongsan.com.vn',
                                  // 'address'=>trim($address),
                                   'created'=>trim($createAt),
                                   'detailUrl'=>trim($itemUrl),
                                   'validate'=>($validate != null)?0:1,
                                   'systemRealtyId'=>0,
                                   'categoryId' =>$categorySystem
                               ];
                           }
                       }
                       return $data;
                   });

                   if(!empty($items)){
                       foreach($items as $item) {
                               if (!empty($item) && isset($item['detailUrl']) && !empty($item['detailUrl'])) {
                                   if($item['validate'] != 0) {
                                   $detailData = $this->_crawlingBDSDetail($item['detailUrl']);
                                   $data = array_merge($detailData, $item);
                                   $rs = $this->_saveItemCrawling(json_encode($data));
                                   if ($rs) {
                                       echo '</br> ****';
                                       echo '</br> Successfull save item';
                                       echo '</br> ' . $rs->title;
                                       echo '</br> ****';
                                   }

                               }
                           }
                       }
                   }

                   if(sleep(2)!=0)
                   {
                       echo "sleep failed script terminating";
                       break;
                   }

               }
            }
        }
    }
    public function actionChoTotHcm($from){

        $categories = RealEstateCategory::find()
            ->where(['site_crawling_id'=>3,'status'=>1])
            ->andOnCondition(['in','id',[8585,8584]])->all();

        $client = new Client();
        $data = [];
        $data['items'] = [];
        $site = 'https://www.chotot.com/';
        $cateName = null;
        $validate = null;
        $categorySystem = null;
        $parentCategoryId = 1;
        $urlAvatar = null;
        $page = '&o=';
        $to = $from+5;
        if($categories){
            for($from;$from<$to;$from++){
                $from++;
                $page = $page.$from.'#';
                foreach($categories as $cate) {
                    if ($cate) {
                        $cateName = null;
                        $validate = null;
                        $categorySystem = null;

                        $crawler = $client->request('GET', $site . trim($cate->href).$page );
                        //$crawler = $client->request('GET', $site . 'ha-noi/quan-hoan-kiem/sang-nhuong-van-phong-mat-bang-kinh-doanh#');
                    }
                    $categorySystem = $cate->id;

                    $items = $crawler->filter('.chotot-list-row')->each(function (Crawler $node, $i) use($categorySystem) {
                        try{
                            $div = $node->filter('.listing_thumbs_image .extra_img-b img')->last();
                            $urlAvatar = $div->attr('data-original');

                            $urlAvatar = str_replace('listing_thumbs','wm_images',$urlAvatar);

                            if(strpos(mb_strtolower($urlAvatar, 'UTF-8'), 'DefaultAdImage.jpg')  !== FALSE ){
                                $urlAvatar = null;
                            }

                        }catch (\InvalidArgumentException $e){
                            $urlAvatar =  $node->filter('.listing_thumbs_image .ad-image-b img')->attr('data-original');
                            $urlAvatar = str_replace('listing_thumbs','wm_images',$urlAvatar);

                            if(strpos(mb_strtolower($urlAvatar, 'UTF-8'), 'DefaultAdImage.jpg')  !== FALSE){
                                $urlAvatar = null;
                            }
                        }

                        if(empty($urlAvatar)){
                            try{
                                $urlAvatar =  $node->filter('.listing_thumbs_image .ad-image-b img')->attr('src');
                                $urlAvatar = str_replace('listing_thumbs','wm_images',$urlAvatar);

                                if(strpos(mb_strtolower($urlAvatar, 'UTF-8'), 'DefaultAdImage.jpg')  !== FALSE){
                                    $urlAvatar = null;
                                }
                            }catch (\InvalidArgumentException $e){
                                $urlAvatar = null;
                            }
                        }

                        try{
                            $itemUrl = $node->filter('.ad-subject')->attr('href');
                        }catch (\InvalidArgumentException $e){
                            $itemUrl = null;
                        }
                        try{
                            $title = $node->filter('.ad-subject')->text();
                        }catch (\InvalidArgumentException $e){
                            $title = null;
                        }
                        try{
                            $price = $node->filter('.ad-price')->text();
                        }catch (\InvalidArgumentException $e){
                            $price = null;
                        }
                        try{
                            $district = $node->filter('.municipality')->text();
                        }catch (\InvalidArgumentException $e){
                            $district = null;
                        }
                        try{
                            $isSelf = $node->filter('.subtext')->text();
                            $isSelf = str_replace(['(',')'],'',$isSelf);
                            $isSelf = trim($isSelf);
                        }catch (\InvalidArgumentException $e){
                            $isSelf = null;
                        }
                        $data = [];
                        if($itemUrl){
                            $validate = RealEstateUrlCrawlingItem::findOne(['url'=>$itemUrl]);

                            if($validate !== null){
                                UtilHelper::verifyRePostRealty($validate);
                            }else{
                                $data = [
                                    'title'=>trim($title),
                                    'price'=>trim($price),
                                    'summary'=>'',
                                    'district'=>trim($district),
                                    'isSelf'=>($isSelf == 'Môi giới')?0:1,
                                    //'isSelf'=>$isSelf,
                                    'detailUrl'=>trim($itemUrl),
                                    'source'=>'https://www.chotot.com',
                                    'avatarUrl'=>trim($urlAvatar),
                                    'created'=>Yii::$app->formatter->asDate('now', 'yyyy-MM-dd'),
                                    'validate'=>($validate != null)?0:1,
                                    'systemRealtyId'=>0,
                                    'categoryId' =>$categorySystem
                                ];
                            }
                        }
                        //die(var_dump($urlAvatar));

                        return $data;
                    });

                    if(!empty($items)){
                        foreach($items as $item){
                            if (!empty($item) && isset($item['detailUrl']) && !empty($item['detailUrl'])) {
                                if($item['validate'] != 0) {
                                    $detailData = $this->_crawlingCT($item['detailUrl']);
                                    $data = array_merge($detailData, $item);

                                    if($data['source'] == 'https://www.chotot.com'){
                                        if(strpos(mb_strtolower($data['address'], 'UTF-8'), 'hồ chí minh') === FALSE
                                        || strpos(mb_strtolower($data['address'], 'UTF-8'), 'tp.hcm') === FALSE){
                                            $data['address'] .= ' TP.HCM';
                                        }
                                    }

                                    $rs = $this->_saveItemCrawling(json_encode($data));

                                    if ($rs) {
                                        echo '</br> ****';
                                        echo '</br> Successfull save item';
                                        echo '</br> ' . $rs->title;
                                        echo '</br> ****';
                                    }

                                }
                            }
                        }
                    }
                    if(sleep(2)!=0)
                    {
                        echo "sleep failed script terminating";
                        break;
                    }

                }
            }

        }

    }
    public function actionChoTot($from){

        $categories = RealEstateCategory::find()
            ->where(['site_crawling_id'=>3,'status'=>1])
            ->andOnCondition(['not in','id',[8585,8584]])->all();

        $client = new Client();
        $data = [];
        $data['items'] = [];
        $site = 'https://www.chotot.com/';
        $cateName = null;
        $validate = null;
        $categorySystem = null;
        $parentCategoryId = 1;
        $urlAvatar = null;
        $page = '&o=';
        $to = $from+5;
        if($categories){
            for($from;$from<$to;$from++){
                $from++;
                $page = $page.$from.'#';
                foreach($categories as $cate) {
                    if ($cate) {
                        $cateName = null;
                        $validate = null;
                        $categorySystem = null;

                        $crawler = $client->request('GET', $site . trim($cate->href).$page );
                        //$crawler = $client->request('GET', $site . 'ha-noi/quan-hoan-kiem/sang-nhuong-van-phong-mat-bang-kinh-doanh#');
                    }
                    $categorySystem = $cate->id;

                    $items = $crawler->filter('.chotot-list-row')->each(function (Crawler $node, $i) use($categorySystem) {
                        try{
                            $div = $node->filter('.listing_thumbs_image .extra_img-b img')->last();
                            $urlAvatar = $div->attr('data-original');

                            $urlAvatar = str_replace('listing_thumbs','wm_images',$urlAvatar);

                            if(strpos(mb_strtolower($urlAvatar, 'UTF-8'), 'DefaultAdImage.jpg')  !== FALSE ){
                                $urlAvatar = null;
                            }

                        }catch (\InvalidArgumentException $e){
                            $urlAvatar =  $node->filter('.listing_thumbs_image .ad-image-b img')->attr('data-original');
                            $urlAvatar = str_replace('listing_thumbs','wm_images',$urlAvatar);

                            if(strpos(mb_strtolower($urlAvatar, 'UTF-8'), 'DefaultAdImage.jpg')  !== FALSE){
                                $urlAvatar = null;
                            }
                        }

                        if(empty($urlAvatar)){
                            try{
                                $urlAvatar =  $node->filter('.listing_thumbs_image .ad-image-b img')->attr('src');
                                $urlAvatar = str_replace('listing_thumbs','wm_images',$urlAvatar);

                                if(strpos(mb_strtolower($urlAvatar, 'UTF-8'), 'DefaultAdImage.jpg')  !== FALSE){
                                    $urlAvatar = null;
                                }
                            }catch (\InvalidArgumentException $e){
                                $urlAvatar = null;
                            }
                        }

                        try{
                            $itemUrl = $node->filter('.ad-subject')->attr('href');
                        }catch (\InvalidArgumentException $e){
                            $itemUrl = null;
                        }
                        try{
                            $title = $node->filter('.ad-subject')->text();
                        }catch (\InvalidArgumentException $e){
                            $title = null;
                        }
                        try{
                            $price = $node->filter('.ad-price')->text();
                        }catch (\InvalidArgumentException $e){
                            $price = null;
                        }
                        try{
                            $district = $node->filter('.municipality')->text();
                        }catch (\InvalidArgumentException $e){
                            $district = null;
                        }
                        try{
                            $isSelf = $node->filter('.subtext')->text();
                            $isSelf = str_replace(['(',')'],'',$isSelf);
                            $isSelf = trim($isSelf);
                        }catch (\InvalidArgumentException $e){
                            $isSelf = null;
                        }
                        $data = [];
                        if($itemUrl){
                            $validate = RealEstateUrlCrawlingItem::findOne(['url'=>$itemUrl]);

                            if($validate !== null){
                                UtilHelper::verifyRePostRealty($validate);
                            }else{
                                $data = [
                                    'title'=>trim($title),
                                    'price'=>trim($price),
                                    'summary'=>'',
                                    'district'=>trim($district),
                                    'isSelf'=>($isSelf == 'Môi giới')?0:1,
                                    //'isSelf'=>$isSelf,
                                    'detailUrl'=>trim($itemUrl),
                                    'source'=>'https://www.chotot.com',
                                    'avatarUrl'=>trim($urlAvatar),
                                    'created'=>Yii::$app->formatter->asDate('now', 'yyyy-MM-dd'),
                                    'validate'=>($validate != null)?0:1,
                                    'systemRealtyId'=>0,
                                    'categoryId' =>$categorySystem
                                ];
                            }
                        }
                        //die(var_dump($urlAvatar));

                        return $data;
                    });

                    if(!empty($items)){
                        foreach($items as $item){
                            if (!empty($item) && isset($item['detailUrl']) && !empty($item['detailUrl'])) {
                                if($item['validate'] != 0) {
                                    $detailData = $this->_crawlingCT($item['detailUrl']);
                                    $data = array_merge($detailData, $item);

                                    if($data['source'] == 'https://www.chotot.com'){
                                        if(strpos(mb_strtolower($data['address'], 'UTF-8'), 'hà nội') === FALSE){
                                            $data['address'] .= ' Hà Nội';
                                        }
                                    }

                                    $rs = $this->_saveItemCrawling(json_encode($data));

                                    if ($rs) {
                                        echo '</br> ****';
                                        echo '</br> Successfull save item';
                                        echo '</br> ' . $rs->title;
                                        echo '</br> ****';
                                    }

                                }
                            }
                        }
                    }
                    if(sleep(2)!=0)
                    {
                        echo "sleep failed script terminating";
                        break;
                    }

                }
            }

        }

    }
    public function actionMuaBanB(){
        $client = new Client();
        $site = 'https://muaban.net/';
        $pages = ['?cp=1','?cp=2','?cp=3','?cp=4','?cp=5','?cp=6','?cp=7','?cp=8','?cp=9','?cp=10'];
        $validate = null;
        $categorySystem = null;

        $categories = RealEstateCategory::find()
            ->where(['site_crawling_id'=>1,'status'=>1])
            ->offset(5)
            ->orderBy('id')->all();

        if($categories){
            foreach($pages as $page){
                foreach($categories as $cate){
                    if($cate){
                        $cateName= null;
                        $validate = null;
                        $categorySystem = null;

                        $crawler = $client->request('GET', $site . trim($cate->href) . $page);

                    }
                    $categorySystem = $cate->id;

                    $items = $crawler->filter('.mbn-box-list-content')->each(function (Crawler $node, $i ) use ( $categorySystem) {
                        try{
                            $title = $node->filter('.mbn-content h2')->text();
                        }catch (\InvalidArgumentException $e){
                            $title = null;
                        }
                        try{
                            $price = $node->filter('.mbn-price')->text();
                        }catch (\InvalidArgumentException $e){
                            $price = null;
                        }
                        try{
                            $address = $node->filter('.mbn-address')->text();
                        }catch (\InvalidArgumentException $e){
                            $address = null;
                        }
                        try{
                            $createAt = $node->filter('.mbn-date')->text();

                        }catch (\InvalidArgumentException $e){
                            $createAt = null;
                        }
                        try{
                            $area = $node->filter('.mbn-item-area')->text();
                            $area = str_replace('Diện tích:','',$area);
                        }catch (\InvalidArgumentException $e){
                            $area = null;
                        }
                        try{
                            $img = $node->filter('.mbn-image img')->attr('src');
                        }catch (\InvalidArgumentException $e){
                            $img = null;
                        }
                        try{
                            $itemUrl =  $node->filter('a')->attr('href');
                        }catch (\InvalidArgumentException $e){
                            $itemUrl = null;
                        }
                        if($img == null || empty($img)){
                            $img = null;
                        }
                        $validate = null;
                        $data = [];
                        if($itemUrl){
                            $validate = RealEstateUrlCrawlingItem::findOne(['url'=>$itemUrl]);
                            if($validate !== null){
                                UtilHelper::verifyRePostRealty($validate);
                            }else{
                                $data = [
                                    'avatarUrl'=>trim(str_replace('thumb-list','thumb-detail',$img)),
                                    'title'=>trim($title),
                                    'summary'=>trim(''),
                                    //'category'=>trim($category),
                                    'price'=>trim($price),
                                    'area'=>trim($area),
                                    'source'=>'https://muaban.net',
                                    'address'=>trim($address),
                                    'created'=>trim($createAt),
                                    'detailUrl'=>trim($itemUrl),
                                    'validate'=>($validate != null)?0:1,
                                    'systemRealtyId'=>0,
                                    'categoryId' =>$categorySystem
                                ];
                            }
                        }
                        return $data;
                    });

                    if(!empty($items)){
                        foreach($items as $item){
                                if (!empty($item) && isset($item['detailUrl']) && !empty($item['detailUrl'])) {
                                    if($item['validate'] != 0) {
                                    $detailData = $this->_crawlingMBDetail($item['detailUrl']);
                                    $data = array_merge($detailData, $item);
                                    $rs = $this->_saveItemCrawling(json_encode($data));
                                    if ($rs) {
                                        echo '</br> ****';
                                        echo '</br> Successfull save item';
                                        echo '</br> ' . $rs->title;
                                        echo '</br> ****';
                                    }
                                }
                            }
                        }
                    }

                    if(sleep(1))
                    {
                        echo "sleep failed script terminating";
                        break;
                    }
                }
            }
        }

    }
    public function actionMuaBan(){
        $client = new Client();
        $site = 'https://muaban.net/';
        $pages = ['?cp=1','?cp=2','?cp=3','?cp=4','?cp=5','?cp=6','?cp=7','?cp=8','?cp=9','?cp=10'];
        $validate = null;
        $categorySystem = null;

        $categories = RealEstateCategory::find()->where(['site_crawling_id'=>1,'status'=>1])
            ->limit(5)
            ->orderBy('id')
            ->all();

        if($categories){
            foreach($pages as $page){
                foreach($categories as $cate){
                    if($cate){
                        $cateName= null;
                        $validate = null;
                        $categorySystem = null;

                        $crawler = $client->request('GET', $site .trim($cate->href). $page);

                    }
                    $categorySystem = $cate->id;

                    $items = $crawler->filter('.mbn-box-list-content')->each(function (Crawler $node, $i ) use ( $categorySystem) {
                        try{
                            $title = $node->filter('.mbn-content h2')->text();
                        }catch (\InvalidArgumentException $e){
                            $title = null;
                        }
                        try{
                            $price = $node->filter('.mbn-price')->text();
                        }catch (\InvalidArgumentException $e){
                            $price = null;
                        }
                        try{
                            $address = $node->filter('.mbn-address')->text();
                        }catch (\InvalidArgumentException $e){
                            $address = null;
                        }
                        try{
                            $createAt = $node->filter('.mbn-date')->text();

                        }catch (\InvalidArgumentException $e){
                            $createAt = null;
                        }
                        try{
                            $area = $node->filter('.mbn-item-area')->text();
                            $area = str_replace('Diện tích:','',$area);
                        }catch (\InvalidArgumentException $e){
                            $area = null;
                        }
                        try{
                            $img = $node->filter('.mbn-image img')->attr('src');
                        }catch (\InvalidArgumentException $e){
                            $img = null;
                        }
                        try{
                            $itemUrl =  $node->filter('a')->attr('href');
                        }catch (\InvalidArgumentException $e){
                            $itemUrl = null;
                        }
                        if($img == null || empty($img)){
                            $img = null;
                        }
                        $validate = null;
                        $data = [];
                        if($itemUrl){
                            $validate = RealEstateUrlCrawlingItem::findOne(['url'=>$itemUrl]);
                            if($validate !== null){
                                UtilHelper::verifyRePostRealty($validate);
                            }else{
                                $data = [
                                    'avatarUrl'=>trim(str_replace('thumb-list','thumb-detail',$img)),
                                    'title'=>trim($title),
                                    'summary'=>trim(''),
                                    //'category'=>trim($category),
                                    'price'=>trim($price),
                                    'area'=>trim($area),
                                    'source'=>'https://muaban.net',
                                    'address'=>trim($address),
                                    'created'=>trim($createAt),
                                    'detailUrl'=>trim($itemUrl),
                                    'validate'=>($validate != null)?0:1,
                                    'systemRealtyId'=>0,
                                    'categoryId' =>$categorySystem
                                ];
                            }
                        }
                        return $data;
                    });

                    if(!empty($items)){
                        foreach($items as $item) {
                                if (!empty($item) && isset($item['detailUrl']) && !empty($item['detailUrl'])) {
                                    if($item['validate'] != 0) {
                                    $detailData = $this->_crawlingMBDetail($item['detailUrl']);
                                    $data = array_merge($detailData, $item);
                                    $rs = $this->_saveItemCrawling(json_encode($data));
                                    if ($rs) {
                                        echo '</br> ****';
                                        echo '</br> Successfull save item';
                                        echo '</br> ' . $rs->title;
                                        echo '</br> ****';
                                    }
                                }
                            }
                        }
                    }

                    if(sleep(1))
                    {
                        echo "sleep failed script terminating";
                        break;
                    }
                }
            }
        }

    }
    public function actionMuaBanStartDayA(){
        $client = new Client();
        $site = 'https://muaban.net/';
        $pages = ['?cp=1','?cp=2','?cp=3','?cp=4','?cp=5'];
        $validate = null;
        $categorySystem = null;

        $categories = RealEstateCategory::find()->where(['site_crawling_id'=>1,'status'=>1])
            ->limit(3)
            ->orderBy('id')
            ->all();

        if($categories){
            for($i=5;$i<20;$i++){
                $i++;
                foreach($categories as $cate){
                    if($cate){
                        $cateName= null;
                        $validate = null;
                        $categorySystem = null;

                        $crawler = $client->request('GET', $site .trim($cate->href). '?cp='.$i);

                    }
                    $categorySystem = $cate->id;

                    $items = $crawler->filter('.mbn-box-list-content')->each(function (Crawler $node, $i ) use ( $categorySystem) {
                        try{
                            $title = $node->filter('.mbn-content h2')->text();
                        }catch (\InvalidArgumentException $e){
                            $title = null;
                        }
                        try{
                            $price = $node->filter('.mbn-price')->text();
                        }catch (\InvalidArgumentException $e){
                            $price = null;
                        }
                        try{
                            $address = $node->filter('.mbn-address')->text();
                        }catch (\InvalidArgumentException $e){
                            $address = null;
                        }
                        try{
                            $createAt = $node->filter('.mbn-date')->text();

                        }catch (\InvalidArgumentException $e){
                            $createAt = null;
                        }
                        try{
                            $area = $node->filter('.mbn-item-area')->text();
                            $area = str_replace('Diện tích:','',$area);
                        }catch (\InvalidArgumentException $e){
                            $area = null;
                        }
                        try{
                            $img = $node->filter('.mbn-image img')->attr('src');
                        }catch (\InvalidArgumentException $e){
                            $img = null;
                        }
                        try{
                            $itemUrl =  $node->filter('a')->attr('href');
                        }catch (\InvalidArgumentException $e){
                            $itemUrl = null;
                        }
                        if($img == null || empty($img)){
                            $img = null;
                        }
                        $validate = null;
                        $data = [];
                        if($itemUrl){
                            $validate = RealEstateUrlCrawlingItem::findOne(['url'=>$itemUrl]);
                            if($validate !== null){
                                UtilHelper::verifyRePostRealty($validate);
                            }else{
                                $data = [
                                    'avatarUrl'=>trim(str_replace('thumb-list','thumb-detail',$img)),
                                    'title'=>trim($title),
                                    'summary'=>trim(''),
                                    //'category'=>trim($category),
                                    'price'=>trim($price),
                                    'area'=>trim($area),
                                    'source'=>'https://muaban.net',
                                    'address'=>trim($address),
                                    'created'=>trim($createAt),
                                    'detailUrl'=>trim($itemUrl),
                                    'validate'=>($validate != null)?0:1,
                                    'systemRealtyId'=>0,
                                    'categoryId' =>$categorySystem
                                ];
                            }
                        }
                        return $data;
                    });

                    if(!empty($items)){
                        foreach($items as $item){
                                if(!empty($item) && isset($item['detailUrl']) && !empty($item['detailUrl'])){
                                    if($item['validate'] != 0) {
                                    $detailData = $this->_crawlingMBDetail($item['detailUrl']);
                                    $data = array_merge($detailData,$item);
                                    $rs = $this->_saveItemCrawling(json_encode($data));
                                    if($rs){
                                        echo '</br> ****';
                                        echo '</br> Successfull save item';
                                        echo '</br> ' . $rs->title;
                                        echo '</br> ****';
                                    }
                                }
                            }
                        }
                    }

                    if(sleep(1))
                    {
                        echo "sleep failed script terminating";
                        break;
                    }
                }
            }
        }
    }

    public function actionMuaBanStartDayB(){
        $client = new Client();
        $site = 'https://muaban.net/';
        $validate = null;
        $categorySystem = null;

        $categories = RealEstateCategory::find()->where(['site_crawling_id'=>1,'status'=>1])
            ->offset(3)
            ->limit(3)
            ->orderBy('id')
            ->all();

        if($categories){
            for($i=5;$i<20;$i++){
                $i++;
                foreach($categories as $cate){
                    if($cate){
                        $cateName= null;
                        $validate = null;
                        $categorySystem = null;

                        $crawler = $client->request('GET', $site .trim($cate->href). '?cp='.$i);

                    }
                    $categorySystem = $cate->id;

                    $items = $crawler->filter('.mbn-box-list-content')->each(function (Crawler $node, $i ) use ( $categorySystem) {
                        try{
                            $title = $node->filter('.mbn-content h2')->text();
                        }catch (\InvalidArgumentException $e){
                            $title = null;
                        }
                        try{
                            $price = $node->filter('.mbn-price')->text();
                        }catch (\InvalidArgumentException $e){
                            $price = null;
                        }
                        try{
                            $address = $node->filter('.mbn-address')->text();
                        }catch (\InvalidArgumentException $e){
                            $address = null;
                        }
                        try{
                            $createAt = $node->filter('.mbn-date')->text();

                        }catch (\InvalidArgumentException $e){
                            $createAt = null;
                        }
                        try{
                            $area = $node->filter('.mbn-item-area')->text();
                            $area = str_replace('Diện tích:','',$area);
                        }catch (\InvalidArgumentException $e){
                            $area = null;
                        }
                        try{
                            $img = $node->filter('.mbn-image img')->attr('src');
                        }catch (\InvalidArgumentException $e){
                            $img = null;
                        }
                        try{
                            $itemUrl =  $node->filter('a')->attr('href');
                        }catch (\InvalidArgumentException $e){
                            $itemUrl = null;
                        }
                        if($img == null || empty($img)){
                            $img = null;
                        }
                        $validate = null;
                        $data = [];
                        if($itemUrl){
                            $validate = RealEstateUrlCrawlingItem::findOne(['url'=>$itemUrl]);
                            if($validate !== null){
                                UtilHelper::verifyRePostRealty($validate);
                            }else{
                                $data = [
                                    'avatarUrl'=>trim(str_replace('thumb-list','thumb-detail',$img)),
                                    'title'=>trim($title),
                                    'summary'=>trim(''),
                                    //'category'=>trim($category),
                                    'price'=>trim($price),
                                    'area'=>trim($area),
                                    'source'=>'https://muaban.net',
                                    'address'=>trim($address),
                                    'created'=>trim($createAt),
                                    'detailUrl'=>trim($itemUrl),
                                    'validate'=>($validate != null)?0:1,
                                    'systemRealtyId'=>0,
                                    'categoryId' =>$categorySystem
                                ];
                            }
                        }
                        return $data;
                    });

                    if(!empty($items)){
                        foreach($items as $item){
                                if (!empty($item) && isset($item['detailUrl']) && !empty($item['detailUrl'])) {
                                    if($item['validate'] != 0) {
                                    $detailData = $this->_crawlingMBDetail($item['detailUrl']);
                                    $data = array_merge($detailData, $item);
                                    $rs = $this->_saveItemCrawling(json_encode($data));
                                    if ($rs) {
                                        echo '</br> ****';
                                        echo '</br> Successfull save item';
                                        echo '</br> ' . $rs->title;
                                        echo '</br> ****';
                                    }
                                }
                            }
                        }
                    }

                    if(sleep(1))
                    {
                        echo "sleep failed script terminating";
                        break;
                    }
                }
            }
        }
    }

    public function actionMuaBanStartDayC(){
        $client = new Client();
        $site = 'https://muaban.net/';
        $validate = null;
        $categorySystem = null;

        $categories = RealEstateCategory::find()->where(['site_crawling_id'=>1,'status'=>1])
            ->offset(6)
            ->orderBy('id')
            ->all();

        if($categories){
            for($i=5;$i<20;$i++){
                $i++;
                foreach($categories as $cate){
                    if($cate){
                        $cateName= null;
                        $validate = null;
                        $categorySystem = null;

                        $crawler = $client->request('GET', $site .trim($cate->href). '?cp='.$i);

                    }
                    $categorySystem = $cate->id;

                    $items = $crawler->filter('.mbn-box-list-content')->each(function (Crawler $node, $i ) use ( $categorySystem) {
                        try{
                            $title = $node->filter('.mbn-content h2')->text();
                        }catch (\InvalidArgumentException $e){
                            $title = null;
                        }
                        try{
                            $price = $node->filter('.mbn-price')->text();
                        }catch (\InvalidArgumentException $e){
                            $price = null;
                        }
                        try{
                            $address = $node->filter('.mbn-address')->text();
                        }catch (\InvalidArgumentException $e){
                            $address = null;
                        }
                        try{
                            $createAt = $node->filter('.mbn-date')->text();

                        }catch (\InvalidArgumentException $e){
                            $createAt = null;
                        }
                        try{
                            $area = $node->filter('.mbn-item-area')->text();
                            $area = str_replace('Diện tích:','',$area);
                        }catch (\InvalidArgumentException $e){
                            $area = null;
                        }
                        try{
                            $img = $node->filter('.mbn-image img')->attr('src');
                        }catch (\InvalidArgumentException $e){
                            $img = null;
                        }
                        try{
                            $itemUrl =  $node->filter('a')->attr('href');
                        }catch (\InvalidArgumentException $e){
                            $itemUrl = null;
                        }
                        if($img == null || empty($img)){
                            $img = null;
                        }
                        $validate = null;
                        $data = [];
                        if($itemUrl){
                            $validate = RealEstateUrlCrawlingItem::findOne(['url'=>$itemUrl]);
                            if($validate !== null){
                                UtilHelper::verifyRePostRealty($validate);
                            }else{
                                $data = [
                                    'avatarUrl'=>trim(str_replace('thumb-list','thumb-detail',$img)),
                                    'title'=>trim($title),
                                    'summary'=>trim(''),
                                    //'category'=>trim($category),
                                    'price'=>trim($price),
                                    'area'=>trim($area),
                                    'source'=>'https://muaban.net',
                                    'address'=>trim($address),
                                    'created'=>trim($createAt),
                                    'detailUrl'=>trim($itemUrl),
                                    'validate'=>($validate != null)?0:1,
                                    'systemRealtyId'=>0,
                                    'categoryId' =>$categorySystem
                                ];
                            }
                        }
                        return $data;
                    });

                    if(!empty($items)){
                        foreach($items as $item){
                                if (!empty($item) && isset($item['detailUrl']) && !empty($item['detailUrl'])) {
                                    if($item['validate'] != 0) {
                                    $detailData = $this->_crawlingMBDetail($item['detailUrl']);
                                    $data = array_merge($detailData, $item);
                                    $rs = $this->_saveItemCrawling(json_encode($data));
                                    if ($rs) {
                                        echo '</br> ****';
                                        echo '</br> Successfull save item';
                                        echo '</br> ' . $rs->title;
                                        echo '</br> ****';
                                    }
                                }
                            }
                        }
                    }

                    if(sleep(1))
                    {
                        echo "sleep failed script terminating";
                        break;
                    }
                }
            }
        }
    }

    public function actionMuaBanNhaNgoHemHaNoi($from=20){
        $client = new Client();
        $site = 'https://muaban.net/';
        $validate = null;
        $categorySystem = null;

        $cate = RealEstateCategory::find()
            ->where(['site_crawling_id'=>1,'status'=>1])
            ->andFilterWhere(['like','href','nha-hem-ngo-ha-noi-l24-c3202'])
            ->one();
        $to = $from + 20;
        if($cate){
            for($from;$from < $to;$from++){
                $from++;

                $cateName= null;
                $validate = null;
                $categorySystem = null;

                $crawler = $client->request('GET', $site .trim($cate->href). '?cp='.$from);


                    $categorySystem = $cate->id;

                    $items = $crawler->filter('.mbn-box-list-content')->each(function (Crawler $node, $i ) use ( $categorySystem) {
                        try{
                            $title = $node->filter('.mbn-content h2')->text();
                        }catch (\InvalidArgumentException $e){
                            $title = null;
                        }
                        try{
                            $price = $node->filter('.mbn-price')->text();
                        }catch (\InvalidArgumentException $e){
                            $price = null;
                        }
                        try{
                            $address = $node->filter('.mbn-address')->text();
                        }catch (\InvalidArgumentException $e){
                            $address = null;
                        }
                        try{
                            $createAt = $node->filter('.mbn-date')->text();

                        }catch (\InvalidArgumentException $e){
                            $createAt = null;
                        }
                        try{
                            $area = $node->filter('.mbn-item-area')->text();
                            $area = str_replace('Diện tích:','',$area);
                        }catch (\InvalidArgumentException $e){
                            $area = null;
                        }
                        try{
                            $img = $node->filter('.mbn-image img')->attr('src');
                        }catch (\InvalidArgumentException $e){
                            $img = null;
                        }
                        try{
                            $itemUrl =  $node->filter('a')->attr('href');
                        }catch (\InvalidArgumentException $e){
                            $itemUrl = null;
                        }
                        if($img == null || empty($img)){
                            $img = null;
                        }
                        $validate = null;
                        $data = [];
                        if($itemUrl){
                            $validate = RealEstateUrlCrawlingItem::findOne(['url'=>$itemUrl]);
                            if($validate !== null){
                                UtilHelper::verifyRePostRealty($validate);
                            }else{
                                $data = [
                                    'avatarUrl'=>trim(str_replace('thumb-list','thumb-detail',$img)),
                                    'title'=>trim($title),
                                    'summary'=>trim(''),
                                    //'category'=>trim($category),
                                    'price'=>trim($price),
                                    'area'=>trim($area),
                                    'source'=>'https://muaban.net',
                                    'address'=>trim($address),
                                    'created'=>trim($createAt),
                                    'detailUrl'=>trim($itemUrl),
                                    'validate'=>($validate != null)?0:1,
                                    'systemRealtyId'=>0,
                                    'categoryId' =>$categorySystem
                                ];
                            }
                        }
                        return $data;
                    });

                    if(!empty($items)){
                        foreach($items as $item){
                                if (!empty($item) && isset($item['detailUrl']) && !empty($item['detailUrl'])) {
                                    if($item['validate'] != 0) {
                                    $detailData = $this->_crawlingMBDetail($item['detailUrl']);
                                    $data = array_merge($detailData, $item);
                                    $rs = $this->_saveItemCrawling(json_encode($data));
                                    if ($rs) {
                                        echo '</br> ****';
                                        echo '</br> Successfull save item';
                                        echo '</br> ' . $rs->title;
                                        echo '</br> ****';
                                    }
                                }
                            }
                        }
                    }

                    if(sleep(1))
                    {
                        echo "sleep failed script terminating";
                        break;
                    }

            }
        }
    }

    public function actionCrawlTesting(){
        $url = 'https://www.chotot.com/ha-noi/quan-hoan-kiem/sang-nhuong-van-phong-mat-bang-kinh-doanh#';
        $this->_crawlingCT($url);
    }
    private  function _crawlingMBDetail($url){
        $data = [];
        $client = new Client();
        $crawler = $client->request('GET', $url);

        try{
            $data['title'] = trim($crawler->filter('.cl-title > h1')->text());
        }catch (\InvalidArgumentException $e){
            $data['title'] = null;
        }
        try{
            $data['region_sale'] = trim($crawler->filter('.diadiem-title a')->text());
        }catch (\InvalidArgumentException $e){
            $data['region_sale'] = null;
        }
        try{
            $data['region_sale_url'] = $crawler->filter('.diadiem-title a')->attr('href');
        }catch (\InvalidArgumentException $e){
            $data['region_sale_url'] = null;
        }
        try{
            $data['price'] = trim($crawler->filter('.price-value')->text());
        }catch (\InvalidArgumentException $e){
            $data['price'] = null;
        }

        try{
            $data['description'] = $crawler->filter('.ct-body')->text();

        }catch (\InvalidArgumentException $e){
            $data['description'] = null;
        }

        try{
            $images = $crawler->filter('#owl-carousel-detail img')->each(function (Crawler $node, $i) {
                return ['src'=>$node->attr('src')];
            });
        }catch (\InvalidArgumentException $e){
            $images = [];
        }

        try{
            $itemDetail = $crawler->filter('.tect-item')->each(function (Crawler $node, $i) {
                if(strpos(mb_strtolower(trim($node->text()), 'UTF-8'), 'địa chỉ') !== FALSE){
                    $addressDetail = trim($node->text());
                    return str_replace('Địa chỉ:','',$addressDetail);
                };
            });
        }catch (\InvalidArgumentException $e){
            $itemDetail = [];
        }

        $itemDetail =  array_filter($itemDetail);
        $addressDetail =  array_shift($itemDetail);

        $data['addressStreet'] = str_replace('-','',$addressDetail);

        $data['lat'] = null;
        $data['long'] = null;
        /*try{
            $data['lat'] = trim($crawler->filter('#hdLat')->attr('value'));
        }catch (\InvalidArgumentException $e){
            $data['lat'] = null;
        }
        try{
            $data['long'] = trim($crawler->filter('#hdLong')->attr('value'));
        }catch (\InvalidArgumentException $e){
            $data['long'] = null;
        }*/
        try{
            $data['contact_phone'] = trim($crawler->filter('.contact-mobile')->text());
        }catch (\InvalidArgumentException $e){
            $data['contact_phone'] = null;
        }
        try{
            $data['contact_name'] = trim($crawler->filter('.contact-name')->text());
        }catch (\InvalidArgumentException $e){
            $data['contact_name'] =null;
        }

        $data['contact_phone_backup'] = null;


        $data['images'] = $images;

        return $data;
    }
    public function _saveItemCrawling($data){

        $avatar = null;
        $phone = null;
        $phone_backup = null;
        $data = json_decode($data);
        $numberPrice = null;
        $model = new RealEstateItem();

        if(isset($data->validate) && $data->validate == 0 ){
            return null;
//            $model =  RealEstateItem::findOne(['id'=>isset($data->systemRealtyId)?$data->systemRealtyId:0]);
//
//            if(!$model){
//                $model = new RealEstateItem();
//            }else{
//
//                $repost_date_timestamp = UtilHelper::getNowUnixUTC();
//
//                $itemDate = Yii::$app->formatter->asDate($model->created_at,'Y-M-d');
//                $now = Yii::$app->formatter->asDate($repost_date_timestamp,'Y-M-d');
//
//                if($itemDate !== $now){
//                    $model->repost_date  = $repost_date_timestamp;
//                    $model->is_new  = 0;
//                    $model->save(false);
//                    return $model;
//                }
//            }
        }
        if(!empty($data)){
//            if(isset($data->avatarUrl) && $data->avatarUrl && !empty($data->avatarUrl)){
//                //$avatar = $data->avatarUrl;
//                $avatar = UtilHelper::downloadImageFromLink(Yii::getAlias('@publicProductImagePatch'),$data->avatarUrl,false);
//            }
            $phone = $data->contact_phone;
            $phone_backup = $data->contact_phone_backup;
            if($data->contact_phone && !UtilHelper::commonIsImageUrl($data->contact_phone)){
                $phone = str_replace('.','',$data->contact_phone);
            }
            if($data->contact_phone_backup && !UtilHelper::commonIsImageUrl($data->contact_phone_backup)){
                $phone_backup = str_replace('.','',$data->contact_phone_backup);
            }
            $date = trim($data->created);

            $date = explode('/',$date);
            if(is_array($date) && count($date) == 3 ){
                $date = $date[2].'-'.$date[1].'-'.$date[0];
            }else{
                $date = $date[0];
            }

            if(isset($data->price) && $data->price){
                $numberPrice = UtilHelper::convertPriceStringToNumber($data->price);
            }
            $address = (isset($data->address) && $data->address)?trim($data->address):null;
            $addressStreet = (isset($data->addressStreet) && $data->addressStreet)?trim($data->addressStreet):null;

            if(!empty($addressStreet)){
                $addressFull = $addressStreet.', '.$address;
            }else{
                $addressFull = $address;
            }
            $date = new \DateTime($date, new \DateTimeZone('UTC'));
            $timestamp = $date->format('U');

            $description = UtilHelper::clearSpecialCharacter(($data->description)?trim($data->description):null);

            $description = preg_replace('/<[^>]*>/', '', $description);

            $realEstate = [
                'avatar'=>$avatar,
                'title'=>($data->title)?trim($data->title):null,
                'description'=>$description,
                'summary'=>($data->summary)?trim($data->summary):null,
                'source'=>($data->source)?trim($data->source):null,
                'status'=>1,
                'phone_contact'=>($phone)?trim($phone):$phone_backup,
                'phone_backup'=>($phone_backup)?trim($phone_backup):$phone,
                'address'=>$addressFull,
                'country'=>'Việt Nam',
                'type'=>($data->region_sale)?trim($data->region_sale):null,
                'price_string'=>($data->price)?trim($data->price):null,
                'price'=>$numberPrice,
                'unit'=>'VND',
                'contact_name'=>(isset($data->contact_name) && $data->contact_name)?trim($data->contact_name):null,
                'area'=>($data->area)?trim($data->area):null,

                'category_name'=>($data->region_sale)?trim($data->region_sale):null,
                'real_estate_date'=>($timestamp)?$timestamp:null,
               // 'latitude'=>$lat,
               // 'longitude'=>$lng,
               // 'real_estate_date'=>(isset($data->created) && !empty($data->created))
                'is_self'=>0,
                'district'=>(isset($data->district) && $data->district)?trim($data->district):null,
                'category_id'=>$data->categoryId
            ];

            $model->attributes = $realEstate;
           // $model->description = "Bán nhà hẻm 417 Quang Trung, p10, GV, hẻm đoos diện ủy ban Quận, hẻm 1 trục rộng 7m thông, vị trí đẹp tiện buôn bán như tạp hóa, spa, gội đầu, thuốc tây, văn phòng... DTKV: 4.2x8m, ko bị lộ giới, nhà xây 1 lửng 3 lầu, thiết kế cầu thang cuối.Nhà sổ hồng chính chủ, đang cho thuê 9 triệu/ tháng.Giá bán 2.6 tỷ.Lh: 096543 2345. Văn Quyết. Mtg";
            if(empty($phone)){
                $phone = $phone_backup;
            }
           $area_number =  str_replace(' m²','',$model->area);
            $area_number=  str_replace(' m2','',$model->area);

            $model->area_number = (int)$area_number;

            $checkNumber = RealEstateItem::find()
                ->andOnCondition(['phone_contact'=>$phone])->all();

            if (UtilHelper::validateIsSelfRealItem($model->title, $model->description,$model->address)) {

                $model->is_self = 1;

                $lat = null;
                $lng = null;

                //get location
                $location = UtilHelper::detectAddress($addressFull);

                if(!empty($location)){
                    $lat = isset($location['lat'])?$location['lat']:null;
                    $lng = isset($location['lng'])?$location['lng']:null;
                }

                $model->latitude = $lat;
                $model->longitude = $lng;

                try{
                    $model->save(false);
                }catch (Exception $e){
                    Yii::error('Error \'s name: '.$e->getMessage(), 'Crawling');
                }

            }else{
                $model->is_self = 0;
            }

            if(count($checkNumber) > 3){
                $model->is_self = 0;
                foreach($checkNumber as $itemBefore){
                    $itemBefore->is_self=0;
                    $model->is_not_sure_self = 0;

                    try{
                        $itemBefore->save(false);
                    }catch (Exception $e){
                        Yii::error('Error \'s name: '.$e->getMessage(), 'Crawling');
                    }

                }
            }

            if(count($checkNumber) > 0 && count($checkNumber) <= 3){
                $model->is_not_sure_self = 1;

                foreach($checkNumber as $itemBefore){
                    $model->is_not_sure_self = 1;
                    try{
                        $itemBefore->save(false);
                    }catch (Exception $e){
                        Yii::error('Error \'s name: '.$e->getMessage(), 'Crawling');
                    }
                }

            }

            try{
                if($model->save(false)) {
                    if ($data->validate == true && $data->systemRealtyId == 0) {
                        $detailUrlItem = new RealEstateUrlCrawlingItem();
                        $detailUrlItem->url = $data->detailUrl;
                        $detailUrlItem->real_estate_item_id = $model->id;
                        $detailUrlItem->save();
                    }

                    if($model->source == 'https://www.chotot.com'){
                        $phone_number =  UtilHelper::detectPhoneNumberFromImage($model->phone_contact);
                        $model->is_not_sure_self = 1;
                        $model->phone_contact = $phone_number;
                        $model->save(false);

                    }

                    //check số đt từ kho môi giới
                    $checkAgencyPhone = RealEstatePhoneContact::findOne($model->phone_contact);
                    if($checkAgencyPhone){
                        $model->is_self  = 0;
                        $model->save(false);
                    }

                    if ($model->is_self == 0) {
                        if ($phone && !empty($phone)) {
                            $phoneContactModel = new RealEstatePhoneContact();
                            $phoneContactModel->phone = $phone;
                            try {
                                $phoneContactModel->save(false);
                                $itemHasPhone = new RealEstateItemHasPhoneContact();
                                $itemHasPhone->item_id = $model->id;
                                $itemHasPhone->phone_contact_id = $phoneContactModel->id;
                                $itemHasPhone->phone = $phone;
                                $itemHasPhone->save(false);
                            } catch (\yii\db\IntegrityException $e) {

                            }
                        }
                        if ($phone_backup && !empty($phone_backup)) {
                            $phoneContactModelBackup = new RealEstatePhoneContact();
                            $phoneContactModelBackup->phone = $phone_backup;
                            try {
                                $phoneContactModelBackup->save(false);
                                $itemHasPhone = new RealEstateItemHasPhoneContact();
                                $itemHasPhone->item_id = $model->id;
                                $itemHasPhone->phone_contact_id = $phoneContactModelBackup->id;
                                $itemHasPhone->phone = $phone_backup;
                                $itemHasPhone->save(false);
                            } catch (\yii\db\IntegrityException $e) {

                            }
                        }
                    }
                    //tracking it is a new item
                    $repost_date_timestamp = UtilHelper::getNowUnixUTC();
                    UtilHelper::trackingRePostDate($model, $repost_date_timestamp, true);

                    return $model;
                }else{

                    Yii::error('Error \'s name: save reatlty failed Crawling');
                }
            }catch (Exception $e){
                Yii::error('Error \'s name: '.$e->getMessage(), 'Crawling');
            }
        }
    }

    private function _crawlingBDSDetail($url){
   //public function actionTest(){
        //$url = 'http://batdongsan.com.vn/ban-nha-mat-pho-duong-tran-duy-hung-7/toa-van-ng-9-tang-dien-tich-so-do-95m2-1000m2-xay-dung-mt-6m-pr10432608';
        $data = [];
        $client = new Client();
        $array = [];
        $crawler = $client->request('GET', $url);
        try{
            $data['title'] = trim($crawler->filter('.pm-title > h1')->text());
        }catch (\InvalidArgumentException $e){
            $data['title'] = null;
        }
        try{
            $data['region_sale'] = trim($crawler->filter('.diadiem-title a')->text());
        }catch (\InvalidArgumentException $e){
            $data['region_sale'] = null;
        }
        try{
            $data['region_sale_url'] = $crawler->filter('.diadiem-title a')->attr('href');
        }catch (\InvalidArgumentException $e){
            $data['region_sale_url'] = null;
        }
        try{
            $data['price'] = trim($crawler->filter('.gia-title strong')->text());
        }catch (\InvalidArgumentException $e){
            $data['price'] = null;
        }
        try{
            $array = $crawler->filter('.gia-title')->each(function (Crawler $node, $i) {
                return $node->text();
            });
        }catch (\InvalidArgumentException $e){
            $array = [];
        }
        try{
            $data['description'] = $crawler->filter('.pm-content')->html();
        }catch (\InvalidArgumentException $e){
            $data['description'] = null;
        }
        try{
            $tag = $crawler->filter('#LeftMainContent__productDetail_panelTag')->html();
        }catch (\InvalidArgumentException $e){
            $tag = null;
        }
        try{
            $images = $crawler->filter('#thumbs li img')->each(function (Crawler $node, $i) {
                return ['src'=>str_replace('80x60','745x510',$node->attr('src'))];
            });
        }catch (\InvalidArgumentException $e){
            $images = [];
        }
        try{
            $data['lat'] = trim($crawler->filter('#hdLat')->attr('value'));
        }catch (\InvalidArgumentException $e){
            $data['lat'] = null;
        }
        try{
            $data['long'] = trim($crawler->filter('#hdLong')->attr('value'));
        }catch (\InvalidArgumentException $e){
            $data['long'] = null;
        }
        try{
            $data['contact_phone'] = trim(str_replace('Điện thoại','',$crawler->filter('#LeftMainContent__productDetail_contactPhone')->text()));
        }catch (\InvalidArgumentException $e){
            $data['contact_phone'] = null;
        }
        try{
            $data['contact_name'] = trim(str_replace('Tên liên lạc','',$crawler->filter('#LeftMainContent__productDetail_contactName')->text()));
        }catch (\InvalidArgumentException $e){
            $data['contact_name'] =null;
        }
        try{
            $data['contact_phone_backup'] = trim(str_replace('Mobile','',$crawler->filter('#LeftMainContent__productDetail_contactMobile')->text()));
        }catch (\InvalidArgumentException $e){
            $data['contact_phone_backup'] = null;
        }
        try{
            $tagVideo = $crawler->filter('#LeftMainContent__productDetail_ltVideo')->html();
        }catch (\InvalidArgumentException $e){
            $tagVideo = null;
        }
       try{
           $addressA = $crawler->filter('.diadiem-title a')->text();

       }catch (\InvalidArgumentException $e){
           $addressA = null;
       }
        try{
            $address = $crawler->filter('.diadiem-title')->text();

        }catch (\InvalidArgumentException $e){
            $address = null;
        }

        try{
            $detail = $crawler->filter('.left-detail div')->each(function (Crawler $node, $i) {
                return $node->text();
            });
        }catch (\InvalidArgumentException $e){
            $detail = [];
        }

        $data['address'] = (isset($detail[2]))?$detail[2]:$detail[0];

        $data['address'] =  mb_convert_encoding($data['address'], 'UTF-8', 'UTF-8');

        $data['price'] =trim(str_replace('Giá:','',(isset($array[0]))?$array[0]:null));
        $data['area'] = trim(str_replace('Diện tích:','',(isset($array[1]))?$array[1]:null));
        $data['description'] = str_replace($tag,'',$data['description']);
        $data['description'] = str_replace($tagVideo,'',$data['description']);
        $data['description'] = str_replace('<div id="LeftMainContent__productDetail_panelTag" class="tagpanel"></div>',
            '',$data['description']);
        $data['images'] = $images;
        return $data;
    }
    private function _crawlingCT($url){
   // public function actionDetail(){
       // $url = 'https://www.chotot.com/quan-thu-duc/mua-ban-nha-dat/nha-huong-tb-1125-tinh-lo-43-binh-chieu-thu-duc-25478436.htm#';
        $data = [];
        $client = new Client();
        $array = [];
        $crawler = $client->request('GET', $url);
        $data['region_sale'] = null;
        $data['region_sale_url'] = null;

        try{
            $data['title'] = trim($crawler->filter('.adview_subject > h2')->text());
            $data['title'] = trim($data['title']);
        }catch (\InvalidArgumentException $e){
            $data['title'] = null;
        }

        try{
            $data['price'] = trim($crawler->filter('.price-value')->text());
        }catch (\InvalidArgumentException $e){
            $data['price'] = null;
        }

        try{
            $data['description'] = $crawler->filter('.body_text')->text();
            $data['description'] = trim($data['description']);
        }catch (\InvalidArgumentException $e){
            $data['description'] = null;
        }

        try{
            $images = $crawler->filter('.thumb_image_single')->each(function (Crawler $node, $i) {
               $src = str_replace('small_thumbs','wm_images', $node->attr('src'));
                return ['src'=>$src];
            });
        }catch (\InvalidArgumentException $e){
            $images = [];
        }

        if(empty($images)){
            try{
                $src = $crawler->filter('#display_image img')->attr('src');
                $images[] = ['src' => $src];

            }catch (\InvalidArgumentException $e){
                $images = [];
            }
        }


        try{
            $information =  $crawler->filter('.adparam_item')->each(function (Crawler $node, $i) {
                return $node->text();
            });
        }catch (\InvalidArgumentException $e){
            $information= [];
        }
        try{
            $contact_name =  $crawler->filter('.advertised_user')->text();
        }catch (\InvalidArgumentException $e){
            $contact_name= null;
        }
        try{
            $address =  $crawler->filter('.adparam_long_item')->text();
            if(strpos(mb_strtolower($address, 'UTF-8'), 'đã sử dụng') !== FALSE){
                $address = null;
            }
            if(strpos(mb_strtolower($address, 'UTF-8'), 'tình trạng') !== FALSE){
                $address = null;
            }

            $data['address']= trim(str_replace('Địa chỉ:','',$address));
        }catch (\InvalidArgumentException $e){
            $address= [];
        }

        try{
            $phoneImage = $crawler->filter('#real-phone')->attr('src');
//            $phoneImageUrl  = UtilHelper::downloadImageFromLink(Yii::getAlias('@assetsUploadPhoneImagePatch'),$phoneImage,false);
//            $phoneImageUrl = Yii::getAlias('@assetsUploadUrl').'/phone_images/'.$phoneImageUrl;
            $phoneImageUrl = $phoneImage;
        }catch (\InvalidArgumentException $e){
            $phoneImageUrl = null;
        }

        $data['lat'] = null;
        $data['long'] = null;
        $data['contact_phone'] = $phoneImageUrl;
        $data['contact_name'] = $contact_name;


        $data['contact_phone_backup'] = null;

        $data['description'] ;

        $data['images'] = $images;
        $data['area'] = null;
        $data['district'] = null;

        if(!empty($information)){
            foreach($information as $info){
                if(strpos(mb_strtolower($info, 'UTF-8'), 'diện tích:') !== false){
                    $data['area'] = trim(str_replace('Diện tích:','',$info));
                }elseif(strpos(mb_strtolower($info, 'UTF-8'), 'tỉnh, thành, quận:') !== false){
                    $data['district'] = trim(str_replace('Tỉnh, thành, quận:','',$info));
                    $district = trim(str_replace('Quận',' ',$data['district']));

                    if(strpos(mb_strtolower($data['address'], 'UTF-8'), mb_strtolower($district, 'UTF-8')) === false){
                        $data['address'] .=  ', '.$data['district'];
                    }
                   // $data['address'] .=  ', '.$data['district'];
                }
            }
        }

        return $data;
    }
}
