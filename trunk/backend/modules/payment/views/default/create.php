<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\entities\PaymentHistory */

$this->title = Yii::t('backend', 'Create Payment History');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Payment Histories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
