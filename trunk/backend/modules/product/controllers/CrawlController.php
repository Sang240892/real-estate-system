<?php
/**
 * User: sangnguyen on  6/29/16 at 20:48
 * File name: CrawlController.php
 * Project name: real-estate-system
 * Copyright (c) 2016 by SkillSpars
 * All rights reserved
 */

namespace backend\modules\product\controllers;

use backend\commons\helpers\UtilHelper;
use common\models\entities\RealEstateUrlCrawlingItem;
use common\models\entities\SiteCrawlingHref;
use Goutte\Client;
use Sunra\PhpSimple\HtmlDomParser;
use Symfony\Component\CssSelector\Exception\ParseException;
use Symfony\Component\DomCrawler\Crawler;
use Yii;
use backend\modules\auth\controllers\AuthenticateController;
use backend\modules\product\models\SearchRealEstateItem;

class CrawlController extends AuthenticateController
{
    public function actionIndex($site){

        $categoriesQuery = UtilHelper::getCategoriesQueryToCrawl($site);

        $searchModel = new SearchRealEstateItem();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'categoriesQuery' => $categoriesQuery
        ]);

    }
    public function actionGetDetailItem(){
        $response=[];
        $data = null;
        $url = Yii::$app->request->post('url');
        $site  =    Yii::$app->request->post('baseUrl');
        switch($site){
            case 'bat-dong-san':
                $data = $this->_crawlingBDSDetail($url);
                break;
            case 'mua-ban':
                $data = $this->_crawlingMBDetail($url);
                break;
//            case 'cho-tot':
//                $data = $this->_crawlingCTDetail($url);
//                break;
        }

        $response=[
            'status'=>true,
            'data'=>$data
        ];
        echo die(json_encode($response));
    }

    /**
     *
     */
    public function actionCrawling(){
        $category   =    Yii::$app->request->post('category');
        $pages =    Yii::$app->request->post('page');
        $site  =    Yii::$app->request->post('baseUrl');
        $items = [];
        $totalRecord = 0;
//        $site = 'mua-ban';
//        $category = 'ban-nha-can-ho-toan-quoc-l0-c32';
//        $pages = ['?cp=1'];
        if(empty($pages) || !$site || empty($site)){
            echo die(json_encode([
                'status'=>FALSE
            ]));
        }

        $crawlingParams = UtilHelper::buildParamsCrawlData($site);

        $selectedPage = UtilHelper::BuildSelectedPagingSelect2Options($pages,
        $crawlingParams['queryStringPaging']);

        switch($site){
            case 'bat-dong-san':
                $data = $this->_crawlingBDS($pages,$category,$crawlingParams);
                break;
            case 'mua-ban':
                $data = $this->_crawlingMB($pages,$category,$crawlingParams);
                break;
            case 'cho-tot':
                $data = $this->_crawlingCT($pages,$category,$crawlingParams);
                break;
        }

        $response= [
            'status'=>TRUE,
            'data'=>$data['items'],
            'totalRecord'=> $data['totalRecord'],
            'recordPerPage' => $data['recordPerPage'],
            'totalPage' => intval( $data['totalRecord'] / $data['recordPerPage']) + 1,
            'selectedPage'=>$selectedPage,
            'pageRq'=> $pages,
            'category'=>$data['category']
        ];

        echo die(json_encode($response));

    }
    public function _crawlingMB($pages,$category,$crawlingParams){
        $client = new Client();
        $data = [];
        $data['items'] = [];
        $total = 0;
        $cateName = null;
        $validate = null;
        foreach($pages as $page) {
            //$crawler = $client->request('GET', 'http://batdongsan.com.vn/ban-can-ho-chung-cu'.$url);
            $crawler = $client->request('GET', $crawlingParams['baseUrl'] . $category . $page);
             $number = $crawler->filter('.mbn-list-nav li')->each(function (Crawler $node, $i) {
                 try{
                     $nav = $node->filter('a')->text();
                 }catch (\InvalidArgumentException $e){
                     $nav = null;
                 }
                 try{
                     $li =  $node->text();
                 }catch (\InvalidArgumentException $e){
                     $li = null;
                 }
                 $total = str_replace($nav,'', $li);
                 $total = str_replace(['(',')'],'',$total);
                 $total = trim(str_replace('.','',$total));
                 return $total;
            });

           if(!empty($number)){
               foreach($number as $n){
                   $total +=$n;
               }
           }
            $data['totalRecord'] = (int)$total;
            if($category){
                $cate = SiteCrawlingHref::findOne([
                    'site_crawling_id'=>2,
                    'href'=>$category
                ]);
                if($cate){
                    $cateName = $cate->name;
                }
            }
            $data['category'] = $cateName;

            $items = $crawler->filter('.mbn-box-list-content')->each(function (Crawler $node, $i) {
               try{
                   $title = $node->filter('.mbn-content h2')->text();
               }catch (\InvalidArgumentException $e){
                   $title = null;
               }
                try{
                    $price = $node->filter('.mbn-price')->text();
                }catch (\InvalidArgumentException $e){
                    $price = null;
                }
                try{
                    $address = $node->filter('.mbn-address')->text();
                }catch (\InvalidArgumentException $e){
                    $address = null;
                }
                try{
                    $createAt = $node->filter('.mbn-date')->text();
                }catch (\InvalidArgumentException $e){
                    $createAt = null;
                }
                try{
                    $area = $node->filter('.mbn-item-area')->text();
                    $area = str_replace('Diện tích:','',$area);
                }catch (\InvalidArgumentException $e){
                    $area = null;
                }
                try{
                    $img = $node->filter('.mbn-image img')->attr('src');
                }catch (\InvalidArgumentException $e){
                    $img = null;
                }
                try{
                    $itemUrl =  $node->filter('a')->attr('href');
                }catch (\InvalidArgumentException $e){
                    $itemUrl = null;
                }
                if($img == null || empty($img)){
                    $img = Yii::$app->homeUrl.'images/No-image-found.jpg';
                }
                if($itemUrl){
                    $validate = RealEstateUrlCrawlingItem::findOne(['url'=>$itemUrl]);
                }
                $items = [
                    trim(str_replace('thumb-list','thumb-detail',$img)),
                    trim($title),
                    trim(''),
                    //trim($category),
                    trim($price),
                    trim($area),
                    'https://muaban.net',
                    trim($address),
                    trim($createAt),
                    trim($itemUrl),
                    ($validate)?false:true
                ];

                return $items;

            });
            $data['recordPerPage'] = count($items);
            $data['items'] = array_merge($data['items'],$items);
        }
        return $data;
    }
    /**
     * @param $pages
     * @param $category
     * @param $crawlingParams
     * @return array
     */
    public function _crawlingBDS($pages,$category,$crawlingParams){
        $client = new Client();
        $data = [];
        $data['items'] = [];
        $validate = null;
        foreach($pages as $page) {
            //$crawler = $client->request('GET', 'http://batdongsan.com.vn/ban-can-ho-chung-cu'.$url);
            $crawler = $client->request('GET', $crawlingParams['baseUrl'].$category.$page);

            $totalRecord = $crawler->filter('.header-tab > li')->each(function (Crawler $node, $i) {

                return str_replace(['Tất cả tin rao', '(', ')'], '', $node->text());

            });
            $data['category'] = $crawler->filter('.Repeat > h2')->text();
            $data['totalRecord'] = (int)$totalRecord[0];
            $items = $crawler->filter('.search-productItem')->each(function (Crawler $node, $i) {

                try{
                    $title = $node->filter('.p-title')->text();
                }catch (\InvalidArgumentException $e){
                    $title = null;
                }
                try{
                    $itemUrl = $node->filter('.p-title a')->attr('href');
                }catch (\InvalidArgumentException $e){
                    $itemUrl = null;
                }
                try{
                    $imgUrl = $node->filter('.product-avatar img')->attr('src');
                }catch (\InvalidArgumentException $e){
                    $imgUrl = null;
                }
                try{
                    $description = $node->filter('.p-main-text')->text();
                }catch (\InvalidArgumentException $e){
                    $description = null;
                }
                try{
                    $price = $node->filter('.product-price')->text();
                }catch (\InvalidArgumentException $e){
                    $price = null;
                }
                try{
                    $area = $node->filter('.product-area')->text();
                }catch (\InvalidArgumentException $e){
                    $area = null;
                }
                try{
                    $address = $node->filter('.product-city-dist')->text();
                }catch (\InvalidArgumentException $e){
                    $address = null;
                }
                try{
                    $createAt = $node->filter('.floatright')->text();
                }catch (\InvalidArgumentException $e){
                    $createAt = null;
                }

                if($imgUrl == null || empty($imgUrl)){
                    $imgUrl = Yii::$app->homeUrl.'images/No-image-found.jpg';
                }
                //
                if($itemUrl){
                    $validate = RealEstateUrlCrawlingItem::findOne(['url'=>$itemUrl]);

                }
                $items = [
                    trim(str_replace('120x90','745x510',$imgUrl)),
                    trim($title),
                    trim($description),
                    //trim($category),
                    trim($price),
                    trim($area), 'http://batdongsan.com.vn',
                    trim($address),
                    trim($createAt),
                    trim($itemUrl),
                    ($validate)?false:true
                ];

                return $items;

            });

            $data['recordPerPage'] = count($items);

            $data['items'] = array_merge($data['items'],$items);
        }
        return $data;
    }
    private  function _crawlingMBDetail($url){
        $data = [];
        $client = new Client();
        $crawler = $client->request('GET', $url);

        try{
            $data['title'] = trim($crawler->filter('.cl-title > h1')->text());
        }catch (\InvalidArgumentException $e){
            $data['title'] = null;
        }
        try{
            $data['region_sale'] = trim($crawler->filter('.diadiem-title a')->text());
        }catch (\InvalidArgumentException $e){
            $data['region_sale'] = null;
        }
        try{
            $data['region_sale_url'] = $crawler->filter('.diadiem-title a')->attr('href');
        }catch (\InvalidArgumentException $e){
            $data['region_sale_url'] = null;
        }
        try{
            $data['price'] = trim($crawler->filter('.price-value')->text());
        }catch (\InvalidArgumentException $e){
            $data['price'] = null;
        }

        try{
            $data['description'] = $crawler->filter('.ct-body')->html();
        }catch (\InvalidArgumentException $e){
            $data['description'] = null;
        }

        try{
            $images = $crawler->filter('#owl-carousel-detail img')->each(function (Crawler $node, $i) {
                return ['src'=>$node->attr('src')];
            });
        }catch (\InvalidArgumentException $e){
            $images = [];
        }

        $data['lat'] = null;
        $data['long'] = null;
        /*try{
            $data['lat'] = trim($crawler->filter('#hdLat')->attr('value'));
        }catch (\InvalidArgumentException $e){
            $data['lat'] = null;
        }
        try{
            $data['long'] = trim($crawler->filter('#hdLong')->attr('value'));
        }catch (\InvalidArgumentException $e){
            $data['long'] = null;
        }*/
        try{
            $data['contact_phone'] = trim($crawler->filter('.contact-mobile')->text());
        }catch (\InvalidArgumentException $e){
            $data['contact_phone'] = null;
        }
        try{
            $data['contact_name'] = trim($crawler->filter('.contact-name')->text());
        }catch (\InvalidArgumentException $e){
            $data['contact_name'] =null;
        }

        $data['contact_phone_backup'] = null;

        $data['description'] ;

        $data['images'] = $images;
        return $data;
    }
    private function _crawlingBDSDetail($url){
        $data = [];
        $client = new Client();
        $crawler = $client->request('GET', $url);
        try{
            $data['title'] = trim($crawler->filter('.pm-title > h1')->text());
        }catch (\InvalidArgumentException $e){
            $data['title'] = null;
        }
        try{
            $data['region_sale'] = trim($crawler->filter('.diadiem-title a')->text());
        }catch (\InvalidArgumentException $e){
            $data['region_sale'] = null;
        }
        try{
            $data['region_sale_url'] = $crawler->filter('.diadiem-title a')->attr('href');
        }catch (\InvalidArgumentException $e){
            $data['region_sale_url'] = null;
        }
        try{
            $data['price'] = trim($crawler->filter('.gia-title strong')->text());
        }catch (\InvalidArgumentException $e){
            $data['price'] = null;
        }
        try{
            $array = $crawler->filter('.gia-title')->each(function (Crawler $node, $i) {
                return $node->text();
            });
        }catch (\InvalidArgumentException $e){
            $array = [];
        }
        try{
            $data['description'] = $crawler->filter('.pm-content')->html();
        }catch (\InvalidArgumentException $e){
            $data['description'] = null;
        }
        try{
            $tag = $crawler->filter('#LeftMainContent__productDetail_panelTag')->html();
        }catch (\InvalidArgumentException $e){
            $tag = null;
        }
        try{
            $images = $crawler->filter('#thumbs li img')->each(function (Crawler $node, $i) {
                return ['src'=>str_replace('80x60','745x510',$node->attr('src'))];
            });
        }catch (\InvalidArgumentException $e){
            $images = [];
        }
        try{
            $data['lat'] = trim($crawler->filter('#hdLat')->attr('value'));
        }catch (\InvalidArgumentException $e){
            $data['lat'] = null;
        }
        try{
            $data['long'] = trim($crawler->filter('#hdLong')->attr('value'));
        }catch (\InvalidArgumentException $e){
            $data['long'] = null;
        }
        try{
            $data['contact_phone'] = trim(str_replace('Điện thoại','',$crawler->filter('#LeftMainContent__productDetail_contactPhone')->text()));
        }catch (\InvalidArgumentException $e){
            $data['contact_phone'] = null;
        }
        try{
            $data['contact_name'] = trim(str_replace('Tên liên lạc','',$crawler->filter('#LeftMainContent__productDetail_contactName')->text()));
        }catch (\InvalidArgumentException $e){
            $data['contact_name'] =null;
        }
        try{
            $data['contact_phone_backup'] = trim(str_replace('Mobile','',$crawler->filter('#LeftMainContent__productDetail_contactMobile')->text()));
        }catch (\InvalidArgumentException $e){
            $data['contact_phone_backup'] = null;
        }
        $data['price'] =trim(str_replace('Giá:','',$array[0]));
        $data['area'] = trim(str_replace('Diện tích:','',$array[1]));
        $data['description'] = str_replace($tag,'',$data['description']);
        $data['description'] = str_replace('<div id="LeftMainContent__productDetail_panelTag" class="tagpanel"></div>',
            '',$data['description']);
        $data['images'] = $images;
        return $data;
    }
    public function actionChoTot(){
        $client = new Client();
        $data = [];
        $data['items'] = [];
        $total = 0;
        $cateName = null;
        $validate = null;
        $crawler = $client->request('GET', 'https://www.chotot.com/tp-ho-chi-minh/mua-ban-nha-dat#');

        $items = $crawler->filter('.chotot-list-row')->each(function (Crawler $node, $i) {
            try{
                $urlAvatar = $node->filter('.listing_thumbs_image img')->attr('src');
            }catch (\InvalidArgumentException $e){
                $urlAvatar = null;
            }
            try{
                $urlDetail = $node->filter('.ad-subject')->attr('href');
            }catch (\InvalidArgumentException $e){
                $urlDetail = null;
            }
            try{
               $title = $node->filter('.ad-subject')->text();
           }catch (\InvalidArgumentException $e){
               $title = null;
           }
            try{
                $price = $node->filter('.ad-price')->text();
            }catch (\InvalidArgumentException $e){
                $price = null;
            }
            try{
                $district = $node->filter('.municipality')->text();
            }catch (\InvalidArgumentException $e){
                $district = null;
            }
            try{
                $isSelf = $node->filter('.subtext')->text();
                $isSelf = str_replace(['(',')'],'',$isSelf);
            }catch (\InvalidArgumentException $e){
                $isSelf = null;
            }

            $item = [
                'title'=>trim($title),
                'price'=>trim($price),
                'summary'=>'',
                'district'=>trim($district),
                'isSelf'=>(!empty($isSelf) && $isSelf === 'Môi giới')?false:true,
                'detailUrl'=>trim($urlDetail),
                'source'=>'https://www.chotot.com',
                'avatarUrl'=>trim($urlAvatar),
                'created'=>Yii::$app->formatter->asDate('now', 'yyyy-MM-dd'),
               // 'area'=>trim($area),
               // 'categoryId' =>$categorySystem
            ];

            return $item;
        });
    }
    public function actionDetail(){
        $url = 'https://www.chotot.com/quan-12/mua-ban-nha-dat/nha-ngay-nga-tu-ga-1lau-1tret-2pn-2wc-23827804.htm#';
        $data = [];
        $client = new Client();
        $array = [];
        $crawler = $client->request('GET', $url);
        $data['region_sale'] = null;
        $data['region_sale_url'] = null;

        try{
            $data['title'] = trim($crawler->filter('.adview_subject > h2')->text());
            $data['title'] = trim($data['title']);
        }catch (\InvalidArgumentException $e){
            $data['title'] = null;
        }

        try{
            $data['price'] = trim($crawler->filter('.price-value')->text());
        }catch (\InvalidArgumentException $e){
            $data['price'] = null;
        }

        try{
            $data['description'] = $crawler->filter('.body_text')->text();
            $data['description'] = trim($data['description']);
        }catch (\InvalidArgumentException $e){
            $data['description'] = null;
        }

        try{
            $images = $crawler->filter('.ad_thumb img')->each(function (Crawler $node, $i) {
                return ['src'=>$node->attr('src')];
            });
        }catch (\InvalidArgumentException $e){
            $images = [];
        }
        try{
            $information =  $crawler->filter('.adparam_item')->each(function (Crawler $node, $i) {
                return $node->text();
            });
        }catch (\InvalidArgumentException $e){
            $information= [];
        }
        try{
            $address =  $crawler->filter('.adparam_long_item')->text();
            $data['address']= trim(str_replace('Địa chỉ:','',$address));
        }catch (\InvalidArgumentException $e){
            $information= [];
        }

        $data['lat'] = null;
        $data['long'] = null;
        $data['contact_phone'] = null;
        $data['contact_name'] = null;


        $data['contact_phone_backup'] = null;

        $data['description'] ;

        $data['images'] = $images;

        if(!empty($information)){
            foreach($information as $info){
                if(strpos(mb_strtolower($info, 'UTF-8'), 'diện tích:') !== false){
                    $data['area'] = trim(str_replace('Diện tích:','',$info));
                }elseif(strpos(mb_strtolower($info, 'UTF-8'), 'tỉnh, thành, quận:') !== false){
                    $data['district'] = trim(str_replace('Tỉnh, thành, quận:','',$info));
                }
            }
        }

        echo '<pre>';
        print_r($data);
        echo '</pre>';
        die();
        return $data;
    }
}
