<?php

namespace backend\modules\product\controllers;

use backend\commons\components\ExportDataExcel;
use backend\commons\components\XlsExporter;
use backend\commons\helpers\UtilHelper;
use backend\modules\auth\controllers\AuthenticateController;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use common\models\entities\RealEstateCategory;
use common\models\entities\RealEstateCategoryRelation;
use common\models\entities\RealEstateGallery;
use common\models\entities\RealEstateItemHasPhoneContact;
use common\models\entities\RealEstateItemPostDate;
use common\models\entities\RealEstatePhoneContact;
use common\models\entities\RealEstateUrlCrawlingItem;
use Yii;
use common\models\entities\RealEstateItem;
use backend\modules\product\models\SearchRealEstateItem;
use yii\db\Exception;
use yii\db\IntegrityException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Jaybizzle\CrawlerDetect\CrawlerDetect;
/**
 * DefaultController implements the CRUD actions for RealEstateItem model.
 */
class DefaultController extends AuthenticateController
{
    function convert_vi_to_en($str) {
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", "a", $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", "e", $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", "i", $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", "o", $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", "u", $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", "y", $str);
        $str = preg_replace("/(đ)/", "d", $str);
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", "A", $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", "E", $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", "I", $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", "O", $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", "U", $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", "Y", $str);
        $str = preg_replace("/(Đ)/", "D", $str);
        //$str = str_replace(" ", "-", str_replace("&*#39;","",$str));
        return $str;
    }

    function lastIndexOf($string,$item)

    {

        $index=strpos(strrev($string),strrev($item));

        if ($index)

        {

            $index=strlen($string)-strlen($item)-$index;

            return $index;

        }

        else

            return -1;

    }

    public function actionTier(){

        $str = 'Cần bán hoặc cho thuê kiốt số 27- B14 Phạm Ngọc Thạch, Trung Tự, Đống Đa, diện tích sử dụng 48 m2, 2 tầng, mặt tiền 3,6m, giá thuê 12,5 triệu/tháng, bán 4,25 tỷ. Liên hệ điện thoại: Anh Dũng, 0904319579.';

       // $address = UtilHelper::checkAddressInString($str);
       // $address = UtilHelper::checkNumberWithCharacter($str,["mặt phố"],4);
         UtilHelper::showAddressReal($str);

        //die(var_dump($address));
       // $address= UtilHelper::checkNumberWithCharacter($str,["số"]);

      // die(var_dump($address));

        $arr_keywords = array("Nguyễn Hữu Thọ");

        $currpos = 0;
        $newstr = '';
        $kwds_plus_surround = array();
        $len = strlen($str);
        while ($currpos < $len) {
            // Search for the earliest match of any of the keywords from our current position.
            list($newpos, $kw_index) = $this->strpos_arr($str, $arr_keywords, $currpos);

            if ($newpos == -1) {
                // We're beyond the last keyword - do replacement to the end and
                // add to the output.
                $newstr .= $this->do_replace(substr($str, $currpos));
                $currpos = $len + 1;
            } else {
                // Found a keyword.
                // Now look two words back (separating words on single spaces).
                $secondspc_back = $newpos - 1;
                for ($i = 1; $i > 0; $i--) {
                    $secondspc_back = strrpos($str, ' ', $secondspc_back - $len - 1);
                    if ($secondspc_back === false) break;
                }
                if ($secondspc_back === false || $secondspc_back < $currpos) {
                    $secondspc_back = $currpos;
                } else  $secondspc_back++;

                // Do replacement on the stuff between the previous keyword
                // (plus 2 words after) and this one (minus two words before),
                // and add to the output.
                $in_between = substr($str, $currpos, $secondspc_back - $currpos);
                $newstr .= $this->do_replace($in_between);

                // Now look two words forward (separating words on single spaces).
                $secondspc_fwd = $newpos + strlen($arr_keywords[$kw_index]);
//                for ($i = 0; $i > 0; $i--) {
//                    $secondspc_fwd = strpos($str, ' ', $secondspc_fwd + 1);
//                    if ($secondspc_fwd === false) break;
//                }
//                if ($secondspc_fwd === false) $secondspc_fwd = $len + 1;

                // Add the keyword plus two words before and after to both the array
                // and the output.
                $kw_plus = substr($str, $secondspc_back, $secondspc_fwd - $secondspc_back);
                $kwds_plus_surround[] = $kw_plus;
                $newstr .= $kw_plus;

                // Update our current position in the string.
                $currpos = $secondspc_fwd;

            }

        }

        echo '<br/>';
        echo '<pre>';
        print_r($kwds_plus_surround );
        echo '</pre>';
        die();

    }
    // Replaces in $str all occurrences of 'e' with 'U'.
    function do_replace($str) {
        //return str_replace('e', 'U', $str);
    }
// Finds the earliest match, if any, of any of the $needles (an array)
// in $str (a string) starting from $currpos (an integer).
// Returns an array whose first member is the index of the earliest match,
// or -1 if no match was found, and whose second member is the index into
// $needles of the entry that matched in the $str.
    function strpos_arr($str, $needles, $currpos) {
        $ret = array(-1, -1);
        foreach ($needles as $idx => $needle) {
            $offset = stripos($str, $needle, $currpos);
            if ($offset !== false &&
                ($offset < $ret[0] || $ret[0] == -1)) {
                $ret = array($offset, $idx);
            }
        }
        return $ret;
    }
    public function actionFilter(){

        $tweet = "this has a #hashtag a  #badhash-tag and a #goodhash_tag";

        preg_match_all("/(#\w+)/", $tweet, $matches);

        var_dump( $matches );
        die();
        $string = "Cần bán CH Sunrise City Central - 25 Nguyễn Hữu Thọ- 3 PN, diện tích 120 m2.- Full nội thất cao cấp- Dọn vào ở ngay- Tầng hoa hậu- Giá 5 tỷ 2( giá tốt nhất thị trường)Liên hệ 0938.94.35.68 Thảo, tks!Một số thông tin về dự án Sunrise CitySunrise City: Mặt tiền đường Nguyễn Hữu Thọ, đối diện siêu thị Lotte Quận 7.- Cách Phú Mỹ Hưng 1 phút.- Cách trung tâm Quận 1 chỉ 5 phút.- Cách trung tâm Quận 5 chỉ 5 phút.- Gần trường đại học Tôn Đức Thắng, đại học Cảnh Sát Nhân Dân, đại học RMIT, siêu thị Lotte Mart,….* Tận hưởng ngay những tiện ích nội khu của căn hộ:- 4 tầng trung tâm thương mại, mua sắm.- Hồ bơi tràn phong cách Singapore.- Tiệc nướng BBQ ngoài trời.- Công viên, cây xanh, khu vui chơi trẻ em, nhà trẻ,...- Nhà thuốc, phòng khám.- Vườn treo ngoài trời, café,...Liên hệ 0938.94.35.68 Thảo, tks!(Quý khách chỉ cần nhắn tin, chúng tôi sẽ gọi lại để hỗ trợ thông tin. Rất vui lòng được tư vấn cho quý khách, hỗ trợ 24/24).";
        $string =  $this->convert_vi_to_en($string);
        $explore = explode(' ',$string);

        $item="Nguyễn Hữu Thọ";

        $item = $this->convert_vi_to_en($item);

        $item = mb_strtolower($item, 'UTF-8');

        var_dump(strpos(mb_strtolower($string, 'UTF-8'), $item));

        echo '<pre>';
        print_r($explore);
        echo '</pre>';
        die();



       // UtilHelper::filterIsSelf($string);
    }

    public function actionCheckAddress(){
        $address= '351 Le Van Sy, phuong 13, Quan 3';
        $API_KEY = 'AIzaSyCB6pfVpg9zYLejBFwdMJUQ_YrCP3Yu-N4';
        $coordinates = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?key='.$API_KEY.'&address=' . urlencode($address) . '&sensor=true');
        $coordinates = json_decode($coordinates);
        die(var_dump($coordinates));
        if(isset($coordinates->results[0]->geometry->location)){
            return [
                'lat'=>isset($coordinates->results[0]->geometry->location->lat)?$coordinates->results[0]->geometry->location->lat:null,
                'lng'=>isset($coordinates->results[0]->geometry->location->lng)?$coordinates->results[0]->geometry->location->lng:null
            ];
        }

//        $repost_date_timestamp = UtilHelper::getNowUnixUTC();
//        $now = \Yii::$app->formatter->asDate($repost_date_timestamp,'Y-M-d');
//        $realty =  RealEstateItem::find()
//              ->leftJoin(RealEstateItemPostDate::tableName(),
//                  RealEstateItemPostDate::tableName().'.item_id = '.RealEstateItem::tableName().'.id')
//              ->andOnCondition("date(CONVERT_TZ(FROM_UNIXTIME(real_estate_item_post_date.post_date), @@session.time_zone, '+07:00')) = :date ",[
//                  ':date'=>$now
//              ])
//            ->limit(2200)
//            ->all();
//       if($realty){
//
//           foreach($realty as $item){
//               if(empty($item->latitude) || empty($item->longitude)){
//                   $location = UtilHelper::detectAddress($item->address);
//                   if(!empty($location)){
//                       $item->latitude = $location['lat'];
//                       $item->longitude = $location['lng'];
//                       $item->save(false);
//                       $item->refresh();
//                       echo 'lat:'.$item->latitude;
//                       echo '<br/>';
//                       echo 'lat:'.$item->longitude;
//                       echo '<br/>';
//                       echo 'id:'.$item->id;
//                       echo '<br/>';
//                   }
//               }
//           }
//       }
    }

    public function actionTesting(){
        //$address = '222 Phường Phú Hữu, Quận 9, TP.HCM';
        $address= '351/4 Le van Sy, phuong 13, Quan 3, HCM';
        $location = UtilHelper::detectAddress($address);
        die(var_dump($location));
//        $category = RealEstateCategory::find()->andOnCondition(['<>','href',''])->all();
//        if(!empty($category)){
//            foreach($category as $cate){
//                $relation =RealEstateCategoryRelation::findOne(['category_children_id'=>$cate->id]);
//                $cate->finder_name =$relation->categoryParent->name;
//                $cate->save();
//            }
//        }
//        return null;

//        $time = new \yii\db\Expression('UNIX_TIMESTAMP()');
//       UtilHelper::getNow();
//        die(var_dump($time->tsm));



//        $input = "
//        1433&nbsp;Longworth House Office Building Washington,  DC 20515
//         332 Cannon HOB                      Washington   DC   20515
//        1641 LONGWORTH HOUSE OFFICE BUILDING WASHINGTON,  DC   20515
//        1238 Cannon H.O.B.
//        Washington, DC 20515
//        8293 Longworth House Office Building • Washington DC • 20515
//        8293 Longworth House Office Building | Washington DC | 20515";
//        $input = strip_tags($input);
//        $input = preg_replace("/&nbsp;/"," ",$input);
//        $results= array();
//        preg_match("/[0-9]+\s+[^0-9]*?\s+washington,?\s*D\.?C\.?[^0-9]+[0-9]{5}/si",$input,$results);
//        die(var_dump($results));
//        foreach($result[0] as $addr) {
//            echo "$addr<br/>";
//        }



        die();
    }
    public function actionParseArea(){
        $tweet = "this has a #hashtag a  #badhash-tag and a #goodhash_tag";

        preg_match_all("/(#\w+)/", $tweet, $matches);

        var_dump( $matches );
        die();
    }
    public function actionInterval(){
//        $array = [1,2,3];
//        foreach($array as $item){
//            echo $item .'-'.date('h:i:s').'</br>';
//            sleep(2);
//        }
//
        @ini_set("output_buffering", "Off");
        @ini_set('implicit_flush', 2);
        @ini_set('zlib.output_compression', 0);
        @ini_set('max_execution_time',1200);

        header( 'Content-type: text/html; charset=utf-8' );

        echo "Testing time out in seconds\n";
        for ($i = 0; $i < 110; $i++) {
            echo $i." -- ";

            if(sleep(2)!=0)
            {
                echo "sleep failed script terminating";
                break;
            }
            flush();
            ob_flush();
        }
    }
    public function actionConvert(){
        echo Yii::getAlias('@assetsUploadPhoneImagePatch').'/captcha.jpg';


        $locate= Yii::getAlias('@assetsUploadPhoneImagePatch').'/captcha.jpg';
        echo "<br/>";
        echo (new \TesseractOCR($locate))
            ->run();

    }
    public function actionPortfolio($id){
        $model = $this->findModel($id);
        return $this->render('portfolio',[
            'model'=>$model
        ]);
    }
    public function actionSwitchCheckOut(){
        $request = Yii::$app->request->post();
        $id = $request['id'];
        $value = $request['self'];
        $model = RealEstateItem::findOne($id);
        $status = false;
        if($model){
            $model->is_not_sure_self = 0;
            $model->save(false);
        }else{
            echo die(json_encode(['status'=>false]));
        }

        if($model->save(false)){
            $status = true;
        }
        echo die(json_encode(['status'=>$status]));
    }
    public function actionSwitchSelf(){
        $request = Yii::$app->request->post();
        $id = $request['id'];
        $value = $request['self'];
        $model = RealEstateItem::findOne($id);
        $status = false;
        if($model){
            if($value == 'false'){
                $model->is_self = 0;

                if($model->phone_contact){
                    $phoneModel = new RealEstatePhoneContact();
                    $phoneModel->phone = $model->phone_contact;

                    try{
                        $phoneModel->save();
                    }catch (IntegrityException $e){

                    }
                }
                if($model->phone_backup){
                    $phoneModel = new RealEstatePhoneContact();
                    $phoneModel->phone = $model->phone_backup;

                    try{
                        $phoneModel->save();
                    }catch (IntegrityException $e){

                    }
                }
            }
            if($value == 'true'){
                $model->is_self = 1;

                if($model->phone_contact){
                    $phoneContact = RealEstatePhoneContact::findOne(['phone'=>$model->phone_contact]);
                    if($phoneContact){
                        $phoneContact->delete();
                    }
                }
                if($model->phone_backup){
                    $phoneContactBackup = RealEstatePhoneContact::findOne(['phone'=>$model->phone_backup]);
                    if($phoneContactBackup){
                        $phoneContactBackup->delete();
                    }
                }

            }
        }
        if($model->save(false)){
            $status = true;
        }
        echo die(json_encode(['status'=>$status]));
    }
    public function actionSaveItemCrawling(){
       $request = Yii::$app->request->post();
       $avatar = null;
        $phone = null;
        $numberPrice = 0;
        $phone_backup = null;
       $data = json_decode($request['data_crawling']);
       $model = new RealEstateItem();
        if(!empty($data)){
            if(isset($data->img) && $data->img && !empty($data->img)){
                $avatar = UtilHelper::downloadImageFromLink(Yii::getAlias('@publicProductImagePatch'),$data->img);
            }
            if($data->contact_phone){
                $phone = str_replace('.','',$data->contact_phone);
            }
            if($data->contact_phone_backup){
                $phone_backup = str_replace('.','',$data->contact_phone_backup);
            }
            $date = trim($data->created);
            $date = explode('/',$date);
            $date = $date[2].'-'.$date[1].'-'.$date[0];

            if(isset($data->price) && $data->price){
                $numberPrice = UtilHelper::convertPriceStringToNumber($data->price);
            }
            $realEstate = [
                'avatar'=>$avatar,
                'title'=>($data->title)?trim($data->title):null,
                'description'=>($data->description)?trim($data->description):null,
                'summary'=>($data->summary)?trim($data->summary):null,
                'source'=>($data->source)?trim($data->source):null,
                'status'=>1,
                'area'=>($data->area)?trim($data->area):null,
                'phone_contact'=>($phone)?trim($phone):$phone_backup,
                'phone_backup'=>($phone_backup)?trim($phone_backup):$phone,
                'address'=>($data->address)?trim($data->address):null,
                'country'=>'Việt Nam',
                'type'=>($data->region_sale)?trim($data->region_sale):null,
                'price_string'=>($data->price)?trim($data->price):null,
                'price'=>$numberPrice,
                'unit'=>'VND',
                'area_number'=>str_replace('m²','',$data->area),
                'category_name'=>($data->region_sale)?trim($data->region_sale):null,
                'real_estate_date'=>($date)?Yii::$app->formatter->asTimestamp($date):null,
                'latitude'=>($data->lat)?trim($data->lat):null,
                'longitude'=>($data->long)?trim($data->long):null,
                'is_self'=>0
            ];
            $model->attributes = $realEstate;
            if($model->save()){

                $detailUrlItem = new RealEstateUrlCrawlingItem();
                $detailUrlItem->url = $data->detail_url;
                $detailUrlItem->real_estate_item_id = $model->id;
                $detailUrlItem->save();

                if(isset($data->images) && $data->images && !empty($data->images)){
                      foreach($data->images as $image){
                          $img  = UtilHelper::downloadImageFromLink(Yii::getAlias('@publicProductImagePatch'),$image->src);
                          $galleryData = [
                              'file_name'=>$img,
                              'title'=>$img,
                              'item_id'=>$model->id
                          ];
                          $gallery = new RealEstateGallery();
                          $gallery->attributes = $galleryData;
                          $gallery->save();
                      }
              }
                if(UtilHelper::validateIsSelfRealItem($model->title,$model->description)){
                    $model->is_self = 1;
                    $model->save(false);
                }else{
                    if($phone && !empty($phone)){
                        $phoneContactModel = new RealEstatePhoneContact();
                        $phoneContactModel->phone = $phone;
                        try{
                            $phoneContactModel->save(false);
                            $itemHasPhone = new RealEstateItemHasPhoneContact();
                            $itemHasPhone->item_id = $model->id;
                            $itemHasPhone->phone_contact_id = $phoneContactModel->id;
                            $itemHasPhone->phone = $phone;
                            $itemHasPhone->save();
                        }catch (\yii\db\IntegrityException $e){

                        }
                    }
                    if($phone_backup && !empty($phone_backup)){
                        $phoneContactModelBackup = new RealEstatePhoneContact();
                        $phoneContactModelBackup->phone = $phone_backup;
                        try{
                            $phoneContactModelBackup->save(false);
                            $itemHasPhone = new RealEstateItemHasPhoneContact();
                            $itemHasPhone->item_id = $model->id;
                            $itemHasPhone->phone_contact_id = $phoneContactModelBackup->id;
                            $itemHasPhone->phone = $phone_backup;
                            $itemHasPhone->save();
                        }catch (\yii\db\IntegrityException $e){

                        }
                    }

                }
            }else{
                //to do
            }
        }
       $response =[
           'status'=>true,
           'msg'=>Yii::t('backend','successfully save')
       ];
       echo die(json_encode($response));
   }

    public function actionExportExcelAgency(){
        $agency = new RealEstatePhoneContact();
        $data = $agency->find()->limit(10000)->all();

        $agency = new RealEstatePhoneContact();
        $data = $agency->find()->limit(10000)->all();
        $file_name = 'Danh sách môi giới';
        $exporter = new ExportDataExcel('browser', $file_name.'.xls');

        $exporter->initialize(); // starts streaming data to web browser

        $exporter->addRow(array("Số thứ tự","Điện thoại","Tên","Đia chỉ"));
        $i= 1;
        if(!empty($data)){
            foreach($data as $row){
                $phoneMap = $row->getRealEstateItemHasPhoneContacts()->one();
                $item = null;
                if($phoneMap){
                    $item = $phoneMap->getItem()->one();
                }

                $exporter->addRow(array($i,substr_replace($row->phone,"+84",0,1), (!empty($item) && $item->contact_name)?$item->contact_name:"",
                    (!empty($item) && $item->address)?$item->address:""));
                $i++;
            }
        }

        $exporter->finalize(); // writes the footer, flushes remaining data to browser.

        exit(); // all done

    }
    public function actionExportExcel(){

//        $rangeDate = isset(Yii::$app->request->post()['rangeDate'])?Yii::$app->request->post('rangeDate'):null;
//
//        if(!$rangeDate){
//            die(json_encode(['status'=>false]));
//        }
//
//        $rangeDate = explode(',',$rangeDate);
//
//        if(empty($rangeDate)){
//            die(json_encode(['status'=>false]));
//        }
//
//        $rangeDate = array_filter($rangeDate);
//        $rangeDate = array_values($rangeDate);
//
//       // die(var_dump($data));
//
//        $headers = null;
//        $model=  RealEstateItem::find();
//        $model->leftJoin(RealEstateItemPostDate::tableName(),
//            RealEstateItemPostDate::tableName().'.item_id = '.RealEstateItem::tableName().'.id');
//
//        if(count($rangeDate) == 1){
//            $model->andOnCondition("date(CONVERT_TZ(FROM_UNIXTIME(real_estate_item_post_date.post_date), @@session.time_zone, '+07:00')) = :date ",[
//                ':date'=>$rangeDate[0]
//            ]);
//            $file_name = 'Danh_Sách_Bất_Động_Sản_'.$rangeDate[0];
//        }else{
//            $from = (isset($rangeDate[0]) && !empty($rangeDate[0]))?$rangeDate[0]:0;
//            $to  = (isset($rangeDate[1]) && !empty($rangeDate[0]))?$rangeDate[1]:0;
//            $queryDate = $from;
//
//            if($from == 0){
//                $queryDate = $to;
//            }
//
//            if($from == $to || $from == 0 || $to == 0){
//                $model->andOnCondition("date(CONVERT_TZ(FROM_UNIXTIME(real_estate_item_post_date.post_date), @@session.time_zone, '+07:00')) = :date ",[
//                    ':date'=>$queryDate
//                ]);
//                $file_name = 'Danh_Sách_Bất_Động_Sản_'.$queryDate;
//            }else{
//                $model->andOnCondition('real_estate_item_post_date.post_date between UNIX_TIMESTAMP(:from) and UNIX_TIMESTAMP(:to)',[
//                    ':from'=>$rangeDate[0],
//                    ':to'=>$rangeDate[1]
//                ]);
//                $file_name = 'Danh_Sách_Bất_Động_Sản_từ'.$rangeDate[0].'đến_'.$rangeDate[1];
//            }
//        }
//
//        $realty =$model->limit(20000)->all();

        $session = Yii::$app->session;
        $params = $session->get('params');
        $not_sure  = $session->get('not_sure');

        $searchModel = new SearchRealEstateItem();
        //die(var_dump($file_name));
        if(isset($params['SearchRealEstateItem']['real_estate_date']) && !empty($params['SearchRealEstateItem']['real_estate_date'])){
            $file_name = 'Danh-Sách-Bất-Động-Sản-Ngày-';
            $file_name .= $params['SearchRealEstateItem']['real_estate_date'].'-(vào-lúc-'.Yii::$app->formatter->asDate(time(),'Y-M-d-H:i:s').')';
        }else{
            $file_name = 'Danh-Sách-Bất-Động-Sản-(vào-lúc-'.Yii::$app->formatter->asDate(time(),'Y-M-d-H:i:s').')';
        }

        $page_size = 40000;

        $dataProvider = $searchModel->search($params,$not_sure,null,$page_size);

        $realty = $dataProvider->getModels();

        $exporter = new ExportDataExcel('browser', $file_name.'.xls');

        $exporter->initialize('file'); // starts streaming data to web browser

        $exporter->addRow(array("Số thứ tự","Phân khúc",  "Tiêu đề","Mô tả", "Địa chỉ","Ngày đăng tin(dd-mm-yyyy)","Ngày cào tin(dd-mm-yyyy)","Giá (chữ)","Giá (số)","Số điện thoại"));
        $i= 1;
        if(!empty($realty)){
            foreach($realty as $row){
                $exporter->addRow(array($i,$row->category->finder_name,$row->title, $row->description,$row->address,
                    Yii::$app->formatter->asDate($row->real_estate_date,'php:D d M Y'),
                    Yii::$app->formatter->asDate($row->created_at,'php:D d M Y'),
                    $row->price_string,$row->price,$row->phone_contact
                ));
                $i++;
            }
        }

        $exporter->finalize(); // writes the footer, flushes remaining data to browser.
        //echo json_encode(['status'=>true]);
        exit(); // all done
    }

    public function actionExportStream(){
        $session = Yii::$app->session;
        $params = $session->get('params');
        $not_sure  = $session->get('not_sure');

        $searchModel = new SearchRealEstateItem();
        //die(var_dump($file_name));
        if(isset($params['SearchRealEstateItem']['real_estate_date']) && !empty($params['SearchRealEstateItem']['real_estate_date'])){
            $file_name = 'Danh-Sách-Bất-Động-Sản-Ngày-';
            $file_name .= $params['SearchRealEstateItem']['real_estate_date'].'-(vào-lúc-'.Yii::$app->formatter->asDate(time(),'Y-M-d-H:i:s').')';
        }else{
            $file_name = 'Danh-Sách-Bất-Động-Sản-(vào-lúc-'.Yii::$app->formatter->asDate(time(),'Y-M-d-H:i:s').').xlsx';
        }

        $page_size = 40000;

        $dataProvider = $searchModel->search($params,$not_sure,null,$page_size);

        $realty = $dataProvider->getModels();

        $writer = WriterFactory::create(Type::XLSX);

        $writer->setTempFolder(Yii::getAlias('@assetsDownloadPatch').'/'.$file_name)
            ->setShouldUseInlineStrings(true)
            ->openToFile(Yii::getAlias('@assetsDownloadPatch').'/'.$file_name);

        $writer->addRow(array("Số thứ tự","Phân khúc",  "Tiêu đề","Mô tả", "Địa chỉ","Ngày đăng tin(dd-mm-yyyy)","Ngày cào tin(dd-mm-yyyy)","Giá (chữ)","Giá (số)","Số điện thoại"));
        $i= 1;

        if(!empty($realty)){
            foreach($realty as $row){
                $writer->addRows(array($i,$row->category->finder_name,$row->title, $row->description,$row->address,
                    Yii::$app->formatter->asDate($row->real_estate_date,'php:D d M Y'),
                    Yii::$app->formatter->asDate($row->created_at,'php:D d M Y'),
                    $row->price_string,$row->price,$row->phone_contact
                ));
                $i++;
            }
        }

        $writer->close(); // writes the footer, flushes remaining data to browser.
        //echo json_encode(['status'=>true]);
        exit(); // all done
    }

    public function actionDownloadImage(){
        $url_image = 'https://cdn.muaban.net/cdn/images/thumb-detail/201606/04/807/c6bdda3841f64c5ebc978e249d7559e6.jpg';
        $path = Yii::getAlias('@publicProductImagePatch').'/';

        if(!UtilHelper::commonIsImageUrl($url_image)){
            return null;
        }
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        $img_file = file_get_contents ( $url_image , false, stream_context_create($arrContextOptions));

        $filename =Yii::$app->security->generateRandomString() . time() . '.jpg';

        file_put_contents ($path.$filename, $img_file );

        die(var_dump($filename));
    }
    public function actionFiltering(){
        $models =  RealEstateItem::findAll(['is_self'=>0]);
        foreach($models as $item){
            if(UtilHelper::validateIsSelfRealItem($item->title,$item->description,$item->address)){
                $item->is_self = 1;
                $item->save(false);

                if($item->phone_contact){
                    $phoneContact = RealEstatePhoneContact::findOne(['phone'=>$item->phone_contact]);
                    if($phoneContact){
                        $phoneContact->delete();
                    }
                }
                if($item->phone_backup){
                    $phoneContactBackup = RealEstatePhoneContact::findOne(['phone'=>$item->phone_backup]);
                    if($phoneContactBackup){
                        $phoneContactBackup->delete();
                    }
                }
                echo '</br> ****';
                echo '</br> Successfull save item';
                echo '</br> '.$item->title;
                echo '</br> ****';
            }
        }
    }

    /**
     * Lists all RealEstateItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;

        $searchModel = new SearchRealEstateItem();
        $categoryId = null;
        $addressSearching = null;
        $cityFiltering = null;
        $crawlingDateSearching = null;
        $isNew = null;
        $source = null;
        $not_sure = 0;
        $params = Yii::$app->request->queryParams;
        $isSelf = null;
        $price_string = 'bất kỳ';

        $session->set('params', $params);
        $session->set('not_sure',$not_sure);

        if(isset($params['SearchRealEstateItem']['category_id']) && !empty($params['SearchRealEstateItem']['category_id'])){
            $categoryId= $params['SearchRealEstateItem']['category_id'];
        }
        if(isset($params['SearchRealEstateItem']['address']) && !empty($params['SearchRealEstateItem']['address'])){
            $addressSearching = $params['SearchRealEstateItem']['address'];
        }
        if(isset($params['SearchRealEstateItem']['real_estate_date']) && !empty($params['SearchRealEstateItem']['real_estate_date'])){
            $crawlingDateSearching = $params['SearchRealEstateItem']['real_estate_date'];
        }
        if(isset($params['SearchRealEstateItem']['is_new']) && !empty($params['SearchRealEstateItem']['is_new'])){
            $isNew = $params['SearchRealEstateItem']['is_new'];
        }
        if(isset($params['SearchRealEstateItem']['is_self']) && !empty($params['SearchRealEstateItem']['is_self'])){
            $isSelf = $params['SearchRealEstateItem']['is_self'];
        }
        if(isset($params['SearchRealEstateItem']['source']) && !empty($params['SearchRealEstateItem']['source'])){
            $source = $params['SearchRealEstateItem']['source'];
        }
        if(isset($params['SearchRealEstateItem']['price_string']) && !empty($params['SearchRealEstateItem']['price_string'])){
            $price_string = $params['SearchRealEstateItem']['price_string'];
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$not_sure);

        $filter = explode(',',$addressSearching);
        if(!empty($filter)){
            $cityFiltering = $filter[0];
        }
        $cityFiltering = str_replace('+',' ',$cityFiltering);

        $itemNotSureSelf = RealEstateItem::find()->andOnCondition(['is_not_sure_self'=>1])->count();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'categoryId'=>$categoryId,
            'addressSearching'=>str_replace('+',' ',$addressSearching),
            'cityFilter'=>$cityFiltering,
            'crawlingDateSearching'=>$crawlingDateSearching,
            'isNew'=>$isNew,
            'isSelf'=>$isSelf,
            'notSure'=>$not_sure,
            'source'=>$source,
            'itemNotSureSelf'=>$itemNotSureSelf,
            'price_string'=>$price_string
        ]);
    }
    public function actionList()
    {

        $searchModel = new SearchRealEstateItem();
        $categoryId = null;
        $addressSearching = null;
        $cityFiltering = null;
        $crawlingDateSearching = null;
        $isNew = null;
        $not_sure = 0;
        $is_self = 1;
        $price_string = 'bất kỳ';
        $params = Yii::$app->request->queryParams;

        if(isset($params['SearchRealEstateItem']['category_id']) && !empty($params['SearchRealEstateItem']['category_id'])){
            $categoryId= $params['SearchRealEstateItem']['category_id'];
        }
        if(isset($params['SearchRealEstateItem']['address']) && !empty($params['SearchRealEstateItem']['address'])){
            $addressSearching = $params['SearchRealEstateItem']['address'];
        }
        if(isset($params['SearchRealEstateItem']['real_estate_date']) && !empty($params['SearchRealEstateItem']['real_estate_date'])){
            $crawlingDateSearching = $params['SearchRealEstateItem']['real_estate_date'];
        }
        if(isset($params['SearchRealEstateItem']['is_new']) && !empty($params['SearchRealEstateItem']['is_new'])){
            $isNew = $params['SearchRealEstateItem']['is_new'];
        }
        if(isset($params['SearchRealEstateItem']['price_string']) && !empty($params['SearchRealEstateItem']['price_string'])){
            $price_string = $params['SearchRealEstateItem']['price_string'];
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$not_sure,$is_self);

        $filter = explode(',',$addressSearching);
        if(!empty($filter)){
            $cityFiltering = $filter[0];
        }

        $itemNotSureSelf = RealEstateItem::find()->andOnCondition(['is_not_sure_self'=>1])->count();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'categoryId'=>$categoryId,
            'addressSearching'=>$addressSearching,
            'cityFilter'=>$cityFiltering,
            'crawlingDateSearching'=>$crawlingDateSearching,
            'isNew'=>$isNew,
            'notSure'=>$not_sure,
            'itemNotSureSelf'=>$itemNotSureSelf,
            'price_string'=>$price_string
        ]);
    }
    public function actionNotSure()
    {
        $session = Yii::$app->session;


        $searchModel = new SearchRealEstateItem();
        $categoryId = null;
        $addressSearching = null;
        $cityFiltering = null;
        $crawlingDateSearching = null;
        $isNew = null;
        $source = null;
        $params = Yii::$app->request->queryParams;
        $price_string = 'bất kỳ';

        if(isset($params['SearchRealEstateItem']['category_id']) && !empty($params['SearchRealEstateItem']['category_id'])){
            $categoryId= $params['SearchRealEstateItem']['category_id'];
        }
        if(isset($params['SearchRealEstateItem']['address']) && !empty($params['SearchRealEstateItem']['address'])){
            $addressSearching = $params['SearchRealEstateItem']['address'];
        }
        if(isset($params['SearchRealEstateItem']['real_estate_date']) && !empty($params['SearchRealEstateItem']['real_estate_date'])){
            $crawlingDateSearching = $params['SearchRealEstateItem']['real_estate_date'];
        }
        if(isset($params['SearchRealEstateItem']['is_new']) && !empty($params['SearchRealEstateItem']['is_new'])){
            $isNew = $params['SearchRealEstateItem']['is_new'];
        }
        if(isset($params['SearchRealEstateItem']['source']) && !empty($params['SearchRealEstateItem']['source'])){
            $source = $params['SearchRealEstateItem']['source'];
        }
        if(isset($params['SearchRealEstateItem']['price_string']) && !empty($params['SearchRealEstateItem']['price_string'])){
            $price_string = $params['SearchRealEstateItem']['price_string'];
        }
        $not_sure = 1;

        $session->set('params', $params);
        $session->set('not_sure',$not_sure);


        $params['SearchRealEstateItem']['is_not_sure'] = 1;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$not_sure);

        $filter = explode(',',$addressSearching);
        if(!empty($filter)){
            $cityFiltering = $filter[0];
        }

        $itemNotSureSelf = RealEstateItem::find()->andOnCondition(['is_not_sure_self'=>1])->count();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'categoryId'=>$categoryId,
            'addressSearching'=>$addressSearching,
            'cityFilter'=>$cityFiltering,
            'crawlingDateSearching'=>$crawlingDateSearching,
            'isNew'=>$isNew,
            'notSure'=>$not_sure,
            'source'=>$source,
            'itemNotSureSelf'=>$itemNotSureSelf,
            'price_string'=>$price_string
        ]);
    }
    public function actionTest(){

        //$string1 = 'SIÊU CĂN HỘ SẮP RA MẮT VINHOMES CENTRAL PARK TOÀ THE PARK 3 ĐẸP NHẤT DỰ ÁN LIÊN HỆ 0918212122';
        $address ='CHÍNH CHỦ BÁN GẤP NHÀ 1 LẦU ĐÚC, THIẾT KẾ ĐẸP, 4X15M, GIÁ 550 TRIỆU';
//        $address = "";
//        $number = "";
//        $district = "";
//        $city = "";
//
//        $matches = array();
//        if(preg_match('/(?P<number>\d+.?) (?P<address>[^\d]+) (?P<district>[^\d]+) (?P<city>[^\d]+)/' , $string1, $matches)){
//            $address = $matches['address'];
//            $number = $matches['number'];
//            $district = $matches['district'];
//            $city = $matches['city'];
//        } else { // no number found, it is only address
//            $address = $string1;
//        }
//        echo "Input: $string1 <br/>";
//        echo "Number: $number  <br/>";
//        echo "Address: $address <br />";
//        echo "district: $district <br/>";
//        echo "city: $city <br />";
     //   echo $this->str_to_address($string);

       // $address = '6666 Course Dr 666 , Pompano Beach , FL 33333';
        //echo mb_strtolower($address, 'UTF-8');
//        if (strpos(mb_strtolower($address, 'UTF-8'), 'chính chủ') !== FALSE)
//        {
//           echo 'found';
//        }else{
//            echo 'not found';
//        }asDate('28/10', 'dd/MM/yyyy')

       // echo $this->str_to_address($address);
    }

    function str_to_address($context) {
        $array = explode(" ", $context);
        $array_reversed = array_reverse($array);
        $numKey = "";
        $zipKey = "";
        foreach($array_reversed as $k=>$str) {
            if($zipKey) { continue; }
            if(strlen($str)===5 && is_numeric($str)) {
                $zipKey = $k;
            }
        }
        $array_reversed = array_slice($array_reversed, $zipKey);
        $array = array_reverse($array_reversed);
        foreach($array as $k=>$str) {
            if($numKey) { continue; }
            if(strlen($str)>1 && strlen($str)<6 && is_numeric($str)) {
                $numKey = $k;
            }

        }
        $array = array_slice($array, $numKey);
        $string = implode(' ', $array);
        return $string;
    }
    /**
     * Displays a single RealEstateItem model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new RealEstateItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RealEstateItem();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $postDate = new RealEstateItemPostDate();
            $postDate->item_id = $model->id;
            $postDate->post_date = $model->created_at;
            $postDate->price = $model->price;
            $postDate->new_post = 1;
            $postDate->save(false);

            $model->user_id = Yii::$app->user->id;
            $model->real_estate_date = $model->created_at;
            $model->repost_date =  $model->created_at;
            $model->price = UtilHelper::convertPriceStringToNumber($model->price_string);
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RealEstateItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if($model->source == 'https://www.chotot.com'){
            $phone_number =  UtilHelper::detectPhoneNumberFromImage($model->phone_contact);

            $model->phone_contact = trim($phone_number);

            $model->save(false);
            $model->refresh();
        }

        $location = UtilHelper::detectAddress($model->address);
        //die(var_dump($model->address));
        if(!empty($location)){
            $model->latitude = $location['lat'];
            $model->longitude = $location['lng'];
            $model->save(false);
            $model->refresh();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RealEstateItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        $model = $this->findModel($id);

        $portfolio = $model->getRealEstateGalleries()->all();
        if($portfolio && !empty($portfolio)) {
            foreach ($portfolio as $item) {
                $url = $item->file_name;
                try{
                    @unlink(Yii::getAlias('@publicProductImagePatch').'/'.$url);
                    @unlink(Yii::getAlias('@publicProductImagePatch').'/'.Yii::$app->params['thumbName'].$url);
                    @unlink(Yii::getAlias('@publicProductImagePatch').'/'.Yii::$app->params['normalName'].$url);
                }catch (Exception $e){
                    Yii::error($e .'unlink realty images');
                }
            }
        }

        $avatar = $model->avatar;
        try{
            @unlink(Yii::getAlias('@publicProductImagePatch').'/'.$avatar);
            @unlink(Yii::getAlias('@publicProductImagePatch').'/'.Yii::$app->params['thumbName'].$avatar);
            @unlink(Yii::getAlias('@publicProductImagePatch').'/'.Yii::$app->params['normalName'].$avatar);
        }catch (Exception $e){
            Yii::error($e .'unlink realty images avatar');
        }

        try{
            $image_phone = explode('/',$model->phone_contact);
            $index = count($image_phone) - 1;
            @unlink(Yii::getAlias('@assetsUploadPhoneImagePatch').'/'.$image_phone[$index]);

        }catch (Exception $e){
            Yii::error($e .'unlink realty images phone');
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RealEstateItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RealEstateItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RealEstateItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
