<?php
/**
 * User: sangnguyen on  6/29/16 at 20:49
 * File name: index.php
 * Project name: real-estate-system
 * Copyright (c) 2016 by SkillSpars
 * All rights reserved
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
$asset		= backend\assets\AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\product\models\SearchRealEstateItem */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Real Estate Items');
$this->params['breadcrumbs'][] = $this->title;


$asset->css[] = 'theme/assets/global/plugins/datatables/datatables.min.css';
$asset->css[] = 'theme/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css';
$asset->css[] = 'theme/assets/global/plugins/select2/css/select2.min.css';
$asset->css[] = 'theme/assets/global/plugins/select2/css/select2-bootstrap.min.css';
$asset->css[] = 'theme/assets/pages/css/blog.min.css';
$asset->css[] = 'theme/assets/global/plugins/bootstrap-summernote/summernote.css';
$asset->css[] = 'theme/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css  ';
$asset->css[] = 'theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css';


$asset->js[] = 'theme/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js';
$asset->js[] = 'theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js';
$asset->js[] = 'theme/assets/global/plugins/bootstrap-markdown/lib/markdown.js';
$asset->js[] = 'theme/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js';
$asset->js[] = 'theme/assets/global/plugins/bootstrap-summernote/summernote.min.js';
$asset->js[] = 'theme/assets/pages/scripts/ui-blockui.min.js';
$asset->js[] = 'theme/assets/global/scripts/datatable.js';
$asset->js[] = 'theme/assets/global/plugins/datatables/datatables.min.js';
$asset->js[] = 'theme/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js';
$asset->js[] = 'theme/assets/global/plugins/select2/js/select2.full.min.js';

$this->registerJsFile(Yii::$app->homeUrl.'scripts/helpers/mustache.js',[ 'depends' => 'yii\web\YiiAsset']);
$this->registerJsFile(Yii::$app->homeUrl.'scripts/helpers/helpers.js',[ 'depends' => 'yii\web\YiiAsset']);
$this->registerJsFile(Yii::$app->homeUrl.'scripts/modules/crawl.js',[ 'depends' => 'yii\web\YiiAsset']);
$jsScripts = '';

$site = Yii::$app->controller->actionParams['site'];

$jsScripts = 'var site = "'.$site.'";';
$jsScripts .= 'var domain = "'.Yii::$app->params['domain'].'";';

$jsScripts .= <<<JS
	var Crawling = new Crawl(domain);
	Crawling.handleCrawlData(site);
    Crawling.handleGetDetailItem(site);
    Crawling.handleEditor();
    Crawling.handleSubmitFormData();
JS;
$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);

?>

<div class="real-estate-item-index" id="crawling_data">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="col-md-4">
        <a id="btn-crawling" class="btn btn-success" href="javascript:;"><?=Yii::t('backend','Crawl')?></a>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="col-nd-4">
                <div class="form-group">
                    <label class="col-md-2 control-label"><?=Yii::t('backend','Category')?></label>
                    <div class="col-md-4">
                        <select id="urlCrawl" class="form-control">
                            <?php
                            if(isset($categoriesQuery) && !empty($categoriesQuery)){
                                foreach($categoriesQuery as $cat=>$query){
                                    echo '<option value="'.$query.'">'.$cat.'</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6" id="box_crawling_paging" style="display: none">
                <select id="crawling_paging" class="form-control" multiple>
                </select>
            </div>

        </div>
    </div>
    <hr/>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover table-light" id="data_crawled">
            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade in" id="detail_item_model" tabindex="-1" tabindex="-1" role="basic" aria-hidden="true">
    <?php  ActiveForm::begin([
        'action'=>\yii\helpers\Url::to('/product/default/save-item-crawling'),
        'method'=>'POST',
        'id'=>'form_data_crawling'
    ]); ?>
    <input type="hidden" id="data_crawling" name="data_crawling">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Chi tiết bất động sản</h4>
            </div>
            <div class="modal-body">
             <div id="item_detail">
             </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('backend','close')?></button>
                <button id="btn_submit" type="submit" class="btn btn-primary"><?php echo Yii::t('backend','submit')?></button>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<script id="crawling_item_detail" type="text/x-mustache">
        <div class="blog-post-lg bordered blog-container">
             <div class="blog-img-thumb">

                  <img style="width:570px" src="{{img}}">
             </div>
             <div class="blog-post-content">

                 <h2 class="blog-title blog-post-title">
                    {{title}}
                 </h2>
                 <strong>Tóm tắt:</strong>

                 <p class="blog-post-desc">{{summary}}</p>
                  <strong>Nội dung</strong>

                 <p>{{description}}</<p>

             </div>
        </div>
        <strong>Đặc điểm bất động sản</strong>

             <ul>
                 <li>Ngày đăng: {{created}}</li>
                     <li>Địa chỉ: {{address}}</li>
                     <li>Diện tích: {{area}}</li>
                     <li>Giá : {{price}}</li>
                 </ul>
                   <strong>Thông tin liên lạc</strong>
                <ul>
                    <li>Tên: {{contact_phone}}</li>
                    <li>Số điện thoại: {{contact_phone}}</li>
                    <li>Số điện thoại 2: {{contact_phone_backup}}</li>
                 </ul>


         <strong>Thư viện hình ảnh:</strong>
        <div class="row ui-margin">
            {{#images}}

             <div class="col-xs-4 ui-padding">
                <ul style="list-style:none">
                    <li><img style="width:70px" src="{{src}}"></li>
                 </ul>
             </div>
             {{/images}}
         </div>
</script>
