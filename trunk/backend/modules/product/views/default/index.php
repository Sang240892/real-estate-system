<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\commons\helpers\UtilHelper;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\product\models\SearchRealEstateItem */
/* @var $dataProvider yii\data\ActiveDataProvider */
$asset		= backend\assets\AppAsset::register($this);

$asset->css[] = 'theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css';
$asset->css[] = 'theme/assets/global/plugins/fancybox/source/jquery.fancybox.css';
$asset->css[] = 'theme/assets/global/plugins/fancybox/source/helpers/jquery.fancybox-buttons.css';
$asset->css[] = 'theme/assets/global/plugins/fancybox/source/helpers/jquery.fancybox-thumbs.css';

$asset->css[] = 'theme/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css';
$asset->css[] = 'theme/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css';
$asset->css[] = 'theme/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css';
$asset->css[] = 'theme/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css';

$asset->js[] = 'theme/assets/global/plugins/moment.min.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/source/jquery.fancybox.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/source/helpers/jquery.fancybox-buttons.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/source/helpers/jquery.fancybox-thumbs.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/source/helpers/jquery.fancybox-media.js';
$asset->js[] = 'theme/assets/global/plugins/fancybox/source/helpers/jquery.fancybox-thumbs.js';
$asset->js[] = 'theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js';
$asset->js[] = 'theme/assets/pages/scripts/components-bootstrap-switch.min.js';

$asset->js[] = 'theme/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js';
$asset->js[] = 'theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
$asset->js[] = 'theme/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js';
$asset->js[] = 'theme/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js';

$this->registerJsFile(Yii::$app->homeUrl.'scripts/plugins/fancybox.js', ['','depends' =>'yii\web\YiiAsset']);
$this->registerJsFile(Yii::$app->homeUrl.'scripts/helpers/helpers.js', ['depends' =>'yii\web\YiiAsset']);

$this->registerJsFile(Yii::$app->homeUrl.'scripts/helpers/config.js', ['depends' =>'yii\web\YiiAsset']);
$this->registerJsFile(Yii::$app->homeUrl.'scripts/modules/real_estate_item.js', ['depends' =>'yii\web\YiiAsset']);
$this->title = Yii::t('backend', 'Real Estate Items');
$this->params['breadcrumbs'][] = $this->title;
$jsScripts = 'var city_filter = "'.$cityFilter.'"; ';
$jsScripts .= 'var province_filter = "'.$addressSearching.'"; ';
$jsScripts .= "
	ComponentsBootstrapSwitch.init();
	var Item = new RealItem()
	Item.handleBootstrapSwitch();
	Item.handleDateRange();
	Item.handleExportExcel();
	Item.handleShowCityList(city_filter,province_filter);
	Item.handleShowProvincesList();
	Item.handleFilterRangDate();
	Item.handleBootstrapSwitchCheckOut();
";
$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);
$parents = \common\models\entities\RealEstateCategory::findAll(['parent'=>0]);

?>
<?php $form = ActiveForm::begin([
    'id' => 'export-form',
    'action'=>['/product/default/export-excel']
]); ?>
<input id="rangeDate" name="rangeDate" type="hidden"/>
<?php ActiveForm::end(); ?>

<div class= "real-estate-item-index row">
    <div class="col-md-12">

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <!--<div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Buttons</span>
                </div>
                <div class="tools"> </div>
            </div>-->
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
<!--                                <div class="col-md-5">-->
<!--                                    <div class="input-group" id="defaultrange">-->
<!--                                        <input type="text" class="form-control">-->
<!--                                            <span class="input-group-btn">-->
<!--                                                <button class="btn default date-range-toggle" type="button">-->
<!--                                                    <i class="fa fa-calendar"></i>-->
<!--                                                </button>-->
<!--                                            </span>-->
<!--                                    </div>-->
<!--                                </div>-->
                                <div class="col-md-7">
                                    <?= Html::a(Yii::t('backend', 'Add new').'<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success']) ?>

                                    <?= Html::a(Yii::t('backend', 'Export to Excel').'<i class="fa fa-file-excel-o"></i></a>',
                                        ['/product/default/export-excel'], ['id'=>'btn_export_excel','class' => 'btn btn-success' ,'target'=>'_blank']) ?>
                                    <?= Html::a(Yii::t('backend', 'Xuất dữ liệu môi giới').'<i class="fa fa-file-excel-o"></i></a>',
                                        ['/product/default/export-excel-agency'], ['id'=>'btn_export_excel','class' => 'btn btn-success']) ?>
                                </div>

                            </div>
                        </div>
<!--                        <div class="col-md-2">-->
<!--                            <div class="btn-group pull-right">-->
<!--                                <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown" aria-expanded="false">--><?//=
//                                    Yii::t('backend','Tools')?>
<!--                                    <i class="fa fa-angle-down"></i>-->
<!--                                </button>-->
<!--                                <ul class="dropdown-menu pull-right">-->
<!--                                    <li>-->
<!--                                        <a href="javascript:;">-->
<!--                                            <i class="fa fa-file-pdf-o"></i> --><?//= Yii::t('backend','Delete')?><!-- </a>-->
<!--                                    </li>-->
<!--                                </ul>-->
<!--                            </div>-->
<!--                        </div>-->
                    </div>
                </div>

                <div id="" class="dataTables_wrapper no-footer">
                    <form id="filteringForm">
                        <div class="row">
                            <div id="product_filter" class="dataTables_filter">
                                <div class="col-md-4">
                                    <label>Doanh mục</label>

                                    <select onchange="this.form.submit()" name="SearchRealEstateItem[category_id]" class="bs-select form-control">
                                        <option value="0">Chọn doanh mục</option>
                                        <?php
                                        $item='';
                                        if(!empty($parents)){
                                            foreach($parents as $parent){
                                                $childs = \common\models\entities\RealEstateCategory::findAll(['parent'=>$parent->id,'is_display'=>1]);
                                                $item.='<optgroup label="'.$parent->name.'">';

                                                if(!empty($childs)){
                                                    foreach($childs as $child){
                                                        $selected = (isset($categoryId) && $categoryId == $child->id)?'selected':'';
                                                        $item.= "<option ".$selected." value='".$child->id."'>".$child->name."</option>";
                                                    }
                                                }
                                                $item.='</optgroup>';
                                            }
                                        }
                                        echo $item;
                                        ?>
                                    </select>

                                </div>
                                <div  class="col-md-3">
                                    <label>Thành phố</label>
                                    <div id="dropCityList" ></div>
                                </div>
                                <div class="col-md-3">
                                    <label>Quận huyện</label>
                                    <div id="dropProvincesList"> <select name="SearchRealEstateItem[address]" id="filter_provinces" class="bs-select form-control"></select></div>
                                </div>
                                <div class="col-md-2">
                                    <label>Giá</label>
                                    <select onchange="this.form.submit()" name="SearchRealEstateItem[price_string]" id="product_filter_price" class="form-control">
                                        <option <?=(isset($price_string) && $price_string == 'bất kỳ')?'selected':''?> value="bất kỳ">Bất kỳ</option>
                                        <option <?=(isset($price_string) && $price_string == 'thỏa thuận')?'selected':''?> value="thỏa thuận">Thoả thuận</option>
                                        <option <?=(isset($price_string) && $price_string == '< 1m')?'selected':''?> value="< 1m">< 1 triệu</option>
                                        <option <?=(isset($price_string) && $price_string == '1,3 m')?'selected':''?> value="1,3 m">1 - 3 triệu</option>
                                        <option <?=(isset($price_string) && $price_string == '3,5 m')?'selected':''?> value="3,5 m">3 - 5 triệu</option>
                                        <option <?=(isset($price_string) && $price_string == '5,10 m')?'selected':''?> value="5,10 m">5 - 10 triệu</option>
                                        <option <?=(isset($price_string) && $price_string == '10,50 m')?'selected':''?> value="10,50 m">10 - 50 triệu</option>
                                        <option <?=(isset($price_string) && $price_string == '50,100 m')?'selected':''?> value="50,100 m">50 - 100 triệu</option>
                                        <option <?=(isset($price_string) && $price_string == '100,500 m')?'selected':''?> value="100,500 m">100 - 500 triệu</option>
                                        <option <?=(isset($price_string) && $price_string == '500,1 b')?'selected':''?> value="500,1 b">500 - 1 tỷ</option>
                                        <option <?=(isset($price_string) && $price_string == '1,3 b')?'selected':''?> value="1,3 b">1 - 3 tỷ</option>
                                        <option <?=(isset($price_string) && $price_string == '3,9 b')?'selected':''?> value="3,9 b">3 - 9 tỷ</option>
                                        <option <?=(isset($price_string) && $price_string == '9,15 b')?'selected':''?> value="9,15 b">9 - 15 tỷ</option>
                                        <option <?=(isset($price_string) && $price_string == '15,30 b')?'selected':''?> value="15,30 b">15 - 30 tỷ</option>
                                        <option <?=(isset($price_string) && $price_string == '>30 b')?'selected':''?> value=">30 b"> > 30 tỷ</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-5">
                                <label>Ngày cập nhật tin</label>
                                <div class="input-group" id="filterRangeDate">
                                    <input value="<?=$crawlingDateSearching?>" name="SearchRealEstateItem[real_estate_date]" id="filterRangeDateInput" type="text" class="form-control">
                                        <span class="input-group-btn">
                                            <button class="btn default date-range-toggle" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label>Loại tin</label>
                                <select onchange="this.form.submit()" name="SearchRealEstateItem[is_new]" id="filter_city" class="bs-select form-control">
                                    <option value="">Tất cả</option>
                                    <option <?= (isset($isNew) && $isNew == 1)?"selected":"" ?> value="1">Tin mới</option>
                                    <option <?= (isset($isNew) && $isNew == -1)?"selected":"" ?> value="-1">Tin đăng lại</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>Loại tin</label>
                                <select onchange="this.form.submit()" name="SearchRealEstateItem[is_self]" id="" class="bs-select form-control">
                                    <option value="">Tất cả</option>
                                    <option <?= (isset($isSelf) && $isSelf == 1)?"selected":"" ?> value="1">Tin Chính Chủ</option>
                                    <option <?= (isset($isSelf) && $isSelf == -1)?"selected":"" ?> value="-1">Tin Môi Giới</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>Nguồn</label>
                                <select onchange="this.form.submit()" name="SearchRealEstateItem[source]" id="" class="bs-select form-control">
                                    <option value="">Tất cả</option>
                                     <option <?= (isset($source) && $source == 'muaban')?"selected":"" ?> value="muaban">Mua Bán</option>
                                  
                                    <option <?= (isset($source) && $source == 'chotot')?"selected":"" ?> value="chotot">Chợ tốt</option>
                                     <option <?= (isset($source) && $source == 'batdongsan')?"selected":"" ?> value="batdongsan">Bất động sản</option>
                                    <option <?= (isset($source) && $source == 'bdssmart')?"selected":"" ?> value="bdssmart">BdsSmart</option>
                                </select>
                            </div>
                        </div>
                    </form>
                    <hr/>
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs ">
                            <li class="<?=($notSure == 0)?'active':''?>">
                                <?= Html::a(Yii::t('backend', 'Tin đã kiểm tra '),
                                    ['/product/default/index']) ?>
                            </li>
                            <li class="<?=($notSure == 1)?'active':''?>">
                                <?= Html::a(Yii::t('backend', 'Tin cần kiểm tra <span id="numberOfNotSureItem" class="label label-sm label-danger circle">'.$itemNotSureSelf.'</span>'),
                                    ['/product/default/not-sure']) ?>
                            </li>
                        </ul>
                    </div>
                    <hr/>
                    <div class="row">
                        <?php
                        $sort = $dataProvider->getSort();
                        //echo $sort->link('phoneExistAmount')
                        ?>
                        <div class="col-md-6 col-sm-12">
                            <?php
                            // You can choose to render your own GridView separately
                            echo \nterms\pagesize\PageSize::widget([
                                'label' => false,
                                'template'=> Yii::t('backend','View {list} record(s) | Found total {totalCount} record(s)',[
                                    'totalCount'=>$dataProvider->getTotalCount()
                                ]),
                                'options'=> ['class'=>'form-control input-sm input-xsmall input-inline','id'=>'perpage']
                            ]);
                            $gridColumns = [

                                [
                                    'class' => 'yii\grid\CheckboxColumn',
                                    'checkboxOptions'=>[
                                        'class'=>'checkboxHandle'
                                    ]
                                ],

                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{view} {update} {delete} ',
                                    'buttons' => [
//                                        'portfolio'=>function($url,$model){
//                                            $icon = '<span style="" class="fa fa-file-image-o"></span>';
//                                            $url = \yii\helpers\Url::to(['portfolio','id'=>$model->id]);
//                                            return Html::a($icon,$url,['title'=>Yii::t('backend','portfolio')]);
//                                        }
                                    ],
                                ],
                                //'id',
//                                    [
//                                        'attribute'=>'category_id',
//                                        'format' => 'html',
//                                        'header'=>'Loại tin',
//                                        'filter'=>['8509'=>'8509','8510'=>'8510'],
//                                        'content'=> function ($model) {
//                                            return $model->category_id;
//                                        },
//                                    ],
                                [
                                    'attribute'=>'is_self',
                                    'format' => 'html',
                                    'header'=>'Loại tin',
                                    'filter'=>false,
                                    'content'=> function ($model) {
                                        $status = ($model->is_self == 1)?'checked':'';
                                        $check = ($model->is_not_sure_self == 0)?'checked':'';

                                        $html = '<div><input id="'.$model->id.'" class="make-switch real_estate_item_switch" '.$status.'  data-on-text="&nbsp;C.Chủ&nbsp;&nbsp;" data-off-text="&nbsp;M.Giới  &nbsp;" type="checkbox"  data-on-color="success" data-off-color="warning"></div>';
                                        $html .= '<br/><div><input data-id="'.$model->id.'" id="check_'.$model->id.'" class="make-switch real_estate_item_switch_check_out" '.$check.'  data-on-text="&nbsp;Đã.KT&nbsp;&nbsp;" data-off-text="&nbsp;Chưa.KT&nbsp;" type="checkbox"  data-on-col="success" data-off-color="warning"></div>';
                                        return $html;
                                    },
                                ],
//                                [
//                                    'format' => 'html',
//                                    'filter'=>false,
//                                    'label'=>Yii::t('backend','Avatar'),
//                                    'headerOptions' => ['width' => '100px'],
//                                    'value' => function ($searchModel, $index, $widget) {
//                                        $image = null;
//                                        if($searchModel->avatar){
//                                            $image = $searchModel->avatar;
//                                        }else{
//                                            if(isset($searchModel->realEstateGalleries) && !empty($searchModel->realEstateGalleries)){
//                                                $image = $searchModel->realEstateGalleries[0]->file_name;
//                                            }
//                                        }
//                                        $url = UtilHelper::buildUrlImageFromSiteOriginal($image);
//                                        if($url !== null){
//                                            return  Html::tag('a',Html::img($url,['style'=>'width:100px']) , [
//                                                'href'=>$url,
//                                                'class'=>'fancybox-thumbs',
//                                                'data-fancybox-group'=>'thumb'
//                                            ]);
//                                        }
//                                        return  Html::tag('a',Html::img(UtilHelper::builtUrlImages(Yii::$app->params['productFinder'],$image),['style'=>'width:100px']) , [
//                                            'href'=>UtilHelper::builtUrlImages(Yii::$app->params['productFinder'],$image),
//                                            'class'=>'fancybox-thumbs',
//                                            'data-fancybox-group'=>'thumb'
//                                        ]);
//
//                                    }
//                                ],
                                [
                                    'attribute'=>'title',
                                    'format' => 'html',
                                    'value'=> function ($model) {
                                        $status = ($model->is_self == 1)?'checked':'';
                                        $isNew = ($model->is_new == 1)?'Tin mới':'Tin đăng lại';
                                        $class = ($model->is_new == 1)?'danger':'warning';

                                        $html = '';
                                        $html .= '<span class="label label-sm label-'.$class.'">'.$isNew.'</span>';
                                        $html .=  wordwrap($model->title, 95, "<br />\n");
                                        $html .= '<br/><small><i class="fa fa-location-arrow" aria-hidden="true"></i> Địa chỉ: '.$model->address.'</small>';
                                       if(isset($model->category) && $model->category->finder_name){
                                           $html .= '<br/><small><i class="fa fa-folder-o" aria-hidden="true"></i> Doanh mục: '.$model->category->finder_name.'</small>';
                                       }

                                        $html .= '<br/><small><i class="fa fa-money" aria-hidden="true"></i> Giá: '.$model->price_string.'</small>';
                                        $html .= '<br/><small><i class="fa fa-square-o" aria-hidden="true"></i> Diện tích: '.$model->area.'</small>';

//                                        $html .= '<br/><small><i class="fa fa-calendar" aria-hidden="true"></i> Ngày đăng: '.Yii::$app->formatter->asDate($model->created_at,'Y/MM/d').'</small>';
                                        $title = ($model->user_id == null)?'Ngày cào về':'Ngày đăng tin';

                                        $html .= '<br/><small><i class="fa fa-calendar" aria-hidden="true"></i> '.$title.' : '.Yii::$app->formatter->asDate($model->created_at,'Y/MM/d').'</small>';

                                       // $postDate = $model->getRealEstateItemPostDates()->select('post_date')->orderBy(['post_date'=>SORT_DESC])->one();
                                        $postDates = $model->realEstateItemPostDates;
////
                                        if($model->is_new == 0 && !empty($model->repost_date ) && isset($postDates[0])){
                                            $html .= '<br/><small><i class="fa fa-calendar" aria-hidden="true"></i> Ngày đăng lại : '.Yii::$app->formatter->asDate($postDates[0]->post_date,'Y/MM/d').'</small>';
                                        }



                                        $description = $model->description;
                                        $lib = array_merge(\backend\commons\helpers\UtilHelper::street_lib_ha_noi(),
                                            \backend\commons\helpers\UtilHelper::street_lib_ho_chi_minh());

                                        $address= \backend\commons\helpers\UtilHelper::findBeforeWithKeyword($description,$lib);

                                        if(!empty($address)){
                                            foreach($address as $ad){
                                                if(\backend\commons\helpers\UtilHelper::checkAddress(trim($ad)) === true){
                                                    $description = str_replace(mb_strtolower($ad, 'UTF-8'),"<strong>".trim($ad)."</strong>",mb_strtolower($description, 'UTF-8'));
                                                }
                                            }
                                        }

                                        $description = UtilHelper::showAddressReal($description);

                                        $characters = \backend\commons\helpers\UtilHelper::character_lib();

                                        foreach($characters as $c){
                                            if(strpos(mb_strtolower($description, 'UTF-8'), trim($c)) !== FALSE
                                                && strpos(mb_strtolower($description, 'UTF-8'),"chút" ) === FALSE
                                                && strpos(mb_strtolower($description, 'UTF-8'),"công" ) === FALSE
                                                && strpos(mb_strtolower($description, 'UTF-8'),"bách" ) === FALSE
                                                && strpos(mb_strtolower($description, 'UTF-8'),"chúng" ) === FALSE) {
                                                $description = str_replace(trim($c),"<strong>".trim($c)."</strong>",mb_strtolower($description, 'UTF-8'));
                                            }
                                        }

                                        $description =  wordwrap($description, 95, "<br />\n");
                                        return $html.'<hr/> <strong>Mô tả:</strong> '.$description;
                                    },
                                ],
//                                [
//                                    'attribute'=>'description',
//                                    'format' => 'html',
//                                    'value'=> function ($model) {
//                                        return wordwrap(\backend\commons\helpers\UtilHelper::shorten($model->description,900 ), 40, "<br />\n");
//                                    },
//                                ],
                                //'summary',
                                // 'description',
                                [
                                    'attribute'=>'phone_contact',
                                    'format' => 'html',
                                    'value'=> function ($model) {
                                        if(UtilHelper::commonIsImageUrl($model->phone_contact)){
                                            return Html::img($model->phone_contact,['style'=>'width:100px']);
                                        }
                                        if(!empty($model->phone_contact)){
                                        	 $number = UtilHelper::countRealtyOfPhoneNumber($model->phone_contact);
                                            //return $model->phone_contact;
                                            return $model->phone_contact .' <span id="" class="label label-sm label-danger circle">'.$number.'</span>';
                                    	}else{
                                    		return null;
                                    	}
                                       
                                    },
                                ],

                                [
                                    'attribute'=>'source',
                                    'format' => 'html',
                                    'filter'=>false,
                                    'value'=> function ($model) {
                                        return $model->source;
                                    },
                                ],
                                // 'status',
                                // 'confirmed',
                                // 'is_spam',
                                // 'street',
                                // 'district',
                                // 'ward',
                                // 'city',
                                // 'country',
                                //'phone_contact',
                                

                                // 'start_date',
                                // 'end_date',
//                                    [
//                                        'attribute'=>'type',
//                                        'format' => 'html',
//                                        'value'=> function ($model) {
//                                            return wordwrap(\backend\commons\helpers\UtilHelper::shorten($model->type,100 ), 20, "<br />\n");
//                                        },
//                                    ],

                                // 'unit',
                                // 'category_name',
                                // 'created_at',
                                // 'updated_at',
                                // 'user_id',
                                // 'category_id',
                                // 'latitude',
                                // 'longitude',,

//                                [
//                                    'class' => 'yii\grid\ActionColumn',
//                                    'template' => '{portfolio} {view} {update} {delete} ',
//                                    'buttons' => [
//                                        //                    'viewEvent'=>function($url, $model){
//                                        //                        $icon = '<span class="glyphicon glyphicon-picture"></span>';
//                                        //                        $url = Url::to(['/photo/event','event_id'=>$model->id]);
//                                        //                        return Html::a($icon,$url,['title'=>'View photo of event']);
//                                        //                    }
//                                        'portfolio'=>function($url,$model){
//                                            $icon = '<span style="" class="fa fa-file-image-o"></span>';
//                                            $url = \yii\helpers\Url::to(['portfolio','id'=>$model->id]);
//                                            return Html::a($icon,$url,['title'=>Yii::t('backend','portfolio')]);
//                                        }
//                                    ],
//                                ]
                            ];
                            ?>
                        </div>

                    </div>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'filterSelector' => 'select[name="per-page"]',
                        'layout'=>"<div class='table-scrollable'>{items}</div>\n<div class='row'>
                                            <div class='col-md-5 col-sm-12'>
                                            <div class='dataTables_info'>{summary}</div></div>
                                            \n<div class='col-md-7 col-sm-12'>
                                            <div class='pull-right dataTables_paginate paging_bootstrap_full_number'>
                                            {pager}</div></div></div>",
                        'sorter'=>[
                            'linkOptions'=>[
                                'class'=>'sorting'
                            ]
                        ],
                        'tableOptions' => [
                            'id'=> 'girdView',
                            'class' => 'table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer'
                        ],
                        'columns' =>$gridColumns
                    ]); ?>
                   
                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<div>

