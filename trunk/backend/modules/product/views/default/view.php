<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\entities\RealEstateItem */

//$this->title = $model->title;
$this->params['breadcrumbs'][] = $model->title;
$asset		= backend\assets\AppAsset::register($this);

$detailUrl = $model->getRealEstateUrlCrawlingItem()->one();

$asset->css[] = 'theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css';

$asset->js[] = 'theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js';
$asset->js[] = 'theme/assets/pages/scripts/components-bootstrap-switch.min.js';
$this->registerJsFile(Yii::$app->homeUrl.'scripts/helpers/helpers.js', ['depends' =>'yii\web\YiiAsset']);
$this->registerJsFile(Yii::$app->homeUrl.'scripts/modules/real_estate_item.js', ['depends' =>'yii\web\YiiAsset']);
$jsScripts = '';
$jsScripts .= <<<JS
	ComponentsBootstrapSwitch.init();
	var Item = new RealItem()
	Item.handleBootstrapSwitch();

JS;
$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);
?>
<div class="real-estate-item-view">
    <p>
        <?= Html::a(Yii::t('backend', 'Link chi tiết'),($detailUrl)?$detailUrl->url:'javascript:;', ['target'=>'_new','class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a(Yii::t('backend', 'Thư viện ảnh'), ['portfolio', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <input data-on-text="C.Chủ" data-off-text="M.Giới"  id="<?=$model->id?>" type="checkbox" class=" col-md-3 make-switch real_estate_item_switch" <?=($model->is_self == 1)?'checked':''?> data-on-color="success" data-off-color="warning">


    </p>
    <table id="w0" class="table table-striped table-bordered detail-view"><tbody>
        <tr><th>Tiêu đề</th><td>
                <?php
                $html = $model->title;
                $description = trim($model->description);
                $lib = array_merge(\backend\commons\helpers\UtilHelper::street_lib_ha_noi(),
                    \backend\commons\helpers\UtilHelper::street_lib_ho_chi_minh());

                $address= \backend\commons\helpers\UtilHelper::findBeforeWithKeyword($html,$lib);

                if(!empty($address)){
                    foreach($address as $ad){
                        if(\backend\commons\helpers\UtilHelper::checkAddress(trim($ad)) === true){
                            $html = str_replace(mb_strtolower($ad, 'UTF-8'),"<strong>".trim($ad)."</strong>",mb_strtolower($html, 'UTF-8'));
                        }
                    }
                }

                $html = \backend\commons\helpers\UtilHelper::showAddressReal($html);

                $characters = \backend\commons\helpers\UtilHelper::character_lib();

                foreach($characters as $c){
                    if(strpos(mb_strtolower($html, 'UTF-8'), trim($c)) !== FALSE
                        && strpos(mb_strtolower($html, 'UTF-8'),"chút" ) === FALSE
                        && strpos(mb_strtolower($html, 'UTF-8'),"công" ) === FALSE
                        && strpos(mb_strtolower($html, 'UTF-8'),"bách" ) === FALSE
                        && strpos(mb_strtolower($html, 'UTF-8'),"chúng" ) === FALSE) {
                        $description = str_replace(trim($c),"<strong>".trim($c)."</strong>",mb_strtolower($description, 'UTF-8'));
                    }
                }

                echo $html;
                ?>
                <br/>
                Ngày cập nhật: <?= Yii::$app->formatter->asDate($model->real_estate_date,'Y/MM/d')?></td></tr>
                <tr><th>Nội dung</th><td>
                <?php
                $html = $model->description;
                $lib = array_merge(\backend\commons\helpers\UtilHelper::street_lib_ha_noi(),
                    \backend\commons\helpers\UtilHelper::street_lib_ho_chi_minh());

                $address= \backend\commons\helpers\UtilHelper::findBeforeWithKeyword($html,$lib);

                if(!empty($address)){
                    foreach($address as $ad){
                        if(\backend\commons\helpers\UtilHelper::checkAddress(trim($ad)) === true){
                            $html = str_replace(mb_strtolower($ad, 'UTF-8'),"<strong>".trim($ad)."</strong>",mb_strtolower($html, 'UTF-8'));
                        }
                    }
                }

                $html = \backend\commons\helpers\UtilHelper::showAddressReal($html);

                $characters = \backend\commons\helpers\UtilHelper::character_lib();

                foreach($characters as $c){
                    if(strpos(mb_strtolower($html, 'UTF-8'), trim($c)) !== FALSE
                        && strpos(mb_strtolower($html, 'UTF-8'),"chút" ) === FALSE
                        && strpos(mb_strtolower($html, 'UTF-8'),"công" ) === FALSE
                        && strpos(mb_strtolower($html, 'UTF-8'),"bách" ) === FALSE
                        && strpos(mb_strtolower($html, 'UTF-8'),"chúng" ) === FALSE){
                        $html = str_replace(trim($c),"<strong>".trim($c)."</strong>",mb_strtolower($html, 'UTF-8'));
                    }
                }

                echo $html;
                ?>
            </td></tr>
        <tr><th>Nguồn</th><td><?=$model->source?></td></tr>
        <tr><th>Địa chỉ</th><td><?php
                $address = $model->address;
                if(\backend\commons\helpers\UtilHelper::checkAddress(trim($address)) === true){
                    $address = str_replace(mb_strtolower($address, 'UTF-8'),"<strong>".trim($address)."</strong>",mb_strtolower($address, 'UTF-8'));
                }
                echo $address;
                ?></td></tr>
        <tr><th>Số điện thoại 1</th><td><?=$model->phone_contact?></td></tr>
        <tr><th>Số điện thoại 2</th><td><?=$model->phone_contact?></td></tr>
        <tr><th>Giá bằng chữ</th><td><?=$model->price_string?></td></tr>
        <tr><th>Giá bằng số</th><td><?=$model->price?></td></tr>
        </tbody></table>


</div>
