<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\entities\RealEstateItem */

//$this->title = Yii::t('backend', 'Update {modelClass}: ', [
//    'modelClass' => 'Real Estate Item',
//]) . $model->title;

$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];

?>
<div class="real-estate-item-update">
    <?= $this->render('_form', [
        'model' => $model,
        //'relationId'=>$relationId
    ]) ?>

</div>
