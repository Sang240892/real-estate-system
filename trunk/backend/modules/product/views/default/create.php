<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\entities\RealEstateItem */

$this->title = Yii::t('backend', 'Create Real Estate Item');

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="real-estate-item-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
