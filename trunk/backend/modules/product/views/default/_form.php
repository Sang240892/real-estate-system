<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$asset		= backend\assets\AppAsset::register($this);

$asset->css[] = 'theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css';
$asset->css[] = 'theme/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css';
$asset->css[] = 'theme/assets/global/plugins/bootstrap-summernote/summernote.css';

$asset->js[] = 'theme/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js';
$asset->js[] = 'theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js';
$asset->js[] = 'theme/assets/global/plugins/bootstrap-markdown/lib/markdown.js';
$asset->js[] = 'theme/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js';
$asset->js[] = 'theme/assets/global/plugins/bootstrap-summernote/summernote.min.js';

$this->registerJsFile(Yii::$app->homeUrl.'scripts/helpers/components-editors.js', ['depends' =>'yii\web\YiiAsset']);
/* @var $this yii\web\View */
/* @var $model common\models\entities\RealEstateItem */
/* @var $form yii\widgets\ActiveForm */

$cate = \common\models\entities\RealEstateCategory::find()->andOnCondition(['status'=>1])->andOnCondition('site_crawling_id <> :site and href <> :href',[
    ':href'=>'',
    ':site'=>''
])->all();
$cate = \yii\helpers\ArrayHelper::map($cate,'id','name');
?>

<div class="real-estate-item-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'summary')->textarea([
        'maxlength' => true,
        'class'=>'wysihtml5 form-control',
        'rows'=>6
    ]) ?>

    <?= $form->field($model, 'description')->textarea([
        'maxlength' => true,
        'class'=>'wysihtml5 form-control',
        'rows'=>6
    ]) ?>

    <?= $form->field($model, 'is_self')->textInput(['maxlength' => true,
        'value'=>1,'type'=>'hidden'])->label(false) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contact_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_contact')->textInput(['type'=>'number','maxlength' => true]) ?>

    <?= $form->field($model, 'area_number')->textInput(['type'=>'number','maxlength' => true]) ?>

    <?= $form->field($model, 'price_string')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'source')->dropDownList(
        ['https://www.chotot.com'=>'https://www.chotot.com','https://muaban.net'=>'https://muaban.net','http://batdongsan.com.vn'=>'http://batdongsan.com.vn','http://www.bdssmart.com/'=>'http://www.bdssmart.com/'],
        [
            'class'=>['form-control'],
            'options'=>[
                $model->source => ['selected' => true]
            ]
        ]
    );?>

    <?= $form->field($model, 'category_id')
        ->label(Yii::t('backend', 'Category Name'),['class'=>'control-label'])
        ->dropDownList(
            $cate,
            [
                'class'=>['form-control'],
                'options'=>[
                    $model->category_id => ['selected' => true]
                ]
            ]
        );?>

    <?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
