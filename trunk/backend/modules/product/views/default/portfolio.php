<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\commons\helpers\UtilHelper;
/* @var $this yii\web\View */
/* @var $model common\models\entities\RealEstateItem */


$asset		= backend\assets\AppAsset::register($this);
$asset->css[] = 'theme/assets/pages/css/portfolio.min.css';
$asset->css[] = 'theme/assets/global/plugins/cubeportfolio/css/cubeportfolio.css';

$asset->js[] ='theme/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js';
$this->registerJsFile(Yii::$app->homeUrl.'scripts/helpers/load_portfolio.js', ['depends' =>'yii\web\YiiAsset']);
?>
<div class="real-estate-item-view">
    <!--<p>
        <?/*= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) */?>
        <?/*= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) */?>
    </p>
-->
<h3 class="page-title">Thư viện ảnh
    <small><?=$model->title?></small>
</h3>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="portfolio-content portfolio-4">
    <div id="js-filters-full-width" class="cbp-l-filters-alignCenter">
        <div data-filter="*" class="cbp-filter-item-active cbp-filter-item"> Tất cả ảnh
            <div class="cbp-filter-counter"></div>
        </div>
    </div>
    <div id="js-grid-full-width" class="cbp">
        <?php
        $portfolio = $model->getRealEstateGalleries()->all();
        if($portfolio && !empty($portfolio)):
            foreach($portfolio as $item):
        ?>
        <div class="cbp-item logos">
            <a href="<?= UtilHelper::builtUrlImages(Yii::$app->params['productFinder'],$item->file_name)?>" class="cbp-caption cbp-lightbox" data-title="<?=$item->title?>">
                <div class="cbp-caption-defaultWrap">
                    <img src="<?= UtilHelper::builtUrlImages(Yii::$app->params['productFinder'],$item->file_name)?>" alt=""> </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignLeft">
                        <div class="cbp-l-caption-body">
                            <div class="cbp-l-caption-title"><?=Yii::t('backend','Real Estate')?></div>
                            <div class="cbp-l-caption-desc"><?=$model->title?></div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <?php
            endforeach;
        endif
        ?>
    </div>
</div>
