<?php

namespace backend\modules\category\controllers;

use common\models\entities\RealEstateCategoryRelation;
use Yii;
use common\models\entities\RealEstateCategory;
use backend\modules\category\models\SearchCategoryModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for RealEstateCategory model.
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RealEstateCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchCategoryModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RealEstateCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RealEstateCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RealEstateCategory();

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RealEstateCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RealEstateCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RealEstateCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RealEstateCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RealEstateCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionMergeCategory(){
        $params = Yii::$app->request->post();
        if(!empty($params)){
            $newName = isset($params['new_category_name'])?$params['new_category_name']:null;
            $cate1 = isset($params['category1'])?$params['category1']:null;
            $cate2 = isset($params['category2'])?$params['category2']:null;

            $cateCategory1 = RealEstateCategory::findOne(['id'=>$cate1]);
            $cateCategory2 = RealEstateCategory::findOne(['id'=>$cate2]);
            $relation2  = RealEstateCategoryRelation::findAll(['category_parent_id'=>$cate2]);

            if($relation2 && !empty($relation2)){
                foreach($relation2 as $r){
                    $r->parent_old_id = $r->category_parent_id;
                    $r->category_parent_id = $cate1;
                    $r->save();
                }
            }

            if($newName){
                $cateCategory1->name = $newName;
                $cateCategory1->save(false);
            }elseif($cateCategory1 && $cateCategory2){
                $cateCategory1->name .= ' - '.$cateCategory2->name;
                $cateCategory1->save(false);
            }
            $relation2  = RealEstateCategoryRelation::findAll(['category_parent_id'=>$cate2]);

            if(!$relation2){
                $cateCategory2->delete();
            }

            return $this->redirect(['view', 'id' => $cateCategory1->id]);
        }
        return $this->render('merge-cate');
    }
}
