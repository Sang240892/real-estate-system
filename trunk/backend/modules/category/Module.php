<?php

namespace backend\modules\category;

/**
 * category module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\category\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        \Yii::$app->language = 'vn_VN';
        // custom initialization code goes here
    }
}
