<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\entities\RealEstateCategory */
/* @var $form yii\widgets\ActiveForm */
$asset		= backend\assets\AppAsset::register($this);

$category = \common\models\entities\RealEstateCategory::find()
    ->andOnCondition(['parent'=>0])->all();

$categoryList = \yii\helpers\ArrayHelper::map($category,'id','name');
?>

<div class="real-estate-category-form">

    <div class="col-md-6">
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'parent')
            ->label(Yii::t('backend', 'Parent Category'),['class'=>'control-label'])
            ->dropDownList(
                $categoryList,
                [
                    'class'=>['form-control'],
                    'options'=>[
                        $model->parent => ['selected' => true]
                    ]
                ]
            );?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'is_display')->hiddenInput(['maxlength' => true,'value'=>1])->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>
