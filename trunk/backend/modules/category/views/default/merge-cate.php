<?php
use yii\helpers\Html;
$form = \yii\widgets\ActiveForm::begin([
    'method' => 'post',
    'action' => ['/category/default/merge-category'],
]);
$category = \common\models\entities\RealEstateCategory::find()
    ->andOnCondition(['is_display'=>1])->all();
$list = [];
if($category){
    foreach($category as $c){
        $list[] = [
            'name'=>$c->name,
            'id'=>$c->id
        ];
    }
}

$this->title = Yii::t('backend', 'Merge Category');
$this->params['breadcrumbs'][] = $this->title;

$asset		= backend\assets\AppAsset::register($this);
$jsScripts = '';
$jsScripts .= "var category = '".json_encode($list)."' ;";
$jsScripts .= <<<JS

	var Item = new RealItem()
	Item.handleShowCategory2ToMerge(category);
JS;
$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);

$this->registerJsFile(Yii::$app->homeUrl.'scripts/modules/real_estate_item.js', ['depends' =>'yii\web\YiiAsset']);
?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend','Merge'), ['class' => 'btn btn-success']) ?>
    </div>
<hr/>
   <div class="row">
       <div class="col-md-8">
           <div class="form-group">
               <label class="control-label" >Tên mơi</label>
               <input class="form-control" name="new_category_name" id="new_category_name" placeholder="Tên mới" />
           </div>
           <div class="form-group">
               <label class="control-label" >Mục 1</label>
               <select id="category1" class="form-control" name="category1">
                   <?php
                   if(!empty($category)){
                       foreach($category as $cat){
                           echo '<option value="'.$cat->id.'">'.$cat->name.'</option>';
                       }

                   }
                   ?>
               </select>

               <div class="help-block"></div>
           </div>
           <div class="form-group">
               <label class="control-label" >Mục 2</label>
               <div id="dropDownCategory2List">
                   <select id="category2" class="form-control" name="category2">
                       <?php
                       if(!empty($category)){
                           $i=0;
                           foreach($category as $cat){
                               if($i != 0){
                                   echo '<option value="'.$cat->id.'">'.$cat->name.'</option>';
                               }
                               $i++;
                           }
                       }
                       ?>
                   </select>
               </div>

           </div>

       </div>
   </div>
<?php
$form->end();
