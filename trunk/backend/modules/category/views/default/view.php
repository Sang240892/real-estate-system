<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\entities\RealEstateCategory */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="real-estate-category-view">


    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'name',
            //'description',


        ],
    ]) ?>

</div>
