<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\entities\RealEstateCategory */

$this->title = Yii::t('backend', 'Add new');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Real Estate Categories Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="real-estate-category-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
