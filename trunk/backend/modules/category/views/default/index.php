<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\category\models\SearchCategoryModel */
/* @var $dataProvider yii\data\ActiveDataProvider */
$asset		= backend\assets\AppAsset::register($this);
$this->title = Yii::t('backend', 'Real Estate Categories Management');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="real-estate-category-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Create Real Estate Category'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <hr/>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'name',
            //'description',
           // 'status',
           // 'created_at',
            // 'updated_at',
            // 'parent',
            // 'is_display',
            // 'href',
            // 'site_crawling_id',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} ',
            ],
        ],
    ]); ?>
</div>
