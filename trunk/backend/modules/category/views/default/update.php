<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\entities\RealEstateCategory */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Real Estate Category',
]) . $model->name;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Real Estate Categories'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="real-estate-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
