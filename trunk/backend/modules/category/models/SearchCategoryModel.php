<?php

namespace backend\modules\category\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\entities\RealEstateCategory;

/**
 * SearchCategoryModel represents the model behind the search form about `common\models\entities\RealEstateCategory`.
 */
class SearchCategoryModel extends RealEstateCategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at', 'parent', 'is_display', 'site_crawling_id'], 'integer'],
            [['name', 'description', 'href'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RealEstateCategory::find()->andOnCondition(['is_display'=>1]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'parent' => $this->parent,
            'is_display' => $this->is_display,
            'site_crawling_id' => $this->site_crawling_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'href', $this->href]);

        return $dataProvider;
    }
}
