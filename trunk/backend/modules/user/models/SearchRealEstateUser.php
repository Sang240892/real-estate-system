<?php

namespace backend\modules\user\models;

use common\components\repositories\EntityFactory;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\entities\RealEstateUser;

/**
 * SearchRealEstateUser represents the model behind the search form about `common\models\entities\RealEstateUser`.
 */
class SearchRealEstateUser extends RealEstateUser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'gender', 'status'], 'integer'],
            [['last_name','tracking_by' ,'first_name', 'email', 'password', 'password_reset_token', 'phone', 'role', 'address', 'role_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RealEstateUser::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        /**
         * Setup your sorting attributes
         * Note: This is setup before the $this->load($params)
         * statement below
         */
        $dataProvider->setSort([
            'defaultOrder' => [
                //'real_estate_date'=>SORT_DESC,
                'id' => SORT_DESC,
            ]
        ]);

        $query->andOnCondition(['not in','role',[EntityFactory::USER_ROOT]]);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'gender' => $this->gender,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'role', $this->role])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'role_name', $this->role_name]);

        return $dataProvider;
    }
}
