<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\payment\models\SearchPaymentHistoryModel */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('backend', 'Lịch sử thanh toán');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-history-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Đăng ký sử dụng tin chính chủ'), ['create-history','user_id'=>$user->id], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'start_date',
                'value'=> function ($model) {
                    return Yii::$app->formatter->asDate($model->start_date,'Y/MM/d');
                },
            ],
            [
                'attribute'=>'end_date',
                'value'=> function ($model) {
                    return Yii::$app->formatter->asDate($model->end_date,'Y/MM/d');
                },
            ],
            'amount',
            [
                'attribute'=>'created_at',
                'value'=> function ($model) {
                    return Yii::$app->formatter->asDate($model->created_at,'Y/MM/d');
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
