<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\entities\PaymentHistory */
/* @var $form yii\widgets\ActiveForm */
$asset		= backend\assets\AppAsset::register($this);

$asset->css[] = 'theme/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css';
$asset->css[] = 'theme/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css';
$asset->css[] = 'theme/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css';
$asset->css[] = 'theme/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css';

$asset->js[] = 'theme/assets/global/plugins/moment.min.js';


$asset->js[] = 'theme/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js';
$asset->js[] = 'theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
$asset->js[] = 'theme/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js';
$asset->js[] = 'theme/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js';

$this->registerJsFile(Yii::$app->homeUrl.'scripts/helpers/helpers.js', ['depends' =>'yii\web\YiiAsset']);

?>

<div class="payment-history-form">
    <div class="col-md-12">
        <div class="col-md-4">
            <div class="portlet light ">
                <div>
                    <div class="margin-top-20 profile-desc-link">
                        <i class="fa fa-envelope-o"></i>
                       <a href="mailto:<?=$user->email?>"><?=$user->email?></a>
                    </div>
                    <div class="margin-top-20 profile-desc-link">
                        <i class="fa fa-user"></i>
                        <?= $user->first_name .' '.$user->last_name?>
                    </div>
                    <div class="margin-top-20 profile-desc-link">
                        <i class="fa fa-mobile"></i>
                        <?=$user->phone?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'user_id')->textInput(['type'=>'hidden','disable'=>true])->label(false); ?>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group field-paymenthistory-time required" id="datepicker">
                            <label class="control-label col-md-3" for="paymenthistory-amount">Thời hạn</label>
                            <div class="col-md-9 input-group">
                                <div class="input-daterange input-group" id="datepicker">
                                    <?= $form->field($model, 'start_date',['template'=>"{label}\n<div class='col-md-10'>{input}\n{hint}</div>"])
                                        ->label(false)
                                        ->textInput(['type' => 'text','class'=>'input-sm form-control' ,'placeholder'=>Yii::t('backend', 'Ngày bắt đầu')]) ?>

                                    <span class="input-group" style="top: -9px;left: -13px;display: table-cell;">đến</span>
                                    <?= $form->field($model, 'end_date',['template'=>"{label}\n<div class='col-md-10'>{input}\n{hint}</div>"])
                                        ->label(false)
                                        ->textInput(['type' => 'text','class'=>'input-sm form-control','placeholder'=>Yii::t('backend', 'Ngày hết hạn')]) ?>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-7">
                        <?= $form->field($model, 'amount',['template'=>"{label}\n<div class='col-md-9'>{input}\n{hint}\n{error}</div>"])
                            ->label(Yii::t('backend', 'Lệ phí'),['class'=>'control-label col-md-3'])
                            ->textInput(['type' => 'number','placeholder'=>Yii::t('backend', 'Lệ phí')]) ?>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Đồng ý') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
