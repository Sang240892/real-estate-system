<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\entities\RealEstateUser */

$this->title = Yii::t('backend', 'Create Real Estate User');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="real-estate-user-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
