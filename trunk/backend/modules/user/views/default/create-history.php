<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $model common\models\entities\PaymentHistory */

$this->title = Yii::t('backend', 'Tạo lịch sử thanh toán mới');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-history-create">

    <?= $this->render('_form_history', [
        'model' => $model,
        'user'=>$user
    ]) ?>
  <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'start_date',
                'value'=> function ($model) {
                    return Yii::$app->formatter->asDate($model->start_date,'Y/MM/d');
                },
            ],
            [
                'attribute'=>'end_date',
                'value'=> function ($model) {
                    return Yii::$app->formatter->asDate($model->end_date,'Y/MM/d');
                },
            ],
            'amount',
            [
                'attribute'=>'created_at',
                'value'=> function ($model) {
                    return Yii::$app->formatter->asDate($model->created_at,'Y/MM/d');
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update'=>function($url,$model) use ($user) {
                        $icon = '<span style="" class="glyphicon glyphicon-pencil"></span>';
                        $url = \yii\helpers\Url::to(['create-history','user_id'=>$user->id,'h_id'=>$model->id]);
                        return Html::a($icon,$url,['title'=>Yii::t('backend','Sửa')]);
                    },
                    'delete'=>function($url,$model)use ($user){
                        $icon = '<span style="" class="glyphicon glyphicon-trash"></span>';
                        $url = \yii\helpers\Url::to(['delete-history','user_id'=>$user->id,'h_id'=>$model->id]);
                        return Html::a($icon,$url,['title'=>Yii::t('backend','Xóa')]);
                    }
                ],
            ],
        ],
    ]); ?>

</div>
