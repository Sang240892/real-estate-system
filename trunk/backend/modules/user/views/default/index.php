<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\user\models\SearchRealEstateUser */
/* @var $dataProvider yii\data\ActiveDataProvider */
$asset		= backend\assets\AppAsset::register($this);
$this->title = Yii::t('backend', 'Real Estate Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="real-estate-user-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Create Real Estate User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php
    echo \nterms\pagesize\PageSize::widget([
        'label' => false,
        'template'=> Yii::t('backend','View {list} record(s) | Found total {totalCount} record(s)',[
            'totalCount'=>$dataProvider->getTotalCount()
        ]),
        'options'=> ['class'=>'form-control input-sm input-xsmall input-inline','id'=>'perpage']
    ]);
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'filterSelector' => 'select[name="per-page"]',
        'layout'=>"<div class='table-scrollable'>{items}</div>\n<div class='row'>
                                            <div class='col-md-5 col-sm-12'>
                                            <div class='dataTables_info'>{summary}</div></div>
                                            \n<div class='col-md-7 col-sm-12'>
                                            <div class='pull-right dataTables_paginate paging_bootstrap_full_number'>
                                            {pager}</div></div></div>",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'last_name',
            'first_name',
            'email:email',
            //'password',
            // 'password_reset_token',
             'phone',
            'tracking_by',
            'note',
            // 'role',

             'address',
            // 'role_name',
            [
                'attribute'=>'role',
                'filter'=>FALSE,
                'value'=> function ($model) {
                    if($model->role == \common\components\repositories\EntityFactory::USER_MANAGER){
                        return 'Nhân viên';
                    };
                    if($model->role == \common\components\repositories\EntityFactory::USER_ADMIN){
                        return 'Quản trị web site';
                    };
                    if($model->role == \common\components\repositories\EntityFactory::USER_STAFF){
                        return 'Thành viên cao cấp';
                    };
                    if($model->role == \common\components\repositories\EntityFactory::USER_CUSTOMER){
                        return 'Thành viên bình thường';
                    };
                },
            ],
            [
                'attribute'=>'created_at',
                'filter'=>FALSE,
                'value'=> function ($model) {
                    return ($model->created_at)?\Yii::$app->formatter->asDate($model->created_at,'php:D d M Y'):null;
                },
            ],
            // 'updated_at',
            // 'gender',
            // 'status',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {paymentHistory} ',
                'buttons' => [
                    'paymentHistory'=>function($url,$model){
                        $icon = '<span style="" class="fa fa-calendar"></span>';
                        $url = \yii\helpers\Url::to(['create-history','user_id'=>$model->id]);
                        return Html::a($icon,$url,['title'=>Yii::t('backend','Lịch sử thanh toán')]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
