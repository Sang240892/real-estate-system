<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model common\models\entities\RealEstateUser */
/* @var $form yii\widgets\ActiveForm */
$asset		= backend\assets\AppAsset::register($this);
$roles  = \backend\modules\authorization\models\entity\AuthItem::find()
    ->andOnCondition(['type'=>1])
    ->andOnCondition(['not in','name',['sysadmin','admin']])->all();


$arrRoleManage = \yii\helpers\ArrayHelper::map($roles,'role_id','description');
?>

<div class="real-estate-user-form">
    <div class="row">
        <div class="col-md-10">
            <?php $form = ActiveForm::begin(); ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
            <hr/>
            <div class="tabbable-line">
                <ul class="nav nav-tabs ">
                    <li class="active">
                        <a href="#tab_15_1" data-toggle="tab" aria-expanded="true"><?=Yii::t('backend/app','Thông tin cơ bản')?></a>
                    </li>
                    <li class="">
                        <a href="#tab_15_2" data-toggle="tab" aria-expanded="false"> <?=Yii::t('backend/app','Mật khẩu')?></a>
                    </li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_15_1">
                        <div class="col-md-8">
                            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'tracking_by')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'note')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'role')
                                ->label(Yii::t('backend', 'Role'),['class'=>'control-label'])
                                ->dropDownList(
                                    $arrRoleManage,
                                    [
                                        'class'=>['form-control'],
                                        'options'=>[
                                            $model->role => ['selected' => true]
                                        ]
                                    ]
                                );?>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_15_2">
                        <div class="col-md-8">
                            <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'password_repeat')->passwordInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>

                </div>

        </div>
    </div>
</div>
