<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\entities\RealEstateUser */

$this->title = Yii::t('backend', 'Update User:') .' '. $model->first_name .' '.$model->last_name;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Real Estate Users'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="real-estate-user-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
