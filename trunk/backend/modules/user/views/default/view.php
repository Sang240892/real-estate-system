<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\entities\RealEstateUser */

$this->title = $model->first_name .' '.$model->last_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Real Estate Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="real-estate-user-view">
    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'id',
            'last_name',
            'first_name',
            'email:email',
           // 'password',
           // 'password_reset_token',
            'phone',
            //'role',
            'address',
            'tracking_by',
            [                      // the owner name of the model
                'label' => Yii::t('backend','Created At'),
                'value' => Yii::$app->formatter->asDate($model->created_at,'php:D d M Y'),
            ]
           // 'updated_at',
           // 'gender',
           // 'status',
        ],
    ]) ?>

</div>
