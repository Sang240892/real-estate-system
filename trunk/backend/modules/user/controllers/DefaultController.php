<?php

namespace backend\modules\user\controllers;

use backend\commons\forms\SignUpForm;
use backend\modules\auth\controllers\AuthenticateController;
use backend\modules\payment\models\SearchPaymentHistoryModel;
use common\models\entities\PaymentHistory;
use common\models\UserIdentity;
use Yii;
use common\models\entities\RealEstateUser;
use backend\modules\user\models\SearchRealEstateUser;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for RealEstateUser model.
 */
class DefaultController extends AuthenticateController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RealEstateUser models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchRealEstateUser();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RealEstateUser model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RealEstateUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SignUpForm(['scenario' => SignUpForm::SCENARIO_REGISTER]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $pw  = Yii::$app->security->generatePasswordHash(Yii::$app->request->post()['SignUpForm']['password']);
            $model->password = $pw;
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RealEstateUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
       // $historySearchModel = new SearchPaymentHistoryModel();


        $model = new SignUpForm(['scenario' => SignUpForm::SCENARIO_UPDATE]);
        $model = $model->findOne(['id'=>$id]);
        $oldPw = $model->password;
        if(!$model){
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        //$historyDataProvider = $historySearchModel->search(Yii::$app->request->queryParams,$id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(isset(Yii::$app->request->post()['SignUpForm']['password']) && !empty(Yii::$app->request->post()['SignUpForm']['password'])){
                $pw  = Yii::$app->security->generatePasswordHash(Yii::$app->request->post()['SignUpForm']['password']);
                $model->password = $pw;
            }else{
                $model->password = $oldPw;
            }

            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {

            $model->password = null;
            return $this->render('update', [
                'model' => $model,
//                'historySearchModel' => $historySearchModel,
//                'historyDataProvider' => $historyDataProvider,
            ]);
        }
    }

    /**
     * Deletes an existing RealEstateUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    /**
     * Deletes an existing PaymentHistory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteHistory($user_id)
    {
        $h_id = Yii::$app->request->get('h_id');

        $user = $this->findModel($user_id);

        $this->findModelPaymentHistory($h_id)->delete();

        return $this->redirect(['create-history','user_id'=>$user_id]);
    }
    public function actionCreateHistory($user_id)
    {
        $model = new PaymentHistory();

        $h_id = Yii::$app->request->get('h_id');

        $user = $this->findModel($user_id);

        if($h_id != 0){
            $model = $this->findModelPaymentHistory($h_id);
        }else{
            $model->start_date = isset(Yii::$app->request->post()['PaymentHistory']['start_date'])
                ?Yii::$app->request->post()['PaymentHistory']['start_date']:null;
            $model->end_date = isset(Yii::$app->request->post()['PaymentHistory']['end_date'])
                ?Yii::$app->request->post()['PaymentHistory']['end_date']:null;
        }

        $model->user_id =   $user->id;

        $searchModel = new SearchPaymentHistoryModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$user_id);

        //die(var_dump(Yii::$app->request->post()['PaymentHistory']));
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['create-history', 'user_id' => $user->id]);
        } else {
            return $this->render('create-history', [
                'model' => $model,
                'user'=>$user,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Finds the PaymentHistory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PaymentHistory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelPaymentHistory($id)
    {
        if (($model = PaymentHistory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionPaymentHistory($user_id){
        $user = $this->findModel($user_id);

        $searchModel = new SearchPaymentHistoryModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$user_id);

        return $this->render('payment-history', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user'=>$user
        ]);
    }
    /**
     * Finds the RealEstateUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RealEstateUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RealEstateUser::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
