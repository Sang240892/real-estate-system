<?php
$asset		= \frontend\assets\AppAsset::register($this);

$asset->css[] = 'theme/global/plugins/fancybox/source/jquery.fancybox.css';
$asset->css[] = 'theme/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css';
$asset->css[] = 'theme/global/plugins/slider-revolution-slider/rs-plugin/css/settings.css';
$asset->css[] = 'theme/frontend/pages/css/style-revolution-slider.css';
$asset->css[] = 'theme/frontend/layout/css/style-responsive.css';

$asset->js[] = "theme/frontend/layout/scripts/layout.js";
$asset->js[] = 'theme/global/plugins/fancybox/source/jquery.fancybox.pack.js';
$asset->js[] = 'theme/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js';
$asset->js[] = 'theme/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js';
$asset->js[] = 'theme/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js';
$asset->js[] = 'theme/frontend/pages/scripts/revo-slider-init.js';
$asset->js[] = 'theme/frontend/layout/scripts/layout.js';

$jsScripts ='
Layout.init();
Layout.initOWL();
RevosliderInit.initRevoSlider();

	';

$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);
$parents = \common\models\entities\RealEstateCategory::findAll(['parent'=>0]);

$this->title = 'Hướng Dẫn';
?>
<div class="main">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="<?=Yii::$app->urlManager->createUrl('/')?>">Trang Chủ</a></li>
            <li class="active">Hương Dẫn</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
            <div class="col-md-12">
                <div class="row margin-bottom-40">
                    <!-- BEGIN CONTENT -->
                    <div class="row front-steps-wrapper front-steps-count-3">
                        <div class="col-md-4 col-sm-4 front-step-col">
                            <div class="front-step front-step1" style="height: 137px;">
                                <h2>Tải app BDS Smart về máy.</h2>
                                <p>Cách 1 : vào appstore tìm BDS Smart và tải về.</p>
                                <p>Cách 2 vào link sau tải về : https://itunes.apple.com/us/app/bds-smart-thong-tin-bat-ong/id1152225435?mt=8</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 front-step-col">
                            <div class="front-step front-step2" style="height: 137px;">
                                <h2>Đăng ký tài khoản với đầy đủ các thông tin sau.</h2>
                                <p>Đầy đủ tên, Số điện thoại, Email, Khu vực hoạt động,</p>
                                <p>Công ty hoặc lĩnh vực hoạt động ( chung cư hay thổ cư, cho thuê hay bán )</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 front-step-col">
                            <div class="front-step front-step3" style="height: 137px;">
                                <h2>Bạn có thể đăng ký tài khoản.</h2>
                                <p>Ngay trên app BDS Smart.</p>
                                <p>Trên website www.bdssmart.com</p>
                                <p>Hoặc liên hệ hotline và nhận hướng dẫn.</p>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="margin-bottom-40"></div>
            <div class="col-md-12">
                <div class="content-page">
                    <div class="row margin-bottom-30">
                        <!-- BEGIN INFO BLOCK -->
                        <div class="col-md-12">
                            <h1>Video Hướng Dẫn</h1>
                            <div class="col-md-6">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/9Jjn7SS7xIE" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <div class="col-md-6">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/m6VrPj0A4hQ" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <!-- BEGIN LISTS -->
                            <!-- END LISTS -->

                        </div>
                        <!-- END INFO BLOCK -->

                        <!-- BEGIN CAROUSEL -->

                        <!-- END CAROUSEL -->
                    </div>
                </div>
            </div>

            <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
    </div>
</div>
