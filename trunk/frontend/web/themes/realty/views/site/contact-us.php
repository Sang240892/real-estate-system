<?php
/**
 * User: sangnguyen on  5/18/16 at 20:34
 * File name: contact-us.php
 * Project name: Fit-Road
 * Copyright (c) 2016 by MyCompany
 * All rights reserved
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
//$this->registerJsFile('http://maps.google.com/maps/api/js?sensor=true',[ 'depends' => 'yii\web\YiiAsset']);
$asset		= \frontend\assets\AppAsset::register($this);

$asset->css[] = 'theme/global/plugins/fancybox/source/jquery.fancybox.css';
$asset->css[] = 'theme/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css';
$asset->css[] = 'theme/global/plugins/slider-revolution-slider/rs-plugin/css/settings.css';
$asset->css[] = 'theme/frontend/pages/css/style-revolution-slider.css';
$asset->css[] = 'theme/frontend/layout/css/style-responsive.css';

$asset->js[] = "theme/frontend/layout/scripts/layout.js";
$asset->js[] = 'theme/global/plugins/fancybox/source/jquery.fancybox.pack.js';
$asset->js[] = 'theme/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js';
$asset->js[] = 'theme/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js';
$asset->js[] = 'theme/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js';
$asset->js[] = 'theme/frontend/pages/scripts/revo-slider-init.js';
$asset->js[] = 'theme/frontend/layout/scripts/layout.js';

$jsScripts ='
Layout.init();
Layout.initOWL();
RevosliderInit.initRevoSlider();

	';

$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);
$parents = \common\models\entities\RealEstateCategory::findAll(['parent'=>0]);

$this->title="Liên Hệ";
?>
<div class="main" style="min-height: 563px">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="<?=Yii::$app->urlManager->createUrl('/')?>">Trang Chủ</a></li>
            <li class="active">Bảng Giá</li>
        </ul>

        <div class="col-md-6 col-sm-6">
            <h2>Bộ phận Hỗ trợ khách hàng:</h2>
            <p>Hỗ trợ kỹ thuật: 0984 121 764</p>
            <p>Phòng kinh doanh miền Bắc: Mr. Sáng (+84) 1252 389 893</p>
            <p>Phòng kinh doanh miền Nam: Mr Dũng (+84) 987 627 944</p>
            <p>Hotline: 0904697566</p>
        </div>

        <div class="col-md-6 col-sm-6 sidebar2" >
            <h2>Địa Chỉ Liên Hệ</h2>
            <address>
                <strong>Trụ sở chính tại Hà Nội.</strong><br>
                Tòa nhà CT4 Khu đô thị Mỹ Đình 2, Phường Mỹ Đình 2, Quận Nam Từ Liêm, TP Hà Nội.
                <br/>
                <abbr title="Phone">P:</abbr> (+84) 904697566
            </address>
            <address>
                <strong>Email</strong><br>
                <a href="mailto:appbdssmart@gmail.com">appbdssmart@gmail.com</a>
            </address>

        </div>
    </div>
</div>
