<?php
$asset		= \frontend\assets\AppAsset::register($this);

$asset->css[] = 'theme/global/plugins/fancybox/source/jquery.fancybox.css';
$asset->css[] = 'theme/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css';
$asset->css[] = 'theme/global/plugins/slider-revolution-slider/rs-plugin/css/settings.css';
$asset->css[] = 'theme/frontend/pages/css/style-revolution-slider.css';
$asset->css[] = 'theme/frontend/layout/css/style-responsive.css';

$asset->js[] = "theme/frontend/layout/scripts/layout.js";
$asset->js[] = 'theme/global/plugins/fancybox/source/jquery.fancybox.pack.js';
$asset->js[] = 'theme/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js';
$asset->js[] = 'theme/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js';
$asset->js[] = 'theme/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js';
$asset->js[] = 'theme/frontend/pages/scripts/revo-slider-init.js';
$asset->js[] = 'theme/frontend/layout/scripts/layout.js';

$jsScripts ='
Layout.init();
Layout.initOWL();
RevosliderInit.initRevoSlider();

	';

$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);
$parents = \common\models\entities\RealEstateCategory::findAll(['parent'=>0]);


$this->title = 'Giới Thiệu';
?>
<div class="main">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="<?=Yii::$app->urlManager->createUrl('/')?>">Trang Chủ</a></li>
            <li class="active">Giới Thiệu</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
            <!-- BEGIN CONTENT -->
            <div class="col-md-12 col-sm-12">
                <h1>BDSSmart sẽ giúp bạn.</h1>
                <div class="content-page">
                    <div class="row margin-bottom-30">
                        <!-- BEGIN INFO BLOCK -->
                        <div class="col-md-12">
                            <h2 class="no-top-space"></h2>
                            <p>Bạn là chuyên viên tư vấn, nhà môi giới giàu kinh nghiệm trong lĩnh vực BẤT ĐỘNG SẢN? hay một “lính mới”  trong nghề thì bạn vẫn đang mất nhiều thời gian trong việc tìm kiếm nguồn thông tin BẤT ĐỘNG SẢN chính chủ, mất rất nhiều thời gian để ghi chép lại danh sách khách hàng nguồn hàng cũng như không có gì để hỗ trợ bạn khi đàm phán với khách hàng.</p>
                                <p>Chúng tôi xây dựng web và app BDS Smart giúp bạn tiết kiệm thời gian, giảm chi phí, nâng cao hiệu quả trong việc tìm kiếm nguồn hàng cũng như chuyên nghiệp hơn khi đi đàm phán với chủ nhà và khách hàng.</p>
                               <p> BDSSmart là đơn vị công nghệ đầu tiên đưa APP Bất Động Sản vào thị trường, hướng tới con đường đưa công nghệ vào đồng hàng cùng môi giới Bất Động sản. Tạo được sự thuận tiện và phong cách chuyên nghiệp cho từng môi giới.</p>
                               <p> BDSSmart là nơi cung cấp nguồn hàng ký gửi cậy nhất cho môi giới của các khu vực trên  đất nước Việt Nam.</p>
                                <p>BDSSmart được tích hợp cả dạng web và app giúp cho môi giới bất động sản có thể xem nguồn hàng mọi lúc mọi nơi kể cả khi đang làm việc, đang ngồi quán café cũng như đang đi tiếp khách</p>
                                <p>BDSSmart luôn đồng hàng cùng bạn từ khi bạn mới chân bước chân ráo vào thị trường Bất Động Sản, luôn đi cùng bạn qua những vụ giao dịch này đến giao dịch khác để bạn không phải rời khỏi thị trường với sự nối tiếc.</p>
                                <br/>
                            <p>Chúc các bạn thành công trên con đường MÔI GIỚI BẤT ĐỘNG SẢN</p>

                            </p>
                            <!-- BEGIN LISTS -->

                            <!-- END LISTS -->
                        </div>
                        <!-- END INFO BLOCK -->

                        <!-- BEGIN CAROUSEL -->

                        <!-- END CAROUSEL -->
                    </div>
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
    </div>
</div>
