<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */


$asset	= frontend\assets\AppAsset::register($this);
//$this->registerJsFile("https://www.google.com/recaptcha/api.js",['position' => \yii\web\View::POS_HEAD]);

$this->title = 'Đăng Ký Nhanh';
?>
<div class="main" style="min-height: 565px">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="<?=Yii::$app->urlManager->createUrl('/')?>">Trang Chủ</a></li>
            <li class="active">Đăng Ký Nhanh</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
            <!-- BEGIN SIDEBAR -->
            <div class="sidebar col-md-3 col-sm-3">
                <ul class="list-group margin-bottom-25 sidebar-menu">
                    <li class="list-group-item clearfix"><a href="<?=Yii::$app->urlManager->createUrl('/users/signin')?>"><i class="fa fa-angle-right"></i>Đăng Nhập</a></li>
                </ul>
            </div>
            <!-- END SIDEBAR -->

            <!-- BEGIN CONTENT -->
            <div class="col-md-9 col-sm-9">
                <h1>Thông Tin Đăng Ký</h1>
                <?php $form = \yii\bootstrap\ActiveForm::begin([
                    'layout' => 'horizontal',
                    'successCssClass'=>'has-success has-feedback',
                    'errorCssClass' => 'has-error has-feedback',
                    'fieldConfig'=>[
                    ]

                ]);?>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <?= $form->field($model, 'first_name',['template'=>"{label}\n<div class='col-md-8'>{input}\n{hint}\n{error}</div>"])
                                    ->label(Yii::t('backend', 'Thông Tin'),['class'=>'control-label col-md-4'])
                                    ->textInput(['maxlength' => true,'placeholder'=>Yii::t('backend', 'Họ')]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'last_name',['template'=>"<div class='col-md-10'>{input}\n{hint}\n{error}</div>"])
                                    ->textInput(['maxlength' => true,'placeholder'=>Yii::t('backend', 'Tên')]) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?= $form->field($model, 'phone',[
                    'template'=>"{label}\n<div class='col-md-9'>{input}\n{hint}\n{error}</div>"
                ]) ->label(Yii::t('backend', 'Số Điện Thoại'),['class'=>'control-label col-md-2'])
                    ->textInput(['type'=>'number','maxlength' => true,'placeholder'=>Yii::t('backend', 'Số Điện Thoại')]) ?>

                <?= $form->field($model, 'email',['template'=>"{label}\n<div class='col-md-9'>{input}\n{hint}\n{error}</div>"])
                    ->label(Yii::t('backend', 'Email'),['class'=>'control-label col-md-2'])
                    ->textInput(['maxlength' => true,'placeholder'=>Yii::t('backend', 'Email')]) ?>

                <?= $form->field($model, 'areaWorking',[
                    'template'=>"{label}\n<div class='col-md-9'>{input}\n{hint}\n{error}</div>"
                ]) ->label(Yii::t('backend', 'Khu vưc'),['class'=>'control-label col-md-2'])
                    ->textInput(['type'=>'text','maxlength' => true,'placeholder'=>Yii::t('backend', 'Khu vực')]) ?>


                <?= $form->field($model, 'password',['template'=>"{label}\n<div class='col-md-9'>{input}\n{hint}\n{error}</div>"])
                    ->label(Yii::t('backend', 'Mật Khẩu'),['class'=>'control-label col-md-2'])
                    ->passwordInput(['placeholder'=>Yii::t('backend', 'Mật Khẩu'),'maxlength' => true])?>

                <?= $form->field($model, 'password_repeat',['template'=>"{label}\n<div class='col-md-9'>{input}\n{hint}\n{error}</div>"])
                    ->label(Yii::t('backend', 'Xác Nhận Mật Khẩu'),['class'=>'control-label col-md-2'])
                    ->passwordInput(['placeholder'=>Yii::t('backend', 'Xác Nhận Mật Khẩu'),'maxlength' => true])?>
                <?= \himiklab\yii2\recaptcha\ReCaptcha::widget([
                    'name' => 'reCaptcha',
                    'siteKey' => Yii::$app->params['GG_RECAPTCHA_KEY'],
                    'widgetOptions' => ['class' => 'col-sm-offset-2']
                ]) ?>
                <div class="margin-top-10"></div>
                <div class="form-group">
                        <div class="col-md-4" style="margin-left: 140px">
                            <?= Html::submitButton(
                                '<i class="fa fa-check"></i> '.Yii::t('backend', 'Đăng ký'),
                                ['class' =>'btn btn-primary']) ?>
                            </div>
                      </div>


                    <?php ActiveForm::end(); ?>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
    </div>
</div>
</div>
