<?php
$asset		= \frontend\assets\AppAsset::register($this);

$asset->css[] = 'theme/global/plugins/fancybox/source/jquery.fancybox.css';
$asset->css[] = 'theme/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css';
$asset->css[] = 'theme/global/plugins/slider-revolution-slider/rs-plugin/css/settings.css';
$asset->css[] = 'theme/frontend/pages/css/style-revolution-slider.css';
$asset->css[] = 'theme/frontend/layout/css/style-responsive.css';

$asset->js[] = "theme/frontend/layout/scripts/layout.js";
$asset->js[] = 'theme/global/plugins/fancybox/source/jquery.fancybox.pack.js';
$asset->js[] = 'theme/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js';
$asset->js[] = 'theme/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js';
$asset->js[] = 'theme/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js';
$asset->js[] = 'theme/frontend/pages/scripts/revo-slider-init.js';
$asset->js[] = 'theme/frontend/layout/scripts/layout.js';

$jsScripts ='
Layout.init();
Layout.initOWL();
RevosliderInit.initRevoSlider();

	';

$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);
$parents = \common\models\entities\RealEstateCategory::findAll(['parent'=>0]);


$this->title = 'Giá Sử Dụng Sản Phẩm';
?>
<div class="main" style="min-height: 563px">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="<?=Yii::$app->urlManager->createUrl('/')?>">Trang Chủ</a></li>
            <li class="active">Bảng Giá</li>
        </ul>
        <!-- BEGIN CONTENT -->
        <div class="row margin-bottom-40">
            <!-- BEGIN CONTENT -->
            <div class="col-md-12 col-sm-12">
                <h3>Hệ thống đang cập nhật.</h3>
            </div>
        </div>
        <!-- END CONTENT -->
    </div>
</div>
