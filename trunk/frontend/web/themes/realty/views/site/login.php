<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */


$asset	= frontend\assets\AppAsset::register($this);

$this->title = 'Đăng Nhập';
?>
<div class="main" style="min-height: 565px">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="<?=Yii::$app->urlManager->createUrl('/')?>">Trang Chủ</a></li>
            <li class="active">Đăng Nhập</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
            <!-- BEGIN SIDEBAR -->
            <div class="sidebar col-md-3 col-sm-3">
                <ul class="list-group margin-bottom-25 sidebar-menu">
                    <li class="list-group-item clearfix"><a href="<?=Yii::$app->urlManager->createUrl('/users/signup')?>"><i class="fa fa-angle-right"></i>Đăng Ký Nhanh </a></li>

                </ul>
            </div>
            <!-- END SIDEBAR -->

            <!-- BEGIN CONTENT -->
            <div class="col-md-9 col-sm-9">
                <h1>Đăng Nhập</h1>
                <div class="content-form-page">
                    <div class="row">
                        <div class="col-md-7 col-sm-7">
                            <?php $form = \yii\bootstrap\ActiveForm::begin([
                                'layout' => 'horizontal',
                                'successCssClass'=>'has-success has-feedback',
                                'errorCssClass' => 'has-error has-feedback',
                                'fieldConfig'=>[
                                ]

                            ]);?>
                            <?= $form->field($model, 'username',['template'=>"{label}\n<div class='col-md-8'>{input}\n{hint}\n{error}</div>"])
                                ->label(Yii::t('backend', 'Tên Đăng Nhập'),['class'=>'control-label col-md-4'])
                                ->textInput(['maxlength' => true,'placeholder'=>Yii::t('backend', 'Tên Đăng Nhập')]) ?>

                            <?= $form->field($model, 'password',['template'=>"{label}\n<div class='col-md-8'>{input}\n{hint}\n{error}</div>"])
                                ->label(Yii::t('backend', 'Mật Khẩu'),['class'=>'control-label col-md-4'])
                                ->passwordInput(['placeholder'=>Yii::t('backend', 'Mật Khẩu'),'maxlength' => true])?>

                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2"> <?= $form->field($model, 'rememberMe')->checkbox() ?></div>
                                    <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
                                        <button type="submit" class="btn btn-primary">Đăng Nhập</button>
                                    </div>
                                </div>

                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
    </div>
</div>
