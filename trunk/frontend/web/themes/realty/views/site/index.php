<?php
/**
 * User: sangnguyen on  5/10/16 at 23:15
 * File name: index.php
 * Project name: Fit-Road
 * Copyright (c) 2016 by MyCompany
 * All rights reserved
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
$asset		= \frontend\assets\AppAsset::register($this);

$asset->css[] = 'theme/global/plugins/fancybox/source/jquery.fancybox.css';
$asset->css[] = 'theme/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css';
$asset->css[] = 'theme/global/plugins/slider-revolution-slider/rs-plugin/css/settings.css';
$asset->css[] = 'theme/frontend/pages/css/style-revolution-slider.css';

$asset->css[] = 'theme/frontend/layout/css/style-responsive.css';

$asset->js[] = "theme/frontend/layout/scripts/layout.js";
$asset->js[] = 'theme/global/plugins/fancybox/source/jquery.fancybox.pack.js';
$asset->js[] = 'theme/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js';
$asset->js[] = 'theme/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js';
$asset->js[] = 'theme/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js';
//$asset->js[] = 'theme/frontend/pages/scripts/revo-slider-init.js';

$asset->js[] = 'theme/frontend/layout/scripts/layout.js';

$this->registerJsFile(Yii::$app->homeUrl.'scripts/modules/revo-slider-init.js', ['depends' =>'yii\web\YiiAsset']);

$this->title = 'Trang Chủ';

$jsScripts ='
Layout.init();
Layout.initOWL();
  RevosliderInit.initRevoSlider();

	';

$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);
$parents = \common\models\entities\RealEstateCategory::findAll(['parent'=>0]);

?>
<div class="page-slider margin-bottom-40">
    <div class="fullwidthbanner-container revolution-slider">
        <div class="fullwidthabnner">
            <ul id="revolutionul">
                <!-- THE NEW SLIDE -->
                <li data-bgfit="contain" data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="../../assets/frontend/pages/img/revolutionslider/thumbs/thumb2.jpg">
                    <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                    <img data-bgfit="contain" src="http://static.bdssmart.com/assets/images/page/frontend/bg1.jpg" alt="">
                </li>
                <li data-bgfit="contain" data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="../../assets/frontend/pages/img/revolutionslider/thumbs/thumb2.jpg">
                    <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                    <img data-bgfit="contain" src="http://static.bdssmart.com/assets/images/page/frontend/bg2.jpg" alt="">
                </li>
                <li data-bgfit="contain" data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="../../assets/frontend/pages/img/revolutionslider/thumbs/thumb2.jpg">
                    <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                    <img data-bgfit="contain" src="http://static.bdssmart.com/assets/images/page/frontend/bg3.jpg" alt="">
                </li>
                <li data-bgfit="contain" data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="../../assets/frontend/pages/img/revolutionslider/thumbs/thumb2.jpg">
                    <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                    <img data-bgfit="contain" src="http://static.bdssmart.com/assets/images/page/frontend/bg4.jpg" alt="">
                </li>
                <!-- THE FIRST SLIDE -->

            </ul>
            <div class="tp-bannertimer tp-bottom"></div>
        </div>
    </div>
</div>
<!---->
<div class="main">
    <div class="container">
        <!-- BEGIN SERVICE BOX -->
        <div class="row service-box margin-bottom-40">
            <div class="col-md-4 col-sm-4">
                <div class="service-box-heading">
                    <em><i class="fa fa-location-arrow blue"></i></em>
                    <span>Thông tin bất động sản chính chủ</span>
                </div>
                <p>Chúng tôi tự hào đơn vị tổng hợp rất nhiều nguồn thông tin bất động sản từ người chủ nhà muốn ký gửi thông qua các bạn môi giới. Thông tin trên bdssmart luôn đảm bảo chính xác nhanh nhất giúp các bạn môi giới lựa chọn nguồn hàng đẹp nhất.
                </p>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="service-box-heading">
                    <em><i class="fa fa-check red"></i></em>
                    <span>Quản lý nguồn hàng</span>
                </div>
                <p>Tiếp đến chúng tôi cung cấp cho bạn tính năng quản lý nguồn hàng giúp bạn quản lý tốt nguồn hàng và lưu trữ thông tin thông số ngôi nhà, hoa hồng, hình ảnh, định vị vị trí bản đồ…… ngay trên trên chiếc điện thoại thông minh mà bạn đang dùng hàng ngày.
                </p>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="service-box-heading">
                    <em><i class="fa fa-compress green"></i></em>
                    <span>Quản lý khách hàng</span>
                </div>
                <p>
                    Giống như 1 cuốn sổ tay APP BDSSMART giúp bạn quản lý khoa học chi tiết thông tin khách hàng rất dễ sử dụng thuận tiện hỗ trợ bạn rất nhiều trong quá trình bán hàng. Bạn không phải băn khoăn vấn đề lưu trữ mất hoặc quên thông tin khách hàng.
                </p>
            </div>
        </div>
        <!-- END SERVICE BOX -->

        <!-- BEGIN BLOCKQUOTE BLOCK -->
        <!-- BEGIN CLIENTS -->

        <!-- END CLIENTS -->
    </div>
</div>
