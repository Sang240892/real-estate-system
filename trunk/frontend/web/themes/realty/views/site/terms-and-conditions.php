<?php
/**
 * User: sangnguyen on  5/10/16 at 23:55
 * File name: terms-and-conditions.php
 * Project name: Fit-Road
 * Copyright (c) 2016 by MyCompany
 * All rights reserved
 */
$asset		= \frontend\assets\AppAsset::register($this);
$jsScripts = <<<JS
   $('html, body').stop().animate( { scrollTop: 600 }, 1500, 'easeInOutExpo' );
JS;
$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);
?>
<div class="container">
    <div class="col-md-12 wow fadeInRight animated">
        <div class="device-text-bottom"> <p><?= $model->content?></p></div>
    </div>

</div>
