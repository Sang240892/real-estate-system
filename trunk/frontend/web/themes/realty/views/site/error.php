<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
$asset		= \frontend\assets\AppAsset::register($this);

$this->title = Yii::t('frontend',$name);

//$asset->css[] = 'theme/assets/admin/pages/css/error.css';
//$asset->js[] = 'theme/assets/admin/layout/scripts/demo.js';

$baseUrl 	= $asset->baseUrl;
$dashboard  = (new \yii\web\Request())->getBaseUrl();
$jsScripts = <<<JS
    //Metronic.init(); // init metronic core componets
    //Layout.init(); // init layout
    //QuickSidebar.init(); // init quick sidebar
JS;
//$this->registerCssFile(Yii::$app->homeUrl.'css/site.css');
$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);
?>
<!-- BEGIN PAGE CONTENT-->
<div class="main">
	<div class="container">
		<div class="row">
			<div class="col-md-12 page-404">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 page-404">
				<div class="number">
				</div>
				<div class="details" style="height: 800px;">
					<h3><?php echo $message;?></h3>
					<p>
						<a href="/">
							Quay lại trang chủ</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT-->
