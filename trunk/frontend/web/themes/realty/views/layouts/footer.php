<div class="footer">
    <div class="container">
        <div class="row">
            <!-- BEGIN COPYRIGHT -->
            <div class="col-md-6 col-sm-6 padding-top-10">
                2016 © Bản quyền .
            </div>
            <!-- END COPYRIGHT -->
            <!-- BEGIN PAYMENTS -->
            <div class="col-md-6 col-sm-6">
                <ul class="social-footer list-unstyled list-inline pull-right">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>

                    <li><a href="#"><i class="fa fa-skype"></i></a></li>

                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>

                </ul>
            </div>
            <!-- END PAYMENTS -->
        </div>
    </div>
</div>
