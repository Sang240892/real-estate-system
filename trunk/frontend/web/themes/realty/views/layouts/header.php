
<div class="pre-header">
    <div class="container">
        <div class="row">
            <!-- BEGIN TOP BAR LEFT PART -->
            <div class="col-md-8 col-sm-8 additional-shop-info">
                <ul class="list-unstyled list-inline">
                    <li><i class="fa fa-map-marker"></i><span>Tòa nhà CT4 Khu đô thị Mỹ Đình 2, Phường Mỹ Đình 2, Quận Nam Từ Liêm, TP Hà Nội</span></li>
                    <li><i class="fa fa-phone"></i><span>Hot line: (+84) 904 697 566</span></li>
                </ul>
            </div>
            <!-- END TOP BAR LEFT PART -->
            <!-- BEGIN TOP BAR MENU -->
            <div class="col-md-4 col-sm-4 additional-nav">
                <ul class="list-unstyled list-inline pull-right">
                    <?php
                    if(Yii::$app->user->identity):
                    ?>
                    <li class="langs-block">
                        <a href="javascript:void(0);" class="current navigate-profile">Hi <?=(Yii::$app->user->identity->email)?Yii::$app->user->identity->email:Yii::$app->user->identity->phone?><i class="fa fa-angle-down"></i></a>
                        <div class="langs-block-others-wrapper"><div class="langs-block-others">
                                <a href="<?=Yii::$app->urlManager->createUrl('/users/payment-history')?>">Thôn tin thanh toán</a>
                                <a href="<?=Yii::$app->urlManager->createUrl('/users/logout')?>">Đăng Xuất</a>
                            </div></div>
                    </li>
                    <?php endif?>
                    <?php  if(!Yii::$app->user->identity):?>
                    <li><a href="<?=Yii::$app->urlManager->createUrl('/users/auth')?>">Đăng nhập</a></li>
                    <li><a href="<?=Yii::$app->urlManager->createUrl('/users/signup')?>">Đăng ký</a></li>
                    <?php endif?>
                </ul>

            </div>
            <!-- END TOP BAR MENU -->
        </div>
    </div>
</div>

<div class="header">
    <div class="container">
        <a class="site-logo" href=""></a>

        <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>

        <!-- BEGIN NAVIGATION -->
        <div class="header-navigation pull-right font-transform-inherit">
            <ul>
                <li class="<?=(Yii::$app->controller->action->id == 'index')?'active':''?>"><a class="" href="<?=Yii::$app->urlManager->createUrl('/')?>" >Trang Chủ</a></li>
                <li class="<?=(Yii::$app->controller->action->id == 'about-us')?'active':''?>"><a href="<?=Yii::$app->urlManager->createUrl('/about')?>">Giới Thiệu</a></li>
                <li class="<?=(Yii::$app->controller->action->id == 'guide')?'active':''?>"><a href="<?=Yii::$app->urlManager->createUrl('/guide')?>" >Hướng Dẫn</a></li>
                <li class="<?=(Yii::$app->controller->action->id == 'pricing')?'active':''?>"><a href="<?=Yii::$app->urlManager->createUrl('/pricing')?>" >Bảng Giá</a></li>
                <li class="<?=(Yii::$app->controller->action->id == 'contact')?'active':''?>"><a href="<?=Yii::$app->urlManager->createUrl('/contact')?>" >Liên Hệ</a></li>
                <!-- END TOP SEARCH -->
                <?php
                if(Yii::$app->user->identity):
                ?>
                    <li class="<?=(Yii::$app->controller->action->id == 'product-list')?'active':''?>"><a href="<?=Yii::$app->urlManager->createUrl('/product/list')?>" >BDS Chính Chủ</a></li>
                <?php endif?>
            </ul>
        </div>
        <!-- END NAVIGATION -->
    </div>
</div>
