<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use \yii\web\Request;
use yii\helpers\Url;
// use yii\bootstrap\Nav;
// use yii\bootstrap\NavBar;
// use yii\widgets\Breadcrumbs;
// use common\widgets\Alert;

// AppAsset::register($this);
$asset		= \frontend\assets\AppAsset::register($this);
$baseUrl 	= $asset->baseUrl;
$dashboard  = (new Request)->getBaseUrl();
$jsScripts = <<<JS

//Layout.init();
//Layout.initOWL();
//RevosliderInit.initRevoSlider();
            //Layout.initTwitter();
            //Layout.initNavScrolling();

JS;

$this->registerCssFile(Yii::$app->homeUrl.'css/site.css',[ 'depends' => 'yii\web\YiiAsset']);
//$this->registerCssFile('http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900',[ 'depends' => 'yii\web\YiiAsset']);
//$this->registerCssFile('http://fonts.googleapis.com/css?family=Montserrat:400,700',[ 'depends' => 'yii\web\YiiAsset']);
//$this->registerCssFile('http://fonts.googleapis.com/css?family=Roboto:400,300italic,300,100italic,100,400italic,500,500italic,700,700italic,900,900italic',[ 'depends' => 'yii\web\YiiAsset']);

$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);


?>
<?php $this->beginPage();?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>

    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" type="image/x-icon" href="http://static.bdssmart.com/assets/images/page/frontend/favicon.ico">
    <script>
		var BASE_URL = '<?php echo Yii::$app->getUrlManager()->getHostInfo();?>';
    </script>
</head>
<body class="corporate">
<div class="contact-buttons">
    <div class="item">
        <span class="icon"><i class="fa fa-envelope-o"></i></span>
        <span class="content">appbdssmart@gmail.com</span>
    </div>
    <div class="item">
        <span class="icon"><i class="fa fa-phone"></i></span>
        <span class="content">Khu Vực Miền Bắc: (+84) 1252 389 893</span>
    </div>
    <div class="item">
        <span class="icon"><i class="fa fa-phone"></i></span>
        <span class="content">Khu Vực Miền Nam: (+84) 987 627 944</span>
    </div>
    <div class="item">
        <span class="icon"><i class="fa fa-phone"></i></span>
        <span class="content">Hot line: (+84) 904 697 566</span>
    </div>
</div>
    <?php $this->beginBody() ?>
    <a id="top"></a>
<!---->
    <?= $this->render('header.php',['baseUrl'=>$baseUrl]);?>
    <?= $content?>
    <?= $this->render('footer.php',['baseUrl'=>$baseUrl]);?>

    <!-- END FOOTER -->
    <?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
