/************************************************
 * User: sangnguyen on  7/14/16 at 09:12         *
 * File name:                       *
 * Project name:               *
 * Copyright (c) 2016 by                  *
 * All rights reserved                          *
 ************************************************
 */
RealItem = function (){

};
RealItem.prototype = {
  Constructor : RealItem,
    
    handleFilterRangDate: function(){
		 $('#filterRangeDate').daterangepicker({
                "locale": {
                    "format": "MM/DD/YYYY",
                    "separator": " - ",
                    "applyLabel": "Chọn",
                    "cancelLabel": "Hủy",
                    "fromLabel": "Từ",
                    "toLabel": "Đến",
                    "customRangeLabel": "Tùy chỉnh",
                    "weekLabel": "W",
                    "daysOfWeek": [
                        "Su",
                        "Mo",
                        "Tu",
                        "We",
                        "Th",
                        "Fr",
                        "Sa"
                    ],
                    "monthNames": [
                        "January",
                        "February",
                        "March",
                        "April",
                        "May",
                        "June",
                        "July",
                        "August",
                        "September",
                        "October",
                        "November",
                        "December"
                    ],
                    "firstDay": 1
                },
              
                opens: 'right',
                format: 'YYYY-MM-DD',
                separator: ' to ',
                startDate: moment().subtract('days', 1),
                endDate: moment(),
                ranges: {
                    'Hôm nay': [moment(), moment()],
                    'Hôm qua': [moment().subtract('days', 1), moment().subtract('days', 1)],
                    '7 ngày trước': [moment().subtract('days', 6), moment()],
                    '30 ngày trước': [moment().subtract('days', 29), moment()],
                    'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                    'Tháng trước': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                },
                minDate: '01/01/2012',
                maxDate: '12/31/2018'
            },
            function (start, end) {
                $('#filterRangeDateInput').val(start.format('YYYY-MM-DD') + ',' + end.format('YYYY-MM-DD'));
            }
        );
		 $('#filterRangeDate').on('apply.daterangepicker', function(ev, picker) {
			
		 	$('#filteringForm').submit();

		});

    },
   
    
    handleShowCityList: function(cityAddress,address){
    	var self = this;
    		
    	var list = Config.provinces_list;
    	
		var html = '';

			html += '<select id="filter_city" class="bs-select form-control">';

    	for(var i in list){

    		if(cityAddress == list[i].name){

    			html += '<option selected value="'+i+'">'+list[i].name+'</option>';

    			var ctiySelected = i;

    		}else{

				html += '<option value="'+i+'">'+list[i].name+'</option>';

    		}
    		
    	}

    	html += '</select>';

    	if(cityAddress != ''){
    		console.log('city '+cityAddress);

    		self.handleBuildDropListProvince(ctiySelected,address)
    		
    	}

    	$('#dropCityList').html(html);

  
    },
    handleShowProvincesList:function(){
    	var self = this;

    	jQuery('#product_filter').on('change','#filter_city',function(){
    		
    		var city = $(this).val();

    		self.handleBuildDropListProvince(city,'');

    		this.form.submit();

    	});

    },
    handleShowProductDetail: function(){
        $('.product_popup').on('click', function () {
            var id,title,des,cate,address,date,phone,price,is_image=false;
            	id = $(this).attr('id');
            	
                title = $(this).find('strong').text();
                des = $(this).find('.product-full-des').text();
                if(des == ''){
                    des = $(this).find('.realty-description').text();
                }
                cate = $(this).find('.product-cate').text();
                address = $(this).find('.product-address').text();
                date = $(this).find('.product-date').text();
                price = $('#product_price_'+id).text();
                phone = $('#product_phone_'+id).text();
                console.log(price);
                if(phone == ''){
					is_image =	true;
                	phone = $('#product_phone_'+id).attr('src');
                }
                
            $('#product_popup_title').html(title);
            $('#product_popup_des').html(des);
            $('#product_popup_cate').html(cate);
            $('#product_popup_address').html(address);
            $('#product_popup_date').html(date);
            $('#product_popup_price').html(price);
            if(is_image){
            	$('#product_popup_phone').html('<img src="'+phone+'" style="width:75px"/>')
            }else{
            	$('#product_popup_phone').html(phone);
            }


            $('#show_popup').trigger('click');
                  
        })

        //    $('#product_detail').on('shown.bs.modal', function () {
        //     var title ;
        //         title = $(this).find('strong');
        //         console.log(title);
        //     $('#product_popup_title').html(title);
        //     $('#product_popup_des').html('');
                  
        // })
    },
     
    handleBuildDropListProvince:function(city,address){
    		console.log(address);
			var list = Config.provinces_list[city].districts;

	    	var html = '';

				html += '<select name="SearchRealEstateItem[address]" onchange="this.form.submit()" id="filter_provinces" class="bs-select form-control">';

	    	for(var i in list){
	    		var value = Config.provinces_list[city].name+', '+list[i];

	    		if(address == value){

    					html += '<option selected value="'+value+'">'+list[i]+'</option>';

		    		}else{

						html += '<option value="'+value+'">'+list[i]+'</option>';

		    		}		
	    		
	    	}

	    	html += '</select>';

	    	$('#dropProvincesList').html(html);


    }

};






























