<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
use \yii\web\Request;
$baseUrl = str_replace('/backend/web', '', (new Request)->getBaseUrl());

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'users' => [
            'class' => 'frontend\modules\users\Module',
        ],
        'auth' => [
            'class' => 'frontend\modules\auth\Module',
        ],
    ],
    'components' => [
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LcHzwcUAAAAACwNZM4F6FIFnSYtM6auNqGn9_gB',
            'secret' => '6LcHzwcUAAAAAJxCj4IrdcrHQsS8Qn9zqIKsLcPt',
        ],
        'user' => [
            'identityClass' => 'common\models\UserIdentity',
            'loginUrl' => ['auth/authenticate/verify'],
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<module:[\w\-]+>/<controller:[\w\-]+>/<action:[\w\-]+>' => '<module>/<controller>/<action>',
                '/'=>'site/index',
                '/about'=>'site/about-us',
                '/guide'=>'site/guide',
                '/pricing'=>'site/pricing',
                '/contact'=>'site/contact',
                '/users/signup'=>'site/signup',
                '/users/logout'=>'auth/authenticate/logout',
                '/users/sigin'=>'site/login',
                '/users/reset-password'=>'auth/authenticate/reset-password',
                '/users/auth'=>'auth/authenticate/verify',
                '/users/payment-history'=>'users/default/payment-history',
                '/product/list'=>'users/default/product-list'

//                'debug/<controller>/<action>' => 'debug/<controller>/<action>',

                //'/auth/reset-password/<token:[\w\-]+>'                    => 'auth/authenticate/reset-password',
                //                'permission/<action:[\w\-]+>/<id:\d+>'           => 'permission/permission/<action>',
            ],
        ],
        'view' => [
            'theme'=>[
                'pathMap' => ['@app/views' => '@webroot/themes/realty/views'],
            ],
        ],
    ],
    'layout'=>'@webroot/themes/realty/views/layouts/main.php',
    'params' => $params,
    'aliases' => [
        // Set the editor language dir
        '@uploadPathName' => '/public/upload',
        '@rootPath' => realpath(dirname(__FILE__).'/../../'),
    ],
];
