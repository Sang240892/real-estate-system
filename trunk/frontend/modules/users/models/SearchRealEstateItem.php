<?php

namespace frontend\modules\users\models;

use backend\commons\helpers\UtilHelper;
use common\models\entities\RealEstateCategory;
use common\models\entities\RealEstateCategoryRelation;
use common\models\entities\RealEstateItemPostDate;
use Yii;
use yii\base\ErrorException;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\entities\RealEstateItem;

/**
 * SearchRealEstateItem represents the model behind the search form about `common\models\entities\RealEstateItem`.
 */
class SearchRealEstateItem extends RealEstateItem
{

    public $post_date;
        /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'confirmed', 'is_spam', 'start_date', 'end_date', 'created_at', 'updated_at', 'user_id', 'category_id'], 'integer'],
            [['title','is_self','price_string','summary', 'description', 'source', 'street', 'district', 'ward', 'city', 'country', 'address', 'phone_contact', 'type', 'unit', 'category_name', 'latitude', 'longitude'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$not_sure,$is_self=null)
    {
        $price_max = null;
        $price_min = null;
        $cat =[];
        $crawlingDateSearching = null;

        if(isset($params['SearchRealEstateItem']['category_id']) && !empty($params['SearchRealEstateItem']['category_id'])){
            $categories = RealEstateCategoryRelation::findAll(['category_parent_id'=>$params['SearchRealEstateItem']['category_id']]);

            if(!empty($categories)){
                foreach($categories as $re){
                    $cat[] = $re->category_children_id;
                }
            }else{
                $cat[] = 0;
            }
        }

        $query = RealEstateItem::find()->andOnCondition(['is_self'=>1,'is_not_sure_self'=>0]);

        $query->leftJoin(RealEstateItemPostDate::tableName(),
            RealEstateItemPostDate::tableName().'.item_id = '.RealEstateItem::tableName().'.id');
        // add conditions that should always apply here
        $query->joinWith(['realEstateItemPostDates post'=>function (\yii\db\ActiveQuery $query) {
            $query->orderBy('id DESC');
        }
        ], true, 'LEFT JOIN');

        //$query->andOnCondition(['is_self'=>1]);
        $query->distinct();
        //$query->with('realEstateItemPostDates');
        $query->with('category');
        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        if(isset($params['SearchRealEstateItem']['real_estate_date']) && !empty($params['SearchRealEstateItem']['real_estate_date'])){
            $rangeDate  = $params['SearchRealEstateItem']['real_estate_date'];
            $rangeDate = explode(',',$rangeDate);
            $rangeDate = array_filter($rangeDate);
            $rangeDate = array_values($rangeDate);

            if(count($rangeDate) == 1){
                $query->andOnCondition("date(CONVERT_TZ(FROM_UNIXTIME(real_estate_item_post_date.post_date), @@session.time_zone, '+07:00')) = :date ",[
                    ':date'=>$rangeDate[0]
                ]);
            }else{
                $from = (isset($rangeDate[0]) && !empty($rangeDate[0]))?$rangeDate[0]:0;
                $to  = (isset($rangeDate[1]) && !empty($rangeDate[0]))?$rangeDate[1]:0;
                if($from == $to || $from == 0 || $to == 0){
                    $query->andOnCondition("date(CONVERT_TZ(FROM_UNIXTIME(real_estate_item_post_date.post_date), @@session.time_zone, '+07:00')) = :date ",[
                        ':date'=>$from
                    ]);
//
                }else{
                    $query->andOnCondition('real_estate_item_post_date.post_date between UNIX_TIMESTAMP(:from) and UNIX_TIMESTAMP(:to)',[
                        ':from'=>$rangeDate[0],
                        ':to'=>$rangeDate[1]
                    ]);
                }
            }

        }
        if(isset($params['SearchRealEstateItem']['is_new']) && !empty($params['SearchRealEstateItem']['is_new'])){
            $isNew = $params['SearchRealEstateItem']['is_new'];
            if($params['SearchRealEstateItem']['is_new'] == -1){
                $isNew = 0;
            }
            $query->andOnCondition(['is_new'=>$isNew]);
        }

        if(isset($params['SearchRealEstateItem']['price_string']) && !empty($params['SearchRealEstateItem']['price_string'])){
            $pricing_string_request = $params['SearchRealEstateItem']['price_string'];
            switch($pricing_string_request){
                case 'thỏa thuận':
                    $query->andFilterWhere([
                        'or',
                        ['like','price_string',"thỏa thuận"],
                        ['=','real_estate_item.price',0]
                    ]);
                    break;
                case '< 1m':
                    $price_max = 1000000;
                    $price_min = 0;
                    break;
                case '1,3 m':
                    $price_max = 3000000;
                    $price_min = 1000000;
                    break;
                case '3,5 m':
                    $price_max = 5000000;
                    $price_min = 3000000;
                    break;
                case '5,10 m':
                    $price_max = 10000000;
                    $price_min = 5000000;
                    break;
                case '10,50 m':
                    $price_max = 10000000;
                    $price_min = 50000000;
                    break;
                case '50,100 m':
                    $price_max = 100000000;
                    $price_min = 50000000;
                    break;
                case '100,500 m':
                    $price_max = 100000000;
                    $price_min = 500000000;
                    break;
                case '500,1 b':
                    $price_max = 500000000;
                    $price_min = 1000000000;
                    break;
                case '1,3 b':
                    $price_max = 3000000000;
                    $price_min = 1000000000;
                    break;
                case '3,9 b':
                    $price_max = 9000000000;
                    $price_min = 3000000000;
                    break;
                case '9,15 b':
                    $price_max = 15000000000;
                    $price_min = 9000000000;
                    break;
                case '15,30 b':
                    $price_max = 15000000000;
                    $price_min = 30000000000;
                    break;
                case '>30 b':
                    $price_max = 0;
                    $price_min = 30000000000;
                    break;
            }

            if($price_max > 0){
                $query->andFilterWhere( ['between', 'real_estate_item.price',(int) $price_min,(int) $price_max]);
            }
            if($price_max === 0 && $price_min > 0){
                $query->andFilterWhere( ['>', 'real_estate_item.price',(int) $price_min]);
            }
           // die(var_dump($price_max));
        }

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        /**
         * Setup your sorting attributes
         * Note: This is setup before the $this->load($params)
         * statement below
         */
        $dataProvider->setSort([
            'defaultOrder' => [
                //'real_estate_date'=>SORT_DESC,
                'post.post_date' => SORT_DESC,
            ],
            'attributes' => [
                'id',
                'title',
                'summary',
                'description',
                'source',
                'street',
                'district',
                //'phone_contact',
                'address',
                'type',
                'post.post_date' => [
                    'asc' => [RealEstateItemPostDate::tableName().'.post_date' => SORT_ASC],
                    'desc' => [RealEstateItemPostDate::tableName().'.post_date' => SORT_DESC],
                    'label'=>Yii::t('backend','Re Post Date'),
                    //'default' => SORT_ASC
                ],
                //'price_string',
                'post_date',
                'real_estate_date'=>[
                    'label' => 'Ngày Đăng Tin',
                ]
            ]
        ]);
        if($this->is_self =='-1'){
            $this->is_self = 0;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            RealEstateItem::tableName().'.id' => $this->id,
            RealEstateItem::tableName().'.status' => $this->status,
            RealEstateItem::tableName().'.confirmed' => $this->confirmed,
            RealEstateItem::tableName().'.is_spam' => $this->is_spam,
            RealEstateItem::tableName().'.start_date' => $this->start_date,
            RealEstateItem::tableName().'.end_date' => $this->end_date,
            RealEstateItem::tableName().'.is_self' => $this->is_self,
            RealEstateItem::tableName().'.created_at' => $this->created_at,
            RealEstateItem::tableName().'.updated_at' => $this->updated_at,
            RealEstateItem::tableName().'.user_id' => $this->user_id,
            RealEstateItem::tableName().'.is_new'=>$this->is_new
           // 'category_id' => $this->category_id,
        ]);

        $query->andFilterWhere(['like', RealEstateItem::tableName().'.title', $this->title])
            ->andFilterWhere(['like', RealEstateItem::tableName().'.summary', $this->summary])
            ->andFilterWhere(['like', RealEstateItem::tableName().'.description', $this->description])
          //  ->andFilterWhere(['=', RealEstateItem::tableName().'.price_string', $this->price_string])
            ->andFilterWhere(['like', RealEstateItem::tableName().'.source', $this->source])
            ->andFilterWhere(['like', RealEstateItem::tableName().'.street', $this->street])
            ->andFilterWhere(['like', RealEstateItem::tableName().'.district', $this->district])
            ->andFilterWhere(['like',RealEstateItem::tableName().'.ward', $this->ward])
            ->andFilterWhere(['like', RealEstateItem::tableName().'.city', $this->city])
            ->andFilterWhere(['like', RealEstateItem::tableName().'.country', $this->country])
            ->andFilterWhere(['like', RealEstateItem::tableName().'.phone_contact', $this->phone_contact])
            ->andFilterWhere(['like', RealEstateItem::tableName().'.type', $this->type])
            ->andFilterWhere(['like',RealEstateItem::tableName().'.unit', $this->unit])
            ->andFilterWhere(['like', RealEstateItem::tableName().'.category_name', $this->category_name])
            ->andFilterWhere(['like',RealEstateItem::tableName().'.latitude', $this->latitude])
            ->andFilterWhere(['like',RealEstateItem::tableName().'.longitude', $this->longitude])
            ->andFilterWhere(['in',RealEstateItem::tableName().'.category_id',$cat]);


        if($this->address && !empty($this->address)){
            $query = UtilHelper::parseAddressWhenSearching($query,$this->address);
        }

        return $dataProvider;
    }
    public function exportFields()
    {
        return [
            'id' => function ($model) {
                /* @var $model User */
                return $model->id;
            },
            'title',
        ];
    }
}
