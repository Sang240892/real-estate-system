<?php
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\grid\GridView;
use backend\commons\helpers\UtilHelper;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\product\models\SearchRealEstateItem */
/* @var $dataProvider yii\data\ActiveDataProvider */
$asset		= frontend\assets\AppAsset::register($this);


$asset->css[] = 'theme/global/plugins/fancybox/source/jquery.fancybox.css';
$asset->css[] = 'theme/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css';
$asset->css[] = 'theme/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css';
$asset->css[] = 'theme/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css';
$asset->css[] = 'theme/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css';

$asset->js[] = 'theme/global/plugins/moment.min.js';
$asset->js[] = 'theme/global/plugins/fancybox/source/jquery.fancybox.pack.js';

$asset->js[] = 'theme/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js';
$asset->js[] = 'theme/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
$asset->js[] = 'theme/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js';
$asset->js[] = 'theme/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js';

$this->registerJsFile(Yii::$app->homeUrl.'scripts/helpers/helpers.js', ['depends' =>'yii\web\YiiAsset']);

$this->registerJsFile(Yii::$app->homeUrl.'scripts/helpers/config.js', ['depends' =>'yii\web\YiiAsset']);
$this->registerJsFile(Yii::$app->homeUrl.'scripts/modules/real_estate_item.js', ['depends' =>'yii\web\YiiAsset']);
$this->title = Yii::t('frontend', 'Danh Sách Tin Bất Động Sản Chính Chủ');
$this->params['breadcrumbs'][] = $this->title;
$jsScripts = 'var city_filter = "'.$cityFilter.'"; ';
$jsScripts .= 'var province_filter = "'.$addressSearching.'"; ';
$jsScripts .= '

	var Item = new RealItem()

	Item.handleShowCityList(city_filter,province_filter);
	Item.handleShowProvincesList();
	Item.handleFilterRangDate();
	Item.handleShowProductDetail();

';

$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);
$parents = \common\models\entities\RealEstateCategory::findAll(['parent'=>0]);

?>
<button type="button" class="hidden" id="show_popup" data-toggle="modal" data-target="#product_detail"></button>
<div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="/">Trang Chủ</a></li>
            <li class="active"><a href="<?=Yii::$app->urlManager->createUrl('/product/list')?>">Tin Chính Chủ</a></li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN CONTENT -->
          <div class="col-md-12 col-sm-12">
           <form id="filteringForm">
            <div class="row">
                <div id="product_filter" class="dataTables_filter">
                    <div class="col-md-5">
                        <label>Doanh mục</label>

                        <select onchange="this.form.submit()" name="SearchRealEstateItem[category_id]" class="bs-select form-control">
                            <option value="0">Chọn doanh mục</option>
                            <?php
                            $item='';
                            if(!empty($parents)){
                                foreach($parents as $parent){
                                    $childs = \common\models\entities\RealEstateCategory::findAll(['parent'=>$parent->id,'is_display'=>1]);
                                    $item.='<optgroup label="'.$parent->name.'">';

                                    if(!empty($childs)){
                                        foreach($childs as $child){
                                            $selected = (isset($categoryId) && $categoryId == $child->id)?'selected':'';
                                            $item.= "<option ".$selected." value='".$child->id."'>".$child->name."</option>";
                                        }
                                    }
                                    $item.='</optgroup>';
                                }
                            }
                            echo $item;
                            ?>
                        </select>

                    </div>
                    <div  class="col-md-3">
                        <label>Thành phố</label>
                        <div id="dropCityList" ></div>
                    </div>
                    <div class="col-md-4">
                        <label>Quận huyện</label>
                        <div id="dropProvincesList"> <select name="SearchRealEstateItem[address]" id="filter_provinces" class="bs-select form-control"></select></div>
                    </div>

                </div>

                <div class="col-md-5">
                    <label>Ngày đăng tin</label>
                    <div class="input-group" id="filterRangeDate">
                        <input value="<?=$crawlingDateSearching?>" name="SearchRealEstateItem[real_estate_date]" id="filterRangeDateInput" type="text" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn default date-range-toggle" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                    </div>
                </div>
                <div class="col-md-2">
                    <label>Loại tin</label>
                    <select onchange="this.form.submit()" name="SearchRealEstateItem[is_new]" id="filter_city" class="bs-select form-control">
                        <option value="">Tất cả</option>
                        <option <?= (isset($isNew) && $isNew == 1)?"selected":"" ?> value="1">Tin mới</option>
                        <option <?= (isset($isNew) && $isNew == -1)?"selected":"" ?> value="-1">Tin đăng lại</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <label>Giá</label>
                    <select onchange="this.form.submit()" name="SearchRealEstateItem[price_string]" id="product_filter_price" class="form-control">
                        <option <?=(isset($price_string) && $price_string == 'bất kỳ')?'selected':''?> value="bất kỳ">Bất kỳ</option>
                        <option <?=(isset($price_string) && $price_string == 'thỏa thuận')?'selected':''?> value="thỏa thuận">Thoả thuận</option>
                        <option <?=(isset($price_string) && $price_string == '< 1m')?'selected':''?> value="< 1m">< 1 triệu</option>
                        <option <?=(isset($price_string) && $price_string == '1,3 m')?'selected':''?> value="1,3 m">1 - 3 triệu</option>
                        <option <?=(isset($price_string) && $price_string == '3,5 m')?'selected':''?> value="3,5 m">3 - 5 triệu</option>
                        <option <?=(isset($price_string) && $price_string == '5,10 m')?'selected':''?> value="5,10 m">5 - 10 triệu</option>
                        <option <?=(isset($price_string) && $price_string == '10,50 m')?'selected':''?> value="10,50 m">10 - 50 triệu</option>
                        <option <?=(isset($price_string) && $price_string == '50,100 m')?'selected':''?> value="50,100 m">50 - 100 triệu</option>
                        <option <?=(isset($price_string) && $price_string == '100,500 m')?'selected':''?> value="100,500 m">100 - 500 triệu</option>
                        <option <?=(isset($price_string) && $price_string == '500,1 b')?'selected':''?> value="500,1 b">500 - 1 tỷ</option>
                        <option <?=(isset($price_string) && $price_string == '1,3 b')?'selected':''?> value="1,3 b">1 - 3 tỷ</option>
                        <option <?=(isset($price_string) && $price_string == '3,9 b')?'selected':''?> value="3,9 b">3 - 9 tỷ</option>
                        <option <?=(isset($price_string) && $price_string == '9,15 b')?'selected':''?> value="9,15 b">9 - 15 tỷ</option>
                        <option <?=(isset($price_string) && $price_string == '15,30 b')?'selected':''?> value="15,30 b">15 - 30 tỷ</option>
                        <option <?=(isset($price_string) && $price_string == '>30 b')?'selected':''?> value=">30 b"> > 30 tỷ</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12 margin-top-10 ">
                <div class="pull-right">
                    <?php
                    echo \nterms\pagesize\PageSize::widget([
                        'label' => false,
                        'template'=> Yii::t('backend','View {list} record(s) | Found total {totalCount} record(s)',[
                            'totalCount'=>$dataProvider->getTotalCount()
                        ]),
                        'options'=> ['class'=>'form-control input-sm input-xsmall input-inline','id'=>'perpage']
                    ]);
                    ?>
                </div>

        </div>
        </form>
        <hr/>
        <?php
                // You can choose to render your own GridView separately
                
                $gridColumns = [

                   

                    // [
                    //     'class' => 'yii\grid\ActionColumn',
                    //     'template' => '{view} {update} {delete} ',
                       
                    // ],
                    
                    [
                        'attribute'=>'title',
                        'format' => 'html',
                        'contentOptions' => function($model){
                            return ["class"=>"product_popup",'style'=>'cursor:pointer','id'=>$model->id];
                        },
                        'filterInputOptions' => [
                            'class'       => 'form-control',
                            'placeholder' => 'Tìm kiếm tiêu đề'
                        ],
                        'value'=> function ($model) {
                            $postDates = $model->realEstateItemPostDates;

                            $isNew = ($model->is_new == 1)?'Tin mới':'Tin đăng lại';
                            $class = ($model->is_new == 1)?'danger':'warning';

                            $html = '';
                            $html .= '<span class="label label-sm label-'.$class.'">'.$isNew.'</span>&nbsp;';
                            $html .='<strong>'. wordwrap($model->title, 120, "<br />\n").'</strong>';
                            $html .='<p class="realty-description">'. wordwrap(UtilHelper::shorten($model->description,400), 150, "<br />\n").'</p>';
                            if(strlen($model->description) > 400){
                                $html .='<span class="product-full-des">'.$model->description.'</span>';
                            }
                            if(isset($model->category) && $model->category->finder_name){
                                $html .= 'Doanh mục: <small class="product-cate">'.$model->category->finder_name.'</small>';
                            }

                            $html .= '&nbsp;&nbsp;Địa chỉ: <small class="product-address">'.$model->address.'</small>';
                            if(isset($postDates[0])){
                                $html .= '&nbsp;&nbsp;Ngày đăng: <small class="product-date">'.Yii::$app->formatter->asDate($postDates[0]->post_date,'Y/MM/d').'</small>';
                            }elseif($model->repost_date){
                                $html .= '&nbsp;&nbsp;Ngày đăng: <small class="product-date">'.Yii::$app->formatter->asDate($model->repost_date,'Y/MM/d').'</small>';
                            }elseif($model->created_at){
                                $html .= '&nbsp;&nbsp;Ngày đăng: <small class="product-date">'.Yii::$app->formatter->asDate($model->created_at,'Y/MM/d').'</small>';
                            }

                            return $html;
                        },
                    ],
                    [
                        'attribute'=>'price_string',
                        'format' => 'html',
                        'filter'=>false,
//                        'filterInputOptions' => [
//                            'class'       => 'form-control',
//                            'placeholder' =>'Tìm kiếm giá'
//                        ],
                        'content'=> function ($model) {
                           return '<div class="price" id="product_price_'.$model->id.'">'. $model->price_string.'</div>';
                        },
                    ],
                    [
                        'attribute'=>'phone_contact',
                        'format' => 'html',
                        'filter'=>false,
//                        'filterInputOptions' => [
//                            'class'       => 'form-control',
//                            'placeholder' => 'Tìm kiếm số điện thoại'
//                        ],
                        'content'=> function ($model) {
                            if(UtilHelper::commonIsImageUrl($model->phone_contact)){
                                return Html::img($model->phone_contact,['id'=>'product_phone_'.$model->id,'style'=>'width:75px']);
                            }
                            return '<span id="product_phone_'.$model->id.'">'.trim($model->phone_contact).'</span>';
                        },
                    ],
                ];
                ?>
           
          </div>
          <div class="col-md-12 col-sm-12">
          <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'filterSelector' => 'select[name="per-page"]',
                        'layout'=>"<div class='pull-right dataTables_paginate paging_bootstrap_full_number'>
                                            {pager}</div>\n<div class='table-scrollable'>{items}</div>\n<div class='row'>
                                            <div class='col-md-5 col-sm-12'>
                                          </div>
                                            \n<div class='col-md-7 col-sm-12'>
                                            <div class='pull-right dataTables_paginate paging_bootstrap_full_number'>
                                            {pager}</div></div></div>",
                        'sorter'=>[
                            'linkOptions'=>[
                                'class'=>'sorting'
                            ]
                        ],
                        'tableOptions' => [
                            'id'=> 'girdView',
                            'class' => 'table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer'
                        ],
                        'columns' =>$gridColumns
                    ]); ?>
          </div>
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
<!-- BEGIN fast view of a product -->

<div id="product_detail" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 id="product_popup_title" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p id="product_popup_des"></p>
                <p>
                    <strong>Doanh mục:</strong> <span id="product_popup_cate"></span></br>
                    <strong>Địa chỉ: </strong><span id="product_popup_address"></span></br>
                    <strong>Giá: </strong> <span id="product_popup_price"></span></br>
                    <strong>Số điện thoại: </strong> <span id="product_popup_phone"></span></br>
                    <strong>Ngày đăng: </strong> <span id="product_popup_date"></span></br>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

