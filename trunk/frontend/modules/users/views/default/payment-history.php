<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\payment\models\SearchPaymentHistoryModel */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('backend', 'Lịch sử thanh toán');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main" style="min-height: 563px">
    <div class="container">
        <h1><?=$this->title?></h1>
        <div class="payment-history-index">
            <div class="col-md-12 margin-top-10 margin-bottom-10">
                <div class="pull-right">
                    <?php
                    echo \nterms\pagesize\PageSize::widget([
                        'label' => false,
                        'template'=> Yii::t('backend','View {list} record(s) | Found total {totalCount} record(s)',[
                            'totalCount'=>$dataProvider->getTotalCount()
                        ]),
                        'options'=> ['class'=>'form-control input-sm input-xsmall input-inline','id'=>'perpage']
                    ]);
                    ?>
                </div>

            </div>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
        <?php Pjax::begin(); ?>    <?= GridView::widget([
            'dataProvider' => $dataProvider,
           // 'filterModel' => $searchModel,
            'filterSelector' => 'select[name="per-page"]',
            'layout'=>"<div class='pull-right dataTables_paginate paging_bootstrap_full_number'>
                        {pager}</div>\n<div class='table-scrollable'>{items}</div>\n<div class='row'>
                        <div class='col-md-5 col-sm-12'>
                      </div>
                        \n<div class='col-md-7 col-sm-12'>
                        <div class='pull-right dataTables_paginate paging_bootstrap_full_number'>
                        {pager}</div></div></div>",
            'columns' => [
                [
                    'attribute'=>'start_date',
                    'header'=>'Ngày bắt đầu',
                    'filter'=>false,
                    'value'=> function ($model) {
                        return Yii::$app->formatter->asDate($model->start_date,'Y/MM/d');
                    },
                ],
                [
                    'attribute'=>'end_date',
                    'header'=>'Ngày hết hạn',
                    'filter'=>false,
                    'value'=> function ($model) {
                        return Yii::$app->formatter->asDate($model->end_date,'Y/MM/d');
                    },
                ],
                [
                    'attribute'=>'amount',
                    'header'=>'Chi phí (VNĐ)',
                    'filter'=>false,
                    'value'=> function ($model) {
                        return $model->amount;
                    },
                ],
                [
                    'attribute'=>'created_at',
                    'header'=>'Ngày tạo',
                    'filter'=>false,
                    'value'=> function ($model) {
                        return Yii::$app->formatter->asDate($model->created_at,'Y/MM/d');
                    },
                ],

            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
