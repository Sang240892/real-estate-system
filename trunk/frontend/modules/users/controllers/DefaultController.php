<?php

namespace frontend\modules\users\controllers;
use backend\modules\payment\models\SearchPaymentHistoryModel;
use common\models\entities\PaymentHistory;
use Yii;
use common\models\entities\RealEstateItem;
use frontend\modules\auth\controllers\AuthenticateController;
use frontend\modules\users\models\SearchRealEstateItem;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `users` module
 */
class DefaultController extends AuthenticateController
{

    public function actionPaymentHistory(){
        $user_id = Yii::$app->user->id;
        $searchModel = new SearchPaymentHistoryModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$user_id);

        return $this->render('payment-history', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Finds the PaymentHistory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PaymentHistory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelPaymentHistory($id)
    {
        if (($model = PaymentHistory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionProductList()
    {
        $searchModel = new SearchRealEstateItem();
        $categoryId = null;
        $addressSearching = null;
        $cityFiltering = null;
        $crawlingDateSearching = null;
        $isNew = null;
        $not_sure = 0;
        $params = Yii::$app->request->queryParams;
        $isSelf = null;
        $price_string = 'bất kỳ';

        if(isset($params['SearchRealEstateItem']['category_id']) && !empty($params['SearchRealEstateItem']['category_id'])){
            $categoryId= $params['SearchRealEstateItem']['category_id'];
        }
        if(isset($params['SearchRealEstateItem']['address']) && !empty($params['SearchRealEstateItem']['address'])){
            $addressSearching = $params['SearchRealEstateItem']['address'];
        }
        if(isset($params['SearchRealEstateItem']['real_estate_date']) && !empty($params['SearchRealEstateItem']['real_estate_date'])){
            $crawlingDateSearching = $params['SearchRealEstateItem']['real_estate_date'];
        }
        if(isset($params['SearchRealEstateItem']['is_new']) && !empty($params['SearchRealEstateItem']['is_new'])){
            $isNew = $params['SearchRealEstateItem']['is_new'];
        }
        if(isset($params['SearchRealEstateItem']['is_self']) && !empty($params['SearchRealEstateItem']['is_self'])){
            $isSelf = $params['SearchRealEstateItem']['is_self'];
        }

        if(isset($params['SearchRealEstateItem']['price_string']) && !empty($params['SearchRealEstateItem']['price_string'])){
            $price_string = $params['SearchRealEstateItem']['price_string'];
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$not_sure);

        $filter = explode(',',$addressSearching);
        if(!empty($filter)){
            $cityFiltering = $filter[0];
        }
        $cityFiltering = str_replace('+',' ',$cityFiltering);

        $itemNotSureSelf = RealEstateItem::find()->andOnCondition(['is_not_sure_self'=>1])->count();

        return $this->render('product-list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'categoryId'=>$categoryId,
            'addressSearching'=>$addressSearching,
            'cityFilter'=>$cityFiltering,
            'crawlingDateSearching'=>$crawlingDateSearching,
            'isNew'=>$isNew,
            'isSelf'=>$isSelf,
            'notSure'=>$not_sure,
            'itemNotSureSelf'=>$itemNotSureSelf,
            'price_string'=>$price_string
        ]);
    }
}
