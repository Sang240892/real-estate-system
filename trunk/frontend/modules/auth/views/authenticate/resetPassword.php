<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = 'Cập nhật lại mật khẩu';
?>
<div class="main" style="min-height: 545px">
    <div class="container">
        <div class="site-reset-password">
            <div class="row">
                <div class="col-md-9 col-sm-9">
                    <h1>Đặt lại mất khẩu mới.</h1>
                    <div class="content-form-page">
                        <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                        <?= $form->field($model, 'password')->passwordInput(['autofocus' => true])->label('Mật khẩu') ?>

                        <?= $form->field($model, 'password_repeat')->passwordInput()->label('Xác Nhận Mật khẩu') ?>

                        <div class="form-group">
                            <?= Html::submitButton('Đồng ý', ['class' => 'btn btn-primary']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
