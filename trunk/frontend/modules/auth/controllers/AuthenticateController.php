<?php
/**
 * User: sangnguyen on  4/10/16 at 22:51
 * File name: AuthenticateController.php
 * Project name: Fit-Road
 * Copyright (c) 2016 by MyCompany
 * All rights reserved
 */

namespace frontend\modules\auth\controllers;


use api\commons\forms\SentTokenRestPasswordForm;
use common\models\entities\RealEstateUser;
use common\models\UserIdentity;
use frontend\models\ResetPasswordForm;
use Yii;
use yii\base\InvalidParamException;
use yii\db\Exception;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class AuthenticateController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
//                                'denyCallback' => function ($rule, $action) {
//                                    throw new \Exception('You are not allowed to access this page');
//                                },
                'rules' => [
                    [
                        'actions' => ['verify','logout', 'error','reset-password','password-reset-request','payment-history'],
                        'allow' => true,
                    ],
//                    [
//                        //'actions' => ['logout'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            $module = Yii::$app->controller->module->id;
                            $controller = Yii::$app->controller->id;
                            $acion = Yii::$app->controller->action->id;
                            //                            $post = Yii::$app->request->post();

                            if(\Yii::$app->user->can($module)){
                                return true;
                            }elseif(\Yii::$app->user->can($module.$controller)){
                                return true;
                            }elseif(\Yii::$app->user->can($module.'-'.$controller.'-'.$acion)){
                                return true;
                            }
                             throw new ForbiddenHttpException(Yii::t('backend','Bạn phải đăng ký mới có thể sử dụng tính năng này!'));

                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionVerify()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['/product/list']);
        } else {
            return $this->render('@frontend/views/site/login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);

        } catch (InvalidParamException $e) {

            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Mật khẩu mới đã được cập nhật.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * @var : Password reset request
     */
    public function actionPasswordResetRequest(){

        $form = new SentTokenRestPasswordForm();
        $form->email = (Yii::$app->request->post('email'))?Yii::$app->request->post('email'):null;
        if($form->validate()){
            $send = $form->sendEmail();

        }
        return $this->redirect(['/users/auth']);
    }

    public function actionLogout()
    {
        $user = RealEstateUser::findOne(['id'=> Yii::$app->user->id]);
            if($user){
                $user->is_online = UserIdentity::IS_OFFLINE;
                $user->save(false);
                Yii::$app->user->logout();
            }
        $auth = $user->getAuthenticationTokens()->one();
        if($auth){
            try{
                $auth->delete();
            }catch (Exception $e){
                Yii::error($e->getMessage());
            }
        }
        return $this->redirect(Yii::$app->urlManager->createUrl(['users/auth']));
    }
}
