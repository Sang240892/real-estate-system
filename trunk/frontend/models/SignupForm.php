<?php
namespace frontend\models;

use common\models\UserIdentity;
use himiklab\yii2\recaptcha\ReCaptchaValidator;
use yii\base\Model;
use common\models\entities\RealEstateUser;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $phone;
    public $email;
    public $password;
    public $last_name;
    public $first_name;
    public $password_repeat;
    public $reCaptcha;
    public $areaWorking;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['last_name','first_name'], 'filter', 'filter' => 'trim'],
            [['last_name'], 'required','message'=>'Họ không được phép rỗng.'],
            [['first_name'], 'required','message'=>'Tên không được phép rỗng.'],
            [['phone'], 'required','message'=>'Số điện thoại không được phép rỗng.'],
            [['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator::className(),
               "message"=>'Xác thực thất bại' ,'secret' => \Yii::$app->params['GG_RECAPTCHA_SECRET']],

            ['phone', 'unique', 'targetClass' => '\common\models\entities\RealEstateUser', 'message' => 'Số điện thoại này đã tồn tại.'],
            ['phone', 'integer'],
            ['phone', 'match', 'pattern'=>'/^[0-9]{9,12}$/','message' => 'Số điện thoại không đúng định dạng.'],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\entities\RealEstateUser', 'message' => 'Email này đã tồn tại.'],

            ['areaWorking','filter', 'filter' => 'trim'],
            ['password', 'required','message'=>'Mật khẩu không được phép rỗng.'],
            ['password', 'string', 'min' => 6],
            [['password_repeat'], 'compare', 'compareAttribute' => 'password','message'=>'Mật khẩu xác thực không trùng khớp.'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {

        if (!$this->validate()) {
            return null;
        }

        $user = new UserIdentity();
        $user->last_name = $this->last_name;
        $user->first_name = $this->last_name;
        $user->phone = $this->phone;
        $user->email = $this->email;
        $user->notification_city_rule = $this->areaWorking;
        $user->setPassword($this->password);
        //$user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }
}
