<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@bower/frontend/';

    public $css = [
    'http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all',
        "theme/global/plugins/font-awesome/css/font-awesome.min.css" ,
        "theme/global/plugins/bootstrap/css/bootstrap.min.css" ,

//      "theme/global/plugins/fancybox/source/jquery.fancybox.css" ,
//      "theme/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" ,
//      "theme/global/plugins/slider-revolution-slider/rs-plugin/css/settings.css" ,

      "theme/global/css/components.css" ,
      "theme/frontend/layout/css/style.css" ,
//      "theme/frontend/pages/css/style-revolution-slider.css" ,
//      "theme/frontend/layout/css/style-responsive.css" ,
      "theme/frontend/layout/css/themes/blue.css" ,
      "theme/frontend/layout/css/custom.css" ,
    ];

    public $js = [
    "theme/global/plugins/jquery.min.js",
    "theme/global/plugins/jquery-migrate.min.js",
    "theme/global/plugins/bootstrap/js/bootstrap.min.js",      
//    "theme/frontend/layout/scripts/back-to-top.js",
//
//    "theme/global/plugins/fancybox/source/jquery.fancybox.pack.js",
//    "theme/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js",
//
//    "theme/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js",
//    "theme/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js",
//    "theme/frontend/pages/scripts/revo-slider-init.js",

    //"theme/frontend/layout/scripts/layout.js",

    ];

    public $depends = [
        'yii\web\YiiAsset',
        //         'yii\bootstrap\BootstrapAsset'
    ];
}
