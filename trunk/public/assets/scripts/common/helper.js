/************************************************
 * User: sangnguyen on  6/7/16 at 20:39         *
 * File name:                       *
 * Project name: ecommerce                *
 * Copyright (c) 2016 by YSD                 *
 * All rights reserved                          *
 ************************************************
 */
var imageRemoveArray = [];

 Helper = function(){

};
Helper.prototype = {
    constructor : Helper,

    handleSelect2DropDownList : function (selector,option){
        jQuery(selector).select2(option);
    },
    handleCurrencyInput : function(selector,currency){
        $(selector).maskMoney({prefix:'',allowZero:true, allowNegative: true, thousands:'', decimal:'.', affixesStay: false});
        (function($) {
            $.fn.currencyInput = function() {
                this.each(function() {
                    var wrapper = $("<div class='currency-input' />");
                    $(this).wrap(wrapper);
                    $(this).before("<span class='currency-symbol'>"+currency+"</span>");
                    /* $(this).change(function() {
                     var min = parseFloat($(this).attr("min"));
                     var max = parseFloat($(this).attr("max"));
                     var value = this.valueAsNumber;
                     if(value < min)
                     value = min;
                     else if(value > max)
                     value = max;
                     $(this).val(value.toFixed(3));
                     });*/
                });
            };
        })(jQuery);
        $(selector).currencyInput();
    },
     handleSummernote : function (selector,option) {
        $(selector).summernote(option);
        //API:
        //var sHTML = $('#summernote_1').code(); // get code
        //$('#summernote_1').destroy(); // destroy
    },

    handlePreviewImage : function (selector,preview){
        // Initialize the jQuery File Upload widget:
        $(selector).change(function () {
            resetRemoveImageInput();
            if (typeof (FileReader) != "undefined") {
                var dvPreview = jQuery(preview);
                dvPreview.html("");
                var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                $($(this)[0].files).each(function (i) {
                    var file = $(this);

                    if (regex.test(file[0].name.toLowerCase())) {
                        var reader = new FileReader();
                        reader.onload = function (e) {

                            var img = '<tr id="__preview_'+i+'" class="template-upload fade in">'
                                    +'<td>'
                                    +'<span class="preview"><img style="height:100px;width: 100px" src="'+ e.target.result+'"/></span>'
                                    +'</td>'
                                    +'<td>'
                                    +'<p class="name">'+file[0].name+'</p>'
                                    +'<strong class="error text-danger label label-danger"></strong>'
                                    +'</td>'
                                    +'<td>'
                                        +'<p class="size">'+Math.round(file[0].size/1024)+' KB</p>'
                                    +'</td>'
                                    +'<td>'
                                    +'<button type="button" onclick="removePreviewImage(\''+selector+'\','+i+')" class="btn red btn_remove_image" data-file-index="'+i+'" data-selector="'+selector+'">'
                                    +'<i class="fa fa-ban"></i>'
                                    +'<span>Cancel</span>'
                                    +'</button>'
                                    +'</td>'
                                    +'</tr>';

                            dvPreview.append(img);

                        }
                        reader.readAsDataURL(file[0]);

                    } else {
                        alert(file[0].name + " is not a valid image file.");
                        dvPreview.html("");
                        return false;
                    }
                });

            } else {
                alert("This browser does not support HTML5 FileReader.");
            }
        });

    }

};

function resetRemoveImageInput(){
    imageRemoveArray = [];
    jQuery('#__image_remove__').val('');

}

function removePreviewImage(selector,fileId){
   var _fileList =  $(selector)[0].files;
    imageRemoveArray.push(fileId);
    console.log(JSON.stringify(imageRemoveArray));
    jQuery('#__image_remove__').val(JSON.stringify(imageRemoveArray));
    jQuery('#__preview_'+fileId).remove();
}