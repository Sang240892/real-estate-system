var NotificationMessage = function () {

    return {
        //main function to initiate the module
        init: function ( message,type,callBack) {

        		var i = -1,
                toastCount = 0,
                $toastlast;

                var shortCutFunction = type;
                var msg = $('#message').val();
                var title = $('#title').val() || '';
                var $showDuration = 300;
                var $hideDuration = 1000;
                var $timeOut = 5000;
                var $extendedTimeOut = 10000;
                var $showEasing = 'swing';
                var $hideEasing = 'linear';
                var $showMethod = 'fadeIn';
                var $hideMethod = 'fadeOut';
                var toastIndex = toastCount++;

                toastr.options = {
                    closeButton: $('#closeButton').prop('checked'),
                    debug: $('#debugInfo').prop('checked'),
                    positionClass: $('#positionGroup input:checked').val() || 'toast-top-right',
                    onclick: null
                };

                    toastr.options.onclick = function () {
                     // window.location.href = callBack;
                    };
                
                    toastr.options.showDuration = $showDuration;
                
                    toastr.options.hideDuration = $hideDuration;
                
                    toastr.options.timeOut = $timeOut;
               
                    toastr.options.extendedTimeOut = $extendedTimeOut;
                
                    toastr.options.showEasing = $showEasing;
                
                    toastr.options.hideEasing = $hideEasing;
                
                    toastr.options.showMethod = $showMethod;
                
                    toastr.options.hideMethod = $hideMethod;
                
                if (!msg) {
                    msg = message;
                }

                $("#toastrOptions").text("Command: toastr[" + shortCutFunction + "](\"" + msg + (title ? "\", \"" + title : '') + "\")\n\ntoastr.options = " + JSON.stringify(toastr.options, null, 2));

                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                $toastlast = $toast;
                if ($toast.find('#okBtn').length) {
                    $toast.delegate('#okBtn', 'click', function () {
                        alert('you clicked me. i was toast #' + toastIndex + '. goodbye!');
                        $toast.remove();
                    });
                }
                if ($toast.find('#surpriseBtn').length) {
                    $toast.delegate('#surpriseBtn', 'click', function () {
                        alert('Surprise! you clicked me. i was toast #' + toastIndex + '. You could perform an action here.');
                    });
                }

                $('#clearlasttoast').click(function () {
                    toastr.clear($toastlast);
                });
                
            $('#cleartoasts').click(function () {
                toastr.clear();
            });

        }

    };

}();