<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = $linkRestPassword;
?>
Hello <?= trim($user->first_name.' '.$user->last_name)?>,

Click vào đường link sau để thay đổi mật khẩu của bạn:

<?= $resetLink ?>
