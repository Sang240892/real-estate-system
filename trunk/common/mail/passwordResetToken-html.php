<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = $linkRestPassword;
?>
<div class="password-reset">
    <p>Hi <?= trim($user->first_name .' '.$user->last_name) ?>,</p>

    <p>Click vào đường link sau để thay đổi mật khẩu của bạn:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
