    <?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['sysadmin','admin','staff','manager','member'],
        ],
        'i18n' => [
            'translations' => [
                'yii' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en',
                    'basePath' => '@yii/messages'
                ],
                'backend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/translation',
                    //'sourceLanguage' => 'vn_VN',
                    'fileMap' => [
                        'backend' => 'app.php',
                        'backend/error' => 'error.php',
                    ],
                ],
                'frontend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/translation',
                    //'sourceLanguage' => 'vn_VN',
                    'fileMap' => [
                        'frontend' => 'app.php',
                        'frontend/error' => 'error.php',
                    ],
                ],
                'api*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@api/translation',
                    //'sourceLanguage' => 'vn_VN',
                    'fileMap' => [
                        'api' => 'app.php',
                        'api/error' => 'error.php',
                    ],
                ],
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/translation',
                    //'sourceLanguage' => 'vn_VN',
                    'fileMap' => [
                        'api' => 'app.php',
                        'api/error' => 'error.php',
                    ],
                ]
            ],
        ],
    ],
];
