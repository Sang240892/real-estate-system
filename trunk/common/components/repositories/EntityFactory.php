<?php
namespace common\components\repositories;
use yii\db\ActiveRecord;
use common\components\repositories\ImageInterface;
/**
 * User: sangnguyen on  6/2/16 at 00:15
 * File name: EntityFactory.php
 * Project name: ecommerce
 * Copyright (c) 2016 by MyCompany
 * All rights reserved
 */

/**
 * Class EntityFactory
 * @package common\components\factories
 */

abstract class EntityFactory extends ActiveRecord implements EntityInterface
{
    CONST USER_ROOT = '0001';
    CONST USER_ADMIN = '0010';
    CONST USER_MANAGER = '0011';
    CONST USER_STAFF = '4';
    CONST USER_CUSTOMER = '-1';

    protected $serviceProvider;

    public $changedAttributes;

    public function register($provider){
        return $this->serviceProvider = new $provider($this);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        if(!isset($this->updated_at)){
            return [
                'timestamp' => [
                    'class' => 'yii\behaviors\AttributeBehavior',
                    'attributes' => [
                        \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                       // \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['created_at'],
                    ],
                    'value' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
                ],
            ];
        }elseif(isset($this->repost_date)){
            return [
                'timestamp' => [
                    'class' => 'yii\behaviors\AttributeBehavior',
                    'attributes' => [
                        \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at' ],
                        \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['repost_date' ],
                        \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                    ],
                    'value' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
                ],
            ];
        }else{
            return [
                'timestamp' => [
                    'class' => 'yii\behaviors\AttributeBehavior',
                    'attributes' => [
                        \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at' ],
                        \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                    ],
                    'value' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
                ],
            ];
        }

    }
    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert){
        return parent::beforeSave($insert);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert,$changedAttributes){
        $this->changedAttributes = $changedAttributes;
        return parent::afterSave($insert,$changedAttributes);
    }

    /**
     * @return array $changedAttributes
     */
    public function getChangedAttributes(){
        unset($this->changedAttributes['updated_at']);
        return $this->changedAttributes;
    }

}
