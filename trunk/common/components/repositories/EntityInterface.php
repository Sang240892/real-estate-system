<?php
/**
 * User: sangnguyen on  6/2/16 at 23:31
 * File name: EntityInterface.php
 * Project name: ecommerce
 * Copyright (c) 2016 by MyCompany
 * All rights reserved
 */

namespace common\components\repositories;


interface EntityInterface{

    const ACTIVE_STATUS = 'active';
    const DE_ACTIVE_STATUS = 'de_active';

    /**
     * @param $provider
     * $provider is the service provider where contain the business logic
     */
    public function register($provider);

    /**
     * @return mixed
     */
    public function behaviors();

    /**
     * @param $insert
     * @return @parent::beforeSave($insert);
     */
    public function beforeSave($insert);

    /**
     * @param $insert
     * @param $changedAttributes
     * @return @parent::afterSave($insert,$changedAttributes);
     */
    public function afterSave($insert,$changedAttributes);

    /**
     * @return @changedAttributes;
     */
    public function getChangedAttributes();
}