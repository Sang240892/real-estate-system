<?php
/**
 * User: sangnguyen on  6/12/16 at 22:14
 * File name: ProductInterface.php
 * Project name: ecommerce
 * Copyright (c) 2016 by MyCompany
 * All rights reserved
 */

namespace common\components\repositories;


interface ImageInterface
{
    const TYPE_PRODUCT = 'product';
}