<?php
namespace common\models;

use backend\commons\helpers\UtilHelper;
use common\components\repositories\EntityFactory;
use common\models\entities\AuthenticationToken;
use common\models\entities\RealEstateUser;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use api\commons\repositories\UserRepository;

/**
 * User model
 *
 * @property integer $id
 * @property string $first_name
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $modified_at
 * @property string $password write-only password
 */
class UserIdentity extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const IS_ONLINE = 1;
    const IS_OFFLINE = 0;
    const EXPIRED_TIME = 120;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'real_estate_user';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByEmail($username)
    {
        $user =  static::find()->andOnCondition(['email' => $username, 'status' => self::STATUS_ACTIVE]);

        if($user->one() === null){
            $user =  static::find()->andOnCondition(['phone' => $username, 'status' => self::STATUS_ACTIVE]);
        }
        return $user;
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {

        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];

        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public function getAuthenticationToken(){
        $auth = AuthenticationToken::findOne(['user_id'=>$this->id]);
        return $auth;
    }

    public function checkExpiredAuthenticationToken(){
        $auth = $this->getAuthenticationToken();
        if($auth === null){
            return false;
        }
        $now = UtilHelper::getNowUnixUTC();
        $expired = (int) $auth->last_accessed;
        return $now < $expired ;
    }

    public function generateAuthenticationToken(){
        $tokenParams = [
            'user_id'      =>  $this->id,
            'auth_token'    =>  Yii::$app->security->generateRandomString(),
            'device_token'  =>  null,
            'device_type'   =>  Yii::$app->request->userAgent,
            'user_host'     => Yii::$app->request->userHost,
            'user_ip'       =>Yii::$app->request->userIP,
            'os_type'       =>  3,
            'last_accessed' => UtilHelper::getUnixUTCMinuteAfter(static::EXPIRED_TIME) //expired time
        ];
        $user = RealEstateUser::findOne(['id'=>$this->id]);

        $user->register(UserRepository::class)->verifyAuthKey($tokenParams);

    }
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
//    public function generateAuthKey()
//    {
//        $this->auth_key = Yii::$app->security->generateRandomString();
//    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
