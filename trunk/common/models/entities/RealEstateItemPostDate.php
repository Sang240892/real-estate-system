<?php

namespace common\models\entities;

use common\components\repositories\EntityFactory;
use Yii;

/**
 * This is the model class for table "real_estate_item_post_date".
 *
 * @property integer $id
 * @property integer $item_id
 * @property integer $post_date
 * @property string $price
 * @property integer $new_post
 * @property integer $created_at
 *
 * @property RealEstateItem $item
 */
class RealEstateItemPostDate extends EntityFactory
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'real_estate_item_post_date';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id'], 'required'],
            [['item_id', 'post_date', 'new_post', 'created_at'], 'integer'],
            [['price'], 'number'],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealEstateItem::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'item_id' => Yii::t('backend', 'Item ID'),
            'post_date' => Yii::t('backend', 'Post Date'),
            'price' => Yii::t('backend', 'Price'),
            'new_post' => Yii::t('backend', 'New Post'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(RealEstateItem::className(), ['id' => 'item_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\RealEstateItemPostDateQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\RealEstateItemPostDateQuery(get_called_class());
    }
}
