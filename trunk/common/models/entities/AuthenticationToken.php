<?php

namespace common\models\entities;

use common\components\repositories\EntityFactory;
use Yii;

/**
 * This is the model class for table "authentication_token".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $auth_token
 * @property string $device_token
 * @property string $device_type
 * @property string $os_type
 * @property string $os_info
 * @property integer $last_accessed
 * @property string $last_location
 * @property integer $created_at
 * @property string $user_host
 * @property string $user_ip
 *
 * @property RealEstateUser $user
 */
class AuthenticationToken extends EntityFactory
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authentication_token';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'auth_token'], 'required'],
            [['user_id', 'last_accessed', 'created_at'], 'integer'],
            [['auth_token', 'device_token', 'os_info', 'last_location', 'user_ip'], 'string', 'max' => 255],
            [['device_type'], 'string', 'max' => 700],
            [['os_type'], 'string', 'max' => 100],
            [['user_host'], 'string', 'max' => 555],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealEstateUser::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'user_id' => Yii::t('backend', 'User ID'),
            'auth_token' => Yii::t('backend', 'Auth Token'),
            'device_token' => Yii::t('backend', 'Device Token'),
            'device_type' => Yii::t('backend', 'Device Type'),
            'os_type' => Yii::t('backend', 'Os Type'),
            'os_info' => Yii::t('backend', 'Os Info'),
            'last_accessed' => Yii::t('backend', 'Last Accessed'),
            'last_location' => Yii::t('backend', 'Last Location'),
            'created_at' => Yii::t('backend', 'Created At'),
            'user_host' => Yii::t('backend', 'User Host'),
            'user_ip' => Yii::t('backend', 'User Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(RealEstateUser::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\AuthenticationTokenQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\AuthenticationTokenQuery(get_called_class());
    }
}
