<?php

namespace common\models\entities;

use Yii;

/**
 * This is the model class for table "site_crawling".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property integer $created_at
 *
 * @property SiteCrawlingHref[] $siteCrawlingHrefs
 */
class SiteCrawling extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_crawling';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url'], 'required'],
            [['created_at'], 'integer'],
            [['name', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'url' => Yii::t('backend', 'Url'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiteCrawlingHrefs()
    {
        return $this->hasMany(SiteCrawlingHref::className(), ['site_crawling_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\SiteCrawlingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\SiteCrawlingQuery(get_called_class());
    }
}
