<?php

namespace common\models\entities;

use Yii;

/**
 * This is the model class for table "real_estate_item_has_phone_contact".
 *
 * @property integer $id
 * @property integer $item_id
 * @property integer $phone_contact_id
 * @property string $phone
 *
 * @property RealEstateItem $item
 * @property RealEstatePhoneContact $phoneContact
 */
class RealEstateItemHasPhoneContact extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'real_estate_item_has_phone_contact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'phone_contact_id', 'phone'], 'required'],
            [['item_id', 'phone_contact_id'], 'integer'],
            [['phone'], 'string', 'max' => 45],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealEstateItem::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['phone_contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealEstatePhoneContact::className(), 'targetAttribute' => ['phone_contact_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'item_id' => Yii::t('backend', 'Item ID'),
            'phone_contact_id' => Yii::t('backend', 'Phone Contact ID'),
            'phone' => Yii::t('backend', 'Phone'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(RealEstateItem::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoneContact()
    {
        return $this->hasOne(RealEstatePhoneContact::className(), ['id' => 'phone_contact_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\RealEstateItemHasPhoneContactQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\RealEstateItemHasPhoneContactQuery(get_called_class());
    }
}
