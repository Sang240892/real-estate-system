<?php

namespace common\models\entities;

use common\components\repositories\EntityFactory;
use Yii;

/**
 * This is the model class for table "real_estate_phone_contact".
 *
 * @property integer $id
 * @property string $phone
 * @property string $created_at
 *
 * @property RealEstateItemHasPhoneContact[] $realEstateItemHasPhoneContacts
 */
class RealEstatePhoneContact extends EntityFactory
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'real_estate_phone_contact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone'], 'required'],
            [['phone'], 'string', 'max' => 15],
            [['created_at'], 'string', 'max' => 45],
            [['phone'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'phone' => Yii::t('backend', 'Phone'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealEstateItemHasPhoneContacts()
    {
        return $this->hasMany(RealEstateItemHasPhoneContact::className(), ['phone_contact_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\RealEstatePhoneContactQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\RealEstatePhoneContactQuery(get_called_class());
    }
}
