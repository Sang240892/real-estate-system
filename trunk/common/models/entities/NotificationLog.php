<?php

namespace common\models\entities;

use common\components\repositories\EntityFactory;
use Yii;

/**
 * This is the model class for table "notification_log".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $params_request
 * @property string $fcm_response
 * @property integer $created_at
 *
 * @property RealEstateUser $user
 */
class NotificationLog extends EntityFactory
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'created_at'], 'integer'],
            [['params_request'], 'string', 'max' => 555],
            [['fcm_response'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealEstateUser::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'user_id' => Yii::t('backend', 'User ID'),
            'params_request' => Yii::t('backend', 'Params Request'),
            'fcm_response' => Yii::t('backend', 'Fcm Response'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(RealEstateUser::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\NotificationLogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\NotificationLogQuery(get_called_class());
    }
}
