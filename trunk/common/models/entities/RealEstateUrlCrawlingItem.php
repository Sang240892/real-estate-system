<?php

namespace common\models\entities;

use common\components\repositories\EntityFactory;
use Yii;

/**
 * This is the model class for table "real_estate_url_crawling_item".
 *
 * @property integer $id
 * @property integer $real_estate_item_id
 * @property string $url
 * @property integer $created_at
 *
 * @property RealEstateItem $realEstateItem
 */
class RealEstateUrlCrawlingItem extends EntityFactory
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'real_estate_url_crawling_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['real_estate_item_id', 'url'], 'required'],
            [['real_estate_item_id', 'created_at'], 'integer'],
            [['url'], 'string', 'max' => 555],
            [['real_estate_item_id'], 'unique'],
            [['real_estate_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealEstateItem::className(), 'targetAttribute' => ['real_estate_item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'real_estate_item_id' => Yii::t('backend', 'Real Estate Item ID'),
            'url' => Yii::t('backend', 'Url'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealEstateItem()
    {
        return $this->hasOne(RealEstateItem::className(), ['id' => 'real_estate_item_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\RealEstateUrlCrawlingItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\RealEstateUrlCrawlingItemQuery(get_called_class());
    }
}
