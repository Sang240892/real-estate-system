<?php

namespace common\models\entities;

use common\components\repositories\EntityFactory;
use Yii;

/**
 * This is the model class for table "real_estate_item".
 *
 * @property integer $id
 * @property string $contact_name
 * @property integer $is_new
 * @property string $area_number
 * @property integer $realty_type
 * @property string $floor
 * @property string $front
 * @property string $bedroom
 * @property string $area
 * @property string $title
 * @property string $summary
 * @property string $description
 * @property string $source
 * @property integer $status
 * @property integer $confirmed
 * @property integer $is_spam
 * @property string $street
 * @property string $district
 * @property string $ward
 * @property string $city
 * @property string $country
 * @property string $address
 * @property string $phone_contact
 * @property string $phone_backup
 * @property integer $start_date
 * @property integer $end_date
 * @property string $type
 * @property string $price
 * @property string $price_string
 * @property string $unit
 * @property string $category_name
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $user_id
 * @property integer $category_id
 * @property string $latitude
 * @property string $longitude
 * @property integer $is_self
 * @property integer $real_estate_date
 * @property string $avatar
 * @property integer $local
 * @property integer $is_sold
 * @property integer $repost_date
 * @property integer $is_not_sure_self
 * @property string $tip
 *
 * @property NotificationHistory[] $notificationHistories
 * @property RealEstateBookmark[] $realEstateBookmarks
 * @property RealEstateGallery[] $realEstateGalleries
 * @property RealEstateCategory $category
 * @property RealEstateUser $user
 * @property RealEstateItemHasPhoneContact[] $realEstateItemHasPhoneContacts
 * @property RealEstateItemPostDate[] $realEstateItemPostDates
 * @property RealEstateUrlCrawlingItem $realEstateUrlCrawlingItem
 */
class RealEstateItem extends EntityFactory
{
    CONST REALTY_SALE = 1;
    CONST REALTY_LOAN = 2;
    CONST REALTY_SOLD = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'real_estate_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_new', 'realty_type', 'status', 'confirmed', 'is_spam', 'start_date', 'end_date', 'created_at', 'updated_at', 'user_id', 'category_id', 'is_self', 'real_estate_date', 'local', 'is_sold', 'repost_date', 'is_not_sure_self'], 'integer'],
            [['area_number', 'price'], 'number'],
            [['title'], 'required'],
            [['description'], 'string'],
            [['contact_name', 'floor', 'front', 'bedroom', 'title', 'address', 'avatar'], 'string', 'max' => 255],
            [['area', 'latitude', 'longitude'], 'string', 'max' => 35],
            [['summary'], 'string', 'max' => 555],
            [['source', 'street', 'district', 'ward', 'city', 'country', 'unit'], 'string', 'max' => 45],
            [['phone_contact', 'phone_backup', 'tip'], 'string', 'max' => 115],
            [['type', 'category_name'], 'string', 'max' => 225],
            [['price_string'], 'string', 'max' => 55],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealEstateCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealEstateUser::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'contact_name' => Yii::t('backend', 'Contact Name'),
            'is_new' => Yii::t('backend', 'Is New'),
            'area_number' => Yii::t('backend', 'Area Number'),
            'realty_type' => Yii::t('backend', 'Realty Type'),
            'floor' => Yii::t('backend', 'Floor'),
            'front' => Yii::t('backend', 'Front'),
            'bedroom' => Yii::t('backend', 'Bedroom'),
            'area' => Yii::t('backend', 'Area'),
            'title' => Yii::t('backend', 'Title'),
            'summary' => Yii::t('backend', 'Summary'),
            'description' => Yii::t('backend', 'Description'),
            'source' => Yii::t('backend', 'Source'),
            'status' => Yii::t('backend', 'Status'),
            'confirmed' => Yii::t('backend', 'Confirmed'),
            'is_spam' => Yii::t('backend', 'Is Spam'),
            'street' => Yii::t('backend', 'Street'),
            'district' => Yii::t('backend', 'District'),
            'ward' => Yii::t('backend', 'Ward'),
            'city' => Yii::t('backend', 'City'),
            'country' => Yii::t('backend', 'Country'),
            'address' => Yii::t('backend', 'Address'),
            'phone_contact' => Yii::t('backend', 'Phone Contact'),
            'phone_backup' => Yii::t('backend', 'Phone Backup'),
            'start_date' => Yii::t('backend', 'Start Date'),
            'end_date' => Yii::t('backend', 'End Date'),
            'type' => Yii::t('backend', 'Type'),
            'price' => Yii::t('backend', 'Price'),
            'price_string' => Yii::t('backend', 'Price String'),
            'unit' => Yii::t('backend', 'Unit'),
            'category_name' => Yii::t('backend', 'Category Name'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'user_id' => Yii::t('backend', 'User ID'),
            'category_id' => Yii::t('backend', 'Category ID'),
            'latitude' => Yii::t('backend', 'Latitude'),
            'longitude' => Yii::t('backend', 'Longitude'),
            'is_self' => Yii::t('backend', 'Is Self'),
            'real_estate_date' => Yii::t('backend', 'Real Estate Date'),
            'avatar' => Yii::t('backend', 'Avatar'),
            'local' => Yii::t('backend', 'Local'),
            'is_sold' => Yii::t('backend', 'Is Sold'),
            'repost_date' => Yii::t('backend', 'Repost Date'),
            'is_not_sure_self' => Yii::t('backend', 'Is Not Sure Self'),
            'tip' => Yii::t('backend', 'Tip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationHistories()
    {
        return $this->hasMany(NotificationHistory::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealEstateBookmarks()
    {
        return $this->hasMany(RealEstateBookmark::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealEstateGalleries()
    {
        return $this->hasMany(RealEstateGallery::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(RealEstateCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(RealEstateUser::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealEstateItemHasPhoneContacts()
    {
        return $this->hasMany(RealEstateItemHasPhoneContact::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealEstateItemPostDates()
    {
        return $this->hasMany(RealEstateItemPostDate::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealEstateUrlCrawlingItem()
    {
        return $this->hasOne(RealEstateUrlCrawlingItem::className(), ['real_estate_item_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\RealEstateItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\RealEstateItemQuery(get_called_class());
    }
}
