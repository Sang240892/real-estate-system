<?php

namespace common\models\entities;

use common\components\repositories\EntityFactory;
use Yii;

/**
 * This is the model class for table "real_estate_category".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $parent
 * @property integer $is_display
 * @property string $href
 * @property integer $site_crawling_id
 * @property string $finder_name
 *
 * @property SiteCrawling $siteCrawling
 * @property RealEstateCategoryRelation[] $realEstateCategoryRelations
 * @property RealEstateCategoryRelation[] $realEstateCategoryRelations0
 * @property RealEstateItem[] $realEstateItems
 */
class RealEstateCategory extends EntityFactory
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'real_estate_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status', 'created_at', 'updated_at', 'parent', 'is_display', 'site_crawling_id'], 'integer'],
            [['name'], 'string', 'max' => 45],
            [['description'], 'string', 'max' => 555],
            [['href'], 'string', 'max' => 255],
            [['finder_name'], 'string', 'max' => 115],
            [['site_crawling_id'], 'exist', 'skipOnError' => true, 'targetClass' => SiteCrawling::className(), 'targetAttribute' => ['site_crawling_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'description' => Yii::t('backend', 'Description'),
            'status' => Yii::t('backend', 'Status'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'parent' => Yii::t('backend', 'Parent'),
            'is_display' => Yii::t('backend', 'Is Display'),
            'href' => Yii::t('backend', 'Href'),
            'site_crawling_id' => Yii::t('backend', 'Site Crawling ID'),
            'finder_name' => Yii::t('backend', 'Finder Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiteCrawling()
    {
        return $this->hasOne(SiteCrawling::className(), ['id' => 'site_crawling_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealEstateCategoryRelations()
    {
        return $this->hasMany(RealEstateCategoryRelation::className(), ['category_parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealEstateCategoryRelations0()
    {
        return $this->hasMany(RealEstateCategoryRelation::className(), ['category_children_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealEstateItems()
    {
        return $this->hasMany(RealEstateItem::className(), ['category_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\RealEstateCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\RealEstateCategoryQuery(get_called_class());
    }
}
