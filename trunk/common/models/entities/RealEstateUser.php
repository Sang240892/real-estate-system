<?php

namespace common\models\entities;

use common\components\repositories\EntityFactory;
use Yii;

/**
 * This is the model class for table "real_estate_user".
 *
 * @property integer $id
 * @property string $last_name
 * @property string $first_name
 * @property string $email
 * @property string $password
 * @property string $password_reset_token
 * @property string $phone
 * @property string $role
 * @property string $address
 * @property string $role_name
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $gender
 * @property integer $status
 * @property integer $is_notify
 * @property string $notify_categories
 * @property string $notification_city_rule
 * @property string $notification_price_min_rule
 * @property string $notification_price_max_rule
 * @property integer $is_online
 * @property string $tracking_by
 * @property string $note
 *
 * @property AuthenticationToken[] $authenticationTokens
 * @property NotificationCategoryRule[] $notificationCategoryRules
 * @property NotificationHistory[] $notificationHistories
 * @property NotificationLog[] $notificationLogs
 * @property PaymentHistory[] $paymentHistories
 * @property RealEstateBookmark[] $realEstateBookmarks
 * @property RealEstateCustomer[] $realEstateCustomers
 * @property RealEstateItem[] $realEstateItems
 */
class RealEstateUser extends EntityFactory
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'real_estate_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['last_name', 'first_name'], 'required'],
            [['created_at', 'updated_at', 'gender', 'status', 'is_notify', 'is_online'], 'integer'],
            [['notification_price_min_rule', 'notification_price_max_rule'], 'number'],
            [['last_name', 'first_name', 'email', 'role_name'], 'string', 'max' => 45],
            [['password', 'password_reset_token', 'address', 'notify_categories', 'tracking_by'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 15],
            [['role'], 'string', 'max' => 5],
            [['notification_city_rule'], 'string', 'max' => 35],
            [['note'], 'string', 'max' => 755],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'last_name' => Yii::t('backend', 'Last Name'),
            'first_name' => Yii::t('backend', 'First Name'),
            'email' => Yii::t('backend', 'Email'),
            'password' => Yii::t('backend', 'Password'),
            'password_reset_token' => Yii::t('backend', 'Password Reset Token'),
            'phone' => Yii::t('backend', 'Phone'),
            'role' => Yii::t('backend', 'Role'),
            'address' => Yii::t('backend', 'Address'),
            'role_name' => Yii::t('backend', 'Role Name'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'gender' => Yii::t('backend', 'Gender'),
            'status' => Yii::t('backend', 'Status'),
            'is_notify' => Yii::t('backend', 'Is Notify'),
            'notify_categories' => Yii::t('backend', 'Notify Categories'),
            'notification_city_rule' => Yii::t('backend', 'Notification City Rule'),
            'notification_price_min_rule' => Yii::t('backend', 'Notification Price Min Rule'),
            'notification_price_max_rule' => Yii::t('backend', 'Notification Price Max Rule'),
            'is_online' => Yii::t('backend', 'Is Online'),
            'tracking_by' => Yii::t('backend', 'Tracking By'),
            'note' => Yii::t('backend', 'Note'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthenticationTokens()
    {
        return $this->hasMany(AuthenticationToken::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationCategoryRules()
    {
        return $this->hasMany(NotificationCategoryRule::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationHistories()
    {
        return $this->hasMany(NotificationHistory::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationLogs()
    {
        return $this->hasMany(NotificationLog::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentHistories()
    {
        return $this->hasMany(PaymentHistory::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealEstateBookmarks()
    {
        return $this->hasMany(RealEstateBookmark::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealEstateCustomers()
    {
        return $this->hasMany(RealEstateCustomer::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealEstateItems()
    {
        return $this->hasMany(RealEstateItem::className(), ['user_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\RealEstateUserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\RealEstateUserQuery(get_called_class());
    }
}
