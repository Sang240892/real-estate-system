<?php

namespace common\models\entities;

use common\components\repositories\EntityFactory;
use Yii;

/**
 * This is the model class for table "real_estate_customer".
 *
 * @property integer $id
 * @property string $last_name
 * @property string $first_name
 * @property string $address
 * @property string $phone
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $email
 * @property integer $status
 * @property integer $type
 * @property string $note
 * @property integer $area
 * @property string $price
 * @property string $price_string
 * @property integer $user_id
 *
 * @property RealEstateUser $user
 */
class RealEstateCustomer extends EntityFactory
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'real_estate_customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['last_name', 'first_name'], 'required'],
            [['created_at', 'updated_at', 'status', 'type', 'area', 'user_id'], 'integer'],
            [['price'], 'number'],
            [['last_name', 'first_name', 'phone', 'email', 'price_string'], 'string', 'max' => 45],
            [['address', 'note'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealEstateUser::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'last_name' => Yii::t('backend', 'Last Name'),
            'first_name' => Yii::t('backend', 'First Name'),
            'address' => Yii::t('backend', 'Address'),
            'phone' => Yii::t('backend', 'Phone'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'email' => Yii::t('backend', 'Email'),
            'status' => Yii::t('backend', 'Status'),
            'type' => Yii::t('backend', 'Type'),
            'note' => Yii::t('backend', 'Note'),
            'area' => Yii::t('backend', 'Area'),
            'price' => Yii::t('backend', 'Price'),
            'price_string' => Yii::t('backend', 'Price String'),
            'user_id' => Yii::t('backend', 'User ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(RealEstateUser::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\RealEstateCustomerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\RealEstateCustomerQuery(get_called_class());
    }
}
