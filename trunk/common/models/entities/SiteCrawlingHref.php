<?php

namespace common\models\entities;

use common\components\repositories\EntityFactory;
use Yii;

/**
 * This is the model class for table "site_crawling_href".
 *
 * @property integer $id
 * @property string $name
 * @property string $href
 * @property integer $site_crawling_id
 * @property integer $created_at
 * @property integer $parent
 *
 * @property SiteCrawling $siteCrawling
 */
class SiteCrawlingHref extends EntityFactory
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_crawling_href';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'href'], 'required'],
            [['site_crawling_id', 'created_at', 'parent'], 'integer'],
            [['name', 'href'], 'string', 'max' => 255],
            [['site_crawling_id'], 'exist', 'skipOnError' => true, 'targetClass' => SiteCrawling::className(), 'targetAttribute' => ['site_crawling_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'href' => Yii::t('backend', 'Href'),
            'site_crawling_id' => Yii::t('backend', 'Site Crawling ID'),
            'created_at' => Yii::t('backend', 'Created At'),
            'parent' => Yii::t('backend', 'Parent'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiteCrawling()
    {
        return $this->hasOne(SiteCrawling::className(), ['id' => 'site_crawling_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\SiteCrawlingHrefQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\SiteCrawlingHrefQuery(get_called_class());
    }
}
