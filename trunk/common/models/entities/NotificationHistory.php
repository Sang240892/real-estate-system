<?php

namespace common\models\entities;

use common\components\repositories\EntityFactory;
use Yii;

/**
 * This is the model class for table "notification_history".
 *
 * @property integer $id
 * @property integer $item_id
 * @property integer $user_id
 * @property integer $is_read
 * @property integer $is_push
 * @property integer $created_at
 * @property integer $updated_at
 */
class NotificationHistory extends EntityFactory
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'item_id', 'user_id'], 'required'],
            [['id', 'item_id', 'user_id', 'is_read', 'is_push', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'item_id' => Yii::t('backend', 'Item ID'),
            'user_id' => Yii::t('backend', 'User ID'),
            'is_read' => Yii::t('backend', 'Is Read'),
            'is_push' => Yii::t('backend', 'Is Push'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\NotificationHistoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\NotificationHistoryQuery(get_called_class());
    }
}
