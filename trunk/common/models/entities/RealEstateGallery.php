<?php

namespace common\models\entities;

use common\components\repositories\EntityFactory;
use Yii;

/**
 * This is the model class for table "real_estate_gallery".
 *
 * @property integer $id
 * @property string $title
 * @property string $file_name
 * @property integer $created_at
 * @property integer $item_id
 *
 * @property RealEstateItem $item
 */
class RealEstateGallery extends EntityFactory
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'real_estate_gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file_name', 'item_id'], 'required'],
            [['created_at', 'item_id'], 'integer'],
            [['title'], 'string', 'max' => 115],
            [['file_name'], 'string', 'max' => 255],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealEstateItem::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'title' => Yii::t('backend', 'Title'),
            'file_name' => Yii::t('backend', 'File Name'),
            'created_at' => Yii::t('backend', 'Created At'),
            'item_id' => Yii::t('backend', 'Item ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(RealEstateItem::className(), ['id' => 'item_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\RealEstateGalleryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\RealEstateGalleryQuery(get_called_class());
    }
}
