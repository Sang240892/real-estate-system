<?php

namespace common\models\entities;

use common\components\repositories\EntityFactory;
use Yii;

/**
 * This is the model class for table "notification_category_rule".
 *
 * @property integer $user_id
 * @property integer $category_id
 * @property integer $created_at
 *
 * @property RealEstateUser $user
 * @property RealEstateCategory $category
 */
class NotificationCategoryRule extends EntityFactory
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification_category_rule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'category_id'], 'required'],
            [['user_id', 'category_id', 'created_at'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealEstateUser::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealEstateCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('backend', 'User ID'),
            'category_id' => Yii::t('backend', 'Category ID'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(RealEstateUser::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(RealEstateCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\NotificationCategoryRuleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\NotificationCategoryRuleQuery(get_called_class());
    }
}
