<?php

namespace common\models\entities;

use Yii;

/**
 * This is the model class for table "real_estate_category_relation".
 *
 * @property integer $id
 * @property integer $category_parent_id
 * @property integer $category_children_id
 * @property integer $parent_old_id
 *
 * @property RealEstateCategory $categoryParent
 * @property RealEstateCategory $categoryChildren
 */
class RealEstateCategoryRelation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'real_estate_category_relation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_parent_id', 'category_children_id'], 'required'],
            [['category_parent_id', 'category_children_id', 'parent_old_id'], 'integer'],
            [['category_parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealEstateCategory::className(), 'targetAttribute' => ['category_parent_id' => 'id']],
            [['category_children_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealEstateCategory::className(), 'targetAttribute' => ['category_children_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'category_parent_id' => Yii::t('backend', 'Category Parent ID'),
            'category_children_id' => Yii::t('backend', 'Category Children ID'),
            'parent_old_id' => Yii::t('backend', 'Parent Old ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryParent()
    {
        return $this->hasOne(RealEstateCategory::className(), ['id' => 'category_parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryChildren()
    {
        return $this->hasOne(RealEstateCategory::className(), ['id' => 'category_children_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\RealEstateCategoryRelationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\RealEstateCategoryRelationQuery(get_called_class());
    }
}
