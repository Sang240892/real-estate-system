<?php

namespace common\models\entities;

use common\components\repositories\EntityFactory;
use Yii;

/**
 * This is the model class for table "payment_history".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $start_date
 * @property string $end_date
 * @property string $amount
 * @property integer $created_at
 *
 * @property RealEstateUser $user
 */
class PaymentHistory extends EntityFactory
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'start_date', 'end_date', 'amount'], 'required','message'=>'Không được rỗng!'],
            [['user_id', 'created_at'], 'integer'],
            [['start_date', 'end_date'], 'safe'],
            [['amount'], 'number'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealEstateUser::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'user_id' => Yii::t('backend', 'User ID'),
            'start_date' => Yii::t('backend', 'Start Date'),
            'end_date' => Yii::t('backend', 'End Date'),
            'amount' => Yii::t('backend', 'Amount'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(RealEstateUser::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\PaymentHistoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\PaymentHistoryQuery(get_called_class());
    }
}
