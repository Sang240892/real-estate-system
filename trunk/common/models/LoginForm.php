<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required', 'message'=>\Yii::t('frontend','{attribute} không được phép rỗng.')],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            ['username','validateUserName']
        ];
    }
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('backend', 'Tên đăng nhập'),
            'password' => Yii::t('backend', 'Mật khẩu'),
            ];
    }
    public function validateUserName($attribute, $params){

        if(!filter_var($this->username, FILTER_VALIDATE_EMAIL) && !preg_match("/^[0-9]{9,12}$/", $this->username) ) {
            $this->addError('username',Yii::t('api/error','Tên đăng nhập phải là email hoặc số điện thoại'));
            return false;
        }
    }
    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if(empty($this->password)){
                $this->addError($attribute, Yii::t('api/error', 'Tài khoản hoặc mật khẩu không đúng!'));
            }

            if (!$user || !$user->validatePassword((string)$this->password)) {
                $this->addError($attribute, Yii::t('api/error', 'Tài khoản hoặc mật khẩu không đúng!'));
            }
            elseif((int)$user->is_online == UserIdentity::IS_ONLINE && $user->checkExpiredAuthenticationToken()){
                $this->addError($attribute, Yii::t('api/error','Tài khoản này đang online!'));
                return false;
            }
            elseif($user->status !== UserIdentity::STATUS_ACTIVE){
                $this->addError($attribute, Yii::t('api/error','Tài khoản đã bị khóa!'));
                return false;
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {

            $this->_user->generateAuthenticationToken();

            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return UserIdentity|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = UserIdentity::findByEmail($this->username)->one();
        }
        return $this->_user;
    }
}
