<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[\common\models\entities\AuthenticationToken]].
 *
 * @see \common\models\entities\AuthenticationToken
 */
class AuthenticationTokenQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\entities\AuthenticationToken[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\entities\AuthenticationToken|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
