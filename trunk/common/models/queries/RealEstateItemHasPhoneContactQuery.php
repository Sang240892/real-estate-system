<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[\common\models\entities\RealEstateItemHasPhoneContact]].
 *
 * @see \common\models\entities\RealEstateItemHasPhoneContact
 */
class RealEstateItemHasPhoneContactQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\entities\RealEstateItemHasPhoneContact[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\entities\RealEstateItemHasPhoneContact|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
