<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[\common\models\entities\SiteCrawlingHref]].
 *
 * @see \common\models\entities\SiteCrawlingHref
 */
class SiteCrawlingHrefQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\entities\SiteCrawlingHref[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\entities\SiteCrawlingHref|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
