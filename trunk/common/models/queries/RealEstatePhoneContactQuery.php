<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[\common\models\entities\RealEstatePhoneContact]].
 *
 * @see \common\models\entities\RealEstatePhoneContact
 */
class RealEstatePhoneContactQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\entities\RealEstatePhoneContact[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\entities\RealEstatePhoneContact|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
