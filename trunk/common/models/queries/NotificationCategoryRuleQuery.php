<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[\common\models\entities\NotificationCategoryRule]].
 *
 * @see \common\models\entities\NotificationCategoryRule
 */
class NotificationCategoryRuleQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\entities\NotificationCategoryRule[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\entities\NotificationCategoryRule|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
