<?php
/**
 * User: sangnguyen on  11/13/15 at 08:00
 * File name: StartJobOrderForm.php
 * Project name: ecommerce
 * Copyright (c) 2015 by AppsCyclone
 * All rights reserved
 */

namespace common\forms;

use backend\commons\helpers\UtilHelper as BackendHelper;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * @property string $remark
 * @property \yii\web\UploadedFile $attachmentFiles
 * @property string $path
 * @property boolean $resize
 * @property string $fileName
 * @property array $output
 * @property boolean $compare
 * @property string $oldName
 * @property boolean $errorStatus
 * Class UploadAttachmentFileForm
 * @package api\common\forms
 */
class UploadAttachmentFileForm extends Model{
    /**
     * @var UploadedFile
     */
    public $remark;
    public $attachmentFiles;
    public $path;
    public $resize=true;
    public $fileName = null;
    public $output=[];
    public $compare = FALSE;
    public $oldName = null;
    public $errorStatus =FALSE;

    public function rules()
    {
        return [
            [['attachmentFiles'], 'file', 'skipOnEmpty' => true,
                'extensions' => 'png, jpg,svg,jpeg', 'maxFiles' => 4,
             'maxSize'=>1024 * 1024 * 4
            ],
        ];


    }

    public function save(){
        $out = [];

        if($this->validate()){

            $i= 1;
            foreach ($this->attachmentFiles as $_attachmentFile) {
                $this->fileName = null;

                if (!$this->fileName) {
                    $this->fileName = Yii::$app->security->generateRandomString()
                        . time() . '.' . $_attachmentFile->extension;
                }

                $i++;

                $_attachmentFile->saveAs($this->path . $this->fileName);
                if($this->compare){
                    $data['name'] = $this->fileName;
                    $data['oldName'] = $_attachmentFile->name;
                    $this->output[] = $data;
                }else{
                    $this->output[] = $this->fileName;
                }


                if ($this->resize) {
                    BackendHelper:: resizeImg($this->path, $this->fileName,
                        Yii::$app->params['normalName']. $this->fileName, Yii::$app->params['sizeNormalImg'],
                        Yii::$app->params['sizeNormalImg']);

                    BackendHelper:: resizeImg($this->path, $this->fileName,
                        Yii::$app->params['thumbName']. $this->fileName,
                        Yii::$app->params['sizeThumbImg'], Yii::$app->params['sizeThumbImg']);
                }
            }

            return $this;
        }else{
            $this->errorStatus = true;
            return $this;
        }
    }
}